-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2019 at 01:47 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jymkhana`
--

-- --------------------------------------------------------

--
-- Table structure for table `construction_reports`
--

CREATE TABLE `construction_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `material` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `credit` double(8,2) NOT NULL,
  `payment` double(8,2) NOT NULL,
  `labour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_labour` int(11) NOT NULL,
  `labour_payments` double(8,2) NOT NULL,
  `skills` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saved_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `construction_reports`
--

INSERT INTO `construction_reports` (`id`, `material`, `qty`, `unit_price`, `total`, `credit`, `payment`, `labour`, `no_labour`, `labour_payments`, `skills`, `saved_at`, `created_at`, `updated_at`) VALUES
(1, 'Cement', 2, 1500.00, 3000.00, 1500.00, 1500.00, 'Abc', 5, 3000.00, 'The skills field', '06/11/2019', '2019-11-06 07:41:43', '2019-11-06 07:41:43'),
(2, 'Cancrate', 2, 15000.00, 3000.00, 15000.00, 15000.00, 'cde', 3, 3000.00, 'The skills field', '05/11/2019', '2019-11-06 07:42:02', '2019-11-06 07:42:02'),
(3, 'Cement 2', 2, 1500.00, 3000.00, 1500.00, 1500.00, 'Abc', 5, 3000.00, 'vjh', '06/11/2019', '2019-11-07 01:33:15', '2019-11-07 01:33:15'),
(4, 'Cement3', 2, 1500.00, 3000.00, 1500.00, 1500.00, 'Abc', 5, 3000.00, 'vjh', '06/11/2019', '2019-11-07 04:18:32', '2019-11-07 04:18:32'),
(5, 'Cement 25', 2, 2500.00, 5000.00, 656.00, 56.00, 'Abc', 56, 565.00, 'jk', '07/11/2019', '2019-11-07 04:24:41', '2019-11-07 04:24:41'),
(6, 'Cement 25', 2, 2500.00, 5000.00, 656.00, 56.00, 'Abc', 56, 565.00, 'jk', '07/11/2019', '2019-11-07 04:24:51', '2019-11-07 04:24:51'),
(7, 'Bricks', 10000, 10.00, 100000.00, 40000.00, 60000.00, '20000', 5, 20000.00, 'None', '08/11/2019', '2019-11-08 14:13:11', '2019-11-08 14:13:11'),
(8, 'Bricks', 1, 1.00, 1.00, 11.00, 111.00, '111', 1111111, 1111.00, 'None', '09/11/2019', '2019-11-09 14:29:09', '2019-11-09 14:29:09'),
(9, '33', 11, 12.00, 132.00, 12.00, 123.00, '213213', 21431, 2314.00, '14341', '09/11/2019', '2019-11-09 14:29:23', '2019-11-09 14:29:23'),
(10, 'ssd', 22, 22.00, 484.00, 222.00, 233.00, '344', 444, 3333.00, '444', '09/11/2019', '2019-11-09 14:36:41', '2019-11-09 14:36:41'),
(11, '332', 234, 3342.00, 782028.00, 3255.00, 2345.00, '32221', 324552, 32344.00, '24442', '09/11/2019', '2019-11-09 14:37:00', '2019-11-09 14:37:00'),
(12, '322', 222, 22.00, 4884.00, 2222.00, 22.00, '22', 22, 22.00, '22', '09/11/2019', '2019-11-09 14:38:04', '2019-11-09 14:38:04'),
(13, '223', 22, 232.00, 5104.00, 21.00, 324.00, '232', 322, 224.00, '243', '09/11/2019', '2019-11-09 14:38:17', '2019-11-09 14:38:17'),
(14, '232', 322, 343.00, 110446.00, 234.00, 232.00, '234', 234, 234.00, '3', '09/11/2019', '2019-11-09 14:41:01', '2019-11-09 14:41:01'),
(15, 'Bricks', 10000, 22.00, 220000.00, 233.00, 232.00, '17', 23, 2334.00, 'None', '09/11/2019', '2019-11-09 14:43:13', '2019-11-09 14:43:13'),
(16, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '20000', 444, 2445.00, '22', '09/11/2019', '2019-11-09 14:43:43', '2019-11-09 14:43:43'),
(17, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 3333.00, '444', '09/11/2019', '2019-11-09 14:44:24', '2019-11-09 14:44:24'),
(18, 'ssd', 22, 10.00, 220.00, 40000.00, 232.00, '111', 444, 2314.00, '22', '09/11/2019', '2019-11-09 14:44:49', '2019-11-09 14:44:49'),
(19, 'Bricks', 10000, 10.00, 100000.00, 12.00, 232.00, '213213', 21431, 32344.00, '35444', '09/11/2019', '2019-11-09 14:47:17', '2019-11-09 14:47:17'),
(20, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 214, 2445.00, '444', '09/11/2019', '2019-11-09 14:48:01', '2019-11-09 14:48:01'),
(21, 'ssd', 10000, 22.00, 220000.00, 12.00, 23222.00, '20000', 444, 1111.00, '22', '09/11/2019', '2019-11-09 14:48:21', '2019-11-09 14:48:21'),
(22, 'Bricks', 10000, 22.00, 220000.00, 12.00, 232.00, '111', 444, 3333.00, '444', '09/11/2019', '2019-11-09 14:49:07', '2019-11-09 14:49:07'),
(23, 'Bricks', 10000, 22.00, 220000.00, 12.00, 232.00, '111', 444, 1111.00, 'None', '09/11/2019', '2019-11-09 14:49:28', '2019-11-09 14:49:28'),
(24, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '20000', 21431, 20000.00, '24442', '09/11/2019', '2019-11-09 14:49:42', '2019-11-09 14:49:42'),
(25, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 2445.00, 'None', '09/11/2019', '2019-11-09 14:50:39', '2019-11-09 14:50:39'),
(26, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 20000.00, '35444', '09/11/2019', '2019-11-09 14:50:55', '2019-11-09 14:50:55'),
(27, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 3333.00, '22', '09/11/2019', '2019-11-09 14:53:40', '2019-11-09 14:53:40'),
(28, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 2314.00, '35444', '09/11/2019', '2019-11-09 14:54:01', '2019-11-09 14:54:01');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `country_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cnic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_post_id` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_confirmation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `city`, `cnic`, `phone`, `image`, `employee_post_id`, `password`, `password_confirmation`, `address`, `status`, `created_at`, `updated_at`) VALUES
(7, 'Ali  Gujjar', 'saeedg8879@gmail.com', 'NAROWAL', '54545-4464654-6', '123-456-7895', '1572980190-ali-gujjar.png', 2, '$2y$10$mgvrE12S6c8ayJLkUzDWbe1Fe8rDD4bBAvElFMzOPXRV0yGxo8ftm', '123456', 'New Lahore Road', 1, '2019-11-05 13:56:30', '2019-11-05 13:56:30');

-- --------------------------------------------------------

--
-- Table structure for table `emplyee_posts`
--

CREATE TABLE `emplyee_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `emplyee_posts`
--

INSERT INTO `emplyee_posts` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(2, 'CEO', '<p>Head Of&nbsp; Orginization</p>', '2019-11-05 06:09:07', '2019-11-05 06:09:07'),
(3, 'Peon', '<p>Poen</p>', '2019-11-06 09:33:27', '2019-11-06 09:33:27');

-- --------------------------------------------------------

--
-- Table structure for table `emyloyee_pays`
--

CREATE TABLE `emyloyee_pays` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paid_salary` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_report`
--

CREATE TABLE `hotel_report` (
  `hb_id` bigint(20) NOT NULL,
  `members_id` int(11) NOT NULL,
  `Billamount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Discount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Credit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Paid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Bill_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hotel_report`
--

INSERT INTO `hotel_report` (`hb_id`, `members_id`, `Billamount`, `Discount`, `Credit`, `Paid`, `Bill_no`, `date`, `created_at`, `updated_at`) VALUES
(1, 246, '12000', '10', '10000', '2000', '22123', '0000-00-00', NULL, NULL),
(2, 18, '12', '22', '12', '22', '213', '', NULL, NULL),
(3, 26, '12000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(4, 26, '12000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(5, 26, '12000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(6, 26, '12000', '12', '40000', '11000', '233422', '0000-00-00', NULL, NULL),
(7, 23, '12000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(8, 23, '12000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(9, 21, '12000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(10, 5, '12000', '0', '40000', '11000', '233422', '0000-00-00', NULL, NULL),
(11, 8, '11000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(12, 8, '11000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(13, 8, '11000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(14, 8, '11000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(15, 8, '12000', '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(16, 12, '2000', '0', '1000', '1000', '123456', '0000-00-00', '2019-11-13 09:15:29', '2019-11-13 09:15:29'),
(18, 12, '12000', '0', '40000', '11000', '23213', '13/11/2019', '2019-11-13 09:19:55', '2019-11-13 09:19:55'),
(19, 2, '12000', '0', '1000', '11000', '747474', '13/11/2019', '2019-11-13 09:28:59', '2019-11-13 09:28:59'),
(20, 250, '2', '12', '12', '23', '23123', '', NULL, NULL),
(21, 184, '2', '11', '1', '1', '1', '', NULL, NULL),
(22, 26, '12000', '0', '40000', '11000', '23213', '13/11/2019', '2019-11-13 16:44:41', '2019-11-13 16:44:41'),
(23, 26, '12000', '0', '12', '12', '12', '13/11/2019', '2019-11-13 16:45:41', '2019-11-13 16:45:41'),
(24, 26, '12000', '0', '40000', '11000', '233422', '14/11/2019', '2019-11-14 01:59:09', '2019-11-14 01:59:09'),
(25, 8, '2000', '0', '1000', '1000', '76576', '14/11/2019', '2019-11-14 02:51:53', '2019-11-14 02:51:53');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Unique_idM` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `father_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qualification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `land_line` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_cnic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Type` enum('Vhr','New','Old') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `RegistrationFee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blood_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temporary_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permanent_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_of_organization` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_land_line` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Relation` enum('Wife','Son','Daughter','Mother','Father','Other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `spouse_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spouse_dob` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spouse_cnic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'N/A',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_confirmation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `Unique_idM`, `full_name`, `father_name`, `qualification`, `dob`, `mobile`, `land_line`, `twitter`, `member_cnic`, `Type`, `RegistrationFee`, `marital_status`, `blood_group`, `temporary_address`, `permanent_address`, `name_of_organization`, `designation`, `business_address`, `business_land_line`, `business_fax`, `Relation`, `spouse_name`, `spouse_dob`, `spouse_cnic`, `email`, `password`, `password_confirmation`, `image`, `status`, `created_at`, `updated_at`) VALUES
(6, '1', 'Hafiz Junaid Ahmad', 'Ahmad Waseem Sheikh', 'Bachalor', '11/11/1992', '0333-3022229', '3333-022229', 'sdsad', '23334-3243242-3', 'New', NULL, 'single', 'b+', 'Al-Wasal street 2A, Villa 7, behind emarat petrol station', 'Al-Wasal street 2A, Villa 7, behind emarat petrol station', 'cc', 'cc', 'cc', '3333-022229', '3333-022229', 'Other', 'cc', '11/11/1992', '33333-3333333-3', '1admin@gmail.com', '$2y$10$GJZLdBs7I8vsuRDXzVe3LO/M8w9c3j0oueEMvo87DeEL7k7xJEwde', '123456', '1573377994-hafiz-junaid-ahmad.jpg', 1, '2019-11-10 04:26:34', '2019-11-10 04:26:34'),
(7, '2', 'Rao Ghaffar Ali', 'Rao Khurshad Ali', 'Metric', NULL, '0300-7592902', NULL, NULL, NULL, 'New', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin@gmail.com', '$2y$10$zV4onPm8pJ.8fMvQRtk10O5kWlxjFgzWlUKHRlKRotGtSsCUoFaN6', '123456', NULL, 1, '2019-11-10 04:42:42', '2019-11-10 04:42:42'),
(8, '3', 'Riffat Tahir', 'Chudhary Muhammad Jamil', 'Metric', NULL, '0300-6993431', NULL, NULL, NULL, 'Vhr', '105000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'riffat@gmail.com', '$2y$10$s2iVCJyQFEM0Q1Lk9Ahoseuzlrnga1mzH2M80szculzcsbENR7.HS', '123456', NULL, 1, '2019-11-10 05:00:01', '2019-11-10 05:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `members_pay_records`
--

CREATE TABLE `members_pay_records` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `member_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `monthly_payment` double(8,2) DEFAULT NULL,
  `Unique_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberShip_fee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_fee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Aug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Sep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Oct` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instrument_No_105000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instrument_No_100000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instrument_No_55000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instrument_No_50000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insturment_No_5000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insturment_No_32500` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Instrument_No_Aug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Instrument_No_Sep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Instrument_No_Oct` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `members_pay_records`
--

INSERT INTO `members_pay_records` (`id`, `member_id`, `month`, `monthly_payment`, `Unique_id`, `created_at`, `updated_at`, `name`, `father_name`, `mobile_number`, `memberShip_fee`, `registration_fee`, `Aug`, `Sep`, `Oct`, `instrument_No_105000`, `instrument_No_100000`, `instrument_No_55000`, `instrument_No_50000`, `insturment_No_5000`, `insturment_No_32500`, `Instrument_No_Aug`, `Instrument_No_Sep`, `Instrument_No_Oct`, `Status`, `Comment`, `Date`) VALUES
(270, '1', NULL, NULL, NULL, '2019-11-09 18:45:49', '2019-11-09 18:45:49', NULL, 'Ahmad Waseem Sheikh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(271, '2', NULL, NULL, NULL, '2019-11-09 18:45:49', '2019-11-09 18:45:49', NULL, 'Rao Khurshad Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, '3', NULL, NULL, NULL, '2019-11-09 18:45:50', '2019-11-09 18:45:50', NULL, 'Chudhary Muhammad Jamil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, '4', NULL, NULL, NULL, '2019-11-09 18:45:50', '2019-11-09 18:45:50', NULL, 'Muhammad Ud din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(274, '5', NULL, NULL, NULL, '2019-11-09 18:45:50', '2019-11-09 18:45:50', NULL, 'Maqsood Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(275, '6', NULL, NULL, NULL, '2019-11-09 18:45:50', '2019-11-09 18:45:50', NULL, 'Muhammad Bukhtiar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(276, '7', NULL, NULL, NULL, '2019-11-09 18:45:50', '2019-11-09 18:45:50', NULL, 'Marghoob Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(277, '8', NULL, NULL, NULL, '2019-11-09 18:45:50', '2019-11-09 18:45:50', NULL, 'Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(278, '9', NULL, NULL, NULL, '2019-11-09 18:45:51', '2019-11-09 18:45:51', NULL, 'Chuadhary Mukhtayar Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(279, '10', NULL, NULL, NULL, '2019-11-09 18:45:51', '2019-11-09 18:45:51', NULL, 'Abdul Shakoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(280, '11', NULL, NULL, NULL, '2019-11-09 18:45:51', '2019-11-09 18:45:51', NULL, 'Haji Usman Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(281, '12', NULL, NULL, NULL, '2019-11-09 18:45:51', '2019-11-09 18:45:51', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(282, '13', NULL, NULL, NULL, '2019-11-09 18:45:51', '2019-11-09 18:45:51', NULL, 'Muhammad Nadeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(283, '14', NULL, NULL, NULL, '2019-11-09 18:45:51', '2019-11-09 18:45:51', NULL, 'Syed Soulat Hussain Naqvi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(284, '15', NULL, NULL, NULL, '2019-11-09 18:45:52', '2019-11-09 18:45:52', NULL, 'Maghoob Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(285, '16', NULL, NULL, NULL, '2019-11-09 18:45:52', '2019-11-09 18:45:52', NULL, 'Syed Mehdi Hassan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(286, '17', NULL, NULL, NULL, '2019-11-09 18:45:52', '2019-11-09 18:45:52', NULL, 'Muhammad Mateen Zaman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(287, '18', NULL, NULL, NULL, '2019-11-09 18:45:52', '2019-11-09 18:45:52', NULL, 'Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(288, '19', NULL, NULL, NULL, '2019-11-09 18:45:53', '2019-11-09 18:45:53', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, '20', NULL, NULL, NULL, '2019-11-09 18:45:53', '2019-11-09 18:45:53', NULL, 'Masood Ahmad Kahloon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(290, '21', NULL, NULL, NULL, '2019-11-09 18:45:53', '2019-11-09 18:45:53', NULL, 'Abdul Razaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, '22', NULL, NULL, NULL, '2019-11-09 18:45:53', '2019-11-09 18:45:53', NULL, 'Muhammad Aslam ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, '23', NULL, NULL, NULL, '2019-11-09 18:45:53', '2019-11-09 18:45:53', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(293, '24', NULL, NULL, NULL, '2019-11-09 18:45:53', '2019-11-09 18:45:53', NULL, 'Ch Amin wahla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(294, '25', NULL, NULL, NULL, '2019-11-09 18:45:53', '2019-11-09 18:45:53', NULL, 'Muhammad Iqbal Saleemi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(295, '26', NULL, NULL, NULL, '2019-11-09 18:45:54', '2019-11-09 18:45:54', NULL, 'Sheikh Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(296, '27', NULL, NULL, NULL, '2019-11-09 18:45:54', '2019-11-09 18:45:54', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, '28', NULL, NULL, NULL, '2019-11-09 18:45:54', '2019-11-09 18:45:54', NULL, 'Abdul Ghaffar Butt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(298, '29', NULL, NULL, NULL, '2019-11-09 18:45:54', '2019-11-09 18:45:54', NULL, 'Chudhary Ali Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, '30', NULL, NULL, NULL, '2019-11-09 18:45:54', '2019-11-09 18:45:54', NULL, 'Abdul Razzaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, '31', NULL, NULL, NULL, '2019-11-09 18:45:54', '2019-11-09 18:45:54', NULL, 'Abdul Ghafoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, '32', NULL, NULL, NULL, '2019-11-09 18:45:55', '2019-11-09 18:45:55', NULL, 'Haji Muhammad Raza ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(302, '33', NULL, NULL, NULL, '2019-11-09 18:45:55', '2019-11-09 18:45:55', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, '34', NULL, NULL, NULL, '2019-11-09 18:45:55', '2019-11-09 18:45:55', NULL, 'Haji Muhammad Raza ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(304, '35', NULL, NULL, NULL, '2019-11-09 18:45:55', '2019-11-09 18:45:55', NULL, 'Khalid Farooq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(305, '36', NULL, NULL, NULL, '2019-11-09 18:45:55', '2019-11-09 18:45:55', NULL, 'Muhammad Ayub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, '37', NULL, NULL, NULL, '2019-11-09 18:45:56', '2019-11-09 18:45:56', NULL, 'Amanat Ali Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(307, '38', NULL, NULL, NULL, '2019-11-09 18:45:56', '2019-11-09 18:45:56', NULL, 'Ch Abdul Sattar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(308, '39', NULL, NULL, NULL, '2019-11-09 18:45:56', '2019-11-09 18:45:56', NULL, 'Muhammad Akbar Wahla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, '40', NULL, NULL, NULL, '2019-11-09 18:45:56', '2019-11-09 18:45:56', NULL, 'Chuadhary Abdul Ghanni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, '41', NULL, NULL, NULL, '2019-11-09 18:45:56', '2019-11-09 18:45:56', NULL, 'Ameer Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, '42', NULL, NULL, NULL, '2019-11-09 18:45:56', '2019-11-09 18:45:56', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(312, '43', NULL, NULL, NULL, '2019-11-09 18:45:57', '2019-11-09 18:45:57', NULL, 'Muhammad Din Sheikh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(313, '44', NULL, NULL, NULL, '2019-11-09 18:45:57', '2019-11-09 18:45:57', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(314, '45', NULL, NULL, NULL, '2019-11-09 18:45:57', '2019-11-09 18:45:57', NULL, 'Atta Ur Rehman Maqsood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(315, '46', NULL, NULL, NULL, '2019-11-09 18:45:57', '2019-11-09 18:45:57', NULL, 'Chudhary Khusi Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, '47', NULL, NULL, NULL, '2019-11-09 18:45:57', '2019-11-09 18:45:57', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(317, '48', NULL, NULL, NULL, '2019-11-09 18:45:58', '2019-11-09 18:45:58', NULL, 'Ghulam Mustafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(318, '49', NULL, NULL, NULL, '2019-11-09 18:45:58', '2019-11-09 18:45:58', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, '50', NULL, NULL, NULL, '2019-11-09 18:45:58', '2019-11-09 18:45:58', NULL, 'Maqsood Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(320, '151', NULL, NULL, NULL, '2019-11-09 18:45:58', '2019-11-09 18:45:58', NULL, 'Chaudhary Muhammad Sabir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(321, '52', NULL, NULL, NULL, '2019-11-09 18:45:58', '2019-11-09 18:45:58', NULL, 'Muhammad Suleman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(322, '53', NULL, NULL, NULL, '2019-11-09 18:45:58', '2019-11-09 18:45:58', NULL, 'Abdul Ghafoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(323, '54', NULL, NULL, NULL, '2019-11-09 18:45:59', '2019-11-09 18:45:59', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(324, '55', NULL, NULL, NULL, '2019-11-09 18:45:59', '2019-11-09 18:45:59', NULL, 'Muhammad Hanif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(325, '56', NULL, NULL, NULL, '2019-11-09 18:45:59', '2019-11-09 18:45:59', NULL, 'Muhammad Sadique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(326, '57', NULL, NULL, NULL, '2019-11-09 18:45:59', '2019-11-09 18:45:59', NULL, 'Rao Irshad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(327, '58', NULL, NULL, NULL, '2019-11-09 18:45:59', '2019-11-09 18:45:59', NULL, 'Chaudhary Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(328, '59', NULL, NULL, NULL, '2019-11-09 18:45:59', '2019-11-09 18:45:59', NULL, 'Nazir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(329, '60', NULL, NULL, NULL, '2019-11-09 18:45:59', '2019-11-09 18:45:59', NULL, 'Rafique Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(330, '61', NULL, NULL, NULL, '2019-11-09 18:46:00', '2019-11-09 18:46:00', NULL, 'Sardar Khalil Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(331, '62', NULL, NULL, NULL, '2019-11-09 18:46:00', '2019-11-09 18:46:00', NULL, 'Rao Nazir Ahmed Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(332, '63', NULL, NULL, NULL, '2019-11-09 18:46:00', '2019-11-09 18:46:00', NULL, 'Muhammad Ramzan Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(333, '64', NULL, NULL, NULL, '2019-11-09 18:46:00', '2019-11-09 18:46:00', NULL, 'Gul Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(334, '65', NULL, NULL, NULL, '2019-11-09 18:46:00', '2019-11-09 18:46:00', NULL, 'Ghulam Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(335, '66', NULL, NULL, NULL, '2019-11-09 18:46:00', '2019-11-09 18:46:00', NULL, 'Hashmat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(336, '67', NULL, NULL, NULL, '2019-11-09 18:46:01', '2019-11-09 18:46:01', NULL, 'Ch Sher Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(337, '68', NULL, NULL, NULL, '2019-11-09 18:46:01', '2019-11-09 18:46:01', NULL, 'Abdul Ghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(338, '69', NULL, NULL, NULL, '2019-11-09 18:46:01', '2019-11-09 18:46:01', NULL, 'Sheikh Muhammad Nawaz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(339, '70', NULL, NULL, NULL, '2019-11-09 18:46:01', '2019-11-09 18:46:01', NULL, 'Wali Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(340, '71', NULL, NULL, NULL, '2019-11-09 18:46:01', '2019-11-09 18:46:01', NULL, 'Bashir Ahmad Ghumman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(341, '72', NULL, NULL, NULL, '2019-11-09 18:46:02', '2019-11-09 18:46:02', NULL, 'Muhammad Arif Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(342, '73', NULL, NULL, NULL, '2019-11-09 18:46:02', '2019-11-09 18:46:02', NULL, 'Muhammad Hanif Ghumman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(343, '74', NULL, NULL, NULL, '2019-11-09 18:46:02', '2019-11-09 18:46:02', NULL, 'Muhammad Arif Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(344, '75', NULL, NULL, NULL, '2019-11-09 18:46:02', '2019-11-09 18:46:02', NULL, 'Rukan Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(345, '76', NULL, NULL, NULL, '2019-11-09 18:46:03', '2019-11-09 18:46:03', NULL, 'Muhammad Bilal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(346, '77', NULL, NULL, NULL, '2019-11-09 18:46:03', '2019-11-09 18:46:03', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(347, '78', NULL, NULL, NULL, '2019-11-09 18:46:03', '2019-11-09 18:46:03', NULL, 'Naseer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(348, '79', NULL, NULL, NULL, '2019-11-09 18:46:04', '2019-11-09 18:46:04', NULL, 'Muhammad Abdur Ruaf Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(349, '80', NULL, NULL, NULL, '2019-11-09 18:46:04', '2019-11-09 18:46:04', NULL, 'Sardar Ghulam Farid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(350, '81', NULL, NULL, NULL, '2019-11-09 18:46:04', '2019-11-09 18:46:04', NULL, 'Kareem Dad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(351, '82', NULL, NULL, NULL, '2019-11-09 18:46:04', '2019-11-09 18:46:04', NULL, 'Muhammad Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(352, '83', NULL, NULL, NULL, '2019-11-09 18:46:04', '2019-11-09 18:46:04', NULL, 'Rasheed Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(353, '84', NULL, NULL, NULL, '2019-11-09 18:46:05', '2019-11-09 18:46:05', NULL, 'Nazir Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(354, '85', NULL, NULL, NULL, '2019-11-09 18:46:05', '2019-11-09 18:46:05', NULL, 'Muhammad Bota', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(355, '86', NULL, NULL, NULL, '2019-11-09 18:46:05', '2019-11-09 18:46:05', NULL, 'Abdul Ghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(356, '87', NULL, NULL, NULL, '2019-11-09 18:46:05', '2019-11-09 18:46:05', NULL, 'Muhammad Asghar Sabir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(357, '88', NULL, NULL, NULL, '2019-11-09 18:46:05', '2019-11-09 18:46:05', NULL, 'Qazi Maqsood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(358, '89', NULL, NULL, NULL, '2019-11-09 18:46:05', '2019-11-09 18:46:05', NULL, 'Muhammad Shafi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(359, '90', NULL, NULL, NULL, '2019-11-09 18:46:06', '2019-11-09 18:46:06', NULL, 'Muhammad Munir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(360, '91', NULL, NULL, NULL, '2019-11-09 18:46:06', '2019-11-09 18:46:06', NULL, 'Nawab Ali Javed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(361, '92', NULL, NULL, NULL, '2019-11-09 18:46:06', '2019-11-09 18:46:06', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(362, '93', NULL, NULL, NULL, '2019-11-09 18:46:06', '2019-11-09 18:46:06', NULL, 'Khan Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(363, '94', NULL, NULL, NULL, '2019-11-09 18:46:06', '2019-11-09 18:46:06', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(364, '95', NULL, NULL, NULL, '2019-11-09 18:46:06', '2019-11-09 18:46:06', NULL, 'Dr.Muhammad Aslam Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(365, '96', NULL, NULL, NULL, '2019-11-09 18:46:06', '2019-11-09 18:46:06', NULL, 'Abul Razzaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(366, '97', NULL, NULL, NULL, '2019-11-09 18:46:07', '2019-11-09 18:46:07', NULL, 'Ghulam Nabi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(367, '98', NULL, NULL, NULL, '2019-11-09 18:46:07', '2019-11-09 18:46:07', NULL, 'Niaz Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(368, '99', NULL, NULL, NULL, '2019-11-09 18:46:07', '2019-11-09 18:46:07', NULL, 'Muhammad Ayub khan saldera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(369, '100', NULL, NULL, NULL, '2019-11-09 18:46:07', '2019-11-09 18:46:07', NULL, 'Akbar Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(370, '101', NULL, NULL, NULL, '2019-11-09 18:46:07', '2019-11-09 18:46:07', NULL, 'Ch.Asmat saeed Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(371, '102', NULL, NULL, NULL, '2019-11-09 18:46:07', '2019-11-09 18:46:07', NULL, 'Asmat Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(372, '103', NULL, NULL, NULL, '2019-11-09 18:46:08', '2019-11-09 18:46:08', NULL, 'Sardar Muhammad Sajjad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(373, '104', NULL, NULL, NULL, '2019-11-09 18:46:08', '2019-11-09 18:46:08', NULL, 'Raja Muhammad Akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(374, '105', NULL, NULL, NULL, '2019-11-09 18:46:08', '2019-11-09 18:46:08', NULL, 'Sardar Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(375, '106', NULL, NULL, NULL, '2019-11-09 18:46:08', '2019-11-09 18:46:08', NULL, 'Rehmat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(376, '107', NULL, NULL, NULL, '2019-11-09 18:46:09', '2019-11-09 18:46:09', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(377, '108', NULL, NULL, NULL, '2019-11-09 18:46:09', '2019-11-09 18:46:09', NULL, 'Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(378, '109', NULL, NULL, NULL, '2019-11-09 18:46:09', '2019-11-09 18:46:09', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(379, '110', NULL, NULL, NULL, '2019-11-09 18:46:09', '2019-11-09 18:46:09', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(380, '111', NULL, NULL, NULL, '2019-11-09 18:46:09', '2019-11-09 18:46:09', NULL, 'Abdul Majeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(381, '112', NULL, NULL, NULL, '2019-11-09 18:46:09', '2019-11-09 18:46:09', NULL, 'Muhammad Aslam Naseem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(382, '113', NULL, NULL, NULL, '2019-11-09 18:46:10', '2019-11-09 18:46:10', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(383, '114', NULL, NULL, NULL, '2019-11-09 18:46:10', '2019-11-09 18:46:10', NULL, 'Muhammad Ishaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(384, '115', NULL, NULL, NULL, '2019-11-09 18:46:10', '2019-11-09 18:46:10', NULL, 'Muhammad Tufail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(385, '116', NULL, NULL, NULL, '2019-11-09 18:46:10', '2019-11-09 18:46:10', NULL, 'Syed Riaz Hussain Rizvi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(386, '117', NULL, NULL, NULL, '2019-11-09 18:46:10', '2019-11-09 18:46:10', NULL, 'Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(387, '118', NULL, NULL, NULL, '2019-11-09 18:46:10', '2019-11-09 18:46:10', NULL, 'Muhammad  Arshad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(388, '119', NULL, NULL, NULL, '2019-11-09 18:46:11', '2019-11-09 18:46:11', NULL, 'Muhammad Yaseen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(389, '120', NULL, NULL, NULL, '2019-11-09 18:46:11', '2019-11-09 18:46:11', NULL, 'Javed Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(390, '121', NULL, NULL, NULL, '2019-11-09 18:46:11', '2019-11-09 18:46:11', NULL, 'Dawood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(391, '122', NULL, NULL, NULL, '2019-11-09 18:46:11', '2019-11-09 18:46:11', NULL, 'Faqir Muhammad Chaudhary', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(392, '123', NULL, NULL, NULL, '2019-11-09 18:46:11', '2019-11-09 18:46:11', NULL, 'Chudhary KhusiMuhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(393, '124', NULL, NULL, NULL, '2019-11-09 18:46:11', '2019-11-09 18:46:11', NULL, 'Muhammad Akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(394, '125', NULL, NULL, NULL, '2019-11-09 18:46:12', '2019-11-09 18:46:12', NULL, 'Chadhary Asmat Saeed Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(395, '126', NULL, NULL, NULL, '2019-11-09 18:46:12', '2019-11-09 18:46:12', NULL, 'Rao Irshad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(396, '127', NULL, NULL, NULL, '2019-11-09 18:46:12', '2019-11-09 18:46:12', NULL, 'Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(397, '128', NULL, NULL, NULL, '2019-11-09 18:46:12', '2019-11-09 18:46:12', NULL, 'Muhammad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(398, '129', NULL, NULL, NULL, '2019-11-09 18:46:12', '2019-11-09 18:46:12', NULL, 'Muhammad Mansha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(399, '130', NULL, NULL, NULL, '2019-11-09 18:46:12', '2019-11-09 18:46:12', NULL, 'Muhammad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(400, '131', NULL, NULL, NULL, '2019-11-09 18:46:12', '2019-11-09 18:46:12', NULL, 'Abdul Raheem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(401, '132', NULL, NULL, NULL, '2019-11-09 18:46:13', '2019-11-09 18:46:13', NULL, 'Ghulam Mustafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(402, '133', NULL, NULL, NULL, '2019-11-09 18:46:13', '2019-11-09 18:46:13', NULL, 'Malik Karamat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(403, '134', NULL, NULL, NULL, '2019-11-09 18:46:13', '2019-11-09 18:46:13', NULL, 'Naseer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(404, '135', NULL, NULL, NULL, '2019-11-09 18:46:13', '2019-11-09 18:46:13', NULL, 'Rao Khan Bahadar Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, '136', NULL, NULL, NULL, '2019-11-09 18:46:13', '2019-11-09 18:46:13', NULL, 'Mian Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(406, '137', NULL, NULL, NULL, '2019-11-09 18:46:14', '2019-11-09 18:46:14', NULL, 'Zulfiqar Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(407, '138', NULL, NULL, NULL, '2019-11-09 18:46:14', '2019-11-09 18:46:14', NULL, 'Mukhtar Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(408, '139', NULL, NULL, NULL, '2019-11-09 18:46:14', '2019-11-09 18:46:14', NULL, 'Asgher Ali Sandhu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(409, '140', NULL, NULL, NULL, '2019-11-09 18:46:14', '2019-11-09 18:46:14', NULL, 'Khushi Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(410, '141', NULL, NULL, NULL, '2019-11-09 18:46:14', '2019-11-09 18:46:14', NULL, 'Muhammad Shaffi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(411, '142', NULL, NULL, NULL, '2019-11-09 18:46:14', '2019-11-09 18:46:14', NULL, 'Siraj Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(412, '143', NULL, NULL, NULL, '2019-11-09 18:46:15', '2019-11-09 18:46:15', NULL, 'Raja Ghulam Haider Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(413, '144', NULL, NULL, NULL, '2019-11-09 18:46:15', '2019-11-09 18:46:15', NULL, 'Muhammad Ramzan Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(414, '145', NULL, NULL, NULL, '2019-11-09 18:46:15', '2019-11-09 18:46:15', NULL, 'Konwar Akbar Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(415, '146', NULL, NULL, NULL, '2019-11-09 18:46:15', '2019-11-09 18:46:15', NULL, 'Khalid Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(416, '147', NULL, NULL, NULL, '2019-11-09 18:46:15', '2019-11-09 18:46:15', NULL, 'Abdur Rehman ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(417, '148', NULL, NULL, NULL, '2019-11-09 18:46:15', '2019-11-09 18:46:15', NULL, 'Muhammad Amin Chisti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(418, '149', NULL, NULL, NULL, '2019-11-09 18:46:16', '2019-11-09 18:46:16', NULL, 'Muhammad Tufail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(419, '150', NULL, NULL, NULL, '2019-11-09 18:46:16', '2019-11-09 18:46:16', NULL, 'Chadhary Muhammad Saleem Kahloon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(420, '51', NULL, NULL, NULL, '2019-11-09 18:46:16', '2019-11-09 18:46:16', NULL, 'Atta Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(421, '152', NULL, NULL, NULL, '2019-11-09 18:46:16', '2019-11-09 18:46:16', NULL, 'Muhammad Akbar khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(422, '153', NULL, NULL, NULL, '2019-11-09 18:46:16', '2019-11-09 18:46:16', NULL, 'Muhammad Akbar khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(423, '154', NULL, NULL, NULL, '2019-11-09 18:46:16', '2019-11-09 18:46:16', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(424, '155', NULL, NULL, NULL, '2019-11-09 18:46:17', '2019-11-09 18:46:17', NULL, 'Faqir Muhammad ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(425, '156', NULL, NULL, NULL, '2019-11-09 18:46:17', '2019-11-09 18:46:17', NULL, 'Liaqat Ali Rasal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(426, '157', NULL, NULL, NULL, '2019-11-09 18:46:17', '2019-11-09 18:46:17', NULL, 'Muhammad Khaliq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(427, '158', NULL, NULL, NULL, '2019-11-09 18:46:17', '2019-11-09 18:46:17', NULL, 'Malik Nazar Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(428, '159', NULL, NULL, NULL, '2019-11-09 18:46:17', '2019-11-09 18:46:17', NULL, 'Muhammad Sharif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(429, '160', NULL, NULL, NULL, '2019-11-09 18:46:17', '2019-11-09 18:46:17', NULL, 'Abdul Majid ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(430, '161', NULL, NULL, NULL, '2019-11-09 18:46:18', '2019-11-09 18:46:18', NULL, 'Shaukat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(431, '162', NULL, NULL, NULL, '2019-11-09 18:46:18', '2019-11-09 18:46:18', NULL, 'Muhammad Yousaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(432, '163', NULL, NULL, NULL, '2019-11-09 18:46:18', '2019-11-09 18:46:18', NULL, 'Ch Rehmat Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(433, '164', NULL, NULL, NULL, '2019-11-09 18:46:19', '2019-11-09 18:46:19', NULL, 'Shahid Mehmood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(434, '165', NULL, NULL, NULL, '2019-11-09 18:46:19', '2019-11-09 18:46:19', NULL, 'Khalid Mehmood Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(435, '166', NULL, NULL, NULL, '2019-11-09 18:46:20', '2019-11-09 18:46:20', NULL, 'Ahmad Naseem Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(436, '167', NULL, NULL, NULL, '2019-11-09 18:46:20', '2019-11-09 18:46:20', NULL, 'Nabardar Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(437, '168', NULL, NULL, NULL, '2019-11-09 18:46:20', '2019-11-09 18:46:20', NULL, 'Fateh Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(438, '169', NULL, NULL, NULL, '2019-11-09 18:46:20', '2019-11-09 18:46:20', NULL, 'Dilmeer Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(439, '170', NULL, NULL, NULL, '2019-11-09 18:46:20', '2019-11-09 18:46:20', NULL, 'Muhammad Shafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(440, '171', NULL, NULL, NULL, '2019-11-09 18:46:20', '2019-11-09 18:46:20', NULL, 'Sh.Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(441, '172', NULL, NULL, NULL, '2019-11-09 18:46:20', '2019-11-09 18:46:20', NULL, 'Muhammad ishtiaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(442, '173', NULL, NULL, NULL, '2019-11-09 18:46:21', '2019-11-09 18:46:21', NULL, 'Muhammad Arshad Shah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(443, '174', NULL, NULL, NULL, '2019-11-09 18:46:21', '2019-11-09 18:46:21', NULL, 'Muhammad Rafique Dogar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(444, '175', NULL, NULL, NULL, '2019-11-09 18:46:21', '2019-11-09 18:46:21', NULL, 'Muhammad Habib Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(445, '176', NULL, NULL, NULL, '2019-11-09 18:46:21', '2019-11-09 18:46:21', NULL, 'Nazir Ahmad Mehmood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(446, '177', NULL, NULL, NULL, '2019-11-09 18:46:21', '2019-11-09 18:46:21', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(447, '178', NULL, NULL, NULL, '2019-11-09 18:46:21', '2019-11-09 18:46:21', NULL, 'Taj Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(448, '179', NULL, NULL, NULL, '2019-11-09 18:46:21', '2019-11-09 18:46:21', NULL, 'Haji Muhammad ishaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(449, '180', NULL, NULL, NULL, '2019-11-09 18:46:22', '2019-11-09 18:46:22', NULL, 'Mian Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(450, '181', NULL, NULL, NULL, '2019-11-09 18:46:22', '2019-11-09 18:46:22', NULL, 'Muhammad Hanif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(451, '182', NULL, NULL, NULL, '2019-11-09 18:46:22', '2019-11-09 18:46:22', NULL, 'Malik Muhammad Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(452, '183', NULL, NULL, NULL, '2019-11-09 18:46:22', '2019-11-09 18:46:22', NULL, 'Muhammad Sadique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(453, '184', NULL, NULL, NULL, '2019-11-09 18:46:22', '2019-11-09 18:46:22', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(454, '185', NULL, NULL, NULL, '2019-11-09 18:46:22', '2019-11-09 18:46:22', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(455, '186', NULL, NULL, NULL, '2019-11-09 18:46:23', '2019-11-09 18:46:23', NULL, 'Liaqat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(456, '187', NULL, NULL, NULL, '2019-11-09 18:46:23', '2019-11-09 18:46:23', NULL, 'Muhammad Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(457, '188', NULL, NULL, NULL, '2019-11-09 18:46:23', '2019-11-09 18:46:23', NULL, 'Muhammmad Sardar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(458, '189', NULL, NULL, NULL, '2019-11-09 18:46:23', '2019-11-09 18:46:23', NULL, 'Muhammad Sarwar ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(459, '190', NULL, NULL, NULL, '2019-11-09 18:46:23', '2019-11-09 18:46:23', NULL, 'Ch.Faqir Muhammad Arian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(460, '191', NULL, NULL, NULL, '2019-11-09 18:46:24', '2019-11-09 18:46:24', NULL, 'Muhammad Idrees', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(461, '192', NULL, NULL, NULL, '2019-11-09 18:46:24', '2019-11-09 18:46:24', NULL, 'Ghulam Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(462, '193', NULL, NULL, NULL, '2019-11-09 18:46:24', '2019-11-09 18:46:24', NULL, 'Muhammad Akhtar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(463, '194', NULL, NULL, NULL, '2019-11-09 18:46:24', '2019-11-09 18:46:24', NULL, 'Muhammad Mueez ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(464, '195', NULL, NULL, NULL, '2019-11-09 18:46:24', '2019-11-09 18:46:24', NULL, 'Nazeer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(465, '196', NULL, NULL, NULL, '2019-11-09 18:46:24', '2019-11-09 18:46:24', NULL, 'Muhammad Shafi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(466, '197', NULL, NULL, NULL, '2019-11-09 18:46:25', '2019-11-09 18:46:25', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(467, '198', NULL, NULL, NULL, '2019-11-09 18:46:25', '2019-11-09 18:46:25', NULL, 'Abdul Majeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(468, '199', NULL, NULL, NULL, '2019-11-09 18:46:25', '2019-11-09 18:46:25', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(469, '200', NULL, NULL, NULL, '2019-11-09 18:46:25', '2019-11-09 18:46:25', NULL, 'Muhammad Irshad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(470, '201', NULL, NULL, NULL, '2019-11-09 18:46:25', '2019-11-09 18:46:25', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(471, '202', NULL, NULL, NULL, '2019-11-09 18:46:25', '2019-11-09 18:46:25', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(472, '203', NULL, NULL, NULL, '2019-11-09 18:46:25', '2019-11-09 18:46:25', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(473, '204', NULL, NULL, NULL, '2019-11-09 18:46:26', '2019-11-09 18:46:26', NULL, 'Muhammad Mushtaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(474, '205', NULL, NULL, NULL, '2019-11-09 18:46:26', '2019-11-09 18:46:26', NULL, 'Chuadhary Talib Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(475, '206', NULL, NULL, NULL, '2019-11-09 18:46:26', '2019-11-09 18:46:26', NULL, 'Basheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(476, '207', NULL, NULL, NULL, '2019-11-09 18:46:26', '2019-11-09 18:46:26', NULL, 'Muhammad Ibrahim', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(477, '208', NULL, NULL, NULL, '2019-11-09 18:46:26', '2019-11-09 18:46:26', NULL, 'Ch Khadam Hussain Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(478, '209', NULL, NULL, NULL, '2019-11-09 18:46:26', '2019-11-09 18:46:26', NULL, 'Sardar Muhammad Abid Azeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(479, '210', NULL, NULL, NULL, '2019-11-09 18:46:26', '2019-11-09 18:46:26', NULL, 'Muhammad Yaqoob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(480, '211', NULL, NULL, NULL, '2019-11-09 18:46:26', '2019-11-09 18:46:26', NULL, 'Muhammad Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(481, '212', NULL, NULL, NULL, '2019-11-09 18:46:27', '2019-11-09 18:46:27', NULL, 'Tariq Mehmood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(482, '213', NULL, NULL, NULL, '2019-11-09 18:46:27', '2019-11-09 18:46:27', NULL, 'Mian Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(483, '214', NULL, NULL, NULL, '2019-11-09 18:46:27', '2019-11-09 18:46:27', NULL, 'Shafqaat Munir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(484, '215', NULL, NULL, NULL, '2019-11-09 18:46:27', '2019-11-09 18:46:27', NULL, 'Hashim Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(485, '216', NULL, NULL, NULL, '2019-11-09 18:46:27', '2019-11-09 18:46:27', NULL, 'Umer Farooq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(486, '217', NULL, NULL, NULL, '2019-11-09 18:46:27', '2019-11-09 18:46:27', NULL, 'Muhammad Sarwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(487, '218', NULL, NULL, NULL, '2019-11-09 18:46:27', '2019-11-09 18:46:27', NULL, 'Rana Muhammad Sarwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(488, '219', NULL, NULL, NULL, '2019-11-09 18:46:28', '2019-11-09 18:46:28', NULL, 'Riaz Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(489, '220', NULL, NULL, NULL, '2019-11-09 18:46:28', '2019-11-09 18:46:28', NULL, 'Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(490, '221', NULL, NULL, NULL, '2019-11-09 18:46:28', '2019-11-09 18:46:28', NULL, 'Zahoor Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(491, '222', NULL, NULL, NULL, '2019-11-09 18:46:28', '2019-11-09 18:46:28', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(492, '223', NULL, NULL, NULL, '2019-11-09 18:46:29', '2019-11-09 18:46:29', NULL, 'Shafqat Munir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(493, '224', NULL, NULL, NULL, '2019-11-09 18:46:29', '2019-11-09 18:46:29', NULL, 'Muhammad Ashiq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(494, '225', NULL, NULL, NULL, '2019-11-09 18:46:29', '2019-11-09 18:46:29', NULL, 'Muhammad Jamil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(495, '226', NULL, NULL, NULL, '2019-11-09 18:46:29', '2019-11-09 18:46:29', NULL, 'Muhammad Sharif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(496, '227', NULL, NULL, NULL, '2019-11-09 18:46:29', '2019-11-09 18:46:29', NULL, 'Maqbool Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(497, '228', NULL, NULL, NULL, '2019-11-09 18:46:29', '2019-11-09 18:46:29', NULL, 'Inayat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(498, '229', NULL, NULL, NULL, '2019-11-09 18:46:30', '2019-11-09 18:46:30', NULL, 'Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(499, '230', NULL, NULL, NULL, '2019-11-09 18:46:30', '2019-11-09 18:46:30', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(500, '231', NULL, NULL, NULL, '2019-11-09 18:46:30', '2019-11-09 18:46:30', NULL, 'Sheikh Naseer ud din Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(501, '232', NULL, NULL, NULL, '2019-11-09 18:46:30', '2019-11-09 18:46:30', NULL, 'Abdul Razaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(502, '233', NULL, NULL, NULL, '2019-11-09 18:46:30', '2019-11-09 18:46:30', NULL, 'Dr.Zafar Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(503, '234', NULL, NULL, NULL, '2019-11-09 18:46:30', '2019-11-09 18:46:30', NULL, 'Ch.Mukhtar Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(504, '235', NULL, NULL, NULL, '2019-11-09 18:46:31', '2019-11-09 18:46:31', NULL, 'Rao Noor Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(505, '236', NULL, NULL, NULL, '2019-11-09 18:46:31', '2019-11-09 18:46:31', NULL, 'Ch. Noor Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `members_pay_records` (`id`, `member_id`, `month`, `monthly_payment`, `Unique_id`, `created_at`, `updated_at`, `name`, `father_name`, `mobile_number`, `memberShip_fee`, `registration_fee`, `Aug`, `Sep`, `Oct`, `instrument_No_105000`, `instrument_No_100000`, `instrument_No_55000`, `instrument_No_50000`, `insturment_No_5000`, `insturment_No_32500`, `Instrument_No_Aug`, `Instrument_No_Sep`, `Instrument_No_Oct`, `Status`, `Comment`, `Date`) VALUES
(506, '237', NULL, NULL, NULL, '2019-11-09 18:46:31', '2019-11-09 18:46:31', NULL, 'Ch Irshad Ahmad Arian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(507, '238', NULL, NULL, NULL, '2019-11-09 18:46:31', '2019-11-09 18:46:31', NULL, 'Ch Sardar Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(508, '239', NULL, NULL, NULL, '2019-11-09 18:46:31', '2019-11-09 18:46:31', NULL, 'Malik Muhammad Yousaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(509, '240', NULL, NULL, NULL, '2019-11-09 18:46:31', '2019-11-09 18:46:31', NULL, 'Ch Riaz Ul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(510, '241', NULL, NULL, NULL, '2019-11-09 18:46:31', '2019-11-09 18:46:31', NULL, 'Haji Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(511, '242', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, 'Haji Muhammad Waryam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(512, '243', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, 'Muhammad Saleem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(513, '244', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, 'Muhammad Saeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(514, '245', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, 'Abdul Gaffar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(515, '246', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, 'Muhammad Azeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(516, '247', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, 'Muhammad Sharif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(517, '248', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, 'Abdul Latif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(518, '249', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, 'Ch. Abdul khaliq ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(519, '250', NULL, NULL, NULL, '2019-11-09 18:46:32', '2019-11-09 18:46:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(520, '251', NULL, NULL, NULL, '2019-11-09 18:46:33', '2019-11-09 18:46:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(521, '252', NULL, NULL, NULL, '2019-11-09 18:46:33', '2019-11-09 18:46:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(522, '253', NULL, NULL, NULL, '2019-11-09 18:46:33', '2019-11-09 18:46:33', NULL, 'Muhammad Akram ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(523, '254', NULL, NULL, NULL, '2019-11-09 18:46:33', '2019-11-09 18:46:33', NULL, 'Ghulam Sarwar Ch.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(524, '255', NULL, NULL, NULL, '2019-11-09 18:46:34', '2019-11-09 18:46:34', NULL, 'Adbul Hameed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(525, '256', NULL, NULL, NULL, '2019-11-09 18:46:34', '2019-11-09 18:46:34', NULL, 'Muhammad Abdullah khan Asem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(526, '257', NULL, NULL, NULL, '2019-11-09 18:46:34', '2019-11-09 18:46:34', NULL, 'Muhammad Yaqoob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(527, '258', NULL, NULL, NULL, '2019-11-09 18:46:35', '2019-11-09 18:46:35', NULL, 'Zafar Hussain ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(528, '259', NULL, NULL, NULL, '2019-11-09 18:46:35', '2019-11-09 18:46:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(529, '260', NULL, NULL, NULL, '2019-11-09 18:46:35', '2019-11-09 18:46:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(530, NULL, NULL, NULL, NULL, '2019-11-09 18:46:35', '2019-11-09 18:46:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(531, '1', NULL, NULL, NULL, '2019-11-09 18:46:36', '2019-11-09 18:46:36', NULL, 'Ahmad Waseem Sheikh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(532, '2', NULL, NULL, NULL, '2019-11-09 18:46:36', '2019-11-09 18:46:36', NULL, 'Rao Khurshad Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(533, '3', NULL, NULL, NULL, '2019-11-09 18:46:36', '2019-11-09 18:46:36', NULL, 'Chudhary Muhammad Jamil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(534, '4', NULL, NULL, NULL, '2019-11-09 18:46:36', '2019-11-09 18:46:36', NULL, 'Muhammad Ud din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(535, '5', NULL, NULL, NULL, '2019-11-09 18:46:37', '2019-11-09 18:46:37', NULL, 'Maqsood Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(536, '6', NULL, NULL, NULL, '2019-11-09 18:46:37', '2019-11-09 18:46:37', NULL, 'Muhammad Bukhtiar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(537, '7', NULL, NULL, NULL, '2019-11-09 18:46:37', '2019-11-09 18:46:37', NULL, 'Marghoob Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(538, '8', NULL, NULL, NULL, '2019-11-09 18:46:37', '2019-11-09 18:46:37', NULL, 'Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(539, '9', NULL, NULL, NULL, '2019-11-09 18:46:37', '2019-11-09 18:46:37', NULL, 'Chuadhary Mukhtayar Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(540, '10', NULL, NULL, NULL, '2019-11-09 18:46:37', '2019-11-09 18:46:37', NULL, 'Abdul Shakoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(541, '11', NULL, NULL, NULL, '2019-11-09 18:46:38', '2019-11-09 18:46:38', NULL, 'Haji Usman Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(542, '12', NULL, NULL, NULL, '2019-11-09 18:46:38', '2019-11-09 18:46:38', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(543, '13', NULL, NULL, NULL, '2019-11-09 18:46:38', '2019-11-09 18:46:38', NULL, 'Muhammad Nadeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(544, '14', NULL, NULL, NULL, '2019-11-09 18:46:38', '2019-11-09 18:46:38', NULL, 'Syed Soulat Hussain Naqvi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(545, '15', NULL, NULL, NULL, '2019-11-09 18:46:38', '2019-11-09 18:46:38', NULL, 'Maghoob Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(546, '16', NULL, NULL, NULL, '2019-11-09 18:46:39', '2019-11-09 18:46:39', NULL, 'Syed Mehdi Hassan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(547, '17', NULL, NULL, NULL, '2019-11-09 18:46:39', '2019-11-09 18:46:39', NULL, 'Muhammad Mateen Zaman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(548, '18', NULL, NULL, NULL, '2019-11-09 18:46:39', '2019-11-09 18:46:39', NULL, 'Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(549, '19', NULL, NULL, NULL, '2019-11-09 18:46:39', '2019-11-09 18:46:39', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(550, '20', NULL, NULL, NULL, '2019-11-09 18:46:39', '2019-11-09 18:46:39', NULL, 'Masood Ahmad Kahloon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(551, '21', NULL, NULL, NULL, '2019-11-09 18:46:39', '2019-11-09 18:46:39', NULL, 'Abdul Razaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(552, '22', NULL, NULL, NULL, '2019-11-09 18:46:40', '2019-11-09 18:46:40', NULL, 'Muhammad Aslam ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(553, '23', NULL, NULL, NULL, '2019-11-09 18:46:40', '2019-11-09 18:46:40', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(554, '24', NULL, NULL, NULL, '2019-11-09 18:46:40', '2019-11-09 18:46:40', NULL, 'Ch Amin wahla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(555, '25', NULL, NULL, NULL, '2019-11-09 18:46:40', '2019-11-09 18:46:40', NULL, 'Muhammad Iqbal Saleemi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(556, '26', NULL, NULL, NULL, '2019-11-09 18:46:40', '2019-11-09 18:46:40', NULL, 'Sheikh Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(557, '27', NULL, NULL, NULL, '2019-11-09 18:46:40', '2019-11-09 18:46:40', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(558, '28', NULL, NULL, NULL, '2019-11-09 18:46:41', '2019-11-09 18:46:41', NULL, 'Abdul Ghaffar Butt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(559, '29', NULL, NULL, NULL, '2019-11-09 18:46:42', '2019-11-09 18:46:42', NULL, 'Chudhary Ali Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(560, '30', NULL, NULL, NULL, '2019-11-09 18:46:42', '2019-11-09 18:46:42', NULL, 'Abdul Razzaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(561, '31', NULL, NULL, NULL, '2019-11-09 18:46:43', '2019-11-09 18:46:43', NULL, 'Abdul Ghafoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(562, '32', NULL, NULL, NULL, '2019-11-09 18:46:43', '2019-11-09 18:46:43', NULL, 'Haji Muhammad Raza ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(563, '33', NULL, NULL, NULL, '2019-11-09 18:46:43', '2019-11-09 18:46:43', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(564, '34', NULL, NULL, NULL, '2019-11-09 18:46:43', '2019-11-09 18:46:43', NULL, 'Haji Muhammad Raza ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(565, '35', NULL, NULL, NULL, '2019-11-09 18:46:43', '2019-11-09 18:46:43', NULL, 'Khalid Farooq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(566, '36', NULL, NULL, NULL, '2019-11-09 18:46:44', '2019-11-09 18:46:44', NULL, 'Muhammad Ayub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(567, '37', NULL, NULL, NULL, '2019-11-09 18:46:44', '2019-11-09 18:46:44', NULL, 'Amanat Ali Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(568, '38', NULL, NULL, NULL, '2019-11-09 18:46:44', '2019-11-09 18:46:44', NULL, 'Ch Abdul Sattar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(569, '39', NULL, NULL, NULL, '2019-11-09 18:46:44', '2019-11-09 18:46:44', NULL, 'Muhammad Akbar Wahla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(570, '40', NULL, NULL, NULL, '2019-11-09 18:46:44', '2019-11-09 18:46:44', NULL, 'Chuadhary Abdul Ghanni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(571, '41', NULL, NULL, NULL, '2019-11-09 18:46:45', '2019-11-09 18:46:45', NULL, 'Ameer Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(572, '42', NULL, NULL, NULL, '2019-11-09 18:46:45', '2019-11-09 18:46:45', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(573, '43', NULL, NULL, NULL, '2019-11-09 18:46:46', '2019-11-09 18:46:46', NULL, 'Muhammad Din Sheikh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(574, '44', NULL, NULL, NULL, '2019-11-09 18:46:46', '2019-11-09 18:46:46', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(575, '45', NULL, NULL, NULL, '2019-11-09 18:46:46', '2019-11-09 18:46:46', NULL, 'Atta Ur Rehman Maqsood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(576, '46', NULL, NULL, NULL, '2019-11-09 18:46:46', '2019-11-09 18:46:46', NULL, 'Chudhary Khusi Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(577, '47', NULL, NULL, NULL, '2019-11-09 18:46:47', '2019-11-09 18:46:47', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(578, '48', NULL, NULL, NULL, '2019-11-09 18:46:47', '2019-11-09 18:46:47', NULL, 'Ghulam Mustafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(579, '49', NULL, NULL, NULL, '2019-11-09 18:46:47', '2019-11-09 18:46:47', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(580, '50', NULL, NULL, NULL, '2019-11-09 18:46:47', '2019-11-09 18:46:47', NULL, 'Maqsood Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(581, '151', NULL, NULL, NULL, '2019-11-09 18:46:47', '2019-11-09 18:46:47', NULL, 'Chaudhary Muhammad Sabir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(582, '52', NULL, NULL, NULL, '2019-11-09 18:46:48', '2019-11-09 18:46:48', NULL, 'Muhammad Suleman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(583, '53', NULL, NULL, NULL, '2019-11-09 18:46:48', '2019-11-09 18:46:48', NULL, 'Abdul Ghafoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(584, '54', NULL, NULL, NULL, '2019-11-09 18:46:48', '2019-11-09 18:46:48', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(585, '55', NULL, NULL, NULL, '2019-11-09 18:46:48', '2019-11-09 18:46:48', NULL, 'Muhammad Hanif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(586, '56', NULL, NULL, NULL, '2019-11-09 18:46:48', '2019-11-09 18:46:48', NULL, 'Muhammad Sadique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(587, '57', NULL, NULL, NULL, '2019-11-09 18:46:49', '2019-11-09 18:46:49', NULL, 'Rao Irshad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(588, '58', NULL, NULL, NULL, '2019-11-09 18:46:49', '2019-11-09 18:46:49', NULL, 'Chaudhary Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(589, '59', NULL, NULL, NULL, '2019-11-09 18:46:49', '2019-11-09 18:46:49', NULL, 'Nazir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(590, '60', NULL, NULL, NULL, '2019-11-09 18:46:49', '2019-11-09 18:46:49', NULL, 'Rafique Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(591, '61', NULL, NULL, NULL, '2019-11-09 18:46:49', '2019-11-09 18:46:49', NULL, 'Sardar Khalil Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(592, '62', NULL, NULL, NULL, '2019-11-09 18:46:49', '2019-11-09 18:46:49', NULL, 'Rao Nazir Ahmed Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(593, '63', NULL, NULL, NULL, '2019-11-09 18:46:49', '2019-11-09 18:46:49', NULL, 'Muhammad Ramzan Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(594, '64', NULL, NULL, NULL, '2019-11-09 18:46:50', '2019-11-09 18:46:50', NULL, 'Gul Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(595, '65', NULL, NULL, NULL, '2019-11-09 18:46:50', '2019-11-09 18:46:50', NULL, 'Ghulam Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(596, '66', NULL, NULL, NULL, '2019-11-09 18:46:50', '2019-11-09 18:46:50', NULL, 'Hashmat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(597, '67', NULL, NULL, NULL, '2019-11-09 18:46:50', '2019-11-09 18:46:50', NULL, 'Ch Sher Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(598, '68', NULL, NULL, NULL, '2019-11-09 18:46:51', '2019-11-09 18:46:51', NULL, 'Abdul Ghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(599, '69', NULL, NULL, NULL, '2019-11-09 18:46:51', '2019-11-09 18:46:51', NULL, 'Sheikh Muhammad Nawaz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(600, '70', NULL, NULL, NULL, '2019-11-09 18:46:51', '2019-11-09 18:46:51', NULL, 'Wali Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(601, '71', NULL, NULL, NULL, '2019-11-09 18:46:51', '2019-11-09 18:46:51', NULL, 'Bashir Ahmad Ghumman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(602, '72', NULL, NULL, NULL, '2019-11-09 18:46:51', '2019-11-09 18:46:51', NULL, 'Muhammad Arif Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(603, '73', NULL, NULL, NULL, '2019-11-09 18:46:51', '2019-11-09 18:46:51', NULL, 'Muhammad Hanif Ghumman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(604, '74', NULL, NULL, NULL, '2019-11-09 18:46:51', '2019-11-09 18:46:51', NULL, 'Muhammad Arif Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(605, '75', NULL, NULL, NULL, '2019-11-09 18:46:52', '2019-11-09 18:46:52', NULL, 'Rukan Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(606, '76', NULL, NULL, NULL, '2019-11-09 18:46:52', '2019-11-09 18:46:52', NULL, 'Muhammad Bilal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(607, '77', NULL, NULL, NULL, '2019-11-09 18:46:52', '2019-11-09 18:46:52', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(608, '78', NULL, NULL, NULL, '2019-11-09 18:46:52', '2019-11-09 18:46:52', NULL, 'Naseer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(609, '79', NULL, NULL, NULL, '2019-11-09 18:46:52', '2019-11-09 18:46:52', NULL, 'Muhammad Abdur Ruaf Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(610, '80', NULL, NULL, NULL, '2019-11-09 18:46:52', '2019-11-09 18:46:52', NULL, 'Sardar Ghulam Farid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(611, '81', NULL, NULL, NULL, '2019-11-09 18:46:53', '2019-11-09 18:46:53', NULL, 'Kareem Dad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(612, '82', NULL, NULL, NULL, '2019-11-09 18:46:53', '2019-11-09 18:46:53', NULL, 'Muhammad Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(613, '83', NULL, NULL, NULL, '2019-11-09 18:46:53', '2019-11-09 18:46:53', NULL, 'Rasheed Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(614, '84', NULL, NULL, NULL, '2019-11-09 18:46:53', '2019-11-09 18:46:53', NULL, 'Nazir Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(615, '85', NULL, NULL, NULL, '2019-11-09 18:46:53', '2019-11-09 18:46:53', NULL, 'Muhammad Bota', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(616, '86', NULL, NULL, NULL, '2019-11-09 18:46:54', '2019-11-09 18:46:54', NULL, 'Abdul Ghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(617, '87', NULL, NULL, NULL, '2019-11-09 18:46:54', '2019-11-09 18:46:54', NULL, 'Muhammad Asghar Sabir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(618, '88', NULL, NULL, NULL, '2019-11-09 18:46:54', '2019-11-09 18:46:54', NULL, 'Qazi Maqsood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(619, '89', NULL, NULL, NULL, '2019-11-09 18:46:54', '2019-11-09 18:46:54', NULL, 'Muhammad Shafi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(620, '90', NULL, NULL, NULL, '2019-11-09 18:46:55', '2019-11-09 18:46:55', NULL, 'Muhammad Munir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(621, '91', NULL, NULL, NULL, '2019-11-09 18:46:55', '2019-11-09 18:46:55', NULL, 'Nawab Ali Javed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(622, '92', NULL, NULL, NULL, '2019-11-09 18:46:55', '2019-11-09 18:46:55', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(623, '93', NULL, NULL, NULL, '2019-11-09 18:46:56', '2019-11-09 18:46:56', NULL, 'Khan Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(624, '94', NULL, NULL, NULL, '2019-11-09 18:46:56', '2019-11-09 18:46:56', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(625, '95', NULL, NULL, NULL, '2019-11-09 18:46:56', '2019-11-09 18:46:56', NULL, 'Dr.Muhammad Aslam Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(626, '96', NULL, NULL, NULL, '2019-11-09 18:46:56', '2019-11-09 18:46:56', NULL, 'Abul Razzaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(627, '97', NULL, NULL, NULL, '2019-11-09 18:46:56', '2019-11-09 18:46:56', NULL, 'Ghulam Nabi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(628, '98', NULL, NULL, NULL, '2019-11-09 18:46:56', '2019-11-09 18:46:56', NULL, 'Niaz Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(629, '99', NULL, NULL, NULL, '2019-11-09 18:46:57', '2019-11-09 18:46:57', NULL, 'Muhammad Ayub khan saldera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(630, '100', NULL, NULL, NULL, '2019-11-09 18:46:57', '2019-11-09 18:46:57', NULL, 'Akbar Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(631, '101', NULL, NULL, NULL, '2019-11-09 18:46:57', '2019-11-09 18:46:57', NULL, 'Ch.Asmat saeed Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(632, '102', NULL, NULL, NULL, '2019-11-09 18:46:57', '2019-11-09 18:46:57', NULL, 'Asmat Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(633, '103', NULL, NULL, NULL, '2019-11-09 18:46:57', '2019-11-09 18:46:57', NULL, 'Sardar Muhammad Sajjad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(634, '104', NULL, NULL, NULL, '2019-11-09 18:46:57', '2019-11-09 18:46:57', NULL, 'Raja Muhammad Akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(635, '105', NULL, NULL, NULL, '2019-11-09 18:46:58', '2019-11-09 18:46:58', NULL, 'Sardar Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(636, '106', NULL, NULL, NULL, '2019-11-09 18:46:58', '2019-11-09 18:46:58', NULL, 'Rehmat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(637, '107', NULL, NULL, NULL, '2019-11-09 18:46:58', '2019-11-09 18:46:58', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(638, '108', NULL, NULL, NULL, '2019-11-09 18:46:58', '2019-11-09 18:46:58', NULL, 'Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(639, '109', NULL, NULL, NULL, '2019-11-09 18:46:58', '2019-11-09 18:46:58', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(640, '110', NULL, NULL, NULL, '2019-11-09 18:46:58', '2019-11-09 18:46:58', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(641, '111', NULL, NULL, NULL, '2019-11-09 18:46:58', '2019-11-09 18:46:58', NULL, 'Abdul Majeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(642, '112', NULL, NULL, NULL, '2019-11-09 18:46:58', '2019-11-09 18:46:58', NULL, 'Muhammad Aslam Naseem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(643, '113', NULL, NULL, NULL, '2019-11-09 18:46:59', '2019-11-09 18:46:59', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(644, '114', NULL, NULL, NULL, '2019-11-09 18:46:59', '2019-11-09 18:46:59', NULL, 'Muhammad Ishaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(645, '115', NULL, NULL, NULL, '2019-11-09 18:46:59', '2019-11-09 18:46:59', NULL, 'Muhammad Tufail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(646, '116', NULL, NULL, NULL, '2019-11-09 18:46:59', '2019-11-09 18:46:59', NULL, 'Syed Riaz Hussain Rizvi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(647, '117', NULL, NULL, NULL, '2019-11-09 18:46:59', '2019-11-09 18:46:59', NULL, 'Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(648, '118', NULL, NULL, NULL, '2019-11-09 18:46:59', '2019-11-09 18:46:59', NULL, 'Muhammad  Arshad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(649, '119', NULL, NULL, NULL, '2019-11-09 18:46:59', '2019-11-09 18:46:59', NULL, 'Muhammad Yaseen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(650, '120', NULL, NULL, NULL, '2019-11-09 18:46:59', '2019-11-09 18:46:59', NULL, 'Javed Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(651, '121', NULL, NULL, NULL, '2019-11-09 18:47:00', '2019-11-09 18:47:00', NULL, 'Dawood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(652, '122', NULL, NULL, NULL, '2019-11-09 18:47:00', '2019-11-09 18:47:00', NULL, 'Faqir Muhammad Chaudhary', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(653, '123', NULL, NULL, NULL, '2019-11-09 18:47:00', '2019-11-09 18:47:00', NULL, 'Chudhary KhusiMuhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(654, '124', NULL, NULL, NULL, '2019-11-09 18:47:00', '2019-11-09 18:47:00', NULL, 'Muhammad Akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(655, '125', NULL, NULL, NULL, '2019-11-09 18:47:00', '2019-11-09 18:47:00', NULL, 'Chadhary Asmat Saeed Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(656, '126', NULL, NULL, NULL, '2019-11-09 18:47:00', '2019-11-09 18:47:00', NULL, 'Rao Irshad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(657, '127', NULL, NULL, NULL, '2019-11-09 18:47:00', '2019-11-09 18:47:00', NULL, 'Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(658, '128', NULL, NULL, NULL, '2019-11-09 18:47:01', '2019-11-09 18:47:01', NULL, 'Muhammad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(659, '129', NULL, NULL, NULL, '2019-11-09 18:47:01', '2019-11-09 18:47:01', NULL, 'Muhammad Mansha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(660, '130', NULL, NULL, NULL, '2019-11-09 18:47:01', '2019-11-09 18:47:01', NULL, 'Muhammad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(661, '131', NULL, NULL, NULL, '2019-11-09 18:47:01', '2019-11-09 18:47:01', NULL, 'Abdul Raheem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(662, '132', NULL, NULL, NULL, '2019-11-09 18:47:01', '2019-11-09 18:47:01', NULL, 'Ghulam Mustafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(663, '133', NULL, NULL, NULL, '2019-11-09 18:47:01', '2019-11-09 18:47:01', NULL, 'Malik Karamat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(664, '134', NULL, NULL, NULL, '2019-11-09 18:47:02', '2019-11-09 18:47:02', NULL, 'Naseer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(665, '135', NULL, NULL, NULL, '2019-11-09 18:47:02', '2019-11-09 18:47:02', NULL, 'Rao Khan Bahadar Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(666, '136', NULL, NULL, NULL, '2019-11-09 18:47:02', '2019-11-09 18:47:02', NULL, 'Mian Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(667, '137', NULL, NULL, NULL, '2019-11-09 18:47:02', '2019-11-09 18:47:02', NULL, 'Zulfiqar Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(668, '138', NULL, NULL, NULL, '2019-11-09 18:47:02', '2019-11-09 18:47:02', NULL, 'Mukhtar Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(669, '139', NULL, NULL, NULL, '2019-11-09 18:47:02', '2019-11-09 18:47:02', NULL, 'Asgher Ali Sandhu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(670, '140', NULL, NULL, NULL, '2019-11-09 18:47:02', '2019-11-09 18:47:02', NULL, 'Khushi Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(671, '141', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Muhammad Shaffi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(672, '142', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Siraj Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(673, '143', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Raja Ghulam Haider Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(674, '144', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Muhammad Ramzan Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(675, '145', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Konwar Akbar Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(676, '146', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Khalid Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(677, '147', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Abdur Rehman ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(678, '148', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Muhammad Amin Chisti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(679, '149', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Muhammad Tufail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(680, '150', NULL, NULL, NULL, '2019-11-09 18:47:03', '2019-11-09 18:47:03', NULL, 'Chadhary Muhammad Saleem Kahloon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(681, '51', NULL, NULL, NULL, '2019-11-09 18:47:04', '2019-11-09 18:47:04', NULL, 'Atta Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(682, '152', NULL, NULL, NULL, '2019-11-09 18:47:04', '2019-11-09 18:47:04', NULL, 'Muhammad Akbar khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(683, '153', NULL, NULL, NULL, '2019-11-09 18:47:04', '2019-11-09 18:47:04', NULL, 'Muhammad Akbar khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(684, '154', NULL, NULL, NULL, '2019-11-09 18:47:04', '2019-11-09 18:47:04', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(685, '155', NULL, NULL, NULL, '2019-11-09 18:47:05', '2019-11-09 18:47:05', NULL, 'Faqir Muhammad ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(686, '156', NULL, NULL, NULL, '2019-11-09 18:47:05', '2019-11-09 18:47:05', NULL, 'Liaqat Ali Rasal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(687, '157', NULL, NULL, NULL, '2019-11-09 18:47:05', '2019-11-09 18:47:05', NULL, 'Muhammad Khaliq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(688, '158', NULL, NULL, NULL, '2019-11-09 18:47:05', '2019-11-09 18:47:05', NULL, 'Malik Nazar Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(689, '159', NULL, NULL, NULL, '2019-11-09 18:47:05', '2019-11-09 18:47:05', NULL, 'Muhammad Sharif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(690, '160', NULL, NULL, NULL, '2019-11-09 18:47:05', '2019-11-09 18:47:05', NULL, 'Abdul Majid ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(691, '161', NULL, NULL, NULL, '2019-11-09 18:47:05', '2019-11-09 18:47:05', NULL, 'Shaukat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(692, '162', NULL, NULL, NULL, '2019-11-09 18:47:05', '2019-11-09 18:47:05', NULL, 'Muhammad Yousaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(693, '163', NULL, NULL, NULL, '2019-11-09 18:47:06', '2019-11-09 18:47:06', NULL, 'Ch Rehmat Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(694, '164', NULL, NULL, NULL, '2019-11-09 18:47:06', '2019-11-09 18:47:06', NULL, 'Shahid Mehmood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(695, '165', NULL, NULL, NULL, '2019-11-09 18:47:06', '2019-11-09 18:47:06', NULL, 'Khalid Mehmood Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(696, '166', NULL, NULL, NULL, '2019-11-09 18:47:06', '2019-11-09 18:47:06', NULL, 'Ahmad Naseem Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(697, '167', NULL, NULL, NULL, '2019-11-09 18:47:06', '2019-11-09 18:47:06', NULL, 'Nabardar Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(698, '168', NULL, NULL, NULL, '2019-11-09 18:47:06', '2019-11-09 18:47:06', NULL, 'Fateh Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(699, '169', NULL, NULL, NULL, '2019-11-09 18:47:06', '2019-11-09 18:47:06', NULL, 'Dilmeer Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(700, '170', NULL, NULL, NULL, '2019-11-09 18:47:07', '2019-11-09 18:47:07', NULL, 'Muhammad Shafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(701, '171', NULL, NULL, NULL, '2019-11-09 18:47:07', '2019-11-09 18:47:07', NULL, 'Sh.Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(702, '172', NULL, NULL, NULL, '2019-11-09 18:47:07', '2019-11-09 18:47:07', NULL, 'Muhammad ishtiaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(703, '173', NULL, NULL, NULL, '2019-11-09 18:47:07', '2019-11-09 18:47:07', NULL, 'Muhammad Arshad Shah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(704, '174', NULL, NULL, NULL, '2019-11-09 18:47:07', '2019-11-09 18:47:07', NULL, 'Muhammad Rafique Dogar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(705, '175', NULL, NULL, NULL, '2019-11-09 18:47:07', '2019-11-09 18:47:07', NULL, 'Muhammad Habib Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(706, '176', NULL, NULL, NULL, '2019-11-09 18:47:07', '2019-11-09 18:47:07', NULL, 'Nazir Ahmad Mehmood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(707, '177', NULL, NULL, NULL, '2019-11-09 18:47:07', '2019-11-09 18:47:07', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(708, '178', NULL, NULL, NULL, '2019-11-09 18:47:08', '2019-11-09 18:47:08', NULL, 'Taj Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(709, '179', NULL, NULL, NULL, '2019-11-09 18:47:08', '2019-11-09 18:47:08', NULL, 'Haji Muhammad ishaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(710, '180', NULL, NULL, NULL, '2019-11-09 18:47:08', '2019-11-09 18:47:08', NULL, 'Mian Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(711, '181', NULL, NULL, NULL, '2019-11-09 18:47:08', '2019-11-09 18:47:08', NULL, 'Muhammad Hanif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(712, '182', NULL, NULL, NULL, '2019-11-09 18:47:08', '2019-11-09 18:47:08', NULL, 'Malik Muhammad Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(713, '183', NULL, NULL, NULL, '2019-11-09 18:47:08', '2019-11-09 18:47:08', NULL, 'Muhammad Sadique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(714, '184', NULL, NULL, NULL, '2019-11-09 18:47:08', '2019-11-09 18:47:08', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(715, '185', NULL, NULL, NULL, '2019-11-09 18:47:08', '2019-11-09 18:47:08', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(716, '186', NULL, NULL, NULL, '2019-11-09 18:47:09', '2019-11-09 18:47:09', NULL, 'Liaqat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(717, '187', NULL, NULL, NULL, '2019-11-09 18:47:09', '2019-11-09 18:47:09', NULL, 'Muhammad Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(718, '188', NULL, NULL, NULL, '2019-11-09 18:47:09', '2019-11-09 18:47:09', NULL, 'Muhammmad Sardar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(719, '189', NULL, NULL, NULL, '2019-11-09 18:47:09', '2019-11-09 18:47:09', NULL, 'Muhammad Sarwar ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(720, '190', NULL, NULL, NULL, '2019-11-09 18:47:09', '2019-11-09 18:47:09', NULL, 'Ch.Faqir Muhammad Arian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(721, '191', NULL, NULL, NULL, '2019-11-09 18:47:09', '2019-11-09 18:47:09', NULL, 'Muhammad Idrees', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(722, '192', NULL, NULL, NULL, '2019-11-09 18:47:09', '2019-11-09 18:47:09', NULL, 'Ghulam Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(723, '193', NULL, NULL, NULL, '2019-11-09 18:47:09', '2019-11-09 18:47:09', NULL, 'Muhammad Akhtar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(724, '194', NULL, NULL, NULL, '2019-11-09 18:47:10', '2019-11-09 18:47:10', NULL, 'Muhammad Mueez ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(725, '195', NULL, NULL, NULL, '2019-11-09 18:47:10', '2019-11-09 18:47:10', NULL, 'Nazeer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(726, '196', NULL, NULL, NULL, '2019-11-09 18:47:10', '2019-11-09 18:47:10', NULL, 'Muhammad Shafi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(727, '197', NULL, NULL, NULL, '2019-11-09 18:47:10', '2019-11-09 18:47:10', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(728, '198', NULL, NULL, NULL, '2019-11-09 18:47:10', '2019-11-09 18:47:10', NULL, 'Abdul Majeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(729, '199', NULL, NULL, NULL, '2019-11-09 18:47:10', '2019-11-09 18:47:10', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(730, '200', NULL, NULL, NULL, '2019-11-09 18:47:10', '2019-11-09 18:47:10', NULL, 'Muhammad Irshad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(731, '201', NULL, NULL, NULL, '2019-11-09 18:47:11', '2019-11-09 18:47:11', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(732, '202', NULL, NULL, NULL, '2019-11-09 18:47:11', '2019-11-09 18:47:11', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(733, '203', NULL, NULL, NULL, '2019-11-09 18:47:11', '2019-11-09 18:47:11', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(734, '204', NULL, NULL, NULL, '2019-11-09 18:47:11', '2019-11-09 18:47:11', NULL, 'Muhammad Mushtaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(735, '205', NULL, NULL, NULL, '2019-11-09 18:47:11', '2019-11-09 18:47:11', NULL, 'Chuadhary Talib Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(736, '206', NULL, NULL, NULL, '2019-11-09 18:47:11', '2019-11-09 18:47:11', NULL, 'Basheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(737, '207', NULL, NULL, NULL, '2019-11-09 18:47:11', '2019-11-09 18:47:11', NULL, 'Muhammad Ibrahim', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(738, '208', NULL, NULL, NULL, '2019-11-09 18:47:11', '2019-11-09 18:47:11', NULL, 'Ch Khadam Hussain Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(739, '209', NULL, NULL, NULL, '2019-11-09 18:47:12', '2019-11-09 18:47:12', NULL, 'Sardar Muhammad Abid Azeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(740, '210', NULL, NULL, NULL, '2019-11-09 18:47:12', '2019-11-09 18:47:12', NULL, 'Muhammad Yaqoob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(741, '211', NULL, NULL, NULL, '2019-11-09 18:47:12', '2019-11-09 18:47:12', NULL, 'Muhammad Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `members_pay_records` (`id`, `member_id`, `month`, `monthly_payment`, `Unique_id`, `created_at`, `updated_at`, `name`, `father_name`, `mobile_number`, `memberShip_fee`, `registration_fee`, `Aug`, `Sep`, `Oct`, `instrument_No_105000`, `instrument_No_100000`, `instrument_No_55000`, `instrument_No_50000`, `insturment_No_5000`, `insturment_No_32500`, `Instrument_No_Aug`, `Instrument_No_Sep`, `Instrument_No_Oct`, `Status`, `Comment`, `Date`) VALUES
(742, '212', NULL, NULL, NULL, '2019-11-09 18:47:12', '2019-11-09 18:47:12', NULL, 'Tariq Mehmood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(743, '213', NULL, NULL, NULL, '2019-11-09 18:47:12', '2019-11-09 18:47:12', NULL, 'Mian Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(744, '214', NULL, NULL, NULL, '2019-11-09 18:47:12', '2019-11-09 18:47:12', NULL, 'Shafqaat Munir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(745, '215', NULL, NULL, NULL, '2019-11-09 18:47:12', '2019-11-09 18:47:12', NULL, 'Hashim Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(746, '216', NULL, NULL, NULL, '2019-11-09 18:47:12', '2019-11-09 18:47:12', NULL, 'Umer Farooq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(747, '217', NULL, NULL, NULL, '2019-11-09 18:47:13', '2019-11-09 18:47:13', NULL, 'Muhammad Sarwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(748, '218', NULL, NULL, NULL, '2019-11-09 18:47:13', '2019-11-09 18:47:13', NULL, 'Rana Muhammad Sarwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(749, '219', NULL, NULL, NULL, '2019-11-09 18:47:13', '2019-11-09 18:47:13', NULL, 'Riaz Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(750, '220', NULL, NULL, NULL, '2019-11-09 18:47:13', '2019-11-09 18:47:13', NULL, 'Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(751, '221', NULL, NULL, NULL, '2019-11-09 18:47:13', '2019-11-09 18:47:13', NULL, 'Zahoor Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(752, '222', NULL, NULL, NULL, '2019-11-09 18:47:13', '2019-11-09 18:47:13', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(753, '223', NULL, NULL, NULL, '2019-11-09 18:47:13', '2019-11-09 18:47:13', NULL, 'Shafqat Munir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(754, '224', NULL, NULL, NULL, '2019-11-09 18:47:13', '2019-11-09 18:47:13', NULL, 'Muhammad Ashiq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(755, '225', NULL, NULL, NULL, '2019-11-09 18:47:14', '2019-11-09 18:47:14', NULL, 'Muhammad Jamil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(756, '226', NULL, NULL, NULL, '2019-11-09 18:47:14', '2019-11-09 18:47:14', NULL, 'Muhammad Sharif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(757, '227', NULL, NULL, NULL, '2019-11-09 18:47:14', '2019-11-09 18:47:14', NULL, 'Maqbool Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(758, '228', NULL, NULL, NULL, '2019-11-09 18:47:14', '2019-11-09 18:47:14', NULL, 'Inayat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(759, '229', NULL, NULL, NULL, '2019-11-09 18:47:14', '2019-11-09 18:47:14', NULL, 'Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(760, '230', NULL, NULL, NULL, '2019-11-09 18:47:14', '2019-11-09 18:47:14', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(761, '231', NULL, NULL, NULL, '2019-11-09 18:47:14', '2019-11-09 18:47:14', NULL, 'Sheikh Naseer ud din Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(762, '232', NULL, NULL, NULL, '2019-11-09 18:47:15', '2019-11-09 18:47:15', NULL, 'Abdul Razaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(763, '233', NULL, NULL, NULL, '2019-11-09 18:47:15', '2019-11-09 18:47:15', NULL, 'Dr.Zafar Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(764, '234', NULL, NULL, NULL, '2019-11-09 18:47:15', '2019-11-09 18:47:15', NULL, 'Ch.Mukhtar Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(765, '235', NULL, NULL, NULL, '2019-11-09 18:47:15', '2019-11-09 18:47:15', NULL, 'Rao Noor Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(766, '236', NULL, NULL, NULL, '2019-11-09 18:47:15', '2019-11-09 18:47:15', NULL, 'Ch. Noor Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(767, '237', NULL, NULL, NULL, '2019-11-09 18:47:15', '2019-11-09 18:47:15', NULL, 'Ch Irshad Ahmad Arian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(768, '238', NULL, NULL, NULL, '2019-11-09 18:47:15', '2019-11-09 18:47:15', NULL, 'Ch Sardar Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(769, '239', NULL, NULL, NULL, '2019-11-09 18:47:16', '2019-11-09 18:47:16', NULL, 'Malik Muhammad Yousaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(770, '240', NULL, NULL, NULL, '2019-11-09 18:47:16', '2019-11-09 18:47:16', NULL, 'Ch Riaz Ul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(771, '241', NULL, NULL, NULL, '2019-11-09 18:47:16', '2019-11-09 18:47:16', NULL, 'Haji Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(772, '242', NULL, NULL, NULL, '2019-11-09 18:47:16', '2019-11-09 18:47:16', NULL, 'Haji Muhammad Waryam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(773, '243', NULL, NULL, NULL, '2019-11-09 18:47:16', '2019-11-09 18:47:16', NULL, 'Muhammad Saleem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(774, '244', NULL, NULL, NULL, '2019-11-09 18:47:17', '2019-11-09 18:47:17', NULL, 'Muhammad Saeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(775, '245', NULL, NULL, NULL, '2019-11-09 18:47:17', '2019-11-09 18:47:17', NULL, 'Abdul Gaffar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(776, '246', NULL, NULL, NULL, '2019-11-09 18:47:17', '2019-11-09 18:47:17', NULL, 'Muhammad Azeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(777, '247', NULL, NULL, NULL, '2019-11-09 18:47:17', '2019-11-09 18:47:17', NULL, 'Muhammad Sharif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(778, '248', NULL, NULL, NULL, '2019-11-09 18:47:17', '2019-11-09 18:47:17', NULL, 'Abdul Latif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(779, '249', NULL, NULL, NULL, '2019-11-09 18:47:17', '2019-11-09 18:47:17', NULL, 'Ch. Abdul khaliq ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(780, '250', NULL, NULL, NULL, '2019-11-09 18:47:18', '2019-11-09 18:47:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(781, '251', NULL, NULL, NULL, '2019-11-09 18:47:18', '2019-11-09 18:47:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(782, '252', NULL, NULL, NULL, '2019-11-09 18:47:18', '2019-11-09 18:47:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(783, '253', NULL, NULL, NULL, '2019-11-09 18:47:18', '2019-11-09 18:47:18', NULL, 'Muhammad Akram ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(784, '254', NULL, NULL, NULL, '2019-11-09 18:47:18', '2019-11-09 18:47:18', NULL, 'Ghulam Sarwar Ch.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(785, '255', NULL, NULL, NULL, '2019-11-09 18:47:19', '2019-11-09 18:47:19', NULL, 'Adbul Hameed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(786, '256', NULL, NULL, NULL, '2019-11-09 18:47:19', '2019-11-09 18:47:19', NULL, 'Muhammad Abdullah khan Asem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(787, '257', NULL, NULL, NULL, '2019-11-09 18:47:19', '2019-11-09 18:47:19', NULL, 'Muhammad Yaqoob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(788, '258', NULL, NULL, NULL, '2019-11-09 18:47:19', '2019-11-09 18:47:19', NULL, 'Zafar Hussain ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(789, '259', NULL, NULL, NULL, '2019-11-09 18:47:19', '2019-11-09 18:47:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(790, '260', NULL, NULL, NULL, '2019-11-09 18:47:20', '2019-11-09 18:47:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(791, NULL, NULL, NULL, NULL, '2019-11-09 18:47:20', '2019-11-09 18:47:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members_record`
--

CREATE TABLE `members_record` (
  `membership_id` int(5) NOT NULL,
  `name` text NOT NULL,
  `father_name` text NOT NULL,
  `DOB` varchar(255) DEFAULT NULL,
  `Qualification` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MaritaLStatus` enum('Married','Single') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Single',
  `BloodGroup` enum('A+','A-','B+','AB+','AB-','B-','0-','0+') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell_no` varchar(15) NOT NULL,
  `Cnic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Landline` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) DEFAULT 'N/A',
  `registration_fee` varchar(10) DEFAULT NULL,
  `registration_slipNo` varchar(20) DEFAULT NULL,
  `membership_fee` varchar(10) DEFAULT NULL,
  `membership_slipNo` varchar(20) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `TemporaryAddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NameOfOrganization` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Designation` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BuisnessAddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BuisnessMobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `blandline` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Relation` enum('Wife','Son','Daughter','Mother','Father','Other') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Other',
  `SName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SDob` date DEFAULT NULL,
  `SCnic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `payments_status` enum('Paid','Unpaid') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `members_record`
--

INSERT INTO `members_record` (`membership_id`, `name`, `father_name`, `DOB`, `Qualification`, `MaritaLStatus`, `BloodGroup`, `cell_no`, `Cnic`, `email`, `Landline`, `twitter`, `registration_fee`, `registration_slipNo`, `membership_fee`, `membership_slipNo`, `category`, `Address`, `TemporaryAddress`, `NameOfOrganization`, `Designation`, `BuisnessAddress`, `BuisnessMobile`, `blandline`, `Relation`, `SName`, `SDob`, `SCnic`, `image`, `password`, `created_at`, `updated_at`, `status`, `payments_status`) VALUES
(1, 'Hafiz Junaid', 'Ahmad Waseem Sheikh', NULL, NULL, NULL, NULL, '3333022229', '3333022229', 'faizanchohan30@gmail.com', NULL, 'N/A', '5000', '3048025', '100000', '3048047', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-16 07:25:49', 0, ''),
(2, 'Rao Ghaffar Ali', 'Rao Khurshad Ali', '0000-00-00', NULL, 'Single', NULL, '3007592902', NULL, NULL, NULL, 'N/A', '5000', '4579468', '100000', '4609162', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(3, 'Riffat Tahir', 'Chudhary Muhammad Jamil', '0000-00-00', NULL, 'Single', NULL, '3006993431', NULL, NULL, NULL, 'N/A', '5000', '3048030', '100000', '3048046', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(4, 'Khalid Mahmood', 'Muhammad Ud din', '0000-00-00', NULL, 'Single', NULL, '3006999956', NULL, NULL, NULL, 'N/A', '5000', '3048025', '100000', '3048045', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(5, 'Muhammad Aamer Maqsood', 'Maqsood Anwar', '0000-00-00', NULL, 'Single', NULL, '3006999099', NULL, NULL, NULL, 'N/A', '5000', '3048012', '100000', '3048044', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(6, 'Shahbaz Mehmood', 'Muhammad Bukhtiar', '0000-00-00', NULL, 'Single', NULL, '3006999049', NULL, NULL, NULL, 'N/A', '5000', '3048021', '100000', '7222169', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(7, 'Mudassar Marghoob', 'Marghoob Ahmad', '0000-00-00', NULL, 'Single', NULL, '3007592684', NULL, NULL, NULL, 'N/A', '5000', '3048026', '100000', '3048043', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(8, 'Muhammad Nadeem Abbas', 'Muhammad Aslam', '0000-00-00', NULL, 'Single', NULL, '3028732772', NULL, NULL, NULL, 'N/A', '5000', '3048027', '100000', '3048042', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(9, 'Ejaz Ahmad', 'Chuadhary Mukhtayar Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006999136', NULL, NULL, NULL, 'N/A', '5000', '3048023', '100000', '3048041', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(10, 'Awais Shakoor', 'Abdul Shakoor', '0000-00-00', NULL, 'Single', NULL, '3029390262', NULL, NULL, NULL, 'N/A', '5000', '3048024', '100000', '3048051', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(11, 'Irfan Ali Kathiya d.c', 'Haji Usman Khan', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '0', '', '0', '', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(12, 'Atta-ur-Rehman Tahir', 'Abdul Rehman', '0000-00-00', NULL, 'Single', NULL, '3006995499', NULL, NULL, NULL, 'N/A', '5000', '3070693', '100000', '3048010', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(13, 'Muhammad Nabeel', 'Muhammad Nadeem', '0000-00-00', NULL, 'Single', NULL, '3343999611', NULL, NULL, NULL, 'N/A', '5000', '3048200', '100000', '3048040', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(14, 'syed Saim Soulat', 'Syed Soulat Hussain Naqvi', '0000-00-00', NULL, 'Single', NULL, '3024244444', NULL, NULL, NULL, 'N/A', '5000', '3048919', '100000', '3048039', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(15, 'Mazaffar Marghoob', 'Maghoob Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006994106', NULL, NULL, NULL, 'N/A', '5000', '2879176', '100000', '3879257', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(16, 'Sajid Mehadi', 'Syed Mehdi Hassan', '0000-00-00', NULL, 'Single', NULL, '3008448984', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '3048038', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(17, 'Taimoor Azeem', 'Muhammad Mateen Zaman', '0000-00-00', NULL, 'Single', NULL, '3218337959', NULL, NULL, NULL, 'N/A', '5000', '3048431', '100000', '3048037', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(18, 'Abdul Mateen', 'Riaz Ahmad', '0000-00-00', NULL, 'Single', NULL, '3326000666', NULL, NULL, NULL, 'N/A', '5000', '3030062', '100000', '3048036', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(19, 'Ateeq ur Rehman', 'Mushtaq Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006993059', NULL, NULL, NULL, 'N/A', '5000', '2875004', '100000', '30488035', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(20, 'Naeem Ahmad Kalhoon', 'Masood Ahmad Kahloon', '0000-00-00', NULL, 'Single', NULL, '3006999335', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '2879116', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(21, 'Muhammad Rizwan Abid', 'Abdul Razaq', '0000-00-00', NULL, 'Single', NULL, '3336566999', NULL, NULL, NULL, 'N/A', '5000', '4118846', '100000', '4609159', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(22, 'Nazir Ahmad ', 'Muhammad Aslam ', '0000-00-00', NULL, 'Single', NULL, '3218200872', NULL, NULL, NULL, 'N/A', '5000', '4609212', '100000', '4609251', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(23, 'Muhammad Ansar Iqbal', 'Muhammad Iqbal', '0000-00-00', NULL, 'Single', NULL, '3336999030', NULL, NULL, NULL, 'N/A', '5000', '3048017', '100000', '3048034', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(24, 'Bilal Amin Wahla', 'Ch Amin wahla', '0000-00-00', NULL, 'Single', NULL, '3137899425', NULL, NULL, NULL, 'N/A', '5000', '2875152', '100000', '3048033', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(25, 'Qadeer Ahmad', 'Muhammad Iqbal Saleemi', '0000-00-00', NULL, 'Single', NULL, '3006990525', NULL, NULL, NULL, 'N/A', '5000', '2875161', '100000', '3048032', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(26, 'Muhammad Kashif', 'Sheikh Muhammad Aslam', '0000-00-00', NULL, 'Single', NULL, '3006996880', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'bedminton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 127, ''),
(27, 'Muhammad Adnan', 'Muhammad Ramzan', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'bedminton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 127, ''),
(28, 'Rizwan Ghaffar Butt', 'Abdul Ghaffar Butt', '0000-00-00', NULL, 'Single', NULL, '3164014441', NULL, NULL, NULL, 'N/A', '5000', '2875160', '100000', '3048050', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(29, 'Ch Faqir Muhammad', 'Chudhary Ali Muhammad', '0000-00-00', NULL, 'Single', NULL, '3216991929', NULL, NULL, NULL, 'N/A', '5000', '2875005', '100000', '3048031', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(30, 'Hafiz Muhammad Imaran Sajid', 'Abdul Razzaq', '0000-00-00', NULL, 'Single', NULL, '3336999106', NULL, NULL, NULL, 'N/A', '5000', '3894898', '100000', '4609161', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(31, 'Azher Ghafoor ', 'Abdul Ghafoor', '0000-00-00', NULL, 'Single', NULL, '3006991154', NULL, NULL, NULL, 'N/A', '5000', '3048022', '100000', '1848954', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(32, 'Avais Ahmad', 'Haji Muhammad Raza ullah', '0000-00-00', NULL, 'Single', NULL, '3006999157', NULL, NULL, NULL, 'N/A', '5000', '2875007', '100000', '1848953', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(33, 'Ezza Majeed', 'Shahzad Mubeen', '0000-00-00', NULL, 'Single', NULL, '3008690419', NULL, NULL, NULL, 'N/A', '5000', '4542988', '50000', '4542988', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(34, 'Shoib Ahmad', 'Haji Muhammad Raza ullah', '0000-00-00', NULL, 'Single', NULL, '3216990057', NULL, NULL, NULL, 'N/A', '5000', '3048020', '100000', '1848952', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(35, 'Abid Farooq', 'Khalid Farooq', '0000-00-00', NULL, 'Single', NULL, '3006999754', NULL, NULL, NULL, 'N/A', '5000', '2875009', '100000', '1848951', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(36, 'Muhammad Zain AYUB', 'Muhammad Ayub', '0000-00-00', NULL, 'Single', NULL, '3337999040', NULL, NULL, NULL, 'N/A', '5000', '2875130', '100000', '1848958', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(37, 'Muhammad Yaqoob Rana', 'Amanat Ali Khan', '0000-00-00', NULL, 'Single', NULL, '3216991824', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '1848957', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(38, 'Muhammad Imran Staar', 'Ch Abdul Sattar', '0000-00-00', NULL, 'Single', NULL, '3334555864', NULL, NULL, NULL, 'N/A', '5000', '3048379', '100000', '3036246', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(39, 'Muhammad Asif Whala', 'Muhammad Akbar Wahla', '0000-00-00', NULL, 'Single', NULL, '3006999828', NULL, NULL, NULL, 'N/A', '5000', '2875151', '100000', '3036245', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(40, 'Zahid Javed', 'Chuadhary Abdul Ghanni', '0000-00-00', NULL, 'Single', NULL, '3007597800', NULL, NULL, NULL, 'N/A', '5000', '3894899', '100000', '4609160', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(41, 'Majid Ameer', 'Ameer Din', '0000-00-00', NULL, 'Single', NULL, '3007594141', NULL, NULL, NULL, 'N/A', '5000', '4221529', '100000', '4609249', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(42, 'Muhammad Sajjad', 'Muhammad Ashraf', '0000-00-00', NULL, 'Single', NULL, '3017996746', NULL, NULL, NULL, 'N/A', '5000', '2875008', '100000', '3036244', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(43, 'Dr Abdul Majid Hussan Sheikh', 'Muhammad Din Sheikh', '0000-00-00', NULL, 'Single', NULL, '3007590723', NULL, NULL, NULL, 'N/A', '5000', '3048014', '100000', '3036243', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(44, 'Tuba Shahzad', 'Shahzad Mubeen', '0000-00-00', NULL, 'Single', NULL, '3008690419', NULL, NULL, NULL, 'N/A', '5000', '3048061', '50000', '41811719', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(45, 'Zain Ul Abideen', 'Atta Ur Rehman Maqsood', '0000-00-00', NULL, 'Single', NULL, '3337193808', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '3036242', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(46, 'Ch Tanwir Ahmad', 'Chudhary Khusi Muhammad', '0000-00-00', NULL, 'Single', NULL, '3214400622', NULL, NULL, NULL, 'N/A', '5000', '2875010', '100000', '3036241', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(47, 'Shahzad Ashraf', 'Muhammad Ashraf', '0000-00-00', NULL, 'Single', NULL, '3005558045', NULL, NULL, NULL, 'N/A', '5000', '2875012', '100000', '3036240', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(48, 'Shahzad Mustafa', 'Ghulam Mustafa', '0000-00-00', NULL, 'Single', NULL, '3204220000', NULL, NULL, NULL, 'N/A', '5000', '8048016', '100000', '2879403', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(49, 'Muhammad Naveed', 'Muhammad Ashraf', '0000-00-00', NULL, 'Single', NULL, '3006188788', NULL, NULL, NULL, 'N/A', '5000', '3038716', '100000', '3036239', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(50, 'Adeel Maqsood', 'Maqsood Anwar', '0000-00-00', NULL, 'Single', NULL, '3334242400', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609004', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(51, 'Muhammad Amin  2', 'Atta Muhammad', '0000-00-00', NULL, 'Single', NULL, '3006991477', NULL, NULL, NULL, 'N/A', '5000', '4609219', '100000', '4609202+4609026', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(52, 'Sheikh Muhammad Ayoub SB', 'Muhammad Suleman', '0000-00-00', NULL, 'Single', NULL, '3006999040', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(53, 'Dr Farooq Ahmad Sb', 'Abdul Ghafoor', '0000-00-00', NULL, 'Single', NULL, '3006991708', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(54, 'Muhammad Usman 2', 'Muhammad Iqbal', '0000-00-00', NULL, 'Single', NULL, '3002627306', NULL, NULL, NULL, 'N/A', '5000', '41819889', '100000', '4609232', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(55, 'Guhlam Mustafa', 'Muhammad Hanif', '0000-00-00', NULL, 'Single', NULL, '3006999079', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4579093', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(56, 'Dr Zafar Iqbal Mirza', 'Muhammad Sadique', '0000-00-00', NULL, 'Single', NULL, '3336297564', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(57, 'Rao Aziz Ur Rehman', 'Rao Irshad Ahmad', '0000-00-00', NULL, 'Single', NULL, '3008468699', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(58, 'Ch Ijaz Aslam', 'Chaudhary Muhammad Aslam', '0000-00-00', NULL, 'Single', NULL, '3336280628', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(59, 'Ch Ehsan Ullah Cheema', 'Nazir Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006999101', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(60, 'Imran Samiullah', 'Rafique Ahmad', '0000-00-00', NULL, 'Single', NULL, '3326292288', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(61, 'Waqar Ahmad Dar ', 'Sardar Khalil Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006999506', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(62, 'Dr Rao Ghulam Murtaza', 'Rao Nazir Ahmed Khan', '0000-00-00', NULL, 'Single', NULL, '3006989881', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(63, 'Muhammad Sharif Bhatti Sb', 'Muhammad Ramzan Bhatti', '0000-00-00', NULL, 'Single', NULL, '3347196196', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(64, 'Ahmad Yar Mujahid', 'Gul Muhammad', '0000-00-00', NULL, 'Single', NULL, '3007596353', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(65, 'Muhammad Yousaf Kasaliya', 'Ghulam Ali', '0000-00-00', NULL, 'Single', NULL, '3458439684', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(66, 'Shakil Hashmat', 'Hashmat Ali', '0000-00-00', NULL, 'Single', NULL, '3326293949', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4579094', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(67, 'Ch Abdul Majeed', 'Ch Sher Muhammad', '0000-00-00', NULL, 'Single', NULL, '3006991513', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(68, 'Khalid Javid Sb', 'Abdul Ghani', '0000-00-00', NULL, 'Single', NULL, '3217903244', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(69, 'Arslan Nawaz', 'Sheikh Muhammad Nawaz', '0000-00-00', NULL, 'Single', NULL, '3216544888', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(70, 'Muhammad Ghulzar', 'Wali Muhammad', '0000-00-00', NULL, 'Single', NULL, '3007594354', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(71, 'Imaan Ullah Ghumman', 'Bashir Ahmad Ghumman', '0000-00-00', NULL, 'Single', NULL, '3455046786', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(72, 'Usman Arif ', 'Muhammad Arif Awan', '0000-00-00', NULL, 'Single', NULL, '3136882112', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(73, 'Muhammad Shakeel Ghumman', 'Muhammad Hanif Ghumman', '0000-00-00', NULL, 'Single', NULL, '300699915', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(74, 'Imran Arif', 'Muhammad Arif Awan', '0000-00-00', NULL, 'Single', NULL, '3016999777', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(75, 'Muhammad Shafiq', 'Rukan Din', '0000-00-00', NULL, 'Single', NULL, '3336282123', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(76, 'Asif Bilal', 'Muhammad Bilal', '0000-00-00', NULL, 'Single', NULL, '336000102', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(77, 'Shahrukh Khan', 'Shahzad Mubeen', '0000-00-00', NULL, 'Single', NULL, '3028690419', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4579140', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(78, 'Jahanzaib Gujjar', 'Naseer Ahmad', '0000-00-00', NULL, 'Single', NULL, '3336299332', NULL, NULL, NULL, 'N/A', '5000', '4609065', '100000', '2879113', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(79, 'Ahmer Rauf', 'Muhammad Abdur Ruaf Bhatti', '0000-00-00', NULL, 'Single', NULL, '3006995699', NULL, NULL, NULL, 'N/A', '5000', '4221601', '100000', '4609221', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(80, 'Sardar Sajid Ahmad', 'Sardar Ghulam Farid', '0000-00-00', NULL, 'Single', NULL, '3006999222', NULL, NULL, NULL, 'N/A', '5000', '2875144', '100000', '3036238', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(81, 'Tahir kareem khan', 'Kareem Dad Khan', '0000-00-00', NULL, 'Single', NULL, '3017992131', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(82, 'Tahir Mehmood', 'Muhammad Din', '0000-00-00', NULL, 'Single', NULL, '3216991256', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(83, 'Naeem Rashid', 'Rasheed Ahmad', '0000-00-00', NULL, 'Single', NULL, '3336999069', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(84, 'Abid Hussain', 'Nazir Ahmed', '0000-00-00', NULL, 'Single', NULL, '3036294433', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(85, 'Ijaz Ahmad', 'Muhammad Bota', '0000-00-00', NULL, 'Single', NULL, '3007596675', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(86, 'Arshad Pervez', 'Abdul Ghani', '0000-00-00', NULL, 'Single', NULL, '3006998742', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(87, 'Muhammad Abu Bakar Asghar', 'Muhammad Asghar Sabir', '0000-00-00', NULL, 'Single', NULL, '3336999247', NULL, NULL, NULL, 'N/A', '5000', '3048327', '100000', '3036237', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(88, 'Muhammad usman ghani', 'Qazi Maqsood Ahmad', '0000-00-00', NULL, 'Single', NULL, '3007863001', NULL, NULL, NULL, 'N/A', '5000', '8048500', '100000', '6409128', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(89, 'Mian Arshad Javid', 'Muhammad Shafi', '0000-00-00', NULL, 'Single', NULL, '3006992023', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(90, 'Shakeel Sajid', 'Muhammad Munir Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006994129', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(91, 'Nasir Javed', 'Nawab Ali Javed', '0000-00-00', NULL, 'Single', NULL, '3336282662', NULL, NULL, NULL, 'N/A', '5000', '2875423', '100000', '3036236', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(92, 'Muhammad Samer Iqbal', 'Muhammad Iqbal', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '3048018', '100000', '3036235', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(93, 'Khusi Muhammad', 'Khan Muhammad', '0000-00-00', NULL, 'Single', NULL, '3007879779', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609158', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(94, 'Muhammad Saleem', 'Bashir Ahmad', '0000-00-00', NULL, 'Single', NULL, '3005223319', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(95, 'Dr Sultan Waqas Aslam Awan', 'Dr.Muhammad Aslam Awan', '0000-00-00', NULL, 'Single', NULL, '3336282005', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(96, 'Bilal Razzaq', 'Abul Razzaq', '0000-00-00', NULL, 'Single', NULL, '3226828382', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(97, 'Zafar Iqbal Tahir', 'Ghulam Nabi', '0000-00-00', NULL, 'Single', NULL, '3066991223', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(98, 'Muhammad Irshaad', 'Niaz Muhammad', '0000-00-00', NULL, 'Single', NULL, '3146000999', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(99, 'Imraan khan Saldera', 'Muhammad Ayub khan saldera', '0000-00-00', NULL, 'Single', NULL, '3334190064', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '2879107', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(100, 'Majeed Akbar', 'Akbar Ali', '0000-00-00', NULL, 'Single', NULL, '3006970041', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4579139', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(101, 'Ali Usman Bajwa', 'Ch.Asmat saeed Bajwa', '0000-00-00', NULL, 'Single', NULL, '3216666066', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4579223', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(102, 'Pervez Akhter', 'Asmat Ullah', '0000-00-00', NULL, 'Single', NULL, '3006991947', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(103, 'Sardar Asif Sajjad', 'Sardar Muhammad Sajjad', '0000-00-00', NULL, 'Single', NULL, '3214451747', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(104, 'Raja Ehsan Ullah', 'Raja Muhammad Akram', '0000-00-00', NULL, 'Single', NULL, '3316999483', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '3036234', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(105, 'Muhammad Talha', 'Sardar Riaz Ahmad', '0000-00-00', NULL, 'Single', NULL, '3337590344', NULL, NULL, NULL, 'N/A', '5000', '2875006', '100000', '3036232', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(106, 'Muhammad Azam', 'Rehmat Ali', '0000-00-00', NULL, 'Single', NULL, '3006992500', NULL, NULL, NULL, 'N/A', '5000', '4609135', '100000', '2879253', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(107, 'Muhammad Irfan', 'Muhammad Amin', '0000-00-00', NULL, 'Single', NULL, '3006992815', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '3036230', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(108, 'Muhammad Nadeem', 'Muhammad Rafique', '0000-00-00', NULL, 'Single', NULL, '3006879419', NULL, NULL, NULL, 'N/A', '5000', '3048029', '100000', '3036228', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(109, 'Muhammad Amir Shakeel', 'Muhammad Ramzan', '0000-00-00', NULL, 'Single', NULL, '3006999993', NULL, NULL, NULL, 'N/A', '5000', '3048019', '100000', '3036226', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(110, 'Maqsood Ahmad', 'Muhammad Amin', '0000-00-00', NULL, 'Single', NULL, '3216889440', NULL, NULL, NULL, 'N/A', '5000', '3048447', '100000', '3048048', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(111, 'Shahzaad Mubeen', 'Abdul Majeed', '0000-00-00', NULL, 'Single', NULL, '3008690419', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4579141', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(112, 'Ijaz Aslam', 'Muhammad Aslam Naseem', '0000-00-00', NULL, 'Single', NULL, '3006999495', NULL, NULL, NULL, 'N/A', '5000', '3036178', '100000', '3048057', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(113, 'Sohail Amin', 'Muhammad Amin', '0000-00-00', NULL, 'Single', NULL, '3006999076', NULL, NULL, NULL, 'N/A', '5000', '3036030', '100000', '3048059', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(114, 'Ghulam Rasool', 'Muhammad Ishaq', '0000-00-00', NULL, 'Single', NULL, '3016582467', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '2879114', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(115, 'Rana Ghulam Murtaza', 'Muhammad Tufail', '0000-00-00', NULL, 'Single', NULL, '3006999181', NULL, NULL, NULL, 'N/A', '5000', '3036176', '100000', '3048058', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(116, 'Syed Ali Asad Rizvi', 'Syed Riaz Hussain Rizvi', '0000-00-00', NULL, 'Single', NULL, '3006995800', NULL, NULL, NULL, 'N/A', '5000', '3036177', '100000', '30448056', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(117, 'Muhammad Uzair salar', 'Zaheer Ahmad', '0000-00-00', NULL, 'Single', NULL, '3336999511', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609130', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(118, 'Muhammad Ahsan arshad', 'Muhammad  Arshad', '0000-00-00', NULL, 'Single', NULL, '3014594447', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '3048055', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(119, 'Masoom Ali', 'Muhammad Yaseen', '0000-00-00', NULL, 'Single', NULL, '3029390040', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(120, 'Farukh Javed', 'Javed Iqbal', '0000-00-00', NULL, 'Single', NULL, '3024937397', NULL, NULL, NULL, 'N/A', '5000', '', '30000', '', 'bedminton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 127, ''),
(121, 'Muhammad Ahtsham Dawood', 'Dawood Ahmad', '0000-00-00', NULL, 'Single', NULL, '3004444056', NULL, NULL, NULL, 'N/A', '5000', '2879268', '50000', '4609002', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(122, 'Dr Sohail Aziz', 'Faqir Muhammad Chaudhary', '0000-00-00', NULL, 'Single', NULL, '3006993205', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '3036179', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(123, 'Dr Sajjad Ahmad', 'Chudhary KhusiMuhammad', '0000-00-00', NULL, 'Single', NULL, '3336295577', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(124, 'Tariq Mahmood', 'Muhammad Akram', '0000-00-00', NULL, 'Single', NULL, '3006997264', NULL, NULL, NULL, 'N/A', '5000', '2579252', '100000', '4609133', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(125, 'Husnain Saeed Bajwa', 'Chadhary Asmat Saeed Bajwa', '0000-00-00', NULL, 'Single', NULL, '3006991800', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '2879265', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(126, 'Faisal Rehman', 'Rao Irshad Ahmad', '0000-00-00', NULL, 'Single', NULL, '3347197197', NULL, NULL, NULL, 'N/A', '5000', '1848935', '100000', '2879267', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(127, 'Imran Ahmad Bhatti', 'Muhammad Rafique', '0000-00-00', NULL, 'Single', NULL, '3027598451', NULL, NULL, NULL, 'N/A', '5000', '', '10000', '', 'officer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(128, 'Waseem Ahmad', 'Muhammad Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006992717', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(129, 'Zafar ali Chaudhary', 'Muhammad Mansha', '0000-00-00', NULL, 'Single', NULL, '3336297615', NULL, NULL, NULL, 'N/A', '5000', '3036366', '100000', '2879263', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(130, 'Zulafiqar Ahmaad', 'Muhammad Khan', '0000-00-00', NULL, 'Single', NULL, '3216999515', NULL, NULL, NULL, 'N/A', '5000', '3036368', '100000', '2879264', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(131, 'Abdul Rauf', 'Abdul Raheem', '0000-00-00', NULL, 'Single', NULL, '3216934533', NULL, NULL, NULL, 'N/A', '5000', '2879177', '100000', '2879262', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(132, 'Muhammad yasir', 'Ghulam Mustafa', '0000-00-00', NULL, 'Single', NULL, '3339995128', NULL, NULL, NULL, 'N/A', '5000', '3048499', '100000', '2879261', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(133, 'Ali Haider', 'Malik Karamat', '0000-00-00', NULL, 'Single', NULL, '3017999420', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '2879260', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(134, 'Muhammad Azeem', 'Naseer Ahmad', '0000-00-00', NULL, 'Single', NULL, '3000333883', NULL, NULL, NULL, 'N/A', '5000', '3048493', '100000', '3048053', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(135, 'Rao Hassan Akhter', 'Rao Khan Bahadar Khan', '0000-00-00', NULL, 'Single', NULL, '3000768652', NULL, NULL, NULL, 'N/A', '5000', '4542674', '100000', '2879254', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(136, 'Mian Muhammad Haider', 'Mian Muhammad Rafique', '0000-00-00', NULL, 'Single', NULL, '3006991869', NULL, NULL, NULL, 'N/A', '5000', '2879178', '100000', '2879258', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(137, 'waqar Ahmed', 'Zulfiqar Ahmed', '0000-00-00', NULL, 'Single', NULL, '3336991405', NULL, NULL, NULL, 'N/A', '5000', '4609110', '100000', '4609131', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(138, 'Shoukat ali', 'Mukhtar Ahmed', '0000-00-00', NULL, 'Single', NULL, '3006999904', NULL, NULL, NULL, 'N/A', '5000', '2875002', '100000', '2879259', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(139, 'Muhammad Nadeem Anjum', 'Asgher Ali Sandhu', '0000-00-00', NULL, 'Single', NULL, '3006992013', NULL, NULL, NULL, 'N/A', '5000', '4579086', '100000', '4609144', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(140, 'Muhammad Ashiq', 'Khushi Muhammad', '0000-00-00', NULL, 'Single', NULL, '3006999055', NULL, NULL, NULL, 'N/A', '5000', '3048498', '50000', '2879131', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(141, 'Muhammad Mumtaz Shafi', 'Muhammad Shaffi', '0000-00-00', NULL, 'Single', NULL, '3006992086', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(142, 'Abdul Ghaffar But', 'Siraj Din', '0000-00-00', NULL, 'Single', NULL, '3347195562', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4181718', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(143, 'Raja Nisar Ahmad Khan', 'Raja Ghulam Haider Khan', '0000-00-00', NULL, 'Single', NULL, '3006992040', NULL, NULL, NULL, 'N/A', '5000', '2879123', '100000', '2879121', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(144, 'Shahbaz Hussain Bhatti', 'Muhammad Ramzan Bhatti', '0000-00-00', NULL, 'Single', NULL, '3006999037', NULL, NULL, NULL, 'N/A', '5000', '4579097', '50000', '4579203', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(145, 'Rao Faisal Akbar', 'Konwar Akbar Ali', '0000-00-00', NULL, 'Single', NULL, '3336999466', NULL, NULL, NULL, 'N/A', '5000', '4609156', '100000', '4609163', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(146, 'Muhammad Aehtasham Khalid', 'Khalid Hussain', '0000-00-00', NULL, 'Single', NULL, '3336898050', NULL, NULL, NULL, 'N/A', '5000', '4609157', '100000', '4609155', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(147, 'Muhammad Iqbal ', 'Abdur Rehman ', '0000-00-00', NULL, 'Single', NULL, '3006999612', NULL, NULL, NULL, 'N/A', '5000', '4609067', '50000', '2879111', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(148, 'Saeed ud  din Chisti', 'Muhammad Amin Chisti', '0000-00-00', NULL, 'Single', NULL, '3336288927', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609018', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(149, 'Awais Tufail', 'Muhammad Tufail', '0000-00-00', NULL, 'Single', NULL, '3009200561', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609019', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(150, 'Farhan Saleem Kahloon', 'Chadhary Muhammad Saleem Kahloon', '0000-00-00', NULL, 'Single', NULL, '3216005050', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '2879140', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(151, 'Muhammad Naeem Sabir', 'Chaudhary Muhammad Sabir', '0000-00-00', NULL, 'Single', NULL, '3006992878', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(152, 'Muhammad Akmal Khan Rana', 'Muhammad Akbar khan', '0000-00-00', NULL, 'Single', NULL, '3006994019', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609124', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(153, 'Muhammad Ajmal Khan Rana', 'Muhammad Akbar khan', '0000-00-00', NULL, 'Single', NULL, '3006994545', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609118', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(154, 'Muhammad Arshad Shah', 'Muhammad Amin', '0000-00-00', NULL, 'Single', NULL, '3017994546', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(155, 'Karamat Ali Shakeel', 'Faqir Muhammad ', '0000-00-00', NULL, 'Single', NULL, '3336268614', NULL, NULL, NULL, 'N/A', '5000', '4609006', '100000', '4609014', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(156, 'Usman Ali Rasal', 'Liaqat Ali Rasal', '0000-00-00', NULL, 'Single', NULL, '3333304048', NULL, NULL, NULL, 'N/A', '5000', '3036441', '100000', '3048054', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(157, 'Dr.Khalid Mahmood', 'Muhammad Khaliq', '0000-00-00', NULL, 'Single', NULL, '3006995408', NULL, NULL, NULL, 'N/A', '5000', '4181715', '100000', '4609196', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(158, 'KarAmat Hussain', 'Malik Nazar Hussain', '0000-00-00', NULL, 'Single', NULL, '3017999520', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(159, 'Khalid Saeed', 'Muhammad Sharif', '0000-00-00', NULL, 'Single', NULL, '3007720842', NULL, NULL, NULL, 'N/A', '5000', '4542620', '100000', '300117', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(160, 'Shahbab Majid ', 'Abdul Majid ', '0000-00-00', NULL, 'Single', NULL, '3236817990', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '21977866', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(161, 'Muhammad Akram', 'Shaukat Ali', '0000-00-00', NULL, 'Single', NULL, '3015305555', NULL, NULL, NULL, 'N/A', '5000', '4181606', '100000', '4609145', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(162, 'Muhammad Rauf', 'Muhammad Yousaf', '0000-00-00', NULL, 'Single', NULL, '3226156402', NULL, NULL, NULL, 'N/A', '5000', '4181774', '100000', '4609147', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(163, 'Najaf Farooq ', 'Ch Rehmat Ullah', '0000-00-00', NULL, 'Single', NULL, '3007590744', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609143', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(164, 'Ahtasham Shahid', 'Shahid Mehmood Ahmad', '0000-00-00', NULL, 'Single', NULL, '331777174', NULL, NULL, NULL, 'N/A', '5000', '3894742', '100000', '4609146', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(165, 'Muhammad Umair Khalid', 'Khalid Mehmood Khan', '0000-00-00', NULL, 'Single', NULL, '3006999659', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609148', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(166, 'Haris Naseem', 'Ahmad Naseem Khan', '0000-00-00', NULL, 'Single', NULL, '3006999499', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609149', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(167, 'Muhammad Usman Ahmad', 'Nabardar Riaz Ahmad', '0000-00-00', NULL, 'Single', NULL, '3007590344', NULL, NULL, NULL, 'N/A', '5000', '3036452', '100000', '3036231', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(168, 'Pro Rehmat Ali Sidhu', 'Fateh Muhammad', '0000-00-00', NULL, 'Single', NULL, '3017998389', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4181898', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(169, 'Allah Dita', 'Dilmeer Ahmed', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '4181892', '100000', '4579202', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(170, 'Khuram Shafiq', 'Muhammad Shafique', '0000-00-00', NULL, 'Single', NULL, '3006992200', NULL, NULL, NULL, 'N/A', '5000', '4221602', '100000', '4609222', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(171, 'Sh Abrar Ahmad', 'Sh.Abdul Haq', '0000-00-00', NULL, 'Single', NULL, '3336297457', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(172, 'Muhammad ishaq', 'Muhammad ishtiaq', '0000-00-00', NULL, 'Single', NULL, '3347201334', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4181613', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(173, 'Hussain Haider', 'Muhammad Arshad Shah', '0000-00-00', NULL, 'Single', NULL, '33471911008', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4181605', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(174, 'sardar Zahoor Ahmad Dogar', 'Muhammad Rafique Dogar', '0000-00-00', NULL, 'Single', NULL, '3006999445', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4181717', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(175, 'Amer Habib', 'Muhammad Habib Ullah', '0000-00-00', NULL, 'Single', NULL, '3347786002', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4181611', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(176, 'Rao Zaheer Ahmad', 'Nazir Ahmad Mehmood', '0000-00-00', NULL, 'Single', NULL, '3366997778', NULL, NULL, NULL, 'N/A', '5000', '4181539', '100000', '4181721', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(177, 'Waqas Ahmad', 'Mushtaq Ahmad', '0000-00-00', NULL, 'Single', NULL, '3333444913', NULL, NULL, NULL, 'N/A', '5000', '4579300', '100000', '4181716', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(178, 'Sheikh Muhammad Faheem', 'Taj Din', '0000-00-00', NULL, 'Single', NULL, '33347207077', NULL, NULL, NULL, 'N/A', '5000', '4181538', '100000', '4181708', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(179, 'Muhammad Abbas', 'Haji Muhammad ishaq', '0000-00-00', NULL, 'Single', NULL, '3006993296', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(180, 'Umair salar', 'Mian Zaheer Ahmad', '0000-00-00', NULL, 'Single', NULL, '3334048181', NULL, NULL, NULL, 'N/A', '5000', '4609225', '100000', '4609224', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(181, 'Mian Iftakhar Sabar', 'Muhammad Hanif', '0000-00-00', NULL, 'Single', NULL, '3006991373', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609023', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(182, 'Ahmad waqas', 'Malik Muhammad Anwar', '0000-00-00', NULL, 'Single', NULL, '3336282672', NULL, NULL, NULL, 'N/A', '5000', '4392800', '100000', '4609231', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(183, 'Muhammad Shahid Ali', 'Muhammad Sadique', '0000-00-00', NULL, 'Single', NULL, '3017933636', NULL, NULL, NULL, 'N/A', '5000', '4609191', '100000', '4609190', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(184, 'Adnan Ashraf', 'Muhammad Ashraf', '0000-00-00', NULL, 'Single', NULL, '3026999173', NULL, NULL, NULL, 'N/A', '5000', '4609125', '100000', '4609193', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '');
INSERT INTO `members_record` (`membership_id`, `name`, `father_name`, `DOB`, `Qualification`, `MaritaLStatus`, `BloodGroup`, `cell_no`, `Cnic`, `email`, `Landline`, `twitter`, `registration_fee`, `registration_slipNo`, `membership_fee`, `membership_slipNo`, `category`, `Address`, `TemporaryAddress`, `NameOfOrganization`, `Designation`, `BuisnessAddress`, `BuisnessMobile`, `blandline`, `Relation`, `SName`, `SDob`, `SCnic`, `image`, `password`, `created_at`, `updated_at`, `status`, `payments_status`) VALUES
(185, 'Qaiser Waseem', 'Abdul Rehman', '0000-00-00', NULL, 'Single', NULL, '3006999085', NULL, NULL, NULL, 'N/A', '5000', '4609126', '100000', '4609192', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(186, 'Muhammad sadiq', 'Liaqat Ali', '0000-00-00', NULL, 'Single', NULL, '3216999956', NULL, NULL, NULL, 'N/A', '5000', '4609153', '100000', '4609188', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(187, 'Fozia Anwar', 'Muhammad Anwar', '0000-00-00', NULL, 'Single', NULL, '3338006109', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609152+4609032', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(188, 'Abdur Razaq', 'Muhammmad Sardar', '0000-00-00', NULL, 'Single', NULL, '3039389487', NULL, NULL, NULL, 'N/A', '5000', '4609154', '100000', '4609189', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(189, 'muhammad ijaz sarwar ', 'Muhammad Sarwar ', '0000-00-00', NULL, 'Single', NULL, '333828367', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(190, 'Ch Usama ahmad Arian', 'Ch.Faqir Muhammad Arian', '0000-00-00', NULL, 'Single', NULL, '3005257103', NULL, NULL, NULL, 'N/A', '5000', '4221510', '100000', '', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(191, 'Usman Idrees ', 'Muhammad Idrees', '0000-00-00', NULL, 'Single', NULL, '33477963031', NULL, NULL, NULL, 'N/A', '5000', '0', '100000', '4309237', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(192, 'Shakeel Ahmad', 'Ghulam Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006993456', NULL, NULL, NULL, 'N/A', '5000', '2879205', '100000', '2879255', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(193, 'Muhammad Nasir Mehmood', 'Muhammad Akhtar', '0000-00-00', NULL, 'Single', NULL, '3004572886', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609235', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(194, 'Hassan Mueez', 'Muhammad Mueez ', '0000-00-00', NULL, 'Single', NULL, '3006990727', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609208', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(195, 'Qasir Nazeer', 'Nazeer Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006993086', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609216', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(196, 'Pervaiz Ahmad Bhatti', 'Muhammad Shafi', '0000-00-00', NULL, 'Single', NULL, '3213300321', NULL, NULL, NULL, 'N/A', '5000', '4181946', '100000', '4609255', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(197, 'Qaiser Pervaiz', 'Abdul Rehman', '0000-00-00', NULL, 'Single', NULL, '3215991111', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4221606', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(198, 'Muhammad Ashraf', 'Abdul Majeed', '0000-00-00', NULL, 'Single', NULL, '3006999346', NULL, NULL, NULL, 'N/A', '5000', '4221603', '100000', '4609254', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(199, 'Muhammad Zafar ali', 'Bashir Ahmad', '0000-00-00', NULL, 'Single', NULL, '3348989089', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '3894987', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(200, 'Muhammad shazaf farooq', 'Muhammad Irshad Khan', '0000-00-00', NULL, 'Single', NULL, '3017994050', NULL, NULL, NULL, 'N/A', '5000', '4609181', '100000', '4609182', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(201, 'Muhammad Ishaq', 'Mushtaq Ahmad', '0000-00-00', NULL, 'Single', NULL, '3336999399', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609008', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(202, 'khalid Hussain', 'Muhammad Ramzan', '0000-00-00', NULL, 'Single', NULL, '3006992210', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609009', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(203, 'Muhammad Khalid', 'Bashir Ahmad', '0000-00-00', NULL, 'Single', NULL, '3007592347', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609011', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(204, 'Khalid Mahmood', 'Muhammad Mushtaq', '0000-00-00', NULL, 'Single', NULL, '3006992063', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609012', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(205, 'Muhammad Latif', 'Chuadhary Talib Hussain', '0000-00-00', NULL, 'Single', NULL, '3007879558', NULL, NULL, NULL, 'N/A', '5000', '4609227', '100000', '4609226', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(206, 'Ghazanfar ali', 'Basheer Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006999589', NULL, NULL, NULL, 'N/A', '5000', '4392802', '50000', '4609230', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(207, 'Muhammad Asif Ibrahim', 'Muhammad Ibrahim', '0000-00-00', NULL, 'Single', NULL, '3216990222', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609187', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(208, 'Tariq mahmood Bajwa', 'Ch Khadam Hussain Bajwa', '0000-00-00', NULL, 'Single', NULL, '303444313', NULL, NULL, NULL, 'N/A', '5000', '4181914', '50000', '4579255', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(209, 'Muhammad ali Fiaz', 'Sardar Muhammad Abid Azeem', '0000-00-00', NULL, 'Single', NULL, '3347208150', NULL, NULL, NULL, 'N/A', '5000', '4181730', '100000', '4609197', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(210, 'Shahid Javed', 'Muhammad Yaqoob', '0000-00-00', NULL, 'Single', NULL, '3007594157', NULL, NULL, NULL, 'N/A', '5000', '4609141', '100000', '4609142', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(211, 'Muhammad Amin', 'Muhammad Din', '0000-00-00', NULL, 'Single', NULL, '3006991089', NULL, NULL, NULL, 'N/A', '5000', '4609168', '100000', '4609165', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(212, 'Muhammad Bilawal Hameed', 'Tariq Mehmood', '0000-00-00', NULL, 'Single', NULL, '300699229', NULL, NULL, NULL, 'N/A', '5000', '4609108', '100000', '4609016', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(213, 'Muhammad Imran', 'Mian Khan', '0000-00-00', NULL, 'Single', NULL, '3227500120', NULL, NULL, NULL, 'N/A', '5000', '4609007', '100000', '4609015', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(214, 'Hassan Raza', 'Shafqaat Munir', '0000-00-00', NULL, 'Single', NULL, '3216996997', NULL, NULL, NULL, 'N/A', '5000', '4579096', '100000', '4609172', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(215, 'Asim Ali', 'Hashim Ali', '0000-00-00', NULL, 'Single', NULL, '3336293080', NULL, NULL, NULL, 'N/A', '5000', '4181680', '100000', '4609171', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(216, 'Bilal Farooq', 'Umer Farooq', '0000-00-00', NULL, 'Single', NULL, '3017682050', NULL, NULL, NULL, 'N/A', '5000', '4181681', '100000', '4609136+4609174', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(217, 'Wajid Sarwar', 'Muhammad Sarwar', '0000-00-00', NULL, 'Single', NULL, '3084258848', NULL, NULL, NULL, 'N/A', '5000', '4181561', '100000', '4609170', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(218, 'Rana Shahid Sarwar', 'Rana Muhammad Sarwar', '0000-00-00', NULL, 'Single', NULL, '3006998913', NULL, NULL, NULL, 'N/A', '5000', '4579326', '100000', '4609137+4609169', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(219, 'Asif Nadeem', 'Riaz Muhammad', '0000-00-00', NULL, 'Single', NULL, '3004985101', NULL, NULL, NULL, 'N/A', '5000', '4181796', '100000', '4609186', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(220, 'Muhammad afzal', 'Abdul Haq', '0000-00-00', NULL, 'Single', NULL, '3216990416', NULL, NULL, NULL, 'N/A', '5000', '469177', '100000', '4609173', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(221, 'Muhammad Nosher khan Anjum', 'Zahoor Ahmad', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609138', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(222, 'Visha Shahzad', 'Shahzad Mubeen', '0000-00-00', NULL, 'Single', NULL, '3008690419', NULL, NULL, NULL, 'N/A', '5000', '4609132', '50000', '4181720', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(223, 'Sajjad Haider', 'Shafqat Munir', '0000-00-00', NULL, 'Single', NULL, '3006996997', NULL, NULL, NULL, 'N/A', '5000', '4579095', '50000', '4609173', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(224, 'Muhammad Arshad', 'Muhammad Ashiq', '0000-00-00', NULL, 'Single', NULL, '3226824969', NULL, NULL, NULL, 'N/A', '5000', '4579480', '100000', '15538900', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(225, 'Muhammad Nadeem', 'Muhammad Jamil', '0000-00-00', NULL, 'Single', NULL, '3003309455', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(226, 'Sajid Sharif ', 'Muhammad Sharif ', '0000-00-00', NULL, 'Single', NULL, '3360043500', NULL, NULL, NULL, 'N/A', '5000', '4609258', '100000', '4609257', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(227, 'Sikandar Hayat', 'Maqbool Ahmad', '0000-00-00', NULL, 'Single', NULL, '3336281845', NULL, NULL, NULL, 'N/A', '5000', '4181988', '100000', '4609233', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(228, 'Iyaz Haider Khan', 'Inayat Ali', '0000-00-00', NULL, 'Single', NULL, '3336283656', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(229, 'izhar Ahmad', 'Abdul Haq', '0000-00-00', NULL, 'Single', NULL, '300972166', NULL, NULL, NULL, 'N/A', '5000', '4609185', '100000', '4609184', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(230, 'Ali Raza', 'Muhammad Ramzan', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'bedminton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(231, 'Sheikh Noor Ul Amin', 'Sheikh Naseer ud din Ahmad', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'bedminton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(232, 'Zain ul Abdin *Bedminton', 'Abdul Razaq', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '', '25000', '', 'bedminton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(233, 'Hamayun zafar', 'Dr.Zafar Iqbal', '0000-00-00', NULL, 'Single', NULL, '3336998086', NULL, NULL, NULL, 'N/A', '5000', '3709187', '50000', '2879118', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(234, 'Ch.Bakhtayar Ahmed Gujjar', 'Ch.Mukhtar Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006991405', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609017', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(235, 'Muhammad Aslam Khan', 'Rao Noor Muhammad', '0000-00-00', NULL, 'Single', NULL, '3336287522', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '3894719', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(236, 'Muhammad Sageer', 'Ch. Noor Muhammad', '0000-00-00', NULL, 'Single', NULL, '3006991399', NULL, NULL, NULL, 'N/A', '5000', '', '50000', '4609109', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(237, 'Muhammad Imran irshad', 'Ch Irshad Ahmad Arian', '0000-00-00', NULL, 'Single', NULL, '3347202071', NULL, NULL, NULL, 'N/A', '5000', '4609021', '100000', '4609024', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(238, 'Ch.Irshad Ahmad Arain', 'Ch Sardar Muhammad', '0000-00-00', NULL, 'Single', NULL, '3336885051', NULL, NULL, NULL, 'N/A', '5000', '4609020', '100000', '4609022', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(239, 'Muhammad Furqan Yousaf', 'Malik Muhammad Yousaf', '0000-00-00', NULL, 'Single', NULL, '3006991163', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609167', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(240, 'Muhammad Khuram Riaz', 'Ch Riaz Ul Haq', '0000-00-00', NULL, 'Single', NULL, '3006997692', NULL, NULL, NULL, 'N/A', '5000', '4609166', '100000', '4609175', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(241, 'Ch.Shahid Hussain', 'Haji Zaheer Ahmad', '0000-00-00', NULL, 'Single', NULL, '3006993811', NULL, NULL, NULL, 'N/A', '5000', '4181782', '50000', '4609244', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(242, 'Talib Hussain', 'Haji Muhammad Waryam', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '4609243', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(243, 'Muhammad Shahzad', 'Muhammad Saleem', '0000-00-00', NULL, 'Single', NULL, '3006992400', NULL, NULL, NULL, 'N/A', '5000', '4181956', '100000', '4609246', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(244, 'Muhammad Asif Saeed', 'Muhammad Saeed', '0000-00-00', NULL, 'Single', NULL, '300987053', NULL, NULL, NULL, 'N/A', '5000', '4181781', '100000', '4609245', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(245, 'Amir Gaffar', 'Abdul Gaffar', '0000-00-00', NULL, 'Single', NULL, '3006993416', NULL, NULL, NULL, 'N/A', '5000', '4181832', '100000', '4609238', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(246, 'Abdul gaffar', 'Muhammad Azeem', '0000-00-00', NULL, 'Single', NULL, '3006992166', NULL, NULL, NULL, 'N/A', '5000', '4609066', '100000', '4609256', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(247, 'Tayyab Raza', 'Muhammad Sharif ', '0000-00-00', NULL, 'Single', NULL, '3226800746', NULL, NULL, NULL, 'N/A', '5000', '4181834', '100000', '4609239', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(248, 'Professor Salahuddin', 'Abdul Latif ', '0000-00-00', NULL, 'Single', NULL, '3006850024', NULL, NULL, NULL, 'N/A', '0', '', '10000', '3070622', 'officer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(249, 'Muhammad Naeem CH.', 'Ch. Abdul khaliq ', '0000-00-00', NULL, 'Single', NULL, '3223355050', NULL, NULL, NULL, 'N/A', '5000', '', '100000', 'online', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(250, '', '', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '4609214', '100000', '4609201', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(251, 'Dr iqbal ', '', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '4609215', '100000', '4609200', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(252, 'Waseem', '', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '4609206', '50000', '', 'vehari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(253, 'Waseem Akram', 'Muhammad Akram ', '0000-00-00', NULL, 'Single', NULL, '3336291629', NULL, NULL, NULL, 'N/A', '5000', '4609211', '100000', '4609028', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(254, 'Nabeel', 'Ghulam Sarwar Ch.', '0000-00-00', NULL, 'Single', NULL, '3336999123', NULL, NULL, NULL, 'N/A', '5000', '3048496', '100000', '4609203/4609199', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(255, 'Rasheed', 'Adbul Hameed', '0000-00-00', NULL, 'Single', NULL, '3336999444', NULL, NULL, NULL, 'N/A', '5000', '2875003', '100000', '4609205/4609204', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(256, 'Muhammad zafar Abdullah', 'Muhammad Abdullah khan Asem', '0000-00-00', NULL, 'Single', NULL, '3006991686', NULL, NULL, NULL, 'N/A', '5000', '4609210', '100000', '4609252', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(257, 'Khalid Mahmood', 'Muhammad Yaqoob', '0000-00-00', NULL, 'Single', NULL, '3006991257', NULL, NULL, NULL, 'N/A', '5000', '4609139', '100000', '4609140', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(258, 'Javaid Iqbal', 'Zafar Hussain ', '0000-00-00', NULL, 'Single', NULL, '3000767234', NULL, NULL, NULL, 'N/A', '5000', '4609209', '100000', '4609025', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(259, 'Imran Sadiqui', '', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '', '100000', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
(260, 'Faizan', 'Rafique', '0000-00-00', 'Metric', NULL, NULL, '', NULL, 'faizanchohan3@gmail.com', NULL, NULL, '105000', NULL, NULL, NULL, 'New', 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$hHEUehZ/xHMI0Dex.yP2rekDCCJU7qW.tU0uYlG4bHKVyh1iRMX9S', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, ''),
(261, 'Faizan', 'chohan', '16/11/1993', 'Bachalor', NULL, NULL, '', NULL, 'faizanchohan33@gmail.com', NULL, NULL, '105000', NULL, NULL, NULL, 'New', 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$enM4UnfgdT/2P36f5gD.Au9srmvzLZFXsDKoXxLcR3CmtIghb6Eu2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, ''),
(262, 'faaa', 'ddf', '11/03/1111', 'Bachalor', 'Single', 'B+', '3339777676', '36601-6686608-1', 'faizanchohan30@gmail.com', NULL, 'faizan3', '105000', NULL, NULL, NULL, 'New', 'Al-Wasal street 2A, Villa 7, behind emarat petrol station', 'Al-Wasal street 2A', 'SoftTech', 'Lahore', 'Al-Wasal street 2A', '12 121  212-01', '1323-124134', 'Father', 'Rafique', '0000-00-00', '23124-3543151-3', 'C:\\xampp\\tmp\\phpF636.tmp', '$2y$10$mhtGITufzBApaCEyWKgk6.dIWC8AZ0GFdmRdG4Y.LHMRsDF9blbbW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, ''),
(263, 'Faizan', 'Rafique', '12/02/1992', 'Bachalor', 'Married', 'B+', '121223244', '23124-3423425-4', '222dd@gmail.com', NULL, 'dfd', '105000', NULL, NULL, NULL, 'new member', 'admin@gmail.com', 'Apartment 404, Fatima Block, Shamma Apartments, Shamma Stop, Ferozpur Road', 'SoftTech', 'Lahore', 'Apartment 404, Fatima Block, Shamma Apartments, Shamma Stop, Ferozpur Road', '1234-5678', '1234-5678', NULL, NULL, NULL, NULL, 'C:\\xampp\\tmp\\php5EC0.tmp', '$2y$10$Alkn1wnQNqMfL4M92xLAPuYW9uOIxLhTk7duOkXU59z1IqyMk2acm', '2019-11-14 09:08:21', '2019-11-14 09:08:21', 0, NULL),
(264, 'Faizan', 'Rafique', '12/02/1992', 'Bachalor', 'Married', 'B+', '121223244', '23124-3423425-4', '222dd@gmail.com', NULL, 'dfd', '105000', NULL, NULL, NULL, 'new member', 'admin@gmail.com', 'Apartment 404, Fatima Block, Shamma Apartments, Shamma Stop, Ferozpur Road', 'SoftTech12', 'Lahore', 'faizan22', '1234-5678', '1234-5678', NULL, NULL, NULL, NULL, 'C:\\xampp\\tmp\\php2C4F.tmp', '$2y$10$.1UcBWr9NtQiIYNMGmt89.M21xzuIuSiEM2gG0TgIZvC3ldp385/q', '2019-11-14 09:13:35', '2019-11-14 09:13:35', 0, NULL),
(266, 'Faizan', 'Rafique', NULL, 'Metric', NULL, NULL, '0582815867', NULL, 'umairhabib28@yahoo.com', NULL, NULL, '105000', NULL, NULL, NULL, 'new member', 'admin@gmail.com', 'Al-Wasal street 2A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$x10ibNY0UPah3cEYdU9jCe5pCi5czTF2gcan0C1xvOhqQ5tgNzBaq', '2019-11-15 18:37:42', '2019-11-15 18:37:42', 1, NULL),
(267, 'Faizan', 'Raf', NULL, 'Bachalor', NULL, NULL, '03339777676', NULL, 'malik@printingfairy.com', NULL, 'sds', '105000', NULL, NULL, NULL, 'new member', 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$I7Y3uNqsl8/m96KMfhdCEusNORiLrdCOmyXVzFa8DsS3/ekSdLm3y', '2019-11-16 02:00:53', '2019-11-16 02:00:53', 1, NULL),
(268, 'd', 'a', NULL, 'Metric', NULL, NULL, '03339777676', NULL, NULL, NULL, NULL, '105000', NULL, NULL, NULL, 'new member', 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$JzBBYiIDwGStjGgUvT853OkhKVk0yvlH.wPMt59Pby.9KAO1A1VKS', '2019-11-16 07:34:09', '2019-11-16 07:34:09', 1, NULL),
(270, 'Faizan', 'Rafique', NULL, 'Metric', NULL, NULL, '03339777676', NULL, 'faizanchohan33@gmail.com', NULL, NULL, '105000', NULL, NULL, NULL, 'new member', 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$E8ASERxu8.UngapWmew00ufl/U.bwZNs2jsAJ2UHfqUIqvDKZiMe6', '2019-11-16 07:44:05', '2019-11-16 07:44:05', 1, NULL),
(271, 'f', 'a', NULL, 'Metric', NULL, NULL, '00000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'new member', 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$vVuZp9FTMgWgYzY/ZMjoTOQDkFsVGu8kOSRHMHa9dPP8sygtN7rWG', '2019-11-16 07:45:42', '2019-11-16 07:45:42', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_08_03_000000_create_users_table', 1),
(2, '2019_08_03_042342_create_countries_table', 1),
(3, '2019_08_03_043804_add_more_fields_to_users_table', 1),
(4, '2019_08_03_100000_create_password_resets_table', 1),
(5, '2019_10_31_044756_create_customers_table', 2),
(6, '2019_11_05_091246_create_employees_table', 2),
(7, '2019_11_05_093522_create_emplyee_posts_table', 2),
(8, '2019_11_06_055822_create_emyloyee_pays_table', 3),
(9, '2019_11_06_081003_create_members_pay_records_table', 4),
(10, '2019_11_06_112301_create_construction_reports_table', 5),
(11, '2019_11_08_094206_create_members_table', 6),
(12, '2019_11_08_215128_create_hotel_report', 7);

-- --------------------------------------------------------

--
-- Table structure for table `monthly_charges`
--

CREATE TABLE `monthly_charges` (
  `MC_id` int(11) NOT NULL,
  `mr_id` int(5) NOT NULL,
  `Service_charges` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1500',
  `totalhotelcredit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `admin`, `status`, `remember_token`, `created_at`, `updated_at`, `address`, `city`, `cnic`, `image`, `pincode`, `phone`) VALUES
(1, 'Faizan Chohan', 'admin@gmail.com', NULL, '$2y$10$DBs3jWo3Dsm.O5ESBhKx9O97G2PX/ffw6Oj5q2AOhSSRgaaCVBf/G', 1, 0, NULL, NULL, NULL, 'New Lahore Road', NULL, NULL, NULL, NULL, '03339777676');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `construction_reports`
--
ALTER TABLE `construction_reports`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `construction_reports` ADD FULLTEXT KEY `material` (`material`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD UNIQUE KEY `cnic` (`cnic`);

--
-- Indexes for table `emplyee_posts`
--
ALTER TABLE `emplyee_posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emplyee_posts_title_unique` (`title`);

--
-- Indexes for table `emyloyee_pays`
--
ALTER TABLE `emyloyee_pays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_report`
--
ALTER TABLE `hotel_report`
  ADD PRIMARY KEY (`hb_id`),
  ADD KEY `membership_id` (`members_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `members_email_unique` (`email`);

--
-- Indexes for table `members_pay_records`
--
ALTER TABLE `members_pay_records`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `members_record`
--
ALTER TABLE `members_record`
  ADD PRIMARY KEY (`membership_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monthly_charges`
--
ALTER TABLE `monthly_charges`
  ADD PRIMARY KEY (`MC_id`),
  ADD KEY `hb_id` (`mr_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `construction_reports`
--
ALTER TABLE `construction_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `emplyee_posts`
--
ALTER TABLE `emplyee_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `emyloyee_pays`
--
ALTER TABLE `emyloyee_pays`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_report`
--
ALTER TABLE `hotel_report`
  MODIFY `hb_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `members_pay_records`
--
ALTER TABLE `members_pay_records`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=792;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `monthly_charges`
--
ALTER TABLE `monthly_charges`
  MODIFY `MC_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hotel_report`
--
ALTER TABLE `hotel_report`
  ADD CONSTRAINT `membership_id` FOREIGN KEY (`members_id`) REFERENCES `members_record` (`membership_id`);

--
-- Constraints for table `monthly_charges`
--
ALTER TABLE `monthly_charges`
  ADD CONSTRAINT `hb_id` FOREIGN KEY (`mr_id`) REFERENCES `members_record` (`membership_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
