-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 30, 2019 at 04:51 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gymkhana_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `balances`
--

CREATE TABLE `balances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coa_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `balances`
--

INSERT INTO `balances` (`id`, `coa_id`, `description`, `debit`, `credit`, `created_at`, `updated_at`) VALUES
(1, '53', 'AAAA', '122222', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Chinese Cuisine', 'chinese', '2019-11-20 16:23:19', '2019-11-20 16:23:21'),
(2, 'Italian Cuisine', 'italian', '2019-11-20 16:24:04', '2019-11-20 16:24:08'),
(3, 'Pakistani Cuisine', 'pakistani', '2019-11-20 16:24:54', '2019-11-20 16:24:57'),
(4, 'Soup', 'soup', '2019-11-20 16:30:48', '2019-11-20 16:30:51'),
(5, 'Fast Food', 'fast food', '2019-11-20 16:35:35', '2019-11-20 16:35:38'),
(6, 'Starter', 'starter', '2019-11-20 16:38:11', '2019-11-20 16:38:14');

-- --------------------------------------------------------

--
-- Table structure for table `chart_account_types`
--

CREATE TABLE `chart_account_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `chart_account_types`
--

INSERT INTO `chart_account_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Capital', NULL, NULL),
(2, 'Assets', NULL, NULL),
(3, 'Liabilities', NULL, NULL),
(4, 'Expense', NULL, NULL),
(5, 'Revenue', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chart_of_accounts`
--

CREATE TABLE `chart_of_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_parent` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `chart_of_accounts`
--

INSERT INTO `chart_of_accounts` (`id`, `user_id`, `name`, `description`, `type`, `is_parent`, `parent_id`, `created_at`, `updated_at`) VALUES
(52, 1, 'Shahzad Mustafa', 'No', 'Liabilities', 3, 0, '2019-11-27 20:24:57', '2019-11-27 20:25:26'),
(53, 1, 'Faizan', 'No', 'Liabilities', 3, 0, '2019-11-27 20:25:51', '2019-11-27 20:25:51');

-- --------------------------------------------------------

--
-- Table structure for table `construction_reports`
--

CREATE TABLE `construction_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `material` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `credit` double(8,2) NOT NULL,
  `payment` double(8,2) NOT NULL,
  `labour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_labour` int(11) NOT NULL,
  `labour_payments` double(8,2) NOT NULL,
  `skills` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saved_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `construction_reports`
--

INSERT INTO `construction_reports` (`id`, `material`, `qty`, `unit_price`, `total`, `credit`, `payment`, `labour`, `no_labour`, `labour_payments`, `skills`, `saved_at`, `created_at`, `updated_at`) VALUES
(1, 'Cement', 2, 1500.00, 3000.00, 1500.00, 1500.00, 'Abc', 5, 3000.00, 'The skills field', '06/11/2019', '2019-11-06 06:41:43', '2019-11-06 06:41:43'),
(2, 'Cancrate', 2, 15000.00, 3000.00, 15000.00, 15000.00, 'cde', 3, 3000.00, 'The skills field', '05/11/2019', '2019-11-06 06:42:02', '2019-11-06 06:42:02'),
(3, 'Cement 2', 2, 1500.00, 3000.00, 1500.00, 1500.00, 'Abc', 5, 3000.00, 'vjh', '06/11/2019', '2019-11-07 00:33:15', '2019-11-07 00:33:15'),
(4, 'Cement3', 2, 1500.00, 3000.00, 1500.00, 1500.00, 'Abc', 5, 3000.00, 'vjh', '06/11/2019', '2019-11-07 03:18:32', '2019-11-07 03:18:32'),
(5, 'Cement 25', 2, 2500.00, 5000.00, 656.00, 56.00, 'Abc', 56, 565.00, 'jk', '07/11/2019', '2019-11-07 03:24:41', '2019-11-07 03:24:41'),
(6, 'Cement 25', 2, 2500.00, 5000.00, 656.00, 56.00, 'Abc', 56, 565.00, 'jk', '07/11/2019', '2019-11-07 03:24:51', '2019-11-07 03:24:51'),
(7, 'Bricks', 10000, 10.00, 100000.00, 40000.00, 60000.00, '20000', 5, 20000.00, 'None', '08/11/2019', '2019-11-08 13:13:11', '2019-11-08 13:13:11'),
(8, 'Bricks', 1, 1.00, 1.00, 11.00, 111.00, '111', 1111111, 1111.00, 'None', '09/11/2019', '2019-11-09 13:29:09', '2019-11-09 13:29:09'),
(9, '33', 11, 12.00, 132.00, 12.00, 123.00, '213213', 21431, 2314.00, '14341', '09/11/2019', '2019-11-09 13:29:23', '2019-11-09 13:29:23'),
(10, 'ssd', 22, 22.00, 484.00, 222.00, 233.00, '344', 444, 3333.00, '444', '09/11/2019', '2019-11-09 13:36:41', '2019-11-09 13:36:41'),
(11, '332', 234, 3342.00, 782028.00, 3255.00, 2345.00, '32221', 324552, 32344.00, '24442', '09/11/2019', '2019-11-09 13:37:00', '2019-11-09 13:37:00'),
(12, '322', 222, 22.00, 4884.00, 2222.00, 22.00, '22', 22, 22.00, '22', '09/11/2019', '2019-11-09 13:38:04', '2019-11-09 13:38:04'),
(13, '223', 22, 232.00, 5104.00, 21.00, 324.00, '232', 322, 224.00, '243', '09/11/2019', '2019-11-09 13:38:17', '2019-11-09 13:38:17'),
(14, '232', 322, 343.00, 110446.00, 234.00, 232.00, '234', 234, 234.00, '3', '09/11/2019', '2019-11-09 13:41:01', '2019-11-09 13:41:01'),
(15, 'Bricks', 10000, 22.00, 220000.00, 233.00, 232.00, '17', 23, 2334.00, 'None', '09/11/2019', '2019-11-09 13:43:13', '2019-11-09 13:43:13'),
(16, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '20000', 444, 2445.00, '22', '09/11/2019', '2019-11-09 13:43:43', '2019-11-09 13:43:43'),
(17, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 3333.00, '444', '09/11/2019', '2019-11-09 13:44:24', '2019-11-09 13:44:24'),
(18, 'ssd', 22, 10.00, 220.00, 40000.00, 232.00, '111', 444, 2314.00, '22', '09/11/2019', '2019-11-09 13:44:49', '2019-11-09 13:44:49'),
(19, 'Bricks', 10000, 10.00, 100000.00, 12.00, 232.00, '213213', 21431, 32344.00, '35444', '09/11/2019', '2019-11-09 13:47:17', '2019-11-09 13:47:17'),
(20, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 214, 2445.00, '444', '09/11/2019', '2019-11-09 13:48:01', '2019-11-09 13:48:01'),
(21, 'ssd', 10000, 22.00, 220000.00, 12.00, 23222.00, '20000', 444, 1111.00, '22', '09/11/2019', '2019-11-09 13:48:21', '2019-11-09 13:48:21'),
(22, 'Bricks', 10000, 22.00, 220000.00, 12.00, 232.00, '111', 444, 3333.00, '444', '09/11/2019', '2019-11-09 13:49:07', '2019-11-09 13:49:07'),
(23, 'Bricks', 10000, 22.00, 220000.00, 12.00, 232.00, '111', 444, 1111.00, 'None', '09/11/2019', '2019-11-09 13:49:28', '2019-11-09 13:49:28'),
(24, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '20000', 21431, 20000.00, '24442', '09/11/2019', '2019-11-09 13:49:42', '2019-11-09 13:49:42'),
(25, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 2445.00, 'None', '09/11/2019', '2019-11-09 13:50:39', '2019-11-09 13:50:39'),
(26, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 20000.00, '35444', '09/11/2019', '2019-11-09 13:50:55', '2019-11-09 13:50:55'),
(27, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 3333.00, '22', '09/11/2019', '2019-11-09 13:53:40', '2019-11-09 13:53:40'),
(28, 'Bricks', 10000, 22.00, 220000.00, 40000.00, 232.00, '111', 444, 2314.00, '35444', '09/11/2019', '2019-11-09 13:54:01', '2019-11-09 13:54:01');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `country_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cnic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_post_id` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_confirmation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `city`, `cnic`, `phone`, `image`, `employee_post_id`, `password`, `password_confirmation`, `address`, `status`, `created_at`, `updated_at`) VALUES
(7, 'Ali  Gujjar', 'saeedg8879@gmail.com', 'NAROWAL', '54545-4464654-6', '123-456-7895', '1572980190-ali-gujjar.png', 2, '$2y$10$mgvrE12S6c8ayJLkUzDWbe1Fe8rDD4bBAvElFMzOPXRV0yGxo8ftm', '123456', 'New Lahore Road', 1, '2019-11-05 12:56:30', '2019-11-05 12:56:30');

-- --------------------------------------------------------

--
-- Table structure for table `emplyee_posts`
--

CREATE TABLE `emplyee_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `emplyee_posts`
--

INSERT INTO `emplyee_posts` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(2, 'CEO', '<p>Head Of&nbsp; Orginization</p>', '2019-11-05 05:09:07', '2019-11-05 05:09:07'),
(3, 'Peon', '<p>Poen</p>', '2019-11-06 08:33:27', '2019-11-06 08:33:27');

-- --------------------------------------------------------

--
-- Table structure for table `emyloyee_pays`
--

CREATE TABLE `emyloyee_pays` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paid_salary` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_report`
--

CREATE TABLE `hotel_report` (
  `hb_id` bigint(20) NOT NULL,
  `members_id` int(11) NOT NULL,
  `Billamount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Service Charges` int(255) DEFAULT NULL,
  `Total` int(255) DEFAULT NULL,
  `Discount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Credit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Paid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Bill_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `hotel_report`
--

INSERT INTO `hotel_report` (`hb_id`, `members_id`, `Billamount`, `Service Charges`, `Total`, `Discount`, `Credit`, `Paid`, `Bill_no`, `date`, `created_at`, `updated_at`) VALUES
(1, 246, '12000', NULL, NULL, '10', '10000', '2000', '22123', '0000-00-00', NULL, NULL),
(2, 18, '12', NULL, NULL, '22', '12', '22', '213', '', NULL, NULL),
(3, 26, '12000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(4, 26, '12000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(5, 26, '12000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(6, 26, '12000', NULL, NULL, '12', '40000', '11000', '233422', '0000-00-00', NULL, NULL),
(7, 23, '12000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(8, 23, '12000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(9, 21, '12000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(10, 5, '12000', NULL, NULL, '0', '40000', '11000', '233422', '0000-00-00', NULL, NULL),
(11, 8, '11000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(12, 8, '11000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(13, 8, '11000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(14, 8, '11000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(15, 8, '12000', NULL, NULL, '0', '40000', '11000', '23213', '0000-00-00', NULL, NULL),
(16, 12, '2000', NULL, NULL, '0', '1000', '1000', '123456', '0000-00-00', '2019-11-13 08:15:29', '2019-11-13 08:15:29'),
(18, 12, '12000', NULL, NULL, '0', '40000', '11000', '23213', '13/11/2019', '2019-11-13 08:19:55', '2019-11-13 08:19:55'),
(19, 2, '12000', NULL, NULL, '0', '1000', '11000', '747474', '13/11/2019', '2019-11-13 08:28:59', '2019-11-13 08:28:59'),
(20, 250, '2', NULL, NULL, '12', '12', '23', '23123', '', NULL, NULL),
(21, 184, '2', NULL, NULL, '11', '1', '1', '1', '', NULL, NULL),
(22, 26, '12000', NULL, NULL, '0', '40000', '11000', '23213', '13/11/2019', '2019-11-13 15:44:41', '2019-11-13 15:44:41'),
(23, 26, '12000', NULL, NULL, '0', '12', '12', '12', '13/11/2019', '2019-11-13 15:45:41', '2019-11-13 15:45:41'),
(24, 26, '12000', NULL, NULL, '0', '40000', '11000', '233422', '14/11/2019', '2019-11-14 00:59:09', '2019-11-14 00:59:09'),
(25, 8, '2000', NULL, NULL, '0', '1000', '1000', '76576', '14/11/2019', '2019-11-14 01:51:53', '2019-11-14 01:51:53');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Unique_idM` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `father_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qualification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `land_line` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_cnic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Type` enum('Vhr','New','Old') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `RegistrationFee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blood_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temporary_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permanent_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_of_organization` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_land_line` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Relation` enum('Wife','Son','Daughter','Mother','Father','Other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `spouse_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spouse_dob` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spouse_cnic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'N/A',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_confirmation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `membership_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `Unique_idM`, `full_name`, `father_name`, `qualification`, `dob`, `mobile`, `land_line`, `twitter`, `member_cnic`, `Type`, `RegistrationFee`, `marital_status`, `blood_group`, `temporary_address`, `permanent_address`, `name_of_organization`, `designation`, `business_address`, `business_land_line`, `business_fax`, `Relation`, `spouse_name`, `spouse_dob`, `spouse_cnic`, `email`, `password`, `password_confirmation`, `image`, `status`, `created_at`, `updated_at`, `membership_id`) VALUES
(6, '1', 'Hafiz Junaid Ahmad', 'Ahmad Waseem Sheikh', 'Bachalor', '11/11/1992', '0333-3022229', '3333-022229', 'sdsad', '23334-3243242-3', 'New', NULL, 'single', 'b+', 'Al-Wasal street 2A, Villa 7, behind emarat petrol station', 'Al-Wasal street 2A, Villa 7, behind emarat petrol station', 'cc', 'cc', 'cc', '3333-022229', '3333-022229', 'Other', 'cc', '11/11/1992', '33333-3333333-3', '1admin@gmail.com', '$2y$10$GJZLdBs7I8vsuRDXzVe3LO/M8w9c3j0oueEMvo87DeEL7k7xJEwde', '123456', '1573377994-hafiz-junaid-ahmad.jpg', 1, '2019-11-10 03:26:34', '2019-11-10 03:26:34', NULL),
(7, '2', 'Rao Ghaffar Ali', 'Rao Khurshad Ali', 'Metric', NULL, '0300-7592902', NULL, NULL, NULL, 'New', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin@gmail.com', '$2y$10$zV4onPm8pJ.8fMvQRtk10O5kWlxjFgzWlUKHRlKRotGtSsCUoFaN6', '123456', NULL, 1, '2019-11-10 03:42:42', '2019-11-10 03:42:42', NULL),
(8, '3', 'Riffat Tahir', 'Chudhary Muhammad Jamil', 'Metric', NULL, '0300-6993431', NULL, NULL, NULL, 'Vhr', '105000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'riffat@gmail.com', '$2y$10$s2iVCJyQFEM0Q1Lk9Ahoseuzlrnga1mzH2M80szculzcsbENR7.HS', '123456', NULL, 1, '2019-11-10 04:00:01', '2019-11-10 04:00:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members_pay_records`
--

CREATE TABLE `members_pay_records` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `member_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `monthly_payment` double(8,2) DEFAULT NULL,
  `Unique_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberShip_fee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_fee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Aug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Sep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Oct` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instrument_No_105000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instrument_No_100000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instrument_No_55000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instrument_No_50000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insturment_No_5000` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insturment_No_32500` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Instrument_No_Aug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Instrument_No_Sep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Instrument_No_Oct` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `members_pay_records`
--

INSERT INTO `members_pay_records` (`id`, `member_id`, `month`, `monthly_payment`, `Unique_id`, `created_at`, `updated_at`, `name`, `father_name`, `mobile_number`, `memberShip_fee`, `registration_fee`, `Aug`, `Sep`, `Oct`, `instrument_No_105000`, `instrument_No_100000`, `instrument_No_55000`, `instrument_No_50000`, `insturment_No_5000`, `insturment_No_32500`, `Instrument_No_Aug`, `Instrument_No_Sep`, `Instrument_No_Oct`, `Status`, `Comment`, `Date`) VALUES
(270, '1', NULL, NULL, NULL, '2019-11-09 17:45:49', '2019-11-09 17:45:49', NULL, 'Ahmad Waseem Sheikh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, '3', NULL, NULL, NULL, '2019-11-09 17:45:50', '2019-11-09 17:45:50', NULL, 'Chudhary Muhammad Jamil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, '4', NULL, NULL, NULL, '2019-11-09 17:45:50', '2019-11-09 17:45:50', NULL, 'Muhammad Ud din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(274, '5', NULL, NULL, NULL, '2019-11-09 17:45:50', '2019-11-09 17:45:50', NULL, 'Maqsood Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(275, '6', NULL, NULL, NULL, '2019-11-09 17:45:50', '2019-11-09 17:45:50', NULL, 'Muhammad Bukhtiar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(276, '7', NULL, NULL, NULL, '2019-11-09 17:45:50', '2019-11-09 17:45:50', NULL, 'Marghoob Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(277, '8', NULL, NULL, NULL, '2019-11-09 17:45:50', '2019-11-09 17:45:50', NULL, 'Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(278, '9', NULL, NULL, NULL, '2019-11-09 17:45:51', '2019-11-09 17:45:51', NULL, 'Chuadhary Mukhtayar Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(279, '10', NULL, NULL, NULL, '2019-11-09 17:45:51', '2019-11-09 17:45:51', NULL, 'Abdul Shakoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(280, '11', NULL, NULL, NULL, '2019-11-09 17:45:51', '2019-11-09 17:45:51', NULL, 'Haji Usman Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(281, '12', NULL, NULL, NULL, '2019-11-09 17:45:51', '2019-11-09 17:45:51', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(282, '13', NULL, NULL, NULL, '2019-11-09 17:45:51', '2019-11-09 17:45:51', NULL, 'Muhammad Nadeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(283, '14', NULL, NULL, NULL, '2019-11-09 17:45:51', '2019-11-09 17:45:51', NULL, 'Syed Soulat Hussain Naqvi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(284, '15', NULL, NULL, NULL, '2019-11-09 17:45:52', '2019-11-09 17:45:52', NULL, 'Maghoob Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(285, '16', NULL, NULL, NULL, '2019-11-09 17:45:52', '2019-11-09 17:45:52', NULL, 'Syed Mehdi Hassan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(286, '17', NULL, NULL, NULL, '2019-11-09 17:45:52', '2019-11-09 17:45:52', NULL, 'Muhammad Mateen Zaman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(287, '18', NULL, NULL, NULL, '2019-11-09 17:45:52', '2019-11-09 17:45:52', NULL, 'Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(288, '19', NULL, NULL, NULL, '2019-11-09 17:45:53', '2019-11-09 17:45:53', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, '20', NULL, NULL, NULL, '2019-11-09 17:45:53', '2019-11-09 17:45:53', NULL, 'Masood Ahmad Kahloon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(290, '21', NULL, NULL, NULL, '2019-11-09 17:45:53', '2019-11-09 17:45:53', NULL, 'Abdul Razaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, '22', NULL, NULL, NULL, '2019-11-09 17:45:53', '2019-11-09 17:45:53', NULL, 'Muhammad Aslam ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, '23', NULL, NULL, NULL, '2019-11-09 17:45:53', '2019-11-09 17:45:53', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(293, '24', NULL, NULL, NULL, '2019-11-09 17:45:53', '2019-11-09 17:45:53', NULL, 'Ch Amin wahla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(294, '25', NULL, NULL, NULL, '2019-11-09 17:45:53', '2019-11-09 17:45:53', NULL, 'Muhammad Iqbal Saleemi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(295, '26', NULL, NULL, NULL, '2019-11-09 17:45:54', '2019-11-09 17:45:54', NULL, 'Sheikh Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(296, '27', NULL, NULL, NULL, '2019-11-09 17:45:54', '2019-11-09 17:45:54', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, '28', NULL, NULL, NULL, '2019-11-09 17:45:54', '2019-11-09 17:45:54', NULL, 'Abdul Ghaffar Butt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(298, '29', NULL, NULL, NULL, '2019-11-09 17:45:54', '2019-11-09 17:45:54', NULL, 'Chudhary Ali Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, '30', NULL, NULL, NULL, '2019-11-09 17:45:54', '2019-11-09 17:45:54', NULL, 'Abdul Razzaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, '31', NULL, NULL, NULL, '2019-11-09 17:45:54', '2019-11-09 17:45:54', NULL, 'Abdul Ghafoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, '32', NULL, NULL, NULL, '2019-11-09 17:45:55', '2019-11-09 17:45:55', NULL, 'Haji Muhammad Raza ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(302, '33', NULL, NULL, NULL, '2019-11-09 17:45:55', '2019-11-09 17:45:55', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, '34', NULL, NULL, NULL, '2019-11-09 17:45:55', '2019-11-09 17:45:55', NULL, 'Haji Muhammad Raza ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(304, '35', NULL, NULL, NULL, '2019-11-09 17:45:55', '2019-11-09 17:45:55', NULL, 'Khalid Farooq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(305, '36', NULL, NULL, NULL, '2019-11-09 17:45:55', '2019-11-09 17:45:55', NULL, 'Muhammad Ayub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, '37', NULL, NULL, NULL, '2019-11-09 17:45:56', '2019-11-09 17:45:56', NULL, 'Amanat Ali Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(307, '38', NULL, NULL, NULL, '2019-11-09 17:45:56', '2019-11-09 17:45:56', NULL, 'Ch Abdul Sattar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(308, '39', NULL, NULL, NULL, '2019-11-09 17:45:56', '2019-11-09 17:45:56', NULL, 'Muhammad Akbar Wahla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, '40', NULL, NULL, NULL, '2019-11-09 17:45:56', '2019-11-09 17:45:56', NULL, 'Chuadhary Abdul Ghanni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, '41', NULL, NULL, NULL, '2019-11-09 17:45:56', '2019-11-09 17:45:56', NULL, 'Ameer Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, '42', NULL, NULL, NULL, '2019-11-09 17:45:56', '2019-11-09 17:45:56', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(312, '43', NULL, NULL, NULL, '2019-11-09 17:45:57', '2019-11-09 17:45:57', NULL, 'Muhammad Din Sheikh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(313, '44', NULL, NULL, NULL, '2019-11-09 17:45:57', '2019-11-09 17:45:57', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(314, '45', NULL, NULL, NULL, '2019-11-09 17:45:57', '2019-11-09 17:45:57', NULL, 'Atta Ur Rehman Maqsood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(315, '46', NULL, NULL, NULL, '2019-11-09 17:45:57', '2019-11-09 17:45:57', NULL, 'Chudhary Khusi Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, '47', NULL, NULL, NULL, '2019-11-09 17:45:57', '2019-11-09 17:45:57', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(317, '48', NULL, NULL, NULL, '2019-11-09 17:45:58', '2019-11-09 17:45:58', NULL, 'Ghulam Mustafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(318, '49', NULL, NULL, NULL, '2019-11-09 17:45:58', '2019-11-09 17:45:58', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, '50', NULL, NULL, NULL, '2019-11-09 17:45:58', '2019-11-09 17:45:58', NULL, 'Maqsood Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(320, '151', NULL, NULL, NULL, '2019-11-09 17:45:58', '2019-11-09 17:45:58', NULL, 'Chaudhary Muhammad Sabir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(321, '52', NULL, NULL, NULL, '2019-11-09 17:45:58', '2019-11-09 17:45:58', NULL, 'Muhammad Suleman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(322, '53', NULL, NULL, NULL, '2019-11-09 17:45:58', '2019-11-09 17:45:58', NULL, 'Abdul Ghafoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(323, '54', NULL, NULL, NULL, '2019-11-09 17:45:59', '2019-11-09 17:45:59', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(324, '55', NULL, NULL, NULL, '2019-11-09 17:45:59', '2019-11-09 17:45:59', NULL, 'Muhammad Hanif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(325, '56', NULL, NULL, NULL, '2019-11-09 17:45:59', '2019-11-09 17:45:59', NULL, 'Muhammad Sadique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(326, '57', NULL, NULL, NULL, '2019-11-09 17:45:59', '2019-11-09 17:45:59', NULL, 'Rao Irshad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(327, '58', NULL, NULL, NULL, '2019-11-09 17:45:59', '2019-11-09 17:45:59', NULL, 'Chaudhary Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(328, '59', NULL, NULL, NULL, '2019-11-09 17:45:59', '2019-11-09 17:45:59', NULL, 'Nazir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(329, '60', NULL, NULL, NULL, '2019-11-09 17:45:59', '2019-11-09 17:45:59', NULL, 'Rafique Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(330, '61', NULL, NULL, NULL, '2019-11-09 17:46:00', '2019-11-09 17:46:00', NULL, 'Sardar Khalil Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(331, '62', NULL, NULL, NULL, '2019-11-09 17:46:00', '2019-11-09 17:46:00', NULL, 'Rao Nazir Ahmed Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(332, '63', NULL, NULL, NULL, '2019-11-09 17:46:00', '2019-11-09 17:46:00', NULL, 'Muhammad Ramzan Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(333, '64', NULL, NULL, NULL, '2019-11-09 17:46:00', '2019-11-09 17:46:00', NULL, 'Gul Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(334, '65', NULL, NULL, NULL, '2019-11-09 17:46:00', '2019-11-09 17:46:00', NULL, 'Ghulam Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(335, '66', NULL, NULL, NULL, '2019-11-09 17:46:00', '2019-11-09 17:46:00', NULL, 'Hashmat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(336, '67', NULL, NULL, NULL, '2019-11-09 17:46:01', '2019-11-09 17:46:01', NULL, 'Ch Sher Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(337, '68', NULL, NULL, NULL, '2019-11-09 17:46:01', '2019-11-09 17:46:01', NULL, 'Abdul Ghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(338, '69', NULL, NULL, NULL, '2019-11-09 17:46:01', '2019-11-09 17:46:01', NULL, 'Sheikh Muhammad Nawaz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(339, '70', NULL, NULL, NULL, '2019-11-09 17:46:01', '2019-11-09 17:46:01', NULL, 'Wali Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(340, '71', NULL, NULL, NULL, '2019-11-09 17:46:01', '2019-11-09 17:46:01', NULL, 'Bashir Ahmad Ghumman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(341, '72', NULL, NULL, NULL, '2019-11-09 17:46:02', '2019-11-09 17:46:02', NULL, 'Muhammad Arif Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(342, '73', NULL, NULL, NULL, '2019-11-09 17:46:02', '2019-11-09 17:46:02', NULL, 'Muhammad Hanif Ghumman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(343, '74', NULL, NULL, NULL, '2019-11-09 17:46:02', '2019-11-09 17:46:02', NULL, 'Muhammad Arif Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(344, '75', NULL, NULL, NULL, '2019-11-09 17:46:02', '2019-11-09 17:46:02', NULL, 'Rukan Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(345, '76', NULL, NULL, NULL, '2019-11-09 17:46:03', '2019-11-09 17:46:03', NULL, 'Muhammad Bilal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(346, '77', NULL, NULL, NULL, '2019-11-09 17:46:03', '2019-11-09 17:46:03', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(347, '78', NULL, NULL, NULL, '2019-11-09 17:46:03', '2019-11-09 17:46:03', NULL, 'Naseer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(348, '79', NULL, NULL, NULL, '2019-11-09 17:46:04', '2019-11-09 17:46:04', NULL, 'Muhammad Abdur Ruaf Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(349, '80', NULL, NULL, NULL, '2019-11-09 17:46:04', '2019-11-09 17:46:04', NULL, 'Sardar Ghulam Farid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(350, '81', NULL, NULL, NULL, '2019-11-09 17:46:04', '2019-11-09 17:46:04', NULL, 'Kareem Dad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(351, '82', NULL, NULL, NULL, '2019-11-09 17:46:04', '2019-11-09 17:46:04', NULL, 'Muhammad Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(352, '83', NULL, NULL, NULL, '2019-11-09 17:46:04', '2019-11-09 17:46:04', NULL, 'Rasheed Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(353, '84', NULL, NULL, NULL, '2019-11-09 17:46:05', '2019-11-09 17:46:05', NULL, 'Nazir Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(354, '85', NULL, NULL, NULL, '2019-11-09 17:46:05', '2019-11-09 17:46:05', NULL, 'Muhammad Bota', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(355, '86', NULL, NULL, NULL, '2019-11-09 17:46:05', '2019-11-09 17:46:05', NULL, 'Abdul Ghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(356, '87', NULL, NULL, NULL, '2019-11-09 17:46:05', '2019-11-09 17:46:05', NULL, 'Muhammad Asghar Sabir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(357, '88', NULL, NULL, NULL, '2019-11-09 17:46:05', '2019-11-09 17:46:05', NULL, 'Qazi Maqsood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(358, '89', NULL, NULL, NULL, '2019-11-09 17:46:05', '2019-11-09 17:46:05', NULL, 'Muhammad Shafi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(359, '90', NULL, NULL, NULL, '2019-11-09 17:46:06', '2019-11-09 17:46:06', NULL, 'Muhammad Munir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(360, '91', NULL, NULL, NULL, '2019-11-09 17:46:06', '2019-11-09 17:46:06', NULL, 'Nawab Ali Javed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(361, '92', NULL, NULL, NULL, '2019-11-09 17:46:06', '2019-11-09 17:46:06', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(362, '93', NULL, NULL, NULL, '2019-11-09 17:46:06', '2019-11-09 17:46:06', NULL, 'Khan Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(363, '94', NULL, NULL, NULL, '2019-11-09 17:46:06', '2019-11-09 17:46:06', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(364, '95', NULL, NULL, NULL, '2019-11-09 17:46:06', '2019-11-09 17:46:06', NULL, 'Dr.Muhammad Aslam Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(365, '96', NULL, NULL, NULL, '2019-11-09 17:46:06', '2019-11-09 17:46:06', NULL, 'Abul Razzaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(366, '97', NULL, NULL, NULL, '2019-11-09 17:46:07', '2019-11-09 17:46:07', NULL, 'Ghulam Nabi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(367, '98', NULL, NULL, NULL, '2019-11-09 17:46:07', '2019-11-09 17:46:07', NULL, 'Niaz Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(368, '99', NULL, NULL, NULL, '2019-11-09 17:46:07', '2019-11-09 17:46:07', NULL, 'Muhammad Ayub khan saldera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(369, '100', NULL, NULL, NULL, '2019-11-09 17:46:07', '2019-11-09 17:46:07', NULL, 'Akbar Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(370, '101', NULL, NULL, NULL, '2019-11-09 17:46:07', '2019-11-09 17:46:07', NULL, 'Ch.Asmat saeed Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(371, '102', NULL, NULL, NULL, '2019-11-09 17:46:07', '2019-11-09 17:46:07', NULL, 'Asmat Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(372, '103', NULL, NULL, NULL, '2019-11-09 17:46:08', '2019-11-09 17:46:08', NULL, 'Sardar Muhammad Sajjad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(373, '104', NULL, NULL, NULL, '2019-11-09 17:46:08', '2019-11-09 17:46:08', NULL, 'Raja Muhammad Akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(374, '105', NULL, NULL, NULL, '2019-11-09 17:46:08', '2019-11-09 17:46:08', NULL, 'Sardar Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(375, '106', NULL, NULL, NULL, '2019-11-09 17:46:08', '2019-11-09 17:46:08', NULL, 'Rehmat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(376, '107', NULL, NULL, NULL, '2019-11-09 17:46:09', '2019-11-09 17:46:09', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(377, '108', NULL, NULL, NULL, '2019-11-09 17:46:09', '2019-11-09 17:46:09', NULL, 'Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(378, '109', NULL, NULL, NULL, '2019-11-09 17:46:09', '2019-11-09 17:46:09', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(379, '110', NULL, NULL, NULL, '2019-11-09 17:46:09', '2019-11-09 17:46:09', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(380, '111', NULL, NULL, NULL, '2019-11-09 17:46:09', '2019-11-09 17:46:09', NULL, 'Abdul Majeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(381, '112', NULL, NULL, NULL, '2019-11-09 17:46:09', '2019-11-09 17:46:09', NULL, 'Muhammad Aslam Naseem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(382, '113', NULL, NULL, NULL, '2019-11-09 17:46:10', '2019-11-09 17:46:10', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(383, '114', NULL, NULL, NULL, '2019-11-09 17:46:10', '2019-11-09 17:46:10', NULL, 'Muhammad Ishaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(384, '115', NULL, NULL, NULL, '2019-11-09 17:46:10', '2019-11-09 17:46:10', NULL, 'Muhammad Tufail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(385, '116', NULL, NULL, NULL, '2019-11-09 17:46:10', '2019-11-09 17:46:10', NULL, 'Syed Riaz Hussain Rizvi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(386, '117', NULL, NULL, NULL, '2019-11-09 17:46:10', '2019-11-09 17:46:10', NULL, 'Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(387, '118', NULL, NULL, NULL, '2019-11-09 17:46:10', '2019-11-09 17:46:10', NULL, 'Muhammad  Arshad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(388, '119', NULL, NULL, NULL, '2019-11-09 17:46:11', '2019-11-09 17:46:11', NULL, 'Muhammad Yaseen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(389, '120', NULL, NULL, NULL, '2019-11-09 17:46:11', '2019-11-09 17:46:11', NULL, 'Javed Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(390, '121', NULL, NULL, NULL, '2019-11-09 17:46:11', '2019-11-09 17:46:11', NULL, 'Dawood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(391, '122', NULL, NULL, NULL, '2019-11-09 17:46:11', '2019-11-09 17:46:11', NULL, 'Faqir Muhammad Chaudhary', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(392, '123', NULL, NULL, NULL, '2019-11-09 17:46:11', '2019-11-09 17:46:11', NULL, 'Chudhary KhusiMuhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(393, '124', NULL, NULL, NULL, '2019-11-09 17:46:11', '2019-11-09 17:46:11', NULL, 'Muhammad Akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(394, '125', NULL, NULL, NULL, '2019-11-09 17:46:12', '2019-11-09 17:46:12', NULL, 'Chadhary Asmat Saeed Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(395, '126', NULL, NULL, NULL, '2019-11-09 17:46:12', '2019-11-09 17:46:12', NULL, 'Rao Irshad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(396, '127', NULL, NULL, NULL, '2019-11-09 17:46:12', '2019-11-09 17:46:12', NULL, 'Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(397, '128', NULL, NULL, NULL, '2019-11-09 17:46:12', '2019-11-09 17:46:12', NULL, 'Muhammad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(398, '129', NULL, NULL, NULL, '2019-11-09 17:46:12', '2019-11-09 17:46:12', NULL, 'Muhammad Mansha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(399, '130', NULL, NULL, NULL, '2019-11-09 17:46:12', '2019-11-09 17:46:12', NULL, 'Muhammad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(400, '131', NULL, NULL, NULL, '2019-11-09 17:46:12', '2019-11-09 17:46:12', NULL, 'Abdul Raheem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(401, '132', NULL, NULL, NULL, '2019-11-09 17:46:13', '2019-11-09 17:46:13', NULL, 'Ghulam Mustafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(402, '133', NULL, NULL, NULL, '2019-11-09 17:46:13', '2019-11-09 17:46:13', NULL, 'Malik Karamat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(403, '134', NULL, NULL, NULL, '2019-11-09 17:46:13', '2019-11-09 17:46:13', NULL, 'Naseer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(404, '135', NULL, NULL, NULL, '2019-11-09 17:46:13', '2019-11-09 17:46:13', NULL, 'Rao Khan Bahadar Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, '136', NULL, NULL, NULL, '2019-11-09 17:46:13', '2019-11-09 17:46:13', NULL, 'Mian Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(406, '137', NULL, NULL, NULL, '2019-11-09 17:46:14', '2019-11-09 17:46:14', NULL, 'Zulfiqar Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(407, '138', NULL, NULL, NULL, '2019-11-09 17:46:14', '2019-11-09 17:46:14', NULL, 'Mukhtar Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(408, '139', NULL, NULL, NULL, '2019-11-09 17:46:14', '2019-11-09 17:46:14', NULL, 'Asgher Ali Sandhu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(409, '140', NULL, NULL, NULL, '2019-11-09 17:46:14', '2019-11-09 17:46:14', NULL, 'Khushi Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(410, '141', NULL, NULL, NULL, '2019-11-09 17:46:14', '2019-11-09 17:46:14', NULL, 'Muhammad Shaffi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(411, '142', NULL, NULL, NULL, '2019-11-09 17:46:14', '2019-11-09 17:46:14', NULL, 'Siraj Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(412, '143', NULL, NULL, NULL, '2019-11-09 17:46:15', '2019-11-09 17:46:15', NULL, 'Raja Ghulam Haider Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(413, '144', NULL, NULL, NULL, '2019-11-09 17:46:15', '2019-11-09 17:46:15', NULL, 'Muhammad Ramzan Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(414, '145', NULL, NULL, NULL, '2019-11-09 17:46:15', '2019-11-09 17:46:15', NULL, 'Konwar Akbar Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(415, '146', NULL, NULL, NULL, '2019-11-09 17:46:15', '2019-11-09 17:46:15', NULL, 'Khalid Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(416, '147', NULL, NULL, NULL, '2019-11-09 17:46:15', '2019-11-09 17:46:15', NULL, 'Abdur Rehman ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(417, '148', NULL, NULL, NULL, '2019-11-09 17:46:15', '2019-11-09 17:46:15', NULL, 'Muhammad Amin Chisti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(418, '149', NULL, NULL, NULL, '2019-11-09 17:46:16', '2019-11-09 17:46:16', NULL, 'Muhammad Tufail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(419, '150', NULL, NULL, NULL, '2019-11-09 17:46:16', '2019-11-09 17:46:16', NULL, 'Chadhary Muhammad Saleem Kahloon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(420, '51', NULL, NULL, NULL, '2019-11-09 17:46:16', '2019-11-09 17:46:16', NULL, 'Atta Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(421, '152', NULL, NULL, NULL, '2019-11-09 17:46:16', '2019-11-09 17:46:16', NULL, 'Muhammad Akbar khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(422, '153', NULL, NULL, NULL, '2019-11-09 17:46:16', '2019-11-09 17:46:16', NULL, 'Muhammad Akbar khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(423, '154', NULL, NULL, NULL, '2019-11-09 17:46:16', '2019-11-09 17:46:16', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(424, '155', NULL, NULL, NULL, '2019-11-09 17:46:17', '2019-11-09 17:46:17', NULL, 'Faqir Muhammad ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(425, '156', NULL, NULL, NULL, '2019-11-09 17:46:17', '2019-11-09 17:46:17', NULL, 'Liaqat Ali Rasal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(426, '157', NULL, NULL, NULL, '2019-11-09 17:46:17', '2019-11-09 17:46:17', NULL, 'Muhammad Khaliq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(427, '158', NULL, NULL, NULL, '2019-11-09 17:46:17', '2019-11-09 17:46:17', NULL, 'Malik Nazar Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(428, '159', NULL, NULL, NULL, '2019-11-09 17:46:17', '2019-11-09 17:46:17', NULL, 'Muhammad Sharif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(429, '160', NULL, NULL, NULL, '2019-11-09 17:46:17', '2019-11-09 17:46:17', NULL, 'Abdul Majid ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(430, '161', NULL, NULL, NULL, '2019-11-09 17:46:18', '2019-11-09 17:46:18', NULL, 'Shaukat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(431, '162', NULL, NULL, NULL, '2019-11-09 17:46:18', '2019-11-09 17:46:18', NULL, 'Muhammad Yousaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(432, '163', NULL, NULL, NULL, '2019-11-09 17:46:18', '2019-11-09 17:46:18', NULL, 'Ch Rehmat Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(433, '164', NULL, NULL, NULL, '2019-11-09 17:46:19', '2019-11-09 17:46:19', NULL, 'Shahid Mehmood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(434, '165', NULL, NULL, NULL, '2019-11-09 17:46:19', '2019-11-09 17:46:19', NULL, 'Khalid Mehmood Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(435, '166', NULL, NULL, NULL, '2019-11-09 17:46:20', '2019-11-09 17:46:20', NULL, 'Ahmad Naseem Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(436, '167', NULL, NULL, NULL, '2019-11-09 17:46:20', '2019-11-09 17:46:20', NULL, 'Nabardar Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(437, '168', NULL, NULL, NULL, '2019-11-09 17:46:20', '2019-11-09 17:46:20', NULL, 'Fateh Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(438, '169', NULL, NULL, NULL, '2019-11-09 17:46:20', '2019-11-09 17:46:20', NULL, 'Dilmeer Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(439, '170', NULL, NULL, NULL, '2019-11-09 17:46:20', '2019-11-09 17:46:20', NULL, 'Muhammad Shafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(440, '171', NULL, NULL, NULL, '2019-11-09 17:46:20', '2019-11-09 17:46:20', NULL, 'Sh.Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(441, '172', NULL, NULL, NULL, '2019-11-09 17:46:20', '2019-11-09 17:46:20', NULL, 'Muhammad ishtiaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(442, '173', NULL, NULL, NULL, '2019-11-09 17:46:21', '2019-11-09 17:46:21', NULL, 'Muhammad Arshad Shah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(443, '174', NULL, NULL, NULL, '2019-11-09 17:46:21', '2019-11-09 17:46:21', NULL, 'Muhammad Rafique Dogar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(444, '175', NULL, NULL, NULL, '2019-11-09 17:46:21', '2019-11-09 17:46:21', NULL, 'Muhammad Habib Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(445, '176', NULL, NULL, NULL, '2019-11-09 17:46:21', '2019-11-09 17:46:21', NULL, 'Nazir Ahmad Mehmood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(446, '177', NULL, NULL, NULL, '2019-11-09 17:46:21', '2019-11-09 17:46:21', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(447, '178', NULL, NULL, NULL, '2019-11-09 17:46:21', '2019-11-09 17:46:21', NULL, 'Taj Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(448, '179', NULL, NULL, NULL, '2019-11-09 17:46:21', '2019-11-09 17:46:21', NULL, 'Haji Muhammad ishaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(449, '180', NULL, NULL, NULL, '2019-11-09 17:46:22', '2019-11-09 17:46:22', NULL, 'Mian Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(450, '181', NULL, NULL, NULL, '2019-11-09 17:46:22', '2019-11-09 17:46:22', NULL, 'Muhammad Hanif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(451, '182', NULL, NULL, NULL, '2019-11-09 17:46:22', '2019-11-09 17:46:22', NULL, 'Malik Muhammad Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(452, '183', NULL, NULL, NULL, '2019-11-09 17:46:22', '2019-11-09 17:46:22', NULL, 'Muhammad Sadique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(453, '184', NULL, NULL, NULL, '2019-11-09 17:46:22', '2019-11-09 17:46:22', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(454, '185', NULL, NULL, NULL, '2019-11-09 17:46:22', '2019-11-09 17:46:22', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(455, '186', NULL, NULL, NULL, '2019-11-09 17:46:23', '2019-11-09 17:46:23', NULL, 'Liaqat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(456, '187', NULL, NULL, NULL, '2019-11-09 17:46:23', '2019-11-09 17:46:23', NULL, 'Muhammad Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(457, '188', NULL, NULL, NULL, '2019-11-09 17:46:23', '2019-11-09 17:46:23', NULL, 'Muhammmad Sardar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(458, '189', NULL, NULL, NULL, '2019-11-09 17:46:23', '2019-11-09 17:46:23', NULL, 'Muhammad Sarwar ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(459, '190', NULL, NULL, NULL, '2019-11-09 17:46:23', '2019-11-09 17:46:23', NULL, 'Ch.Faqir Muhammad Arian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(460, '191', NULL, NULL, NULL, '2019-11-09 17:46:24', '2019-11-09 17:46:24', NULL, 'Muhammad Idrees', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(461, '192', NULL, NULL, NULL, '2019-11-09 17:46:24', '2019-11-09 17:46:24', NULL, 'Ghulam Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(462, '193', NULL, NULL, NULL, '2019-11-09 17:46:24', '2019-11-09 17:46:24', NULL, 'Muhammad Akhtar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(463, '194', NULL, NULL, NULL, '2019-11-09 17:46:24', '2019-11-09 17:46:24', NULL, 'Muhammad Mueez ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(464, '195', NULL, NULL, NULL, '2019-11-09 17:46:24', '2019-11-09 17:46:24', NULL, 'Nazeer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(465, '196', NULL, NULL, NULL, '2019-11-09 17:46:24', '2019-11-09 17:46:24', NULL, 'Muhammad Shafi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(466, '197', NULL, NULL, NULL, '2019-11-09 17:46:25', '2019-11-09 17:46:25', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(467, '198', NULL, NULL, NULL, '2019-11-09 17:46:25', '2019-11-09 17:46:25', NULL, 'Abdul Majeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(468, '199', NULL, NULL, NULL, '2019-11-09 17:46:25', '2019-11-09 17:46:25', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(469, '200', NULL, NULL, NULL, '2019-11-09 17:46:25', '2019-11-09 17:46:25', NULL, 'Muhammad Irshad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(470, '201', NULL, NULL, NULL, '2019-11-09 17:46:25', '2019-11-09 17:46:25', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(471, '202', NULL, NULL, NULL, '2019-11-09 17:46:25', '2019-11-09 17:46:25', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(472, '203', NULL, NULL, NULL, '2019-11-09 17:46:25', '2019-11-09 17:46:25', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(473, '204', NULL, NULL, NULL, '2019-11-09 17:46:26', '2019-11-09 17:46:26', NULL, 'Muhammad Mushtaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(474, '205', NULL, NULL, NULL, '2019-11-09 17:46:26', '2019-11-09 17:46:26', NULL, 'Chuadhary Talib Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(475, '206', NULL, NULL, NULL, '2019-11-09 17:46:26', '2019-11-09 17:46:26', NULL, 'Basheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(476, '207', NULL, NULL, NULL, '2019-11-09 17:46:26', '2019-11-09 17:46:26', NULL, 'Muhammad Ibrahim', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(477, '208', NULL, NULL, NULL, '2019-11-09 17:46:26', '2019-11-09 17:46:26', NULL, 'Ch Khadam Hussain Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(478, '209', NULL, NULL, NULL, '2019-11-09 17:46:26', '2019-11-09 17:46:26', NULL, 'Sardar Muhammad Abid Azeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(479, '210', NULL, NULL, NULL, '2019-11-09 17:46:26', '2019-11-09 17:46:26', NULL, 'Muhammad Yaqoob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(480, '211', NULL, NULL, NULL, '2019-11-09 17:46:26', '2019-11-09 17:46:26', NULL, 'Muhammad Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(481, '212', NULL, NULL, NULL, '2019-11-09 17:46:27', '2019-11-09 17:46:27', NULL, 'Tariq Mehmood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(482, '213', NULL, NULL, NULL, '2019-11-09 17:46:27', '2019-11-09 17:46:27', NULL, 'Mian Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(483, '214', NULL, NULL, NULL, '2019-11-09 17:46:27', '2019-11-09 17:46:27', NULL, 'Shafqaat Munir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(484, '215', NULL, NULL, NULL, '2019-11-09 17:46:27', '2019-11-09 17:46:27', NULL, 'Hashim Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(485, '216', NULL, NULL, NULL, '2019-11-09 17:46:27', '2019-11-09 17:46:27', NULL, 'Umer Farooq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(486, '217', NULL, NULL, NULL, '2019-11-09 17:46:27', '2019-11-09 17:46:27', NULL, 'Muhammad Sarwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(487, '218', NULL, NULL, NULL, '2019-11-09 17:46:27', '2019-11-09 17:46:27', NULL, 'Rana Muhammad Sarwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(488, '219', NULL, NULL, NULL, '2019-11-09 17:46:28', '2019-11-09 17:46:28', NULL, 'Riaz Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(489, '220', NULL, NULL, NULL, '2019-11-09 17:46:28', '2019-11-09 17:46:28', NULL, 'Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(490, '221', NULL, NULL, NULL, '2019-11-09 17:46:28', '2019-11-09 17:46:28', NULL, 'Zahoor Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(491, '222', NULL, NULL, NULL, '2019-11-09 17:46:28', '2019-11-09 17:46:28', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(492, '223', NULL, NULL, NULL, '2019-11-09 17:46:29', '2019-11-09 17:46:29', NULL, 'Shafqat Munir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(493, '224', NULL, NULL, NULL, '2019-11-09 17:46:29', '2019-11-09 17:46:29', NULL, 'Muhammad Ashiq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(494, '225', NULL, NULL, NULL, '2019-11-09 17:46:29', '2019-11-09 17:46:29', NULL, 'Muhammad Jamil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(495, '226', NULL, NULL, NULL, '2019-11-09 17:46:29', '2019-11-09 17:46:29', NULL, 'Muhammad Sharif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(496, '227', NULL, NULL, NULL, '2019-11-09 17:46:29', '2019-11-09 17:46:29', NULL, 'Maqbool Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(497, '228', NULL, NULL, NULL, '2019-11-09 17:46:29', '2019-11-09 17:46:29', NULL, 'Inayat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(498, '229', NULL, NULL, NULL, '2019-11-09 17:46:30', '2019-11-09 17:46:30', NULL, 'Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(499, '230', NULL, NULL, NULL, '2019-11-09 17:46:30', '2019-11-09 17:46:30', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(500, '231', NULL, NULL, NULL, '2019-11-09 17:46:30', '2019-11-09 17:46:30', NULL, 'Sheikh Naseer ud din Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(501, '232', NULL, NULL, NULL, '2019-11-09 17:46:30', '2019-11-09 17:46:30', NULL, 'Abdul Razaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(502, '233', NULL, NULL, NULL, '2019-11-09 17:46:30', '2019-11-09 17:46:30', NULL, 'Dr.Zafar Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(503, '234', NULL, NULL, NULL, '2019-11-09 17:46:30', '2019-11-09 17:46:30', NULL, 'Ch.Mukhtar Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(504, '235', NULL, NULL, NULL, '2019-11-09 17:46:31', '2019-11-09 17:46:31', NULL, 'Rao Noor Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(505, '236', NULL, NULL, NULL, '2019-11-09 17:46:31', '2019-11-09 17:46:31', NULL, 'Ch. Noor Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(506, '237', NULL, NULL, NULL, '2019-11-09 17:46:31', '2019-11-09 17:46:31', NULL, 'Ch Irshad Ahmad Arian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `members_pay_records` (`id`, `member_id`, `month`, `monthly_payment`, `Unique_id`, `created_at`, `updated_at`, `name`, `father_name`, `mobile_number`, `memberShip_fee`, `registration_fee`, `Aug`, `Sep`, `Oct`, `instrument_No_105000`, `instrument_No_100000`, `instrument_No_55000`, `instrument_No_50000`, `insturment_No_5000`, `insturment_No_32500`, `Instrument_No_Aug`, `Instrument_No_Sep`, `Instrument_No_Oct`, `Status`, `Comment`, `Date`) VALUES
(507, '238', NULL, NULL, NULL, '2019-11-09 17:46:31', '2019-11-09 17:46:31', NULL, 'Ch Sardar Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(508, '239', NULL, NULL, NULL, '2019-11-09 17:46:31', '2019-11-09 17:46:31', NULL, 'Malik Muhammad Yousaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(509, '240', NULL, NULL, NULL, '2019-11-09 17:46:31', '2019-11-09 17:46:31', NULL, 'Ch Riaz Ul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(510, '241', NULL, NULL, NULL, '2019-11-09 17:46:31', '2019-11-09 17:46:31', NULL, 'Haji Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(511, '242', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, 'Haji Muhammad Waryam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(512, '243', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, 'Muhammad Saleem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(513, '244', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, 'Muhammad Saeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(514, '245', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, 'Abdul Gaffar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(515, '246', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, 'Muhammad Azeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(516, '247', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, 'Muhammad Sharif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(517, '248', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, 'Abdul Latif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(518, '249', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, 'Ch. Abdul khaliq ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(519, '250', NULL, NULL, NULL, '2019-11-09 17:46:32', '2019-11-09 17:46:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(520, '251', NULL, NULL, NULL, '2019-11-09 17:46:33', '2019-11-09 17:46:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(521, '252', NULL, NULL, NULL, '2019-11-09 17:46:33', '2019-11-09 17:46:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(522, '253', NULL, NULL, NULL, '2019-11-09 17:46:33', '2019-11-09 17:46:33', NULL, 'Muhammad Akram ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(523, '254', NULL, NULL, NULL, '2019-11-09 17:46:33', '2019-11-09 17:46:33', NULL, 'Ghulam Sarwar Ch.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(524, '255', NULL, NULL, NULL, '2019-11-09 17:46:34', '2019-11-09 17:46:34', NULL, 'Adbul Hameed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(525, '256', NULL, NULL, NULL, '2019-11-09 17:46:34', '2019-11-09 17:46:34', NULL, 'Muhammad Abdullah khan Asem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(526, '257', NULL, NULL, NULL, '2019-11-09 17:46:34', '2019-11-09 17:46:34', NULL, 'Muhammad Yaqoob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(527, '258', NULL, NULL, NULL, '2019-11-09 17:46:35', '2019-11-09 17:46:35', NULL, 'Zafar Hussain ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(528, '259', NULL, NULL, NULL, '2019-11-09 17:46:35', '2019-11-09 17:46:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(529, '260', NULL, NULL, NULL, '2019-11-09 17:46:35', '2019-11-09 17:46:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(530, NULL, NULL, NULL, NULL, '2019-11-09 17:46:35', '2019-11-09 17:46:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(531, '1', NULL, NULL, NULL, '2019-11-09 17:46:36', '2019-11-09 17:46:36', NULL, 'Ahmad Waseem Sheikh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(533, '3', NULL, NULL, NULL, '2019-11-09 17:46:36', '2019-11-09 17:46:36', NULL, 'Chudhary Muhammad Jamil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(534, '4', NULL, NULL, NULL, '2019-11-09 17:46:36', '2019-11-09 17:46:36', NULL, 'Muhammad Ud din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(535, '5', NULL, NULL, NULL, '2019-11-09 17:46:37', '2019-11-09 17:46:37', NULL, 'Maqsood Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(536, '6', NULL, NULL, NULL, '2019-11-09 17:46:37', '2019-11-09 17:46:37', NULL, 'Muhammad Bukhtiar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(537, '7', NULL, NULL, NULL, '2019-11-09 17:46:37', '2019-11-09 17:46:37', NULL, 'Marghoob Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(538, '8', NULL, NULL, NULL, '2019-11-09 17:46:37', '2019-11-09 17:46:37', NULL, 'Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(539, '9', NULL, NULL, NULL, '2019-11-09 17:46:37', '2019-11-09 17:46:37', NULL, 'Chuadhary Mukhtayar Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(540, '10', NULL, NULL, NULL, '2019-11-09 17:46:37', '2019-11-09 17:46:37', NULL, 'Abdul Shakoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(541, '11', NULL, NULL, NULL, '2019-11-09 17:46:38', '2019-11-09 17:46:38', NULL, 'Haji Usman Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(542, '12', NULL, NULL, NULL, '2019-11-09 17:46:38', '2019-11-09 17:46:38', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(543, '13', NULL, NULL, NULL, '2019-11-09 17:46:38', '2019-11-09 17:46:38', NULL, 'Muhammad Nadeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(544, '14', NULL, NULL, NULL, '2019-11-09 17:46:38', '2019-11-09 17:46:38', NULL, 'Syed Soulat Hussain Naqvi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(545, '15', NULL, NULL, NULL, '2019-11-09 17:46:38', '2019-11-09 17:46:38', NULL, 'Maghoob Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(546, '16', NULL, NULL, NULL, '2019-11-09 17:46:39', '2019-11-09 17:46:39', NULL, 'Syed Mehdi Hassan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(547, '17', NULL, NULL, NULL, '2019-11-09 17:46:39', '2019-11-09 17:46:39', NULL, 'Muhammad Mateen Zaman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(548, '18', NULL, NULL, NULL, '2019-11-09 17:46:39', '2019-11-09 17:46:39', NULL, 'Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(549, '19', NULL, NULL, NULL, '2019-11-09 17:46:39', '2019-11-09 17:46:39', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(550, '20', NULL, NULL, NULL, '2019-11-09 17:46:39', '2019-11-09 17:46:39', NULL, 'Masood Ahmad Kahloon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(551, '21', NULL, NULL, NULL, '2019-11-09 17:46:39', '2019-11-09 17:46:39', NULL, 'Abdul Razaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(552, '22', NULL, NULL, NULL, '2019-11-09 17:46:40', '2019-11-09 17:46:40', NULL, 'Muhammad Aslam ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(553, '23', NULL, NULL, NULL, '2019-11-09 17:46:40', '2019-11-09 17:46:40', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(554, '24', NULL, NULL, NULL, '2019-11-09 17:46:40', '2019-11-09 17:46:40', NULL, 'Ch Amin wahla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(555, '25', NULL, NULL, NULL, '2019-11-09 17:46:40', '2019-11-09 17:46:40', NULL, 'Muhammad Iqbal Saleemi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(556, '26', NULL, NULL, NULL, '2019-11-09 17:46:40', '2019-11-09 17:46:40', NULL, 'Sheikh Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(557, '27', NULL, NULL, NULL, '2019-11-09 17:46:40', '2019-11-09 17:46:40', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(558, '28', NULL, NULL, NULL, '2019-11-09 17:46:41', '2019-11-09 17:46:41', NULL, 'Abdul Ghaffar Butt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(559, '29', NULL, NULL, NULL, '2019-11-09 17:46:42', '2019-11-09 17:46:42', NULL, 'Chudhary Ali Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(560, '30', NULL, NULL, NULL, '2019-11-09 17:46:42', '2019-11-09 17:46:42', NULL, 'Abdul Razzaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(561, '31', NULL, NULL, NULL, '2019-11-09 17:46:43', '2019-11-09 17:46:43', NULL, 'Abdul Ghafoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(562, '32', NULL, NULL, NULL, '2019-11-09 17:46:43', '2019-11-09 17:46:43', NULL, 'Haji Muhammad Raza ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(563, '33', NULL, NULL, NULL, '2019-11-09 17:46:43', '2019-11-09 17:46:43', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(564, '34', NULL, NULL, NULL, '2019-11-09 17:46:43', '2019-11-09 17:46:43', NULL, 'Haji Muhammad Raza ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(565, '35', NULL, NULL, NULL, '2019-11-09 17:46:43', '2019-11-09 17:46:43', NULL, 'Khalid Farooq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(566, '36', NULL, NULL, NULL, '2019-11-09 17:46:44', '2019-11-09 17:46:44', NULL, 'Muhammad Ayub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(567, '37', NULL, NULL, NULL, '2019-11-09 17:46:44', '2019-11-09 17:46:44', NULL, 'Amanat Ali Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(568, '38', NULL, NULL, NULL, '2019-11-09 17:46:44', '2019-11-09 17:46:44', NULL, 'Ch Abdul Sattar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(569, '39', NULL, NULL, NULL, '2019-11-09 17:46:44', '2019-11-09 17:46:44', NULL, 'Muhammad Akbar Wahla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(570, '40', NULL, NULL, NULL, '2019-11-09 17:46:44', '2019-11-09 17:46:44', NULL, 'Chuadhary Abdul Ghanni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(571, '41', NULL, NULL, NULL, '2019-11-09 17:46:45', '2019-11-09 17:46:45', NULL, 'Ameer Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(572, '42', NULL, NULL, NULL, '2019-11-09 17:46:45', '2019-11-09 17:46:45', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(573, '43', NULL, NULL, NULL, '2019-11-09 17:46:46', '2019-11-09 17:46:46', NULL, 'Muhammad Din Sheikh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(574, '44', NULL, NULL, NULL, '2019-11-09 17:46:46', '2019-11-09 17:46:46', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(575, '45', NULL, NULL, NULL, '2019-11-09 17:46:46', '2019-11-09 17:46:46', NULL, 'Atta Ur Rehman Maqsood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(576, '46', NULL, NULL, NULL, '2019-11-09 17:46:46', '2019-11-09 17:46:46', NULL, 'Chudhary Khusi Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(577, '47', NULL, NULL, NULL, '2019-11-09 17:46:47', '2019-11-09 17:46:47', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(578, '48', NULL, NULL, NULL, '2019-11-09 17:46:47', '2019-11-09 17:46:47', NULL, 'Ghulam Mustafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(579, '49', NULL, NULL, NULL, '2019-11-09 17:46:47', '2019-11-09 17:46:47', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(580, '50', NULL, NULL, NULL, '2019-11-09 17:46:47', '2019-11-09 17:46:47', NULL, 'Maqsood Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(581, '151', NULL, NULL, NULL, '2019-11-09 17:46:47', '2019-11-09 17:46:47', NULL, 'Chaudhary Muhammad Sabir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(582, '52', NULL, NULL, NULL, '2019-11-09 17:46:48', '2019-11-09 17:46:48', NULL, 'Muhammad Suleman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(583, '53', NULL, NULL, NULL, '2019-11-09 17:46:48', '2019-11-09 17:46:48', NULL, 'Abdul Ghafoor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(584, '54', NULL, NULL, NULL, '2019-11-09 17:46:48', '2019-11-09 17:46:48', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(585, '55', NULL, NULL, NULL, '2019-11-09 17:46:48', '2019-11-09 17:46:48', NULL, 'Muhammad Hanif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(586, '56', NULL, NULL, NULL, '2019-11-09 17:46:48', '2019-11-09 17:46:48', NULL, 'Muhammad Sadique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(587, '57', NULL, NULL, NULL, '2019-11-09 17:46:49', '2019-11-09 17:46:49', NULL, 'Rao Irshad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(588, '58', NULL, NULL, NULL, '2019-11-09 17:46:49', '2019-11-09 17:46:49', NULL, 'Chaudhary Muhammad Aslam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(589, '59', NULL, NULL, NULL, '2019-11-09 17:46:49', '2019-11-09 17:46:49', NULL, 'Nazir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(590, '60', NULL, NULL, NULL, '2019-11-09 17:46:49', '2019-11-09 17:46:49', NULL, 'Rafique Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(591, '61', NULL, NULL, NULL, '2019-11-09 17:46:49', '2019-11-09 17:46:49', NULL, 'Sardar Khalil Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(592, '62', NULL, NULL, NULL, '2019-11-09 17:46:49', '2019-11-09 17:46:49', NULL, 'Rao Nazir Ahmed Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(593, '63', NULL, NULL, NULL, '2019-11-09 17:46:49', '2019-11-09 17:46:49', NULL, 'Muhammad Ramzan Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(594, '64', NULL, NULL, NULL, '2019-11-09 17:46:50', '2019-11-09 17:46:50', NULL, 'Gul Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(595, '65', NULL, NULL, NULL, '2019-11-09 17:46:50', '2019-11-09 17:46:50', NULL, 'Ghulam Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(596, '66', NULL, NULL, NULL, '2019-11-09 17:46:50', '2019-11-09 17:46:50', NULL, 'Hashmat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(597, '67', NULL, NULL, NULL, '2019-11-09 17:46:50', '2019-11-09 17:46:50', NULL, 'Ch Sher Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(598, '68', NULL, NULL, NULL, '2019-11-09 17:46:51', '2019-11-09 17:46:51', NULL, 'Abdul Ghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(599, '69', NULL, NULL, NULL, '2019-11-09 17:46:51', '2019-11-09 17:46:51', NULL, 'Sheikh Muhammad Nawaz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(600, '70', NULL, NULL, NULL, '2019-11-09 17:46:51', '2019-11-09 17:46:51', NULL, 'Wali Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(601, '71', NULL, NULL, NULL, '2019-11-09 17:46:51', '2019-11-09 17:46:51', NULL, 'Bashir Ahmad Ghumman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(602, '72', NULL, NULL, NULL, '2019-11-09 17:46:51', '2019-11-09 17:46:51', NULL, 'Muhammad Arif Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(603, '73', NULL, NULL, NULL, '2019-11-09 17:46:51', '2019-11-09 17:46:51', NULL, 'Muhammad Hanif Ghumman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(604, '74', NULL, NULL, NULL, '2019-11-09 17:46:51', '2019-11-09 17:46:51', NULL, 'Muhammad Arif Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(605, '75', NULL, NULL, NULL, '2019-11-09 17:46:52', '2019-11-09 17:46:52', NULL, 'Rukan Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(606, '76', NULL, NULL, NULL, '2019-11-09 17:46:52', '2019-11-09 17:46:52', NULL, 'Muhammad Bilal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(607, '77', NULL, NULL, NULL, '2019-11-09 17:46:52', '2019-11-09 17:46:52', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(608, '78', NULL, NULL, NULL, '2019-11-09 17:46:52', '2019-11-09 17:46:52', NULL, 'Naseer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(609, '79', NULL, NULL, NULL, '2019-11-09 17:46:52', '2019-11-09 17:46:52', NULL, 'Muhammad Abdur Ruaf Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(610, '80', NULL, NULL, NULL, '2019-11-09 17:46:52', '2019-11-09 17:46:52', NULL, 'Sardar Ghulam Farid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(611, '81', NULL, NULL, NULL, '2019-11-09 17:46:53', '2019-11-09 17:46:53', NULL, 'Kareem Dad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(612, '82', NULL, NULL, NULL, '2019-11-09 17:46:53', '2019-11-09 17:46:53', NULL, 'Muhammad Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(613, '83', NULL, NULL, NULL, '2019-11-09 17:46:53', '2019-11-09 17:46:53', NULL, 'Rasheed Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(614, '84', NULL, NULL, NULL, '2019-11-09 17:46:53', '2019-11-09 17:46:53', NULL, 'Nazir Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(615, '85', NULL, NULL, NULL, '2019-11-09 17:46:53', '2019-11-09 17:46:53', NULL, 'Muhammad Bota', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(616, '86', NULL, NULL, NULL, '2019-11-09 17:46:54', '2019-11-09 17:46:54', NULL, 'Abdul Ghani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(617, '87', NULL, NULL, NULL, '2019-11-09 17:46:54', '2019-11-09 17:46:54', NULL, 'Muhammad Asghar Sabir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(618, '88', NULL, NULL, NULL, '2019-11-09 17:46:54', '2019-11-09 17:46:54', NULL, 'Qazi Maqsood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(619, '89', NULL, NULL, NULL, '2019-11-09 17:46:54', '2019-11-09 17:46:54', NULL, 'Muhammad Shafi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(620, '90', NULL, NULL, NULL, '2019-11-09 17:46:55', '2019-11-09 17:46:55', NULL, 'Muhammad Munir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(621, '91', NULL, NULL, NULL, '2019-11-09 17:46:55', '2019-11-09 17:46:55', NULL, 'Nawab Ali Javed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(622, '92', NULL, NULL, NULL, '2019-11-09 17:46:55', '2019-11-09 17:46:55', NULL, 'Muhammad Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(623, '93', NULL, NULL, NULL, '2019-11-09 17:46:56', '2019-11-09 17:46:56', NULL, 'Khan Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(624, '94', NULL, NULL, NULL, '2019-11-09 17:46:56', '2019-11-09 17:46:56', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(625, '95', NULL, NULL, NULL, '2019-11-09 17:46:56', '2019-11-09 17:46:56', NULL, 'Dr.Muhammad Aslam Awan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(626, '96', NULL, NULL, NULL, '2019-11-09 17:46:56', '2019-11-09 17:46:56', NULL, 'Abul Razzaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(627, '97', NULL, NULL, NULL, '2019-11-09 17:46:56', '2019-11-09 17:46:56', NULL, 'Ghulam Nabi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(628, '98', NULL, NULL, NULL, '2019-11-09 17:46:56', '2019-11-09 17:46:56', NULL, 'Niaz Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(629, '99', NULL, NULL, NULL, '2019-11-09 17:46:57', '2019-11-09 17:46:57', NULL, 'Muhammad Ayub khan saldera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(630, '100', NULL, NULL, NULL, '2019-11-09 17:46:57', '2019-11-09 17:46:57', NULL, 'Akbar Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(631, '101', NULL, NULL, NULL, '2019-11-09 17:46:57', '2019-11-09 17:46:57', NULL, 'Ch.Asmat saeed Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(632, '102', NULL, NULL, NULL, '2019-11-09 17:46:57', '2019-11-09 17:46:57', NULL, 'Asmat Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(633, '103', NULL, NULL, NULL, '2019-11-09 17:46:57', '2019-11-09 17:46:57', NULL, 'Sardar Muhammad Sajjad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(634, '104', NULL, NULL, NULL, '2019-11-09 17:46:57', '2019-11-09 17:46:57', NULL, 'Raja Muhammad Akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(635, '105', NULL, NULL, NULL, '2019-11-09 17:46:58', '2019-11-09 17:46:58', NULL, 'Sardar Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(636, '106', NULL, NULL, NULL, '2019-11-09 17:46:58', '2019-11-09 17:46:58', NULL, 'Rehmat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(637, '107', NULL, NULL, NULL, '2019-11-09 17:46:58', '2019-11-09 17:46:58', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(638, '108', NULL, NULL, NULL, '2019-11-09 17:46:58', '2019-11-09 17:46:58', NULL, 'Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(639, '109', NULL, NULL, NULL, '2019-11-09 17:46:58', '2019-11-09 17:46:58', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(640, '110', NULL, NULL, NULL, '2019-11-09 17:46:58', '2019-11-09 17:46:58', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(641, '111', NULL, NULL, NULL, '2019-11-09 17:46:58', '2019-11-09 17:46:58', NULL, 'Abdul Majeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(642, '112', NULL, NULL, NULL, '2019-11-09 17:46:58', '2019-11-09 17:46:58', NULL, 'Muhammad Aslam Naseem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(643, '113', NULL, NULL, NULL, '2019-11-09 17:46:59', '2019-11-09 17:46:59', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(644, '114', NULL, NULL, NULL, '2019-11-09 17:46:59', '2019-11-09 17:46:59', NULL, 'Muhammad Ishaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(645, '115', NULL, NULL, NULL, '2019-11-09 17:46:59', '2019-11-09 17:46:59', NULL, 'Muhammad Tufail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(646, '116', NULL, NULL, NULL, '2019-11-09 17:46:59', '2019-11-09 17:46:59', NULL, 'Syed Riaz Hussain Rizvi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(647, '117', NULL, NULL, NULL, '2019-11-09 17:46:59', '2019-11-09 17:46:59', NULL, 'Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(648, '118', NULL, NULL, NULL, '2019-11-09 17:46:59', '2019-11-09 17:46:59', NULL, 'Muhammad  Arshad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(649, '119', NULL, NULL, NULL, '2019-11-09 17:46:59', '2019-11-09 17:46:59', NULL, 'Muhammad Yaseen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(650, '120', NULL, NULL, NULL, '2019-11-09 17:46:59', '2019-11-09 17:46:59', NULL, 'Javed Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(651, '121', NULL, NULL, NULL, '2019-11-09 17:47:00', '2019-11-09 17:47:00', NULL, 'Dawood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(652, '122', NULL, NULL, NULL, '2019-11-09 17:47:00', '2019-11-09 17:47:00', NULL, 'Faqir Muhammad Chaudhary', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(653, '123', NULL, NULL, NULL, '2019-11-09 17:47:00', '2019-11-09 17:47:00', NULL, 'Chudhary KhusiMuhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(654, '124', NULL, NULL, NULL, '2019-11-09 17:47:00', '2019-11-09 17:47:00', NULL, 'Muhammad Akram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(655, '125', NULL, NULL, NULL, '2019-11-09 17:47:00', '2019-11-09 17:47:00', NULL, 'Chadhary Asmat Saeed Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(656, '126', NULL, NULL, NULL, '2019-11-09 17:47:00', '2019-11-09 17:47:00', NULL, 'Rao Irshad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(657, '127', NULL, NULL, NULL, '2019-11-09 17:47:00', '2019-11-09 17:47:00', NULL, 'Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(658, '128', NULL, NULL, NULL, '2019-11-09 17:47:01', '2019-11-09 17:47:01', NULL, 'Muhammad Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(659, '129', NULL, NULL, NULL, '2019-11-09 17:47:01', '2019-11-09 17:47:01', NULL, 'Muhammad Mansha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(660, '130', NULL, NULL, NULL, '2019-11-09 17:47:01', '2019-11-09 17:47:01', NULL, 'Muhammad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(661, '131', NULL, NULL, NULL, '2019-11-09 17:47:01', '2019-11-09 17:47:01', NULL, 'Abdul Raheem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(662, '132', NULL, NULL, NULL, '2019-11-09 17:47:01', '2019-11-09 17:47:01', NULL, 'Ghulam Mustafa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(663, '133', NULL, NULL, NULL, '2019-11-09 17:47:01', '2019-11-09 17:47:01', NULL, 'Malik Karamat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(664, '134', NULL, NULL, NULL, '2019-11-09 17:47:02', '2019-11-09 17:47:02', NULL, 'Naseer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(665, '135', NULL, NULL, NULL, '2019-11-09 17:47:02', '2019-11-09 17:47:02', NULL, 'Rao Khan Bahadar Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(666, '136', NULL, NULL, NULL, '2019-11-09 17:47:02', '2019-11-09 17:47:02', NULL, 'Mian Muhammad Rafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(667, '137', NULL, NULL, NULL, '2019-11-09 17:47:02', '2019-11-09 17:47:02', NULL, 'Zulfiqar Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(668, '138', NULL, NULL, NULL, '2019-11-09 17:47:02', '2019-11-09 17:47:02', NULL, 'Mukhtar Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(669, '139', NULL, NULL, NULL, '2019-11-09 17:47:02', '2019-11-09 17:47:02', NULL, 'Asgher Ali Sandhu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(670, '140', NULL, NULL, NULL, '2019-11-09 17:47:02', '2019-11-09 17:47:02', NULL, 'Khushi Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(671, '141', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Muhammad Shaffi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(672, '142', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Siraj Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(673, '143', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Raja Ghulam Haider Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(674, '144', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Muhammad Ramzan Bhatti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(675, '145', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Konwar Akbar Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(676, '146', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Khalid Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(677, '147', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Abdur Rehman ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(678, '148', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Muhammad Amin Chisti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(679, '149', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Muhammad Tufail', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(680, '150', NULL, NULL, NULL, '2019-11-09 17:47:03', '2019-11-09 17:47:03', NULL, 'Chadhary Muhammad Saleem Kahloon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(681, '51', NULL, NULL, NULL, '2019-11-09 17:47:04', '2019-11-09 17:47:04', NULL, 'Atta Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(682, '152', NULL, NULL, NULL, '2019-11-09 17:47:04', '2019-11-09 17:47:04', NULL, 'Muhammad Akbar khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(683, '153', NULL, NULL, NULL, '2019-11-09 17:47:04', '2019-11-09 17:47:04', NULL, 'Muhammad Akbar khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(684, '154', NULL, NULL, NULL, '2019-11-09 17:47:04', '2019-11-09 17:47:04', NULL, 'Muhammad Amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(685, '155', NULL, NULL, NULL, '2019-11-09 17:47:05', '2019-11-09 17:47:05', NULL, 'Faqir Muhammad ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(686, '156', NULL, NULL, NULL, '2019-11-09 17:47:05', '2019-11-09 17:47:05', NULL, 'Liaqat Ali Rasal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(687, '157', NULL, NULL, NULL, '2019-11-09 17:47:05', '2019-11-09 17:47:05', NULL, 'Muhammad Khaliq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(688, '158', NULL, NULL, NULL, '2019-11-09 17:47:05', '2019-11-09 17:47:05', NULL, 'Malik Nazar Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(689, '159', NULL, NULL, NULL, '2019-11-09 17:47:05', '2019-11-09 17:47:05', NULL, 'Muhammad Sharif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(690, '160', NULL, NULL, NULL, '2019-11-09 17:47:05', '2019-11-09 17:47:05', NULL, 'Abdul Majid ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(691, '161', NULL, NULL, NULL, '2019-11-09 17:47:05', '2019-11-09 17:47:05', NULL, 'Shaukat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(692, '162', NULL, NULL, NULL, '2019-11-09 17:47:05', '2019-11-09 17:47:05', NULL, 'Muhammad Yousaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(693, '163', NULL, NULL, NULL, '2019-11-09 17:47:06', '2019-11-09 17:47:06', NULL, 'Ch Rehmat Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(694, '164', NULL, NULL, NULL, '2019-11-09 17:47:06', '2019-11-09 17:47:06', NULL, 'Shahid Mehmood Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(695, '165', NULL, NULL, NULL, '2019-11-09 17:47:06', '2019-11-09 17:47:06', NULL, 'Khalid Mehmood Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(696, '166', NULL, NULL, NULL, '2019-11-09 17:47:06', '2019-11-09 17:47:06', NULL, 'Ahmad Naseem Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(697, '167', NULL, NULL, NULL, '2019-11-09 17:47:06', '2019-11-09 17:47:06', NULL, 'Nabardar Riaz Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(698, '168', NULL, NULL, NULL, '2019-11-09 17:47:06', '2019-11-09 17:47:06', NULL, 'Fateh Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(699, '169', NULL, NULL, NULL, '2019-11-09 17:47:06', '2019-11-09 17:47:06', NULL, 'Dilmeer Ahmed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(700, '170', NULL, NULL, NULL, '2019-11-09 17:47:07', '2019-11-09 17:47:07', NULL, 'Muhammad Shafique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(701, '171', NULL, NULL, NULL, '2019-11-09 17:47:07', '2019-11-09 17:47:07', NULL, 'Sh.Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(702, '172', NULL, NULL, NULL, '2019-11-09 17:47:07', '2019-11-09 17:47:07', NULL, 'Muhammad ishtiaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(703, '173', NULL, NULL, NULL, '2019-11-09 17:47:07', '2019-11-09 17:47:07', NULL, 'Muhammad Arshad Shah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(704, '174', NULL, NULL, NULL, '2019-11-09 17:47:07', '2019-11-09 17:47:07', NULL, 'Muhammad Rafique Dogar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(705, '175', NULL, NULL, NULL, '2019-11-09 17:47:07', '2019-11-09 17:47:07', NULL, 'Muhammad Habib Ullah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(706, '176', NULL, NULL, NULL, '2019-11-09 17:47:07', '2019-11-09 17:47:07', NULL, 'Nazir Ahmad Mehmood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(707, '177', NULL, NULL, NULL, '2019-11-09 17:47:07', '2019-11-09 17:47:07', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(708, '178', NULL, NULL, NULL, '2019-11-09 17:47:08', '2019-11-09 17:47:08', NULL, 'Taj Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(709, '179', NULL, NULL, NULL, '2019-11-09 17:47:08', '2019-11-09 17:47:08', NULL, 'Haji Muhammad ishaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(710, '180', NULL, NULL, NULL, '2019-11-09 17:47:08', '2019-11-09 17:47:08', NULL, 'Mian Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(711, '181', NULL, NULL, NULL, '2019-11-09 17:47:08', '2019-11-09 17:47:08', NULL, 'Muhammad Hanif', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(712, '182', NULL, NULL, NULL, '2019-11-09 17:47:08', '2019-11-09 17:47:08', NULL, 'Malik Muhammad Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(713, '183', NULL, NULL, NULL, '2019-11-09 17:47:08', '2019-11-09 17:47:08', NULL, 'Muhammad Sadique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(714, '184', NULL, NULL, NULL, '2019-11-09 17:47:08', '2019-11-09 17:47:08', NULL, 'Muhammad Ashraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(715, '185', NULL, NULL, NULL, '2019-11-09 17:47:08', '2019-11-09 17:47:08', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(716, '186', NULL, NULL, NULL, '2019-11-09 17:47:09', '2019-11-09 17:47:09', NULL, 'Liaqat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(717, '187', NULL, NULL, NULL, '2019-11-09 17:47:09', '2019-11-09 17:47:09', NULL, 'Muhammad Anwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(718, '188', NULL, NULL, NULL, '2019-11-09 17:47:09', '2019-11-09 17:47:09', NULL, 'Muhammmad Sardar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(719, '189', NULL, NULL, NULL, '2019-11-09 17:47:09', '2019-11-09 17:47:09', NULL, 'Muhammad Sarwar ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(720, '190', NULL, NULL, NULL, '2019-11-09 17:47:09', '2019-11-09 17:47:09', NULL, 'Ch.Faqir Muhammad Arian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(721, '191', NULL, NULL, NULL, '2019-11-09 17:47:09', '2019-11-09 17:47:09', NULL, 'Muhammad Idrees', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(722, '192', NULL, NULL, NULL, '2019-11-09 17:47:09', '2019-11-09 17:47:09', NULL, 'Ghulam Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(723, '193', NULL, NULL, NULL, '2019-11-09 17:47:09', '2019-11-09 17:47:09', NULL, 'Muhammad Akhtar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(724, '194', NULL, NULL, NULL, '2019-11-09 17:47:10', '2019-11-09 17:47:10', NULL, 'Muhammad Mueez ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(725, '195', NULL, NULL, NULL, '2019-11-09 17:47:10', '2019-11-09 17:47:10', NULL, 'Nazeer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(726, '196', NULL, NULL, NULL, '2019-11-09 17:47:10', '2019-11-09 17:47:10', NULL, 'Muhammad Shafi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(727, '197', NULL, NULL, NULL, '2019-11-09 17:47:10', '2019-11-09 17:47:10', NULL, 'Abdul Rehman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(728, '198', NULL, NULL, NULL, '2019-11-09 17:47:10', '2019-11-09 17:47:10', NULL, 'Abdul Majeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(729, '199', NULL, NULL, NULL, '2019-11-09 17:47:10', '2019-11-09 17:47:10', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(730, '200', NULL, NULL, NULL, '2019-11-09 17:47:10', '2019-11-09 17:47:10', NULL, 'Muhammad Irshad Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(731, '201', NULL, NULL, NULL, '2019-11-09 17:47:11', '2019-11-09 17:47:11', NULL, 'Mushtaq Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(732, '202', NULL, NULL, NULL, '2019-11-09 17:47:11', '2019-11-09 17:47:11', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(733, '203', NULL, NULL, NULL, '2019-11-09 17:47:11', '2019-11-09 17:47:11', NULL, 'Bashir Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(734, '204', NULL, NULL, NULL, '2019-11-09 17:47:11', '2019-11-09 17:47:11', NULL, 'Muhammad Mushtaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(735, '205', NULL, NULL, NULL, '2019-11-09 17:47:11', '2019-11-09 17:47:11', NULL, 'Chuadhary Talib Hussain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(736, '206', NULL, NULL, NULL, '2019-11-09 17:47:11', '2019-11-09 17:47:11', NULL, 'Basheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(737, '207', NULL, NULL, NULL, '2019-11-09 17:47:11', '2019-11-09 17:47:11', NULL, 'Muhammad Ibrahim', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(738, '208', NULL, NULL, NULL, '2019-11-09 17:47:11', '2019-11-09 17:47:11', NULL, 'Ch Khadam Hussain Bajwa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(739, '209', NULL, NULL, NULL, '2019-11-09 17:47:12', '2019-11-09 17:47:12', NULL, 'Sardar Muhammad Abid Azeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(740, '210', NULL, NULL, NULL, '2019-11-09 17:47:12', '2019-11-09 17:47:12', NULL, 'Muhammad Yaqoob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(741, '211', NULL, NULL, NULL, '2019-11-09 17:47:12', '2019-11-09 17:47:12', NULL, 'Muhammad Din', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(742, '212', NULL, NULL, NULL, '2019-11-09 17:47:12', '2019-11-09 17:47:12', NULL, 'Tariq Mehmood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(743, '213', NULL, NULL, NULL, '2019-11-09 17:47:12', '2019-11-09 17:47:12', NULL, 'Mian Khan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `members_pay_records` (`id`, `member_id`, `month`, `monthly_payment`, `Unique_id`, `created_at`, `updated_at`, `name`, `father_name`, `mobile_number`, `memberShip_fee`, `registration_fee`, `Aug`, `Sep`, `Oct`, `instrument_No_105000`, `instrument_No_100000`, `instrument_No_55000`, `instrument_No_50000`, `insturment_No_5000`, `insturment_No_32500`, `Instrument_No_Aug`, `Instrument_No_Sep`, `Instrument_No_Oct`, `Status`, `Comment`, `Date`) VALUES
(744, '214', NULL, NULL, NULL, '2019-11-09 17:47:12', '2019-11-09 17:47:12', NULL, 'Shafqaat Munir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(745, '215', NULL, NULL, NULL, '2019-11-09 17:47:12', '2019-11-09 17:47:12', NULL, 'Hashim Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(746, '216', NULL, NULL, NULL, '2019-11-09 17:47:12', '2019-11-09 17:47:12', NULL, 'Umer Farooq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(747, '217', NULL, NULL, NULL, '2019-11-09 17:47:13', '2019-11-09 17:47:13', NULL, 'Muhammad Sarwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(748, '218', NULL, NULL, NULL, '2019-11-09 17:47:13', '2019-11-09 17:47:13', NULL, 'Rana Muhammad Sarwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(749, '219', NULL, NULL, NULL, '2019-11-09 17:47:13', '2019-11-09 17:47:13', NULL, 'Riaz Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(750, '220', NULL, NULL, NULL, '2019-11-09 17:47:13', '2019-11-09 17:47:13', NULL, 'Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(751, '221', NULL, NULL, NULL, '2019-11-09 17:47:13', '2019-11-09 17:47:13', NULL, 'Zahoor Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(752, '222', NULL, NULL, NULL, '2019-11-09 17:47:13', '2019-11-09 17:47:13', NULL, 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(753, '223', NULL, NULL, NULL, '2019-11-09 17:47:13', '2019-11-09 17:47:13', NULL, 'Shafqat Munir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(754, '224', NULL, NULL, NULL, '2019-11-09 17:47:13', '2019-11-09 17:47:13', NULL, 'Muhammad Ashiq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(755, '225', NULL, NULL, NULL, '2019-11-09 17:47:14', '2019-11-09 17:47:14', NULL, 'Muhammad Jamil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(756, '226', NULL, NULL, NULL, '2019-11-09 17:47:14', '2019-11-09 17:47:14', NULL, 'Muhammad Sharif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(757, '227', NULL, NULL, NULL, '2019-11-09 17:47:14', '2019-11-09 17:47:14', NULL, 'Maqbool Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(758, '228', NULL, NULL, NULL, '2019-11-09 17:47:14', '2019-11-09 17:47:14', NULL, 'Inayat Ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(759, '229', NULL, NULL, NULL, '2019-11-09 17:47:14', '2019-11-09 17:47:14', NULL, 'Abdul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(760, '230', NULL, NULL, NULL, '2019-11-09 17:47:14', '2019-11-09 17:47:14', NULL, 'Muhammad Ramzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(761, '231', NULL, NULL, NULL, '2019-11-09 17:47:14', '2019-11-09 17:47:14', NULL, 'Sheikh Naseer ud din Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(762, '232', NULL, NULL, NULL, '2019-11-09 17:47:15', '2019-11-09 17:47:15', NULL, 'Abdul Razaq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(763, '233', NULL, NULL, NULL, '2019-11-09 17:47:15', '2019-11-09 17:47:15', NULL, 'Dr.Zafar Iqbal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(764, '234', NULL, NULL, NULL, '2019-11-09 17:47:15', '2019-11-09 17:47:15', NULL, 'Ch.Mukhtar Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(765, '235', NULL, NULL, NULL, '2019-11-09 17:47:15', '2019-11-09 17:47:15', NULL, 'Rao Noor Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(766, '236', NULL, NULL, NULL, '2019-11-09 17:47:15', '2019-11-09 17:47:15', NULL, 'Ch. Noor Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(767, '237', NULL, NULL, NULL, '2019-11-09 17:47:15', '2019-11-09 17:47:15', NULL, 'Ch Irshad Ahmad Arian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(768, '238', NULL, NULL, NULL, '2019-11-09 17:47:15', '2019-11-09 17:47:15', NULL, 'Ch Sardar Muhammad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(769, '239', NULL, NULL, NULL, '2019-11-09 17:47:16', '2019-11-09 17:47:16', NULL, 'Malik Muhammad Yousaf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(770, '240', NULL, NULL, NULL, '2019-11-09 17:47:16', '2019-11-09 17:47:16', NULL, 'Ch Riaz Ul Haq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(771, '241', NULL, NULL, NULL, '2019-11-09 17:47:16', '2019-11-09 17:47:16', NULL, 'Haji Zaheer Ahmad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(772, '242', NULL, NULL, NULL, '2019-11-09 17:47:16', '2019-11-09 17:47:16', NULL, 'Haji Muhammad Waryam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(773, '243', NULL, NULL, NULL, '2019-11-09 17:47:16', '2019-11-09 17:47:16', NULL, 'Muhammad Saleem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(774, '244', NULL, NULL, NULL, '2019-11-09 17:47:17', '2019-11-09 17:47:17', NULL, 'Muhammad Saeed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(775, '245', NULL, NULL, NULL, '2019-11-09 17:47:17', '2019-11-09 17:47:17', NULL, 'Abdul Gaffar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(776, '246', NULL, NULL, NULL, '2019-11-09 17:47:17', '2019-11-09 17:47:17', NULL, 'Muhammad Azeem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(777, '247', NULL, NULL, NULL, '2019-11-09 17:47:17', '2019-11-09 17:47:17', NULL, 'Muhammad Sharif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(778, '248', NULL, NULL, NULL, '2019-11-09 17:47:17', '2019-11-09 17:47:17', NULL, 'Abdul Latif ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(779, '249', NULL, NULL, NULL, '2019-11-09 17:47:17', '2019-11-09 17:47:17', NULL, 'Ch. Abdul khaliq ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(780, '250', NULL, NULL, NULL, '2019-11-09 17:47:18', '2019-11-09 17:47:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(781, '251', NULL, NULL, NULL, '2019-11-09 17:47:18', '2019-11-09 17:47:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(782, '252', NULL, NULL, NULL, '2019-11-09 17:47:18', '2019-11-09 17:47:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(783, '253', NULL, NULL, NULL, '2019-11-09 17:47:18', '2019-11-09 17:47:18', NULL, 'Muhammad Akram ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(784, '254', NULL, NULL, NULL, '2019-11-09 17:47:18', '2019-11-09 17:47:18', NULL, 'Ghulam Sarwar Ch.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(785, '255', NULL, NULL, NULL, '2019-11-09 17:47:19', '2019-11-09 17:47:19', NULL, 'Adbul Hameed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(786, '256', NULL, NULL, NULL, '2019-11-09 17:47:19', '2019-11-09 17:47:19', NULL, 'Muhammad Abdullah khan Asem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(787, '257', NULL, NULL, NULL, '2019-11-09 17:47:19', '2019-11-09 17:47:19', NULL, 'Muhammad Yaqoob', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(788, '258', NULL, NULL, NULL, '2019-11-09 17:47:19', '2019-11-09 17:47:19', NULL, 'Zafar Hussain ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(789, '259', NULL, NULL, NULL, '2019-11-09 17:47:19', '2019-11-09 17:47:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(790, '260', NULL, NULL, NULL, '2019-11-09 17:47:20', '2019-11-09 17:47:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(791, NULL, NULL, NULL, NULL, '2019-11-09 17:47:20', '2019-11-09 17:47:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members_record`
--

CREATE TABLE `members_record` (
  `membership_id` int(5) NOT NULL,
  `name` text NOT NULL,
  `father_name` text NOT NULL,
  `DOB` varchar(255) DEFAULT NULL,
  `Qualification` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MaritaLStatus` enum('Married','Single','Divorced') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Single',
  `BloodGroup` enum('A+','A-','B+','AB+','AB-','B-','O-','O+') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell_no` varchar(15) NOT NULL,
  `Cnic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Landline` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) DEFAULT 'N/A',
  `registration_fee` varchar(10) DEFAULT NULL,
  `registration_slipNo` varchar(20) DEFAULT NULL,
  `membership_fee` varchar(10) DEFAULT NULL,
  `membership_slipNo` varchar(20) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `TemporaryAddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NameOfOrganization` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Designation` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BuisnessAddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `BuisnessMobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `blandline` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Relation` enum('Wife','Son','Daughter','Mother','Father','Other') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Other',
  `SName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SDob` date DEFAULT NULL,
  `SCnic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `payments_status` enum('Paid','Unpaid') DEFAULT NULL,
  `service_charges` double(255,0) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `members_record`
--

INSERT INTO `members_record` (`membership_id`, `name`, `father_name`, `DOB`, `Qualification`, `MaritaLStatus`, `BloodGroup`, `cell_no`, `Cnic`, `email`, `Landline`, `twitter`, `registration_fee`, `registration_slipNo`, `membership_fee`, `membership_slipNo`, `category`, `Address`, `TemporaryAddress`, `NameOfOrganization`, `Designation`, `BuisnessAddress`, `BuisnessMobile`, `blandline`, `Relation`, `SName`, `SDob`, `SCnic`, `image`, `password`, `created_at`, `updated_at`, `status`, `payments_status`, `service_charges`, `remarks`) VALUES
(1, 'Hafiz Junaid Ahmad', 'Ahmad Waseem Sheikh', '1980-01-24', 'B.A', 'Married', 'A+', '0333-3022229', '36601-1602044-1', 'junaid.ahmed.vrw@gmail.com', NULL, 'N/A', '5000', '3048025', '100000', '3048047', 'new member', 'House No 13 C Lalazar Canal view Burewala Distt Vehari', 'House No 13 C Lalazar Canal view Burewala Distt Vehari', 'asas', 'xx', 'asaasas', '111111111', NULL, 'Wife', 'Fozia Ahmad', '1984-11-29', '36401-2330183-6', 'New Doc 2019-11-19 17.05.31_2.jpg', '$2y$10$2OvBHipHQ55uH/inG2magO35zkqnlA7/0QhWWOnTRIA0bBgvORt7W', NULL, '2019-11-19 13:02:51', 0, '', 1500, 'eligible'),
(2, 'Rao Ghaffar Ali', 'Rao Khurshad Ali', '1949-05-01', 'Matric', 'Married', 'AB+', '3007592902', '36601-5609021-9', 'raoghaffar@outlook.com', NULL, 'N/A', '5000', '4579468', '100000', '4609162', 'new member', 'House No 14 Strt No 2 Mujahid Colony Burewala', 'House No 14 Strt No 2 Mujahid Colony Burewala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rao Ghaffar .jpeg', NULL, NULL, '2019-11-19 11:51:28', 0, '', 1500, 'eligible'),
(4, 'Khalid Mahmood', 'Muhammad Ud din', '1967-08-21', 'Matric', 'Married', NULL, '3006999956', '36601-2873889-5', NULL, NULL, 'N/A', '5000', '3048025', '100000', '3048045', 'new member', 'House No 7 W Block Housing Sheme Burewala', 'House No 7 W Block Housing Sheme Burewala', 'Nagina Motors', 'Owner', '067-3352256', 'Lahor Road Burewala', NULL, 'Wife', 'Mubeen Kousar', '1972-03-01', '36601-2583266-2', 'Khalid Mahmood.jpg', NULL, NULL, '2019-11-19 11:43:28', 0, '', 1500, 'eligible'),
(5, 'Muhammad Aamer Maqsood', 'Maqsood Anwar', '1977-10-30', 'I.Com', 'Married', NULL, '3006999099', '36601-8432442-1', NULL, NULL, 'N/A', '5000', '3048012', '100000', '3048044', 'new member', 'House No 117 N Block Burewala', 'Houe No 117 N Block Burewala', 'Moon Rice Mills', 'Owner', '067-3770002', 'Luddan Road Burewala', NULL, 'Wife', 'Farhat AAmer', '1980-11-14', '36601-5318439-0', 'aamer maqsood.jpg', NULL, NULL, '2019-11-19 11:40:40', 0, '', 1500, 'eligible'),
(6, 'Shahbaz Mehmood', 'Muhammad Bukhtiar', '1970-01-01', 'BA', 'Married', NULL, '3006999049', '36601-5895761-7', 'tfmbrwelive@gmail.com', NULL, 'N/A', '5000', '3048021', '100000', '7222169', 'new member', 'House No 57 Faiz Abad Chichawatni Road Burewala', 'House No 214 D Block Burewala', 'Tahir Flour Mills', 'C.E.O', '067-3359533', 'Chichawatni Road burewala', NULL, 'Wife', 'Khadija Shahbaz', '1971-03-15', '36601-8481645-0', 'Shahbaz Mehmood.jpg', NULL, NULL, '2019-11-19 11:37:42', 0, '', 1500, 'eligible'),
(7, 'Mudassar Marghoob', 'Marghoob Ahmad', '1980-10-30', 'B.A', 'Married', NULL, '3007592684', '36601-2491116-9', 'Mudassamgrb@gmail.com', NULL, 'N/A', '5000', '3048026', '100000', '3048043', 'new member', 'House No 205 D Block Burewala', 'House No 205 D Block Burewala', 'Sahb Jee Fabrics', 'owner', 'Rail Bazar Burewala', '3007592684', NULL, 'Other', NULL, NULL, NULL, 'Mudassar Marghoob.jpg', NULL, NULL, '2019-11-27 20:34:44', 0, '', 1500, 'eligible'),
(8, 'Ch Muhammad Nadeem Abbas', 'Muhammad Aslam', '1969-07-12', 'F.A', 'Married', 'AB+', '3028732772', '36601-2279764-9', NULL, NULL, 'N/A', '5000', '3048027', '100000', '3048042', 'new member', 'Chak No 541 EB burewala', '72 G Block Grain Market Burewala', 'Farmer Cotton Industries Asia Fuel Point', 'Owner', '10 km Multan Road Ada Zaheer Nagar', '067-3351963', NULL, 'Wife', 'Sobia Nadeem', NULL, '36601-9890383-4', 'M.Nadeem Abbas.jpg', NULL, NULL, '2019-11-21 12:40:11', 0, '', 1500, 'eligible'),
(9, 'Ejaz Ahmad', 'Chuadhary Mukhtayar Ahmaad', '1968-05-06', NULL, 'Married', NULL, '3006999136', '36601-7275612-7', NULL, NULL, 'N/A', '5000', '3048023', '100000', '3048041', 'new member', 'House NO 1336 M Block Burewala', 'House NO 1336 M Block Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'Ijaz Ajmad.jpg', NULL, NULL, '2019-11-19 11:28:59', 0, '', 1500, 'eligible'),
(10, 'Awais Shakoor', 'Abdul Shakoor', '1985-06-20', NULL, 'Married', 'A+', '3029390262', '36601-0956693-9', 'Awais.shakoor1988@gmail.com', NULL, 'N/A', '5000', '3048024', '100000', '3048051', 'new member', 'House no 46 A Block Burewala', 'House no 46 A Block Burewala', 'Hafiz Traders', 'Owner', '3029390262', '15 G Grain Market Burewala', NULL, 'Other', NULL, NULL, NULL, 'awais shakoor  new.jpg', NULL, NULL, '2019-11-19 11:26:52', 0, '', 1500, 'eligible'),
(11, 'Irfan Ali Kathiya DC', 'Haji Usman Khan', '1983-02-06', 'M.S.C.  L.L.B', 'Married', NULL, '0321-7773300', '36502-1180619-7', 'irfankathia156@gmail.com', NULL, 'N/A', '0', '', '0', '', 'new member', '56 B GOR II Lahore', 'DC HOUSE VEHARI', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Hina Irfan', '1990-03-31', NULL, NULL, NULL, NULL, '2019-11-19 11:23:50', 0, '', 1500, 'eligible'),
(12, 'Atta-ur-Rehman Tahir', 'Abdul Rehman', '1969-12-05', 'F.A', 'Married', 'B+', '3006995499', '36601-7529996-1', 'yehmancloth@yahoo.com', NULL, 'N/A', '5000', '3070693', '100000', '3048010', 'new member', 'Rehman Villas House No 9 Block C college Road Burewala', 'Rehman Villas 9C College road Burewala', 'REHMAN CLOTH HOUSE', 'owner', '067-3351793', 'RAIL BAZAR COLLGE ROAD BUREWALA', NULL, 'Wife', 'RABIA', '1978-12-24', '36601-3234278-4', 'Atta Ur Rehman.jpg', NULL, NULL, '2019-11-19 11:16:59', 0, '', 1500, 'eligible'),
(13, 'Muhammad Nabeel', 'Muhammad Nadeem', '1996-08-31', 'B.B.A', 'Single', NULL, '3343999611', '36601-1543161-1', 'mnabeel-5566@hotmail.com', NULL, 'N/A', '5000', '3048200', '100000', '3048040', 'new member', 'House No 142 Gulshane Ghani Burewala', 'House No 142 Gulshane Ghani Burewala', 'Al Majeed Service Center', 'Owner', '067-3351325', '05 Km Chichawatni Road Burewala', NULL, 'Other', NULL, NULL, NULL, 'Muhammad Nabeel.jpg', NULL, NULL, '2019-11-19 11:13:23', 0, '', 1500, 'eligible'),
(14, 'syed Saim Soulat', 'Syed Soulat Hussain Naqvi', '1983-03-03', 'M.S.C', 'Married', 'A+', '3024244444', '35202-4121777-3', 'saim-shah@hotmail.com', NULL, 'N/A', '5000', '3048919', '100000', '3048039', 'new member', '16-i-Civil Line Lahore road Burewala', '16-i-Civil Line Lahore road Burewala', 'S.S Forms', 'owner', '067-3355519', '3 km kamand Road Burewala', NULL, 'Wife', 'Ammara saim', '1980-08-10', '37405-0243576-4', 'Syed Saim Soulat.jpg', NULL, NULL, '2019-11-19 11:09:52', 0, '', 1500, 'eligible'),
(15, 'Mazaffar Marghoob', 'Maghoob Ahmad', '1984-10-06', 'F.A', 'Married', NULL, '3006994106', '36601-1282249-3', NULL, NULL, 'N/A', '5000', '2879176', '100000', '3879257', 'new member', 'House No 19 Hameed Block New Model Town Burewala', 'House No 19 Hameed Block New Model Town Burewala', 'Sahb Jee Cloth', 'owner', '3006994106', 'Link Rail Bazar Burewala', NULL, 'Wife', 'Anam Muzaffar', NULL, '36603-9967966-0', 'Muzafar Marghob 2019-10-17 19.49.12_4.jpg', NULL, NULL, '2019-11-19 11:05:40', 0, '', 1500, 'eligible'),
(16, 'Sajid Mehadi', 'Syed Mehdi Hassan', '1948-06-18', 'M.A', 'Married', NULL, '3008448984', '36601-1641741-1', 'sajidmehndibrw@hotmail.com', NULL, 'N/A', '5000', '', '100000', '3048038', 'new member', 'House No 16 Block I Civil Lines Burewala', 'House No 16 Block I Civil Lines Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Qudsia Sajid', '1954-12-13', '36601-0587489-0', 'Sajid Mehdi.jpg', NULL, NULL, '2019-11-19 11:01:13', 0, '', 1500, 'eligible'),
(17, 'Taimoor Azeem', 'Muhammad Mateen Zaman', '1998-10-28', 'FA', 'Single', 'B+', '3218337959', '36601-9295267-3', 'taimoorazeem54@gmail.com', NULL, 'N/A', '5000', '3048431', '100000', '3048037', 'new member', '62 D Block Burewala', '62 D Block Burewala', 'Zaman Bricks', 'Owner', '0321-6994797', '259 EB', NULL, 'Other', NULL, NULL, NULL, 'Taimoor Azeem.jpg', NULL, NULL, '2019-11-19 10:44:11', 0, '', 1500, 'eligible'),
(18, 'Abdul Mateen', 'Riaz Ahmad', '1977-04-03', 'Matric', 'Married', 'B+', '3326000666', '36601-1631611-7', NULL, NULL, 'N/A', '5000', '3030062', '100000', '3048036', 'new member', 'Iteffaq Palace, PO BOX burewala Chak No 437 EB Model Town Burewala', 'Iteffaq Palace, PO BOX burewala Chak No 437 EB Model Town Burewala', 'Almadina Electronics, CNG', 'owner', '3326000666', 'Vehari Bazar Chichawatni Road', NULL, 'Wife', 'Shahzna Mateen', NULL, NULL, 'Abdul Mateen.jpg', NULL, NULL, '2019-11-19 10:41:15', 0, '', 1500, 'eligible'),
(19, 'Ateeq ur Rehman', 'Mushtaq Ahmad', '1968-02-12', 'Matric', 'Married', NULL, '3006993059', '36601-1817773-1', NULL, NULL, 'N/A', '5000', '2875004', '100000', '30488035', 'new member', 'PO.Box Khas Chak No 459 EB TEh Burewala Distt Vehari', 'PO.Box Khas Chak No 459 EB TEh Burewala Distt Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'ateeq.jpg', NULL, NULL, '2019-11-19 10:33:29', 0, '', 1500, 'eligible'),
(20, 'Naeem Ahmad Kalhoon', 'Masood Ahmad Kahloon', '1973-04-27', 'BA.LLB', 'Married', 'B+', '3006999335', '36601-1554745-7', 'taxadv@hotmail.com', NULL, 'N/A', '5000', '', '100000', '2879116', 'new member', 'Kahloon Residence 505/EB Road Burewala', 'Flat No 1119 Sector l-104', 'Naeem ECO', 'CEO', '067-3772337', 'AL-Aziz Market Near Bank ALFALAH', NULL, 'Wife', 'Farhat Naeem', '1981-01-01', '36601-7632885-2', 'Naeem Kahloon .jpg', NULL, NULL, '2019-11-19 10:25:37', 0, '', 1500, 'eligible'),
(21, 'Muhammad Rizwan Abid', 'Abdul Razaq', '1979-01-10', 'BA', 'Married', NULL, '3336566999', '36601-5481667-9', NULL, NULL, 'N/A', '5000', '4118846', '100000', '4609159', 'new member', 'House No 43 Block x Housing Scheme Burewala', 'House No 43 Block x Housing Scheme Burewala', 'Rizwan Brothers Oil Mills', 'owner', 'Multan Road Burewala', '067-3352546', NULL, 'Wife', 'Rukhsana Kousar', '1981-10-01', '36104-0403227-4', 'Rizwan ABid.jpeg', NULL, NULL, '2019-11-19 10:20:08', 0, '', 1500, 'eligible'),
(22, 'Nazir Ahmad Atif Chaudhary', 'Muhammad Aslam Chaudhary', '1981-04-15', 'M.B.A', 'Married', 'AB+', '3218200872', '36601-2083312-9', 'atifch128@gmail.com', NULL, 'N/A', '5000', '4609212', '100000', '4609251', 'new member', 'Chak 128 EB Naimat Market Burewala', '60A J Block Canal Burg Multan Road Lahore', 'Beacon Lighting Solutions', 'C.E.O', '04235292222', '2A OFF Canal View Society Lahore', NULL, 'Wife', 'Fareeha Atif', '1985-11-15', '35201-7411745-0', 'Nazir Ahmad Atif .jpeg', NULL, NULL, '2019-11-19 10:13:58', 0, '', 1500, 'eligible'),
(23, 'Muhammad Ansar Iqbal', 'Muhammad Iqbal', '1972-06-15', 'BA', 'Married', 'A+', '3336999030', '36601-7710248-9', NULL, NULL, 'N/A', '5000', '3048017', '100000', '3048034', 'new member', 'House No 1,Street No 1 shah Faiz Colony Behind Dawn Petroleum Service, Burewala Distt Vehari', 'House No 1,Street No 1 shah Faiz Colony Behind Dawn Petroleum Service, Burewala Distt Vehari', 'Dawn Petroleum Service Burewala', 'owner', '067-3770603', 'Dawn Petroleum Service ,Lahore Road Burewala', NULL, 'Wife', 'Riffat Ansar', '1976-12-04', '36601-8017519-2', 'New Doc 2019-11-19 17.05.31_1_Ansar.jpg', NULL, NULL, '2019-11-19 10:07:42', 0, '', 1500, 'eligible'),
(24, 'Bilal Amin Wahla', 'Ch Amin wahla', '1982-10-25', 'MBA.LLB', 'Married', 'B+', '3137899425', '36601-1627870-1', 'B.Wahla25@gmail.com', NULL, 'N/A', '5000', '2875152', '100000', '3048033', 'new member', 'Chak No 225 EB, Gaggo Mandi Teh Burewala ,Distt Vehari', 'House No 275 QTleet No 11 Musium Town 3 Sargodha Road Faislabad', 'Phalma Innovations', 'Managing Director', '041-8864127', '3JB Near CIty Housing Sheme Faislabad', NULL, 'Wife', 'Rifat Bilal', '1987-04-07', '33100-9934623-0', 'bilal amin.jpg', NULL, NULL, '2019-11-19 09:56:41', 0, '', 1500, 'eligible'),
(25, 'Qadeer Ahmad', 'Muhammad Iqbal Saleemi', '1978-02-11', 'Graduation', 'Married', 'A+', '3006990525', '36601-1608720-7', 'Qadeer666666@gmail.com', NULL, 'N/A', '5000', '2875161', '100000', '3048032', 'new member', 'H.No 113 Block \'O\' Burewala', 'H.No 113 Block \'O\' Burewala', 'Saleemi Traders + Efa Schools Noor Campus', 'Managing Director', '067-3351525', '64 A Burewala + Ahatta Shah Nawaz', NULL, 'Wife', 'Noreen AHmad', '1982-11-30', '36601-1545318-4', 'Qadeer Ahmad.jpg', NULL, NULL, '2019-11-19 09:48:33', 0, '', 1500, 'eligible'),
(26, 'Muhammad Kashif', 'Sheikh Muhammad Aslam', '1984-07-01', 'Graduation', 'Married', NULL, '3006996880', '36601-4421239-9', NULL, NULL, 'N/A', '5000', '', '25000', '', 'bedminton', 'House No 78 H Block Burewala', 'House No 78 H Block Burewala', 'Babar Cloth HOuse', 'Owner', '3006996880', 'Rassem Gali Burewala', NULL, 'Other', NULL, NULL, NULL, 'kashif.jpg', NULL, NULL, '2019-11-19 09:37:43', 127, '', 1500, 'eligible'),
(27, 'Muhammad Adnan', 'Muhammad Ramzan', '1988-12-18', 'Graduation', 'Married', 'B+', '0333-6402308', '36601-488100-0', 'madnansearle@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'bedminton', 'House No 127 P Block Burewala', 'House No 127 P Block Burewala', 'Job In Dalda COmpany', 'DSR', '067-3603254', '64 A BLock Burewala', NULL, 'Wife', 'Saima Adnan', '1988-12-16', '34301-4248923-2', 'Adnan.jpg', NULL, NULL, '2019-11-19 09:32:19', 127, '', 1500, 'eligible'),
(28, 'Rizwan Ghaffar Butt', 'Abdul Ghaffar Butt', '1992-10-28', 'MBA', 'Married', 'A+', '3164014441', '36601-6168979-7', 'riziromio@gmail.com', NULL, 'N/A', '5000', '2875160', '100000', '3048050', 'new member', 'House No 63 st No 1 Madina Colony Burewala', 'House No 63 st No 1 Madina Colony Burewala', 'Abdul Ghaffar Butt SCO', 'CEO', '067-3770127', 'Marzi Pura Galli No 1', NULL, 'Wife', 'Rabia Rizwan', '1997-03-22', '36603-1477215-0', 'rizwan gufaar.jpg', NULL, NULL, '2019-11-19 09:26:23', 0, '', 1500, 'eligible'),
(29, 'Ch Faqir Ahmed', 'Chudhary Ali Muhammad', '1952-07-07', 'MA.LLB', 'Married', 'B+', '3216991929', '36601-1608825-3', 'faqirahmed7@gmail.com', NULL, 'N/A', '5000', '2875005', '100000', '3048031', 'new member', 'Chak No 405 EB TEh Burewala DIST VEhari', 'Hosue NO 55 EBlock Burewala DIST VEhari', 'Ch.Ali Muhammad Model Farms', 'PROPRIETOR', '067-3354020', 'Chak No 108 EB TEH Burewala DIST Vehari', NULL, 'Wife', 'Saima Ahmad', '1964-09-10', '36601-1540289-0', 'Ch Faqir Ahmad.jpg', NULL, NULL, '2019-11-19 08:45:39', 0, '', 1500, 'eligible'),
(30, 'Hafiz Muhammad Imaran Sajid', 'Abdul Razzaq', '1978-02-10', 'BA', 'Married', NULL, '3336999106', '36601-5387712-9', NULL, NULL, 'N/A', '5000', '3894898', '100000', '4609161', 'new member', 'Str No 2 Gulshan e Rehman Burewala', 'Str No 2 Gulshan e Rehman Burewala', 'Hafiz Tradors', 'owner', '067-3352143', '8G Grain Market Burewala', NULL, 'Wife', 'Sadia ishaq', '1977-05-27', '36601-6714620-2', 'imran sajid .jpeg', NULL, NULL, '2019-11-19 08:39:06', 0, '', 1500, 'eligible'),
(31, 'Azher Ghafoor', 'Abdul Ghafoor', '1969-10-18', 'BA', 'Married', 'B+', '3006991154', '36601-2354708-5', 'azharghaffoor@gmail.com', NULL, 'N/A', '5000', '3048022', '100000', '1848954', 'new member', 'House No 4 City Garden Multan Road Burewala', 'House No 4 City Garden Multan Road Burewala', 'Haji Abdul Ghafoor and company cotton factory', 'owner', '067-3786155', '531 EB Multan Road Burewala', NULL, 'Wife', 'Nusrat Azher & Riffat Yasmeen', '1976-11-04', '36601-0439083-4', 'azher ghafoor.jpg', NULL, NULL, '2019-11-19 08:33:08', 0, '', 1500, 'eligible'),
(32, 'Avais Ahmad', 'Haji Muhammad Raza ullah', '1973-11-04', 'MA', 'Married', 'B+', '3006999157', '36601-2048374-1', 'avaisbro157@yahoo.com', NULL, 'N/A', '5000', '2875007', '100000', '1848953', 'new member', 'House No 7 CIty Garden Multan Road Burewala', 'House No 7 CIty Garden Multan Road Burewala', 'Avais Brothers Oil Mill', 'owner', '4 kilo meter 405 ROad Burewala', '067-3784357', NULL, 'Wife', 'Samreen Sabir', '1984-10-10', '36601-8848652-4', 'avais Ahmad.jpg', NULL, NULL, '2019-11-19 08:28:53', 0, '', 1500, 'eligible'),
(33, 'Ezza Majeed', 'Shahzad Mubeen', '2003-08-08', NULL, 'Single', 'B+', '3008690419', '36601-6323158-2', NULL, NULL, 'N/A', '5000', '4542988', '50000', '4542988', 'Blood Relation', 'HOuse No 83 BLOCK D Burewala', 'HOuse No 83 BLOCK D Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-19 08:02:19', 0, '', 1500, 'eligible'),
(34, 'Sohaib Ahmad', 'Haji Muhammad Raza ullah', '1978-03-03', 'FA', 'Married', 'B+', '3216990057', '36601-1578616-5', 'sohaibahmad333@yahoo.com', NULL, 'N/A', '5000', '3048020', '100000', '1848952', 'new member', 'House No 6 CITY Garden Multan Road Burewala', 'House No 6 CITY Garden Multan Road Burewala', 'Ahmad Brothers COtton Factory', 'Owner', '0673784157', 'Road 505 EB 4 KILO meter Burewala', NULL, 'Wife', 'Ambreen Sabir', '1986-03-02', '36601-9052037-6', 'shoaib.jpeg', NULL, NULL, '2019-11-19 08:00:43', 0, '', 1500, 'eligible'),
(35, 'Abid Farooq', 'Khalid Farooq', '0000-00-00', NULL, 'Single', NULL, '3006999754', NULL, NULL, NULL, 'N/A', '5000', '2875009', '100000', '1848951', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-18 15:25:34', 0, '', 1500, NULL),
(36, 'Muhammad Zain AYUB', 'Muhammad Ayub', '0000-00-00', NULL, 'Single', NULL, '3337999040', NULL, NULL, NULL, 'N/A', '5000', '2875130', '100000', '1848958', 'Blood Relation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'zain ayub.jpg', NULL, NULL, '2019-11-19 07:39:58', 0, '', 1500, NULL),
(37, 'DR.Muhammad Yaqoob Rana', 'Amanat Ali Khan', '1956-03-02', 'MBBS , MCPS', 'Married', 'B+', '3216991824', '36601-1179753-9', 'dryaqubrana@gmail.com', NULL, 'N/A', '5000', '', '100000', '1848957', 'new member', '17 Sabzazar Town Canal Bank Burewala Distt Vehari', '17 Sabzazar Town Lahore Road Burewala Distt Vehari', 'Fatima I Hospital', 'Doctor', '067-3771599', '17 Sabzazar Town Lahore Road Burewala Distt Vehari', NULL, 'Wife', 'Nasra & Ms Shamshad', NULL, NULL, 'Muhammad Yaqoob Rana.jpg', NULL, NULL, '2019-11-19 07:39:15', 0, '', 1500, 'eligible'),
(38, 'Muhammad Imran Staar', 'Ch Abdul Sattar', '1980-01-16', 'MBA , LLB', 'Married', NULL, '3334555864', '36601-1417079-3', 'misattar.ch.@gmail.com', NULL, 'N/A', '5000', '3048379', '100000', '3036246', 'new member', 'House No 94 Block G old TEhsil Road Burewala', 'House No 94 Block G old TEhsil Road Burewala', 'Montgomezy Tradors Dealer AGTL Chichawatni', 'owner', '0405485864', 'Opposite Jinnah Town Phase 2 Bai pas', NULL, 'Wife', 'Shumaila Imran', '1984-10-28', '36601-7735011-4', 'Muhammad Imran Sattar.jpg', NULL, NULL, '2019-11-19 07:21:43', 0, '', 1500, 'eligible'),
(39, 'Muhammad Asif Whala', 'Muhammad Akbar Wahla', '1978-03-21', 'FA', 'Married', 'A+', '3006999828', '36601-5623790-1', NULL, NULL, 'N/A', '5000', '2875151', '100000', '3036245', 'new member', 'CHak No 225 EB Gago Mandi TEh Burewala DISTT VEhari', 'Wahla BRothers House NO 20 Grain Market Gango Mandi Burewala', 'M.Hussain Rice Mill Gago', 'owner', '067-3500133', 'Wahla Brothers Shop No 20 GM Gaggo', NULL, 'Other', 'Shahrukh Javed Randhawa', '1981-11-11', '35202-2509497-2', 'Muhammad ASIF.jpg', NULL, NULL, '2019-11-19 07:15:33', 0, '', 1500, 'eligible'),
(40, 'Zahid Javed', 'Chuadhary Abdul Ghanni', '1970-09-02', 'BA', 'Married', NULL, '3007597800', '36601-1624010-7', NULL, NULL, 'N/A', '5000', '3894899', '100000', '4609160', 'new member', 'House No 62 K Block Burewala', 'Multan Road House 109 Gulshane Rehman Burewala', 'New Almahboob Oil Mill', 'owner', '067-3390369', 'Kamand Road Burewala', NULL, 'Other', NULL, NULL, NULL, 'zahid .jpeg', NULL, NULL, '2019-11-19 07:10:05', 0, '', 1500, 'eligible'),
(41, 'Majid Ameer', 'Ameer Din', '1989-07-02', 'Matric', 'Married', 'AB+', '3007594141', '36601-2571213-3', NULL, NULL, 'N/A', '5000', '4221529', '100000', '4609249', 'new member', '32 F Block Lakar Mandi Burewala DISTT VEhari', '32 F Block Lakar Mandi Burewala DISTT VEhari', 'DUbai SHoes Peshawar', 'owner', NULL, 'and Green City Dahranwala', NULL, 'Wife', 'Sidra Majid', NULL, '34601-0804844-2', 'majid Meer.jpeg', NULL, NULL, '2019-11-19 07:04:58', 0, '', 1500, 'eligible'),
(42, 'Muhammad Sajjad', 'Muhammad Ashraf', '1974-05-01', 'FCPS.MBBS', 'Married', NULL, '3017996746', '35200-1557824-5', 'drmsajjad174@gmail.com', NULL, 'N/A', '5000', '2875008', '100000', '3036244', 'new member', 'House No 71 New Model Town Burewala', 'House No 71 New Model Town Burewala', 'Jannat Aziz Hospitle', 'Chief Consultant Eye Specialist', '067-3501096', 'Chak No 193 Tufail Abad Burewala', NULL, 'Wife', 'Rabia Sajjad', '1977-02-12', '35200-1503176-4', 'Muhammad Sajjad.jpg', NULL, NULL, '2019-11-19 12:32:19', 0, '', 1500, 'eligible'),
(43, 'Dr Abdul Majid Hussan Sheikh', 'Muhammad Din Sheikh', '1959-05-15', 'MBBS', 'Married', 'B+', '3007590723', '36601-7564334-7', NULL, NULL, 'N/A', '5000', '3048014', '100000', '3036243', 'new member', '94 I Block CIVILIon Burewala', '94 I Block CIVILIon Burewala', 'AZiz Muhammad din Hospital', 'Doctor', 'AZiz Muhammad din Hospital', '3007590723', NULL, 'Wife', 'Nazia Kousar', NULL, NULL, 'abdul majid hussan.jpg', NULL, NULL, '2019-11-19 06:53:56', 0, '', 1500, 'eligible'),
(44, 'Tuba Shahzad', 'Shahzad Mubeen', '2000-10-02', 'MBBS', 'Single', 'B+', '3008690419', '36601-0713315-2', NULL, NULL, 'N/A', '5000', '3048061', '50000', '41811719', 'Blood Relation', 'House NO 83 D Block Burewala', 'House NO 83 D Block Burewala', NULL, NULL, NULL, NULL, NULL, 'Father', 'Shahzad Mubeen', NULL, NULL, NULL, NULL, NULL, '2019-11-19 06:50:21', 0, '', 1500, 'eligible'),
(45, 'Zain Ul Abideen', 'Atta Ur Rehman Maqsood', '1992-02-08', 'MBA', 'Married', 'B+', '3337193808', '36601-9574307-9', 'zaintraders.brw@gmail.com', NULL, 'N/A', '5000', '', '100000', '3036242', 'new member', 'House No 43 Block D Burewala', 'House No 43 Block D Burewala', 'Zain Brothers', 'Managing Director', '067-3359232', '441 Azeema ABad Burewala', NULL, 'Wife', 'sabra Khalid', '1994-11-07', '35202-5909892-8', 'zain ul abideen.jpg', NULL, NULL, '2019-11-19 06:45:51', 0, '', 1500, 'eligible'),
(46, 'Ch Tanwir Ahmad', 'Chudhary Khusi Muhammad', '1966-11-15', 'BA LLB', 'Married', NULL, '3214400622', '36601-9027081-9', NULL, NULL, 'N/A', '5000', '2875010', '100000', '3036241', 'new member', 'Tanvir AHmad Dhillo Masoom Shah Road Burewala', 'Dhillo PVC Pipe Factory Masood Shah Road Burewala', 'Dhillo PVC PIpe Industry', 'Director', NULL, 'Dhillo PVC PIpe industry Masoom SHah Road Burewala', NULL, 'Wife', 'Asia Nawaz', '1971-01-01', '36601-3413215-0', 'tanwir Ahmad.jpg', NULL, NULL, '2019-11-19 06:42:14', 0, '', 1500, 'eligible'),
(47, 'Shahzad Ashraf', 'Muhammad Ashraf', '1976-04-16', 'FA', 'Married', 'B+', '3005558045', '36601-4722329-9', NULL, NULL, 'N/A', '5000', '2875012', '100000', '3036240', 'new member', 'House No 110 NEW K Block Burewala', 'House No 110 NEW K Block Burewala', 'Javed Ashraf Developer', 'Partner', '067-3354863', 'House NO 110 New K Block Burewala', NULL, 'Wife', 'Nadia Afzal', '1981-06-04', '36502-1317336-8', 'shahzad ashraf.jpg', NULL, NULL, '2019-11-19 06:26:44', 0, '', 1500, 'eligible'),
(48, 'Shahzad Mustafa', 'Ghulam Mustafa', '1976-02-13', 'Master In Tele Communication & IT', 'Married', 'B+', '3204220000', '36601-2082074-7', 'shahzad.mustafa@gmail.com', NULL, 'N/A', '5000', '8048016', '100000', '2879403', 'new member', 'House No 1 Block N burewala', 'House No 1 Block N burewala', 'Yaqoob Jewellers', 'CEO', '067-3354312', '26 H Main Arif Bazar Burewala', NULL, 'Wife', 'Shagufta Parween', '1975-05-17', NULL, 'shahzad muftafa.jpg', NULL, NULL, '2019-11-19 06:22:24', 0, '', 1500, 'eligible'),
(49, 'Muhammad Naveed', 'Muhammad Ashraf', '1980-12-30', 'Matric', 'Married', 'AB+', '3006188788', '34201-7323847-5', NULL, NULL, 'N/A', '5000', '3038716', '100000', '3036239', 'new member', 'Chak Kamala Po Khas Teh Dist Gujrat', 'Chak No 269 EB Jamlera  Road Burewala', 'AL Hussain Proteen Form', 'owner', '03006188788', 'CHak No 269 EB Jamlrela Road Burewala', NULL, 'Wife', 'Sanam Naveed', '1983-07-02', '34201-5783330-8', 'muhammad naveed.jpg', NULL, NULL, '2019-11-19 06:15:25', 0, '', 1500, 'eligible'),
(50, 'Adeel Maqsood', 'Maqsood Anwar', '1978-11-19', 'B.Design', 'Married', NULL, '3334242400', '36601-4716963-3', 'adeelmaqsood@hotmail.com', NULL, 'N/A', '5000', '', '50000', '4609004', 'vehari', 'House NO 117 Block N Burewala Dist Vehari', 'House NO 117 Block N Burewala Dist Vehari', 'Moon Rice Mills', 'Owner', '067-3603000', '1.5 km luddan Road Burewala', NULL, 'Wife', 'Madiha Saeed', '1985-03-01', '35201-2801872-4', 'Adeel Maqsood.jpg', NULL, NULL, '2019-11-19 12:13:17', 0, '', 1500, 'eligible'),
(51, 'Muhammad Naeem Sabir', 'Ch Muhammad Sabir', '1964-06-01', 'M.A', 'Married', 'B+', '0300-6992878', '36601-1555094-9', NULL, NULL, 'N/A', '5000', '4609219', '100000', '4609202+4609026', 'old member', 'House No 19 Block D burewala', 'House No 19 Block D burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Rubina Naeem', '1969-05-18', '36601-15011728-2', 'naeem sabir.jpg', NULL, NULL, '2019-11-19 06:08:57', 0, '', 1500, 'eligible'),
(52, 'Sheikh Muhammad Ayoub SB', 'Muhammad Suleman', '1954-01-01', 'F.A', 'Married', NULL, '3006999040', '36601-1608705-3', 'alkaremdevelopers@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 14 A Model Town Defence Block', 'House No 14 A Model Town Defence Block', 'Al Kareem Holding', 'Partener', '3006999040', 'Chichawatni Road burewala', NULL, 'Wife', 'Firdoos Yasmeen', '1964-10-03', '36601-1540172-4', 'ayub.jpg', NULL, NULL, '2019-11-19 12:06:45', 0, '', 1500, 'eligible'),
(53, 'Dr Farooq Ahmad Sb', 'Abdul Ghafoor', '1967-04-10', 'Fcps Surgeon', 'Married', 'B+', '3006991708', '36601-1608851-7', 'dr_faroq960@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 27 hameed block,New Model town Burewala district vehari', 'House No 27 hameed block,New Model town Burewala district vehari', 'Al-ghafoor Hospital Burewala', 'General Sergeon', '067-355960', 'Al-Ghafoor Hospital Hameed Block  new Model town', NULL, 'Wife', 'dr. shazia farooq', '1970-01-05', '36601-7094437-9', 'farooq.jpg', NULL, NULL, '2019-11-19 05:57:20', 0, '', 1500, 'eligible'),
(54, 'Dr.Arshad Shah', 'Muhammad Amin', '1963-01-01', 'MBBSS', 'Married', 'AB-', '03301-7994546', '36601-1578690-7', 'usman-8844@yahoo.com', NULL, 'N/A', '5000', '41819889', '100000', '4609232', 'Old Member', 'House No 96 Block D Burewala', 'House No 96 Block D Burewala', 'Syed CLinic', 'Doctor', '067-3353820', '8x Housing Scheme Burewala', NULL, 'Wife', 'Iffat Aslam', '1964-09-01', '36601-1519439-6', 'arshad shah.jpg', NULL, NULL, '2019-11-27 15:58:36', 0, '', 1500, 'eligible'),
(55, 'Guhlam Mustafa', 'Muhammad Hanif', '1971-01-01', 'Matric', 'Married', 'B+', '0300-6999079', '36601-5722596-9', 'ghulammustafa7079@gmail.com', NULL, 'N/A', '5000', '', '50000', '4579093', 'vehari', 'ST.No 3 Shah Faisal Colony Burewala DIstt Vehari', 'ST.No 3 Shah Faisal Colony Burewala DIstt Vehari', 'GM Traders', 'owner', '0300-6999079', 'ST.No 4 Shah faisal COlony Burewala', NULL, 'Wife', 'Lubna Parween', '1974-06-19', '36601-6316144-0', 'Ghulam Mustafa2019-10-18 18.08.11_2.jpg', NULL, NULL, '2019-11-19 05:06:31', 0, '', 1500, 'eligible'),
(56, 'Dr Zafar Iqbal Mirza', 'Muhammad Sadique', '1964-03-27', 'MBBS', 'Married', NULL, '3336297564', '36601-1554746-5', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 4Y Sf.V Housing Sheme Burewala', 'House No 12 ST.B Housing Sheme Burewala', 'Ibrahim Memorial Medical CENter', 'Doctor', '067-3353365', '58 A Canal Road Burewala', NULL, 'Wife', 'Musarrat Zafar', '1969-09-21', '36601-1501805-8', 'zafar.jpg', NULL, NULL, '2019-11-19 12:00:49', 0, '', 1500, 'eligible'),
(57, 'Rao Aziz Ur Rehman', 'Rao Irshad Ahmad', '1966-02-09', 'MA', 'Married', 'A+', '0300-8468699', '36601-8288759-7', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 3 D Block College Road Burewala', 'House NO 15 A Defence Block Model Town Burewala', 'Al Kareem Developers', 'Partener', '0300-8468699', 'University Road Burewala Royal Garden', NULL, 'Wife', 'Memoona Aziz', '1972-09-29', '36601-0927610-2', 'rao aziz.jpg', NULL, NULL, '2019-11-19 04:36:12', 0, '', 1500, 'eligible'),
(58, 'Ch Ijaz Aslam', 'Chaudhary Muhammad Aslam', '1973-05-01', 'Graduation', 'Married', 'B+', '03336280628', '36601-2091522-1', 'ijazaslamch@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', '81 D Block Burewala', '81 D Block Burewala', 'Color Max Flex Printing', 'CEO', '067-3353628', '81 D Block Burewala', NULL, 'Wife', 'Ambreen Ijaz', NULL, '36601-3918559-4', 'ijaz aslam.jpg', NULL, NULL, '2019-11-19 04:30:45', 0, '', 1500, 'eligible'),
(59, 'Ch Ehsan Ullah Cheema', 'Nazir Ahmad', '1978-05-26', '03006999101', 'Married', 'B+', '03006999101', '36601-1336731-3', 'ahsancheema101@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Chak No 447EB Burewala', '1.M Blcok,Moblink Ferenchise Burewala', 'Jazz Ferenchise', 'Director', '1,M Block Burewala', '03006999101', NULL, 'Wife', 'Dr.Zarqa Sadiq', '1985-08-09', '35202-0227150-8', 'ehsan ullah chema.jpg', NULL, NULL, '2019-11-19 04:24:30', 0, '', 1500, 'eligible'),
(60, 'Imran Samiullah', 'Rafique Ahmed', '1970-07-31', 'MBBS', 'Married', 'B+', '3326292288', '36601-1628100-7', 'drimransami@yahoo.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', '3-x Block Street No 6 Housing sheme Nasir Park Burewala', '40-Z Block Housing Sheme BUrewala', 'Al Rehman Hospital', 'Doctor (Cardiologist)', '067-3770990', '067-3770990', NULL, 'Wife', 'Hafsa Imran', '1981-07-25', '36601-231851-8', 'imran sa,iullah.jpg', NULL, NULL, '2019-11-19 04:06:40', 0, '', 1500, 'eligible'),
(61, 'Waqar Ahmad Dar', 'Sardar Khalil Ahmad', '1962-04-05', 'B.A', 'Married', 'AB+', '3006999506', '36601-1581361-3', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 78/B Azeemabad Burewala', 'House No 28/A Azeem ABad, Burewala Dist Vehari', 'Jinnah Model High School', 'Director', '067-3354699', '132/P Block Burewala Distt Vehari', NULL, 'Wife', 'Farhat Waqar', '1960-12-09', '36601-1521417-2', 'waqar ahmad.jpg', NULL, NULL, '2019-11-19 20:01:46', 0, '', 1500, 'eligible'),
(62, 'Dr Rao Ghulam Murtaza', 'Rao Nazir Ahmed Khan', '1977-01-05', 'F.S.C DHMS', 'Married', 'B+', '3006989881', '36601-0376004-1', 'drraoghulammurtaza@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 180 A Model Town Burewala', 'House No 180 A Model Town Burewala', 'National Council For Homoeo Pathy Govt Of Pakistan', 'Vice president', '051-5409045', 'Old Airport Link Road Fazal Town Rawal Pindi', NULL, 'Wife', 'Sadaf Rana', '1981-12-26', '31202-9388351-2', 'dr rao ghulam murtaza.jpg', NULL, NULL, '2019-11-19 19:57:16', 0, '', 1500, 'eligible'),
(63, 'Muhammad Sharif Bhatti Sb', 'Muhammad Ramzan Bhatti', '1950-11-08', NULL, 'Married', NULL, '3347196196', '36601-6267186-1', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Residence No B3 Doctors Colony THR Hospital Burewala', 'Residence No B3 Doctors Colony THR Hospital Burewala', 'Government Employ', 'Retired', '06733-55511', NULL, NULL, 'Other', NULL, NULL, NULL, 'sharif bhatti.jpg', NULL, NULL, '2019-11-19 19:52:58', 0, '', 1500, 'eligible'),
(64, 'Ahmad Yar Mujahid', 'Gul Muhammad', '1958-06-15', 'Graduation', 'Married', 'B+', '3007596353', '36601-4619451-7', 'ahmad.khan1958@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Village Mouza Bhatti P10 Sahuka Tehsil Burewala Distt Vehari', 'House No 73 Strt No 2 Muhammad Hussain Town Burewala Dist Vehari', 'Agriculture', 'Farmer', 'Villa Moza Bhatti p10 Shuaka Teh Burewala Vehari', '067-3352713', NULL, 'Wife', 'Hafiza Amra Birjees', '1961-12-31', NULL, 'ahmad yar.jpg', NULL, NULL, '2019-11-19 19:48:57', 0, '', 1500, 'eligible'),
(65, 'Muhammad Yousaf Kasaliya', 'Ghulam Ali', '1974-04-04', 'M.A (Political Science)', 'Married', 'A+', '3458439684', '36601-1628030-9', 'mpapp232@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Chak No 225/EB Tehsil Burewala District Vehari', 'Chak No 225/EB Tehsil Burewala District Vehari', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Dr.Aruj Virk', '1979-07-04', '35403-8523409-4', 'yousaf.jpg', NULL, NULL, '2019-11-19 19:40:32', 0, '', 1500, 'eligible'),
(66, 'Shakil Hashmat', 'Hashmat Ali', '1982-02-05', 'F.A', 'Married', 'B+', '3326293949', '36601-5045124-7', 'shakilhashmat@hotmail.com', NULL, 'N/A', '5000', '', '50000', '4579094', 'vehari', 'House No 7 Lalazar Canal View Burewala', 'Flame coal Hotel 1km Lahore Road Burewala', 'Flame coal Hotel', 'CEO', '067-37727015', '1 Km Lahore road Burewala', NULL, 'Wife', 'Sobia Muhammad ALi', '1985-04-04', '36601-2902376-2', 'Shakil Hashmat 2019-10-18 18.08.11_1.jpg', NULL, NULL, '2019-11-19 19:34:49', 0, '', 1500, 'eligible'),
(67, 'Ch Abdul Majeed', 'Ch Sher Muhammad', '1964-10-16', 'F.A', 'Married', 'B+', '3006991513', '36601-7402932-5', 'chabdulmajeed513@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 146 A Block Burewala', 'House No 146 A Block Burewala', 'Agriculture', 'Farmer', '3006991513', NULL, NULL, 'Wife', 'Shamim Akhtar', '1964-01-01', '36601-8364397-8', 'Abdul Majeed.jpg', NULL, NULL, '2019-11-19 19:10:03', 0, '', 1500, 'eligible'),
(68, 'Khalid Javed', 'Abdul Ghani', '1954-05-15', 'M.A (Pol Sc)', 'Married', 'A+', '3217903244', '36601-4039492-9', 'khalidjaved@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', '94 I Burewala', '94 I Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Noureen Akhter', NULL, '36601-4088481-2', 'khalid javed.jpg', NULL, NULL, '2019-11-19 19:05:38', 0, '', 1500, 'eligible'),
(69, 'Arslan Nawaz', 'Sheikh Muhammad Nawaz', '1988-10-09', 'BBA', 'Single', '', '3216544888', '36601-7160795-7', 'arslan.nawaz9253@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 121 Block G Burewala', 'House no 168 Strt No 2 Raza Block Iqbal Town Lahore', 'Anarkali Bag House', 'owner', '067-3770744', 'Tehsil wale gali', NULL, 'Other', NULL, NULL, NULL, 'arslan nawaz.jpg', NULL, NULL, '2019-11-19 19:02:09', 0, '', 1500, 'eligible'),
(70, 'Muhammad Ghulzar', 'Wali Muhammad', '1964-02-20', 'M.S.C (Statics)', 'Married', '', '3007594354', '36601-1355651-1', 'muhammadgulzar759435@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 17 Block M Burewala', 'House No 17 Block M Burewala', 'Usman Medicine Company', 'PROPRIETOR', '067-3359354', '17 M Block Burewala', NULL, 'Wife', 'Razia Shafi', '1971-11-18', '36601-2694133-6', NULL, NULL, NULL, '2019-11-19 18:57:39', 0, '', 1500, 'eligible'),
(71, 'Amaan Ullah Ghumman', 'Bashir Ahmad Ghumman', '1965-06-01', 'F.A', 'Married', NULL, '3455046786', '36601-9093447-5', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Derra Ghumman Chak No 203 EB Po Box Khas Teh Burewala', 'Derra Ghumman Chak No 203 EB Po Box Khas Teh Burewala', 'Agriculture', 'Farmer', '3455046786', NULL, NULL, 'Wife', 'Nazia Siddiqe', NULL, NULL, 'AMAAN.jpg', NULL, NULL, '2019-11-19 18:52:24', 0, '', 1500, 'eligible'),
(72, 'Usman Arif', 'Muhammad Arif Awan', '1978-11-03', 'B.A L.L.B', 'Married', 'B+', '3136882112', '36601-8835728-9', 'usman-arif-awan@hotamail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 2 Block C Multan Road Burewala Dist Vehari', 'House No 2 Block C Multan Road Burewala Dist Vehari', 'Arif & Company (Tax Consultancy)', 'Tax Consultant', '067-3354994', 'House No 2 Block C Multan Road Burewala', NULL, 'Wife', 'Sumera Saad', '1983-03-13', '33301-2035015-4', 'usman arif.jpg', NULL, NULL, '2019-11-19 18:48:09', 0, '', 1500, 'eligible'),
(73, 'Muhammad Shakeel Ghumman', 'Muhammad Hanif Ghumman', '1968-04-06', 'B.A', 'Married', NULL, '300699915', '36601-1621869-9', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Post Office Gaggo Chak No 201 EB Tehsil Burewala Distt Vehari', 'House No 70 Block C Sukh Chain Gardens Multan Road Lahore', 'Agriculture', 'Farmer', '300699915', NULL, NULL, 'Wife', 'Raheela Shakeel', '1969-10-12', '36601-1548576-2', 'shakeel 2.jpg', NULL, NULL, '2019-11-19 18:43:46', 0, '', 1500, 'eligible'),
(74, 'Imran Arif', 'Muhammad Arif Awan', '1980-04-27', 'L.L.M (LAW)', 'Married', 'B+', '3016999777', '36601-5794881-1', 'arifawan@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 2 Block C Multan Road Burewala Dist Vehari', 'House No 2 Block C Multan Road Burewala Dist Vehari', 'Public Prosecution Department', 'DDPP (Deputy District Public Prosecutor)', '3016999777', NULL, NULL, 'Wife', 'Nida Kareem', '1989-05-07', '31101-2109734-8', 'imran arif.jpg', NULL, NULL, '2019-11-19 18:38:07', 0, '', 1500, 'eligible'),
(75, 'Muhammad Shafiq', 'Rukan Din', '1969-06-15', 'Intermediate', 'Married', 'AB+', '3336282123', '36601-4187668-7', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 98 Resham Gali Block H Burewala Distt vehari', 'House No 98 Resham Gali Block H Burewala Distt vehari', 'Usman Cloth House', 'owner', '3336282123', '94 H Burewala', NULL, 'Wife', 'Shazia Shafiq', NULL, '36601-0295332-2', 'muhammad shafiq.jpg', NULL, NULL, '2019-11-19 18:33:27', 0, '', 1500, 'eligible'),
(76, 'Asif Bilal', 'Muhammad Bilal', '1983-10-07', 'B.Com', 'Married', 'AB+', '3360000102', '36601-6374431-1', 'asifbilal9@Gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 70 Setlite Town Near Fawara Chowk Burewala', 'House No 70 Setlite Town Near Fawara Chowk Burewala', 'The Heaven Shadi Hall', 'owner', '3360000102', 'Setlite Town Burewala', NULL, 'Wife', 'Amara ALi', '1986-12-08', '36601-9539424-4', 'ASIF BILAL.jpg', NULL, NULL, '2019-11-19 18:30:31', 0, '', 1500, 'eligible'),
(77, 'Shahrukh Khan', 'Shahzad Mubeen', '1995-09-27', 'B.S.C Owners', 'Single', 'B+', '3028690419', '36601-0179300-1', 'Shahrukh@almajeed.govp.com', NULL, 'N/A', '5000', '', '50000', '4579140', 'Blood Relation', 'House NO 83 D Block Burewala', 'House NO 83 D Block Burewala', 'Al Majeed Enterprises', 'CEO', '3028690419', 'Office No 1 New Block Burewala', NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-19 18:24:05', 0, '', 1500, 'eligible'),
(78, 'Jahanzaib Gujjar', 'Naseer Ahmad', '1982-02-12', 'Matric', 'Married', '', '3336299332', '36601-7554805-9', NULL, NULL, 'N/A', '5000', '4609065', '100000', '2879113', 'new member', 'Gulshan e Ghani Strt No 3', 'Chak No 405 E/B Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Maryam Saddiq', '1985-09-12', '34601-2923801-2', 'Jahanzaib .jpg', NULL, NULL, '2019-11-19 18:20:28', 0, '', 1500, 'eligible'),
(79, 'Ahmer Rauf', 'Muhammad Abdul Ruaf Bhatti', '1972-06-28', 'MPHIL English', 'Married', 'A+', '3006995699', '36601-1538044-3', 'ahmer.rauf@yahoo.com', NULL, 'N/A', '5000', '4221601', '100000', '4609221', 'new member', 'House No 3 Block C Burewala', 'House No 3 Block C Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Gul E Ahmer', '1976-01-01', '36601-9712447-8', 'ahmer rauf .jpeg', NULL, NULL, '2019-11-19 18:16:31', 0, '', 1500, 'eligible'),
(80, 'Sardar Sajid Ahmad', 'Sardar Ghulam Farid', '1967-12-01', 'B.A L.L.B', 'Married', 'B+', '3006999222', '36601-6365922-3', NULL, NULL, 'N/A', '5000', '2875144', '100000', '3036238', 'new member', 'House No 7 C Block Burewala', 'House No 7 C Block Burewala', 'Dogar Law Associates/ Shell Sruare', 'owner', '3006999222', 'Chamber No 10 Civil Courts Burewala', NULL, 'Wife', 'Dr.Samina Sajid', '1973-10-15', '36601-5804465-0', 'sardar sajid.jpg', NULL, NULL, '2019-11-19 18:09:56', 0, '', 1500, 'eligible'),
(81, 'Tahir kareem khan', 'Kareem Dad Khan', '1972-01-01', 'B.A', 'Married', NULL, '3017992131', '36601-8113386-5', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', '50 New Model Town Burewala', '50 New Model Town Burewala', 'Al Karim Houlding', 'CEO', '3017992131', 'Chichawatni Road burewala', NULL, 'Wife', 'Toba Tahira', '1972-01-01', '36601-0868913-4', 'tahir kareem.jpg', NULL, NULL, '2019-11-19 18:05:38', 0, '', 1500, 'eligible'),
(82, 'Tahir Mehmood', 'Muhammad Din', '1971-10-21', 'Matric', 'Married', 'B+', '3216991256', '36601-1582771-1', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 7 Block W Housing Scheme Burewala Dist Vehari', 'House No 7 Block W Housing Scheme Burewala Dist Vehari', 'Haidri Commission Shop', 'Owner', '067-3353289', '38G Grain Market Burewala', NULL, 'Wife', 'Tahira Tahir', '1976-10-30', '36601-1525692-2', 'tahir mehmood.jpg', NULL, NULL, '2019-11-19 18:01:37', 0, '', 1500, 'eligible'),
(83, 'Naeem Rashid', 'Rashid Ahmad', '1976-09-15', 'F.A', 'Married', 'B+', '3336999069', '36601-6442623-9', 'naeemmedi@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', '123 H Block Burewala', '123 H Block Burewala', 'Naeem Medicine', 'CEO', '123 H Block', '067-3353687', NULL, 'Wife', 'Nafisa Naeem', NULL, NULL, 'naeem rashid.jpg', NULL, NULL, '2019-11-19 17:58:01', 0, '', 1500, 'eligible'),
(84, 'Abid Hussain', 'Nazir Ahmed', '1979-11-01', 'Master', 'Married', 'A+', '3036294433', '36601-9993858-5', 'abidch1000@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Makkah Mechanical Industries Multan Road Burewala', 'Bright Corporation Multan Road Burewala', 'Bright Corporation', 'owner', '067-3354256', 'Multan Road Factory Area Burewala', NULL, 'Wife', 'Nadia Younis', '1981-08-01', '31203-803208-6', 'abid hussain.jpg', NULL, NULL, '2019-11-19 17:53:41', 0, '', 1500, 'eligible'),
(85, 'Ijaz Ahmad', 'Muhammad Bota', '1955-01-01', 'Matric', 'Married', 'A+', '3007596675', '36601-965939-5', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 62 H Block Burewala', 'House No 62 H Block Burewala', 'Kashmir Jwellers', 'Owner', '3007596675', '62/H Block Burewala', NULL, 'Wife', 'Naheed Akhter (Late)', NULL, NULL, 'ijaz ahmad so bota.jpg', NULL, NULL, '2019-11-19 17:48:41', 0, '', 1500, 'eligible'),
(86, 'Arshad Pervaiz', 'Abdul Ghani', '1958-10-15', 'M.B.B.S', 'Married', 'B+', '3006998742', '36601-0585560-3', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', '94 I Ghani Hospital Burewala', '94 I Ghani Hospital Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Noreen Kosar', '1961-10-21', '36601-5749803-4', 'arshad perves.jpg', NULL, NULL, '2019-11-19 17:44:51', 0, '', 1500, 'eligible'),
(87, 'Muhammad Abu Bakar Asghar', 'Muhammad Asghar Sabir', '1987-05-21', 'M.S.C/M.S', 'Married', '', '3336999247', '36601-7833146-7', 'asgha.abubakr@gmail.com', NULL, 'N/A', '5000', '3048327', '100000', '3036237', 'new member', 'House No 155 Nawab Colony Mian Chanu', 'House No 10A Gulshan e Rehman Burewala', 'Pakistan Army', 'Major', '067-3601930', 'GHQ Islamabad', NULL, 'Wife', 'Ariba Shabab', NULL, '31202-9390245-2', 'abubakr.jpg', NULL, NULL, '2019-11-19 17:41:26', 0, '', 1500, 'eligible'),
(88, 'Muhammad Usman Ghani', 'Qazi Maqsood Ahmad', '1989-04-05', 'F.A', 'Single', 'B+', '3007863001', '36601-4189949-7', 'qaziusman110@gmail.com', NULL, 'N/A', '5000', '8048500', '100000', '6409128', 'new member', 'Warraich Town H No 83 Tehsil Burewala Dist Vehari', 'Warraich Town H No 83 Tehsil Burewala Dist Vehari', 'Qazi Developers& Associates PVT (Ltd)', 'Director', '067-3359199', 'Shop No 7,8 lalazar canal view housing sheme Buewala', NULL, 'Other', NULL, NULL, NULL, 'Usman Ghani_1.jpg', NULL, NULL, '2019-11-19 17:34:57', 0, '', 1500, 'eligible'),
(89, 'Mian Arshad Javid', 'Muhammad Shafi', '1954-06-15', 'F.A', 'Married', 'A+', '3006992023', '36601-7543197-1', 'mianarshad2023@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 123/p Lahore Road Burewala', 'House No 123/p Lahore Road Burewala', 'Royal Motors', 'owner', '067-3359242', '123/P Lahore Road Burewala', NULL, 'Wife', 'Zarina Naheed Arshad', '1962-12-15', '36601-9992453-0', 'mian m arshad javed.jpg', NULL, NULL, '2019-11-19 17:30:22', 0, '', 1500, 'eligible'),
(90, 'Shakeel Sajid', 'Muhammad Munir Ahmad', '1983-01-20', 'Matric', 'Married', 'B+', '3006994129', '35301-2910616-3', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 107 Defence Block Street No 4 Burewala Dist Vehari', 'House No 98 Strt No 2 Mohalla Gulshan e Ghani Burewala dist Vehari', 'Al Munir Developers', 'Director', '3006994129', 'Model Town House No 107 Strt No 4 Defence Block Burewala', NULL, 'Wife', 'Khalida Shakeel', '1984-02-09', '35301-7766846-2', 'shakeel.jpg', NULL, NULL, '2019-11-19 17:26:28', 0, '', 1500, 'eligible'),
(91, 'Nasir Javed', 'Nawab Ali Javed', '1975-07-01', 'MBBS', 'Married', 'B+', '3336282662', '36601-7849182-9', 'dr.njaved@yahoo.com', NULL, 'N/A', '5000', '2875423', '100000', '3036236', 'new member', 'House no 1 w block Housing Scheme Burewala', 'House no 1 w block Housing Scheme Burewala', 'Mind Care Cehire', 'Doctor (Incharge)', '067-3352662', 'House no 1 W Block Chowk Chungi No 5 Burewala', NULL, 'Wife', 'Sahar Nasir', '1983-03-23', '35202-2322117-6', 'nasir javed.jpg', NULL, NULL, '2019-11-19 17:20:38', 0, '', 1500, 'eligible'),
(92, 'Muhammad Samer Iqbal', 'Muhammad Iqbal', '1966-05-12', 'Graduation', 'Married', 'A+', '0300-6996651', '36601-5919706-3', NULL, NULL, 'N/A', '5000', '3048018', '100000', '3036235', 'new member', 'House No 34 P Burewala Distt Vehari', 'House No 34 P Burewala Distt Vehari', 'Dawn Pharmacy', 'owner', '067-3354889', 'Dawn Pharmacy Arif Bazar Burewala', NULL, 'Other', NULL, NULL, NULL, 'samar iqbal.jpg', NULL, NULL, '2019-11-19 17:12:54', 0, '', 1500, 'eligible'),
(93, 'Khusi Muhammad', 'Khan Muhammad', '1962-10-10', 'M.A English', 'Married', NULL, '3007879779', '36601-5598996-5', NULL, NULL, 'N/A', '5000', '', '100000', '4609158', 'new member', 'Chak No 95 EB Teh Arifwala Distt Pakpattan', 'House no 14 New Model Town Burewala', 'New Beacon House School Burewala', 'Principal', 'Govt Post Graduate College Burewala', '067-9200071', NULL, 'Wife', 'Farah Deeba', '1962-10-10', '36601-5598996-5', 'Usman Ghani_4_Khusi Muhammad.jpg', NULL, NULL, '2019-11-19 17:08:01', 0, '', 1500, 'eligible'),
(94, 'Muhammad Saleem', 'Bashir Ahmad', '1971-07-05', 'F.A', 'Married', 'AB+', '3005223319', '36601-1635177-9', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 97/N Block Burewala', 'House No 97/N Block Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'saleem.jpg', NULL, NULL, '2019-11-19 17:03:34', 0, '', 1500, 'eligible'),
(95, 'Dr Sultan Waqas Aslam Awan', 'Dr.Muhammad Aslam Awan', '1972-12-08', 'M.D Doctor Of Medicine', 'Married', 'B+', '3336282005', '36601-5152777-9', 'drwaqasaslam@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 179 Block H Stret Joia Road Burewala Dist vehari', 'House No 179 Block H Stret Joia Road Burewala Dist vehari', 'Shifa Hospital', 'Owner', '3336282005', '179 H Joia Road Burewala', NULL, 'Other', NULL, NULL, NULL, 'sultan waqas.jpg', NULL, NULL, '2019-11-19 17:01:27', 0, '', 1500, 'eligible'),
(96, 'Bilal Razzaq', 'Abul Razzaq', '1988-11-30', 'ACCA-CAT', 'Married', NULL, '3226828382', '36601-5365787-1', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 353/437 EB Model Town Burewala', 'House No 353/437 EB Model Town Burewala', 'Almadina Enterprises', 'Currancy Exchange', '067-3770555', 'Shop No 22/Ac Block Alaziz Super Market', NULL, 'Wife', 'Iqra Bilal', '1994-02-28', '36502-7278570-6', 'bilal razzaq.jpg', NULL, NULL, '2019-11-19 16:56:58', 0, '', 1500, 'eligible'),
(97, 'Zafar Iqbal Tahir', 'Ghulam Nabi', '1964-03-06', NULL, 'Married', NULL, '3066991223', '36601-4066338-3', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Chak No 245 EB PO Box Burewala Distt Vehari', 'Chak No 245 EB PO Box Burewala Distt Vehari', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Ashaan Aslam', NULL, '31104-1635730-8', 'zafar iqbal tahir.jpg', NULL, NULL, '2019-11-19 16:51:57', 0, '', 1500, 'eligible'),
(98, 'Muhammad Irshaad', 'Niaz Muhammad', '1968-01-01', 'Matric', 'Married', 'B+', '3146000999', '36601-8382686-5', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 10 Block P Burewala Distt Vehari', 'House No 10 Block P Burewala Distt Vehari', 'Arshad Electronics', 'owner', '3146000999', 'Arif bazar Burewala', NULL, 'Wife', 'Nafeesa parveen', '1965-01-01', '36601-4535619-6', 'muhammad irshad.jpg', NULL, NULL, '2019-11-19 16:48:57', 0, '', 1500, 'eligible'),
(99, 'Imraan khan Saldera', 'Muhammad Ayub khan saldera', '1986-12-21', 'Graduation', 'Married', '', '3334190064', '36601-6752340-1', NULL, NULL, 'N/A', '5000', '', '50000', '2879107', 'vehari', 'Zaman kot Mooza Saldera Tehsil Burewala Distric Vehari', 'Zaman kot Mooza Saldera Tehsil Burewala Distric Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'Usman Ghani_17_Imraan Khan saldera.jpg', NULL, NULL, '2019-11-19 16:42:43', 0, '', 1500, 'eligible'),
(100, 'Majeed Akbar', 'Akbar Ali', '1971-03-01', 'M.S.C', 'Married', 'A+', '3006970041', '35301-1466998-7', 'makbar5500@gmail.com', NULL, 'N/A', '5000', '', '50000', '4579139', 'vehari', 'House No 208 Mohalla Gillanian Depalpur', 'near Punjab College Canal Banglow Azeem Abad Burewala', 'Punjab Group of Colleges', 'Director Principal', '067-3771585,86,87', 'Punjab College Canal Road Burewala', NULL, 'Wife', 'Saima Batool', '1977-07-27', '35301-1928297-2', 'Majeed Akbar 2019-10-17 20.29.13_1.jpg', NULL, NULL, '2019-11-19 16:36:11', 0, '', 1500, 'eligible'),
(101, 'Ali Usman Bajwa', 'Ch.Asmat saeed Bajwa', '1982-01-01', 'F.A', 'Married', '', '3216666066', '36601-4279499-7', NULL, NULL, 'N/A', '5000', '', '50000', '4579223', 'vehari', '30 Civil Park Bajwa House City Burewala Dist Vehari', '30 Civil Park Bajwa House City Burewala Dist Vehari', 'Agriculture', 'Farmer', '3216666066', '313 EB Burewala', NULL, 'Wife', 'Ifrah Imtiaz', NULL, '38401-0223334-6', 'New_Doc_2019-11-19_17.05.31_3_Ali_Usman_bajwa[1].jpg', NULL, NULL, '2019-11-19 16:25:02', 0, '', 1500, 'eligible');
INSERT INTO `members_record` (`membership_id`, `name`, `father_name`, `DOB`, `Qualification`, `MaritaLStatus`, `BloodGroup`, `cell_no`, `Cnic`, `email`, `Landline`, `twitter`, `registration_fee`, `registration_slipNo`, `membership_fee`, `membership_slipNo`, `category`, `Address`, `TemporaryAddress`, `NameOfOrganization`, `Designation`, `BuisnessAddress`, `BuisnessMobile`, `blandline`, `Relation`, `SName`, `SDob`, `SCnic`, `image`, `password`, `created_at`, `updated_at`, `status`, `payments_status`, `service_charges`, `remarks`) VALUES
(102, 'Pervez Akhter', 'Asmat Ullah', '1975-04-18', 'Intermediate', 'Married', 'B+', '3006991947', '36601-1614887-1', 'pervezakhter6433@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House no 286 Satellite Town Burewala', 'Dubai Poultry servis 122/P Lahore Road Burewala', 'Dubai Poultry servis', 'Owner (CMD)', '3006991947', '112/P Lahore Road Burewala', NULL, 'Wife', 'Tahira parveen', '1981-03-07', '36601-6719457-8', 'pervez akhtar.jpg', NULL, NULL, '2019-11-19 16:11:56', 0, '', 1500, 'eligible'),
(103, 'Sardar Asif Sajjad', 'Sardar Muhammad Sajjad', '1958-12-20', 'B.A', 'Single', NULL, '3214451447', '35201-1614760-7', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Azeem Abad Burewala PO Box Khas Teh Dist Vehari', 'Flat No 135 j Muhalla Faiz1 Deffence Housing Athority Cant Lahore', 'Al Munir Developers', 'Director', '3214451447', NULL, NULL, 'Other', NULL, NULL, NULL, 'sardar asif sajjad.jpg', NULL, NULL, '2019-11-19 16:06:30', 0, '', 1500, 'eligible'),
(104, 'Raja Ahsan Ullah', 'Raja Muhammad Akram', '1969-02-02', 'M.A', 'Married', 'A+', '3316999483', '36601-2104501-5', 'aromaburewala@gmail.com', NULL, 'N/A', '5000', '', '100000', '3036234', 'new member', 'House No 05 Block D Burewala', 'House No 05 Block D Burewala', 'Aroma Restuarant & Merraige Hall', 'Chief Executive', '067-3773283', 'House No 9 Block C Burewala', NULL, 'Wife', 'Bakhat Bano', NULL, '36601-7185368-2', 'raja ehsan.jpg', NULL, NULL, '2019-11-19 15:55:39', 0, '', 1500, 'eligible'),
(105, 'Muhammad Talha', 'Sardar Riaz Ahmad', '1997-02-12', NULL, 'Single', NULL, '3337590344', '36601-7180526-3', NULL, NULL, 'N/A', '5000', '2875006', '100000', '3036232', 'new member', 'House No 120 Block D Burewala Dist Vehari', 'House No 120 Block D Burewala Dist Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'muhammad talha.jpg', NULL, NULL, '2019-11-19 15:50:45', 0, '', 1500, 'eligible'),
(106, 'Muhammad Azam', 'Rehmat Ali', '1975-06-01', 'B.A L.L.B', 'Single', NULL, '3006992500', '36601-6780875-7', 'mianazam437@gmail.com', NULL, 'N/A', '5000', '4609135', '100000', '2879253', 'new member', '87 Model Town Main Canal Burewala', '87 Model Town Main Canal Burewala', 'ALi Ice Factory Ali Gardens Gaggo', 'Director', '0673501437', 'Ali Ice & Oil Mills Gaggo Mandi Burewala', NULL, 'Wife', 'Aqsa Khurshid', '1987-08-14', '36601-7101096-8', 'Azam .jpeg', NULL, NULL, '2019-11-25 15:39:17', 0, '', 1500, 'eligible'),
(107, 'Muhammad Irfan', 'Muhammad Amin', '1979-11-08', NULL, 'Single', NULL, '3006992815', '36601-7623816-7', NULL, NULL, 'N/A', '5000', '', '100000', '3036230', 'new member', 'House No 38 G Burewala Distt Vehari', 'House No 38 G Burewala Distt Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-19 15:42:56', 0, '', 1500, 'eligible'),
(108, 'Muhammad Nadeem', 'Muhammad Rafiq', '1981-04-01', 'Graduate', 'Married', NULL, '3006879419', '36601-5480891-9', NULL, NULL, 'N/A', '5000', '3048029', '100000', '3036228', 'new member', '46 Ghalla Mandi Burewala', 'Strt No 4 Riaz Abad Burewala', 'New lasani Traders', 'Owner', '067-3355135', '46 Galla Mandi Burewala', NULL, 'Wife', 'Naheed Kousar', '1984-06-17', '36601-9543988-2', 'NADEEM.jpg', NULL, NULL, '2019-11-19 15:39:57', 0, '', 1500, 'eligible'),
(109, 'Muhammad Amir Shakeel', 'Muhammad Ramzan', '1980-04-15', 'Matric', 'Married', 'A+', '3006999293', '36601-1117160-1', NULL, NULL, 'N/A', '5000', '3048019', '100000', '3036226', 'new member', 'House No 10 Strt No 2 New Block Red muhalla Housing Sheme Burewala Distt Vehari', 'House No 10 Strt No 2 New Block Red muhalla Housing Sheme Burewala Distt Vehari', 'AL Rehman Commision Shop', 'Owner', '3006999293', NULL, NULL, 'Wife', 'Fozia', '1978-12-01', '36601-6550049-6', 'AAMIR SHAKEEL.jpg', NULL, NULL, '2019-11-19 15:35:14', 0, '', 1500, 'eligible'),
(110, 'Maqsood Ahmad', 'Muhammad Amin', '1972-01-01', NULL, 'Married', 'B+', '3216889440', '36601-1954126-5', NULL, NULL, 'N/A', '5000', '3048447', '100000', '3048048', 'new member', 'Al Ameen Hospital 64 Civil Park Main Road Burewala', '64 Civil Park Main Road Burewala', 'Al Ameen Hospital', 'Doctor', '067-3601007', '64 Civil Park Main Road Burewala', NULL, 'Wife', 'Dr.Favida Nafees', '1975-01-01', '36302-7123951-2', 'Maqsood Ahmad.jpg', NULL, NULL, '2019-11-19 15:29:23', 0, '', 1500, 'eligible'),
(111, 'Shahzaad Mubeen', 'Abdul Majeed', '1976-01-01', NULL, 'Single', 'B+', '3008690419', '36601-6750449-5', 'Shahzad.mubeen@gno.com.pk', NULL, 'N/A', '5000', '', '50000', '4579141', 'vehari', 'House NO 83 D Block Burewala', 'House NO 83 D Block Burewala', 'GAS&OIL Pakistan Limited', 'Director', '3008690419', 'Office No 1 New Block Burewala', NULL, 'Wife', 'Shazia', '1970-07-16', '36601-8822479-2', 'Shahzad Mubeen.jpeg', NULL, NULL, '2019-11-19 15:23:50', 0, '', 1500, 'eligible'),
(112, 'Ejaz Aslam', 'Muhammad Aslam Nasim', '1978-12-01', 'Matric', 'Married', 'B+', '3006999495', '36601-4943167-7', NULL, NULL, 'N/A', '5000', '3036178', '100000', '3048057', 'new member', 'Chak No 495/EB Burewala District VEhari', 'Chak No 495/EB Burewala District VEhari', 'Friends Protean Form', 'CEO', '061-3588222', 'Chak No 495/EB Luddan Road', NULL, 'Wife', 'Sarwat Mustafa', '1980-08-15', '36601-1535276-8', 'ejaz aslam.jpg', NULL, NULL, '2019-11-19 15:17:13', 0, '', 1500, 'eligible'),
(113, 'Sohail Amin', 'Muhammad Amin', '1969-10-15', 'Matric', 'Married', NULL, '3006999076', '36601-1585285-7', NULL, NULL, 'N/A', '5000', '3036030', '100000', '3048059', 'new member', 'Po Box  Gago Chak No 187 E.B Teh Burewala Distt Vehari', 'Po Box  Gago Chak No 187 E.B Teh Burewala Distt Vehari', NULL, NULL, '3006999076', '3006999076', NULL, 'Other', NULL, NULL, NULL, 'sohail amin.jpg', NULL, NULL, '2019-11-19 15:00:32', 0, '', 1500, 'eligible'),
(114, 'Ghulam Rasool', 'Muhammad Ishaq', '1975-04-21', 'B.A L.L.B', 'Married', 'A+', '3016582467', '36601-1550988-9', 'gr.manais@gmail.com', NULL, 'N/A', '5000', '', '100000', '2879114', 'new member', 'Chak No 467 E.B', '13.O Block Burewala', 'Friends Law Associates', 'Lawyer', '3016582467', 'Tehsil Courts Burewala', NULL, 'Wife', 'Robina Akram', '1974-02-05', '31202-8169627-2', 'Ghulam Rasool .jpg', NULL, NULL, '2019-11-19 14:56:27', 0, '', 1500, 'eligible'),
(115, 'Rana Ghulam Murtaza', 'Muhammad Tufail', '1968-01-01', 'Middle', 'Married', 'AB+', '3006999181', '36401-0878742-7', 'smarrizvi99@yahoo.com', NULL, 'N/A', '5000', '3036176', '100000', '3048058', 'new member', 'Houser No 89 A Block Burewala Distt VEhari', 'Houser No 89 A Block Burewala Distt VEhari', 'Al Hadeed Enterprises', 'Owner', '3006999181', '255/E/B Burewala', NULL, 'Wife', 'Sumera Asgher', NULL, '36601-6119305-0', 'rana ghulam murtaza.jpg', NULL, NULL, '2019-11-19 14:51:51', 0, '', 1500, 'eligible'),
(116, 'Syed Ali Asad Rizvi', 'Syed Riaz Hussain Rizvi', '1981-12-20', 'M.A', 'Single', '', '3006995800', '36601-7253669-1', 'Aliadv99@gmail.com', NULL, 'N/A', '5000', '3036177', '100000', '30448056', 'new member', 'House No 5 Street 1 Bashir Town Burewala Distt Vehari', 'House No 5 Street 1 Bashir Town Burewala Distt Vehari', 'AM Businesses', 'CEO', '3006995800', '2 km Kmand Road Burewala', NULL, 'Wife', 'Rabab Zahira', '1978-08-31', '33101-4523720-4', 'syed ali asad rizvi.jpg', NULL, NULL, '2019-11-19 14:41:39', 0, '', 1500, 'eligible'),
(117, 'Muhammad Uzair salar', 'Zaheer Ahmad', '1992-04-10', 'Graduation', 'Married', 'B+', '3336999511', '36601-8108845-5', NULL, NULL, 'N/A', '5000', '', '100000', '4609130', 'new member', 'House No 141 A Block Burewala', 'House No 308 Lalazar Colony Burewala', 'Salar Associates', 'owner', '3336999511', NULL, NULL, 'Wife', 'Neeta Nadeem', '1991-10-08', '36601-4985988-8', 'Usman Ghani_2_uzair.jpg', NULL, NULL, '2019-11-19 14:36:45', 0, '', 1500, 'eligible'),
(118, 'Muhammad Ahsan arshad', 'Muhammad  Arshad', '1990-04-01', 'D.V.M', 'Married', 'B+', '3014594447', '36603-0485561-9', 'ahsanarshad128@gmail.com', NULL, 'N/A', '5000', '', '100000', '3048055', 'new member', 'Ahmad House Street 5 Lala zar Colony Vehari', 'House no 111 Gulshan Rahed Phase 2 Defence View Burewala', 'Sadiq Feeds Pvt (Ltd)', 'Area Sales Manager', '04235926751-0', 'Sadiq Feeds office near Chandni Chowk RWP', NULL, 'Wife', 'Zunaira Akram', '1990-08-19', '31102-0332750-2', 'ahsan arshadd.jpg', NULL, NULL, '2019-11-19 14:09:42', 0, '', 1500, 'eligible'),
(119, 'Masoom Ali', 'Muhammad Yaseen', '1984-07-10', 'Matric', 'Married', 'A-', '3029390040', '36601-9353563-9', 'masoomalisoop@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 122 Block P Burewala', 'House No 122 Block P Burewala', 'Maqbool Traders', 'CO Owner', '0302-9390040', 'Reham Gali', NULL, 'Wife', 'Saba Masoom', '1995-06-15', NULL, 'masoom ali.jpg', NULL, NULL, '2019-11-19 14:02:08', 0, '', 1500, 'eligible'),
(120, 'Farukh Javed', 'Javed Iqbal', '1987-12-19', 'D.B.A', 'Married', 'B+', '3024937397', '36601-0675507-1', NULL, NULL, 'N/A', '5000', '', '30000', '', 'bedminton', 'House No 835 Strt No 14 Habib Colony Burewala Distt Vehari', 'House No 835 Strt No 14 Habib Colony Burewala Distt Vehari', 'F.J Construction', 'C.E.O', '3024937397', 'Habib Colony Burewala', NULL, 'Wife', 'Nazia Shaheen', '1987-08-04', '36601-7924985-2', 'Farukh Javaid.jpg', NULL, NULL, '2019-11-19 13:58:10', 127, '', 1500, 'eligible'),
(121, 'Muhammad Ahtsham Dawood', 'Dawood Ahmad', '1983-06-06', 'Master (Political Science)', 'Married', 'B+', '0300-4444056', '36601-1545698-9', NULL, NULL, 'N/A', '5000', '2879268', '50000', '4609002', 'vehari', 'House No 18 New K Block Burewala', 'House No 18 New K Block Burewala', 'Dawood And Company', 'owner', '0300-4444056', 'Shop No 15 G Grain Market Burewala', NULL, 'Wife', 'Rahila Ahtsham', NULL, NULL, 'Ahtsham dawood.jpg', NULL, NULL, '2019-11-20 19:33:08', 0, '', 1500, 'eligible'),
(122, 'Dr Sohail Aziz', 'Faqir Muhammad Chaudhary', '1972-06-02', 'MBBS DCP', 'Married', 'A+', '0300-6993205', '36601-3337185-9', 'sohail-laib@hotmail.com', NULL, 'N/A', '5000', '', '100000', '3036179', 'new member', 'Sohail lab 2 hameed block New Model Town Burewala', 'Sohail lab 2 hameed block New Model Town Burewala', 'Sohail Lab', 'owner', 'Stadium Road New Model Town Burewala', '067-3353205', NULL, 'Wife', 'Samia Mumtaz', '1976-03-11', NULL, 'sohail aziz.jpg', NULL, NULL, '2019-11-20 19:30:17', 0, '', 1500, 'eligible'),
(123, 'Dr Sajjad Ahmad', 'Chudhary KhusiMuhammad', '1964-11-28', 'MBBS', 'Married', '', '0333-6295577', '36601-9774846-5', 'sajjad.dhillen@yahoo.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'Dhillo House Masoom shah Road Burewala', 'Dhillo House Masoom shah Road Burewala', 'Eman Hospital', 'Doctor', '0333-6295577', 'Burewala', NULL, 'Wife', 'mudaszave sajjad', NULL, '36601-1841405-4', 'dr sajjad.jpg', NULL, NULL, '2019-11-20 19:26:16', 0, '', 1500, 'eligible'),
(124, 'Tariq Mahmood', 'Muhammad Akram', '1976-10-10', 'BS Honors', 'Married', 'B+', '0300-6997264', '36601-154724-3', 'islamabadlabbrw@ygmail.com', NULL, 'N/A', '5000', '2579252', '100000', '4609133', 'new member', '6 C Block Burewala', '6 C Block Burewala', 'Islamabad Laib', 'CEO', '067-3770264', '6.G Burewala', NULL, 'Wife', 'Sumaira Ehsan', '1978-11-01', '36601-1532650-2', 'Usman Ghani_7_Tariq Mehmood.jpg', NULL, NULL, '2019-11-20 19:22:03', 0, '', 1500, 'eligible'),
(125, 'Husnain Saeed Bajwa', 'Chadhary Asmat Saeed Bajwa', '1988-09-04', 'B.A', 'Married', 'B+', '0300-6991800', '36601-223328-3', 'u-nain@yahoo.com', NULL, 'N/A', '5000', '', '100000', '2879265', 'new member', '30 Civil Park Bajwa HOuse Brewala', '30 Civil Park Bajwa HOuse Brewala', 'Agriculture', 'Farmer', '0300-6991800', '313/EB Burewala', NULL, 'Wife', 'Rabia Javed', '1996-09-30', '36401-9372299-0', 'Husnain Saeed.jpg', NULL, NULL, '2019-11-20 19:18:15', 0, '', 1500, 'eligible'),
(126, 'Faisal Rehman', 'Rao Irshad Ahmad', '1978-11-11', 'B.A L.L.B', 'Single', '', '0334-7197197', '36601-0529036-7', 'raofaisalrehman@gmail.com', NULL, 'N/A', '5000', '1848935', '100000', '2879267', 'new member', 'House No 3-D Block College Road Burewala', 'House No 3-D Block College Road Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Naila', '1987-02-02', '34101-1760327-6', 'Faisal Ur REhman.jpg', NULL, NULL, '2019-11-20 19:14:34', 0, '', 1500, 'eligible'),
(127, 'Imran Ahmad Bhatti', 'Muhammad Rafiq', '1968-09-05', 'MBBS', 'Married', '', '0302-7598451', '36601-7719896-9', NULL, NULL, 'N/A', '5000', '', '10000', '', 'officer', '03 C  Block Burewala', '03 C  Block Burewala', 'THQ Hospital Burewala', 'Medical Superintendent Burewala', '067-9200147', 'THQ Hospital Burewala', NULL, 'Wife', 'Mariyam Imran', '1974-02-05', '36601-1489015-4', 'Imran Ahmad  bhatti.jpg', NULL, NULL, '2019-11-20 19:11:59', 0, '', 1500, 'eligible'),
(128, 'Waseem Ahmad', 'Muhammad Ahmad', '1975-01-01', 'LLB', 'Married', '', '0300-6992717', '36601-6937670-9', 'waseem717@yahoo.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', '95/A Azeem Abad Chichawatni Road Burewala', '95/A Azeem Abad Chichawatni Road Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Arooba Waseem', '1983-05-21', '31104-733274-4', 'waseem.jpg', NULL, NULL, '2019-11-20 19:08:06', 0, '', 1500, 'eligible'),
(129, 'Zafar ali Chaudhary', 'Muhammad Mansha', '1962-04-12', 'MA.LLB', 'Married', 'B+', '0333-6297615', '36601-0244564-5', 'zafaradvocate504@gmail.com', NULL, 'N/A', '5000', '3036366', '100000', '2879263', 'new member', 'Chak No 515/EB Tehsil Burewala Distt vehari', 'House No 222 Gulshan e Rehman Phase 1 Burewala', 'Zafar Law Chamber', 'CEO', '067-3355236', 'Al Aziz Mrket College Road Burewala', NULL, 'Wife', 'Asma Zafar', '1967-09-17', '36601-5327906-0', 'Zafar Ali Chudhary.jpg', NULL, NULL, '2019-11-20 19:04:18', 0, '', 1500, 'eligible'),
(130, 'Zulafiqar Ahmaad', 'Muhammad Khan', '1965-11-30', 'Matric', 'Married', 'B+', '0321-6999515', '36601-6920676-9', NULL, NULL, 'N/A', '5000', '3036368', '100000', '2879264', 'new member', 'Chak No 515/EB TEh sil Burewala Distt Vehari', 'House NO 88 Block A Burewala', 'Burewala Petrolium', 'Partener', '0321-6999515', 'Kamand Road Burewala', NULL, 'Wife', 'Rakhshanda Zulfiqar', NULL, '36601-1005659-8', 'Zulifiqar Ahmad.jpg', NULL, NULL, '2019-11-20 18:59:33', 0, '', 1500, 'eligible'),
(131, 'Abdul Rauf', 'Abdul Raheem', '2019-10-12', 'MA English', 'Married', 'B+', '0321-6934533', '36601-3813004-5', 'abdulraufpk@yahoo.com', NULL, 'N/A', '5000', '2879177', '100000', '2879262', 'new member', 'English Shoes Rail bazar Burewala', 'English Shoes Rail bazar Burewala', 'English', 'Executive Director', '067-3771199', '127 GRail Bazar Burewala', NULL, 'Wife', 'Tahira Rauf', '1971-04-04', '36601-225386-0', 'Abdur Rauf.jpg', NULL, NULL, '2019-11-20 18:55:23', 0, '', 1500, 'eligible'),
(132, 'Muhammad yasir', 'Ghulam Mustafa', '1987-01-01', 'College', 'Married', 'B+', '0333-9995128', '36601-4250893-5', 'yles227@gmail.com', NULL, 'N/A', '5000', '3048499', '100000', '2879261', 'new member', 'Housing Sheme H.No 1 Lalazar Burewala District Vehari Pakistan', 'Housing Sheme H.No 1 Lalazar Burewala District Vehari Pakistan', 'Qazi Developers& Associates PVT (Ltd)', 'Director', '067-3359199', 'Shop No 7.8 Lalazar Canal View Housing Scheme Burewala', NULL, 'Wife', 'Husneena javaid', '1992-01-10', '91509-0196845-0', NULL, NULL, NULL, '2019-11-20 18:19:56', 0, '', 1500, 'eligible'),
(133, 'Ali Haider', 'Malik Karamat', '1986-03-11', 'Graduation', 'Married', 'A+', '0301-7999420', '36601-1458695-9', 'malik-ali@yahoo.com', NULL, 'N/A', '5000', '', '50000', '2879260', 'Blood Relation', 'House No 147 A Block City Hoisng Sheme Burewala Distt Vehari', 'House No 147 A Block City Hoisng Sheme Burewala Distt Vehari', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Sidra Kalsoom', '1994-10-10', '36302-5953843-0', 'ALi haider 2019-10-17 19.49.12_6.jpg', NULL, NULL, '2019-11-20 18:15:08', 0, '', 1500, 'eligible'),
(134, 'Muhammad Azeem', 'Naseer Ahmad', '1984-04-03', 'MSC', 'Married', 'AB+', '3000333883', '36601-2136054-7', 'Fateh-Mc@hotmail.com', NULL, 'N/A', '5000', '3048493', '100000', '3048053', 'new member', '6 Green Town Shah Faiz Colony Burewala', 'Al Fateh Medicine Co 147.M Arif Bazar Burewala', 'Al Fateh Medicine Co', 'owner', '067-73351832', '147 M Arif Bazar Burewala', NULL, 'Wife', 'Anam Azeem', '1990-11-01', '36603-0873419-6', 'azeem.jpg', NULL, NULL, '2019-11-20 18:11:50', 0, '', 1500, 'eligible'),
(135, 'Rao Hassan Akhter', 'Rao Khan Bahadar Khan', '1967-01-06', 'B.A', 'Married', '', '0300-0768652', '36601-1256638-3', NULL, NULL, 'N/A', '5000', '4542674', '100000', '2879254', 'new member', 'Gulshan Raza Town Burewala', 'Gulshan Raza Town Burewala', 'Rao Brothers Rice Mills', 'Director', '0300-0768652', '6km Luddan Road Burewala', NULL, 'Wife', 'Riffat Sharif', '1973-07-03', '36601-0992765-8', 'Rao Hassan Akhtar  2019-10-17 19.49.12_1.jpg', NULL, NULL, '2019-11-20 18:07:42', 0, '', 1500, 'eligible'),
(136, 'Mian Muhammad Haider', 'Mian Muhammad Rafique', '1991-08-26', 'BSC', 'Married', 'AB+', '0300-6991869', '36601-1032163-1', NULL, NULL, 'N/A', '5000', '2879178', '100000', '2879258', 'new member', 'i Block Civil Line HOuse No 94/28 Lahore Road Burewala', 'i Block Civil Line HOuse No 94/28 Lahore Road Burewala', 'Mian Rent A CAr', 'owner', '0300-6991869', 'Burewala', NULL, 'Other', NULL, NULL, NULL, 'Usman Ghani_9_M.Haider.jpg', NULL, NULL, '2019-11-20 18:04:39', 0, '', 1500, 'eligible'),
(137, 'waqar Ahmed', 'Zulfiqar Ahmed', '1979-11-29', 'B.A', 'Married', 'B+', '0333-6991405', '36601-8017979-9', NULL, NULL, 'N/A', '5000', '4609110', '100000', '4609131', 'new member', 'House No 43 Gulshane Ghani Burewala', 'House No 43 Gulshane Ghani Burewala', 'Agriculture', 'Farmer', '0333-6991405', 'Chak No 405 EB Burewala', NULL, 'Wife', 'Farah Ambreen', '1979-06-01', '36501-7967630-8', 'Usman Ghani_6_Waqar.jpg', NULL, NULL, '2019-11-20 18:01:35', 0, '', 1500, 'eligible'),
(138, 'Shoukat ali', 'Mukhtar Ahmed', '4980-04-12', 'Under Matric', 'Married', '', '0300-6999904', '36603-5599652-5', NULL, NULL, 'N/A', '5000', '2875002', '100000', '2879259', 'new member', 'Street No 1 Phase 2 Ghulshane Rehman Burewala', 'Street No 1 Phase 2 Ghulshane Rehman Burewala', 'Chudhary Motors& Suzuki Chichawatni Motors', 'owner', '0300-6999904', 'Chuadhary Motors City Auto Plaza Multan Road Burewala', NULL, 'Wife', 'Rahil Shoukat', '1980-04-10', '36603-5044694-2', 'Shoukat ali 2019-10-17 19.49.12_3.jpg', NULL, NULL, '2019-11-20 17:58:31', 0, '', 1500, 'eligible'),
(139, 'Muhammad Nadeem Anjum Sandhu', 'Asgher Ali Sandhu', '1977-03-08', 'F.A', 'Married', 'A+', '0300-6992013', '36601-9025707-5', NULL, NULL, 'N/A', '5000', '4579086', '100000', '4609144', 'new member', 'New Model Town Phase 2 House No 30 Burewala (Stedium Road)', 'New Model Town Phase 2 House No 30 Burewala (Stedium Road)', 'Land Lard', NULL, NULL, NULL, NULL, 'Wife', 'Rizwana', '1980-01-01', '36601-1507522-6', 'Nadeem Anjum .jpg', NULL, NULL, '2019-11-20 17:53:46', 0, '', 1500, 'eligible'),
(140, 'Muhammad Ashiq', 'Khushi Muhammad', '1972-08-15', 'B.A', 'Married', 'B+', '0300-6999055', '36601-4843174-7', 'Ashiq-Chaudhary@yahoo.com', NULL, 'N/A', '5000', '3048498', '50000', '2879131', 'vehari', 'House No 26P Block Burewala', 'House No 26P Block Burewala', 'Ashiq Cotton Industries & Oil Mill', 'Sale Propriter', '067-3773378', 'Kamand Road Burewala', NULL, 'Wife', 'Rabia Ashiq', '1990-09-15', '36601-7031665-0', 'Ashiq  2019-10-17 19.49.12_5.jpg', NULL, NULL, '2019-11-20 17:43:46', 0, '', 1500, 'eligible'),
(141, 'Muhammad Mumtaz Shafi', 'Muhammad Shaffi', '1966-01-05', 'MBBS', 'Married', '', '3006992086', '36601-1542063-7', 'drmumtaz86@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', '54 Mumtaz House Phase 2 New Model Town Burewala', 'Al Shafi House Civil Hospital Road New Stadium Burewala', 'SELR EMPCOYED', 'Skin Specialist', '007-3355086', 'MUMTAZ SKIN AND LASER CLINIC', NULL, 'Wife', 'Waheeda Aziz', '1961-02-11', '36601-1500286-0', 'mumtaz shafi.jpg', NULL, NULL, '2019-11-20 17:39:38', 0, '', 1500, 'eligible'),
(142, 'Abdul Ghaffar But', 'Siraj Din', '1970-02-02', 'F.A', 'Married', 'A+', '0334-7195562', '36601-1545461-7', NULL, NULL, 'N/A', '5000', '', '50000', '4181718', 'Blood Relation', 'Marzi Pora Makan No 63 Mohalla Madinah Colony Burewala Distt Vehari', 'Marzi Pora Makan No 63 Mohalla Madinah Colony Burewala Distt Vehari', 'Government Employ', 'CEO Government Cotructor', '0334-7195562', 'Burewala', NULL, 'Wife', 'Razina Shaheen', '1973-01-01', '36601-1499865-8', 'New Doc 2019-10-27 10.41.18_1_Abdul Ghaffar.jpg', NULL, NULL, '2019-11-20 17:33:49', 0, '', 1500, 'eligible'),
(143, 'Raja Nisar Ahmad Khan', 'Raja Ghulam Haider Khan', '1954-05-01', 'M.B.B.S.MSC', 'Married', 'B+', '0300-6992040', '36601-1632243-5', 'drnisarraja@yahoo.com', NULL, 'N/A', '5000', '2879123', '100000', '2879121', 'new member', 'Chak No 217/EB', '1/c Block Burewala Multan Road', 'Haider Mamorial Hosipital', 'Orthopaedic Surgeon', '1/C Multan Road Burewala', '0673353000', NULL, 'Wife', 'Amara Nisar', '1977-01-01', '36601-1555884-2', 'Usman Ghani_10_Raja Nisar.jpg', NULL, NULL, '2019-11-20 17:29:59', 0, '', 1500, 'eligible'),
(144, 'Shahbaz Hussain Bhatti', 'Muhammad Ramzan Bhatti', '1971-11-02', 'B.A', 'Married', 'B+', '0300-6999037', '36601-1579920-5', NULL, NULL, 'N/A', '5000', '4579097', '50000', '4579203', 'vehari', '115/N Block Burewala', '115/N Block Burewala', 'Bismillah Traders', 'owner', '0300-6999037', 'Bismillah Traders Vehari Burewala', NULL, 'Wife', 'Rabia Shahbaz', '1988-01-06', '36601-3275507-2', 'Usman Ghani_18_Shahbaz bhatti.jpg', NULL, NULL, '2019-11-20 17:21:55', 0, '', 1500, 'eligible'),
(145, 'Rao Faisal Akbar', 'Konwar Akbar Ali', '1984-12-20', 'M.Com', 'Married', '', '0333-6999466', '36603-7158329-3', 'raofaisalakbar@gmail.com', NULL, 'N/A', '5000', '4609156', '100000', '4609163', 'new member', 'House No 814 A Defence View Housing Scheme Burewala Road Vehari', 'House No 814 A Defence View Housing Scheme Burewala Road Vehari', 'Bank Al Habib Ltd', 'Branch Manager', '067-3690251', 'Garhamore branch', NULL, 'Wife', 'Mahvish Amjad', '1988-08-10', '36602-0194128-4', 'New Doc 2019-10-27 10.41.18_2_Faisal akbar.jpg', NULL, NULL, '2019-11-20 17:18:35', 0, '', 1500, 'eligible'),
(146, 'Muhammad Ahtsham  Khalid', 'Khalid Hussain', '1997-12-08', 'Bechlor', 'Single', '', '0333-6898050', '35202-4056803-5', 'ehteshamkhlaid8@gmail.com', NULL, 'N/A', '5000', '4609157', '100000', '4609155', 'new member', 'Chak No 142 EB Tehsil Burewala Distt Vehari', 'Chak No 142 EB Tehsil Burewala Distt Vehari', 'Agriculture', 'Farmer', '0333-6898050', 'chak No 142 EB Tehsil Burewala', NULL, 'Wife', NULL, NULL, NULL, 'Ahtasham khalid .jpeg', NULL, NULL, '2019-11-20 17:12:55', 0, '', 1500, 'eligible'),
(147, 'Muhammad Iqbal', 'Abdur Rehman', '1960-03-20', 'Matric', 'Married', NULL, '3006999612', '36601-2989625-1', NULL, NULL, 'N/A', '5000', '4609067', '50000', '2879111', 'vehari', 'Khothi No 4 Hameed Block New Model Town', 'Khothi No 4 Hameed Block New Model Town', 'Agriculture', 'Farmer', '3006999612', 'Noza Bhatti Post Office Shauka Tehsil Burewala District Vehari', NULL, 'Wife', 'Haleema Bibi', NULL, NULL, 'Usman Ghani_15_Muhammad Iqbal.jpg', NULL, NULL, '2019-11-20 17:06:40', 0, '', 1500, 'eligible'),
(148, 'Saeed ud  din Chisti', 'Muhammad Amin Chisti', '1973-01-25', 'F.A', 'Married', '', '3336288927', '36601-1910260-1', NULL, NULL, 'N/A', '5000', '', '50000', '4609018', 'vehari', 'Chak No 427/EB TEh Burewala', 'Chak No 427/EB TEh Burewala', 'Five star Developers', 'Director', '0308-6980109', 'Canal Residencia Canal Road Burewala', NULL, 'Wife', 'Nasira Parveen', '1980-01-06', '36601-6573627-8', 'New Doc 2019-10-27 10.41.18_3_Saeed un Chisti.jpg', NULL, NULL, '2019-11-20 17:03:29', 0, '', 1500, 'eligible'),
(149, 'Awais Tufail', 'Muhammad Tufail', '1983-05-05', 'Matric', 'Married', 'A+', '3009200561', '36601-5031296-5', NULL, NULL, 'N/A', '5000', '', '50000', '4609019', 'vehari', 'Anwer Town House No 140 Burewala', 'Anwer Town House No 140 Burewala', 'Five star Developers', 'Director', '3009200561', 'Canal Residensia Canal Road Burewala 441 EB', NULL, 'Wife', 'Asifa Mushtaq', '1984-06-04', '36601-2337396-2', 'New Doc 2019-10-27 10.41.18_4_Awais Tofail.jpg', NULL, NULL, '2019-11-20 16:59:39', 0, '', 1500, 'eligible'),
(150, 'Farhan Saleem Kahloon', 'Chadhary Muhammad Saleem Kahloon', '1980-01-10', 'LLB,LLM', 'Married', '', '0321-6005050', '36601-1608769-5', 'f.sKahloon@hotmail.com', NULL, 'N/A', '5000', '', '100000', '2879140', 'new member', '4/C Multan Road Burewala', '4/C Multan Road Burewala', 'Imran Petroleum Cug Station', 'Director', '06733-55229', '4/C Multan Road Burewala', NULL, 'Wife', 'Quratul Ain', '1986-06-29', '33100-6234558-4', 'Usman Ghani_11_Farhan Kahloon.jpg', NULL, NULL, '2019-11-20 16:55:12', 0, '', 1500, 'eligible'),
(151, 'Muhammad Amin', 'Atta Muhammad', '1958-04-08', 'Matric', 'Married', 'B+', '0300-6991477', '36601-7259466-9', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 309 Setlite Town Burewala Distt Vehari', 'House No 309 Setlite Town Burewala Distt Vehari', 'National Bank of Punjab', 'Retired Manager', '0300-6991477', NULL, NULL, 'Other', NULL, NULL, NULL, 'Muhammad Amin .jpeg', NULL, NULL, '2019-11-20 16:48:26', 0, '', 1500, 'eligible'),
(152, 'Muhammad Akmal Khan Rana', 'Muhammad Akbar khan', '1968-11-11', 'F.A', 'Married', NULL, '3006994019', '36601-8855029-5', 'akmalkhansab@icloud.com', NULL, 'N/A', '5000', '', '50000', '4609124', 'vehari', 'House No 52 Street 2 Gulshan e ghani burewala District Vehari', 'House No 52 Street 2 Gulshan e ghani burewala District Vehari', 'Five star Developers Real State', 'Chief Executive', '3006994019', 'canal Residencia Canal Road 441 Burewala', NULL, 'Wife', 'Fozia Naseer', NULL, '36601-5459169-0', 'New Doc 2019-10-27 10.41.18_11_Ajmal Rana.jpg', NULL, NULL, '2019-11-20 16:38:30', 0, '', 1500, 'eligible'),
(153, 'Muhammad Ajmal Khan Rana', 'Muhammad Akbar khan', '1970-05-10', 'F.A', 'Married', 'B+', '3006994545', '36601-7013184-3', 'ajmalkhanrana@klaid.com', NULL, 'N/A', '5000', '', '50000', '4609118', 'vehari', 'House No 52 Street Z Gulshan e Ghani Burewala District Vehari', 'House No 52 Street Z Gulshan e Ghani Burewala District Vehari', 'Al Kareem Developers, Real State', 'Director', '3006994545', 'Royal Garden University Road 435.EB', NULL, 'Wife', 'Sajda AJmal', '1972-05-11', '36601-7835014-6', 'New Doc 2019-10-27 10.41.18_10_ajmal rana.jpg', NULL, NULL, '2019-11-20 16:33:46', 0, '', 1500, 'eligible'),
(154, 'Muhammad Usman', 'Muhammad Iqbal', '1987-10-03', 'LLB', 'Married', 'AB-', '0300-2627306', '36601-7455807-9', 'usman-8844@yahoo.com', NULL, 'N/A', '5000', '', '25000', '', 'New Member', 'Riaz Abad St 6 Burewala', 'Riaz Abad St 6 Burewala', 'Syed Clinic', 'Doctor', '8-X Housing Scheme Burewala', '067-3353820', NULL, 'Wife', 'Humaira Noor', '1964-09-01', '36601-1519439-6', 'usman.jpeg', NULL, NULL, '2019-11-27 16:04:23', 0, '', 1500, 'eligible'),
(155, 'Karamat Ali Shakeel', 'Faqir Muhammad', '1985-01-01', 'MA Urdu', 'Married', 'AB+', '3336268614', '36603-4231104-7', 'Karamatalishakeel@gmail.com', NULL, 'N/A', '5000', '4609006', '100000', '4609014', 'new member', 'Chak No 561 LEB Teh Distt Vehari', NULL, 'Federal Govt College Okara Cant', 'Professor', '3336268614', 'Federal Govt College Okara Cant', NULL, 'Wife', 'Bushra Kakab', NULL, NULL, 'Karamat Shakeel .jpeg', NULL, NULL, '2019-11-20 16:19:01', 0, '', 1500, 'eligible'),
(156, 'Usman Ali Rasal', 'Liaqat Ali Rasal', '1992-12-08', 'Graduate', 'Single', 'B+', '0333-3304048', '36601-0320168-1', 'osman.ali4048@gmail.com', NULL, 'N/A', '5000', '3036441', '100000', '3048054', 'new member', 'House No 88 Block H Rashim Ghali Burewala', 'House No 88 Block H Rashim Ghali Burewala', 'Pakeeza Shopping Center', 'Owner', '0333-3304048', 'Rashim Ghali Burewala', NULL, 'Wife', NULL, NULL, NULL, 'Usman Ali Rasal.jpg', NULL, NULL, '2019-11-20 16:13:09', 0, '', 1500, 'eligible'),
(157, 'Dr.Khalid Mahmood', 'Muhammad Khaliq', '1976-01-10', 'F.S.C DHMS', 'Married', 'A+', '3006995408', '36601-1574404-5', 'Doctorkhlaidmahmood@gmail.com', NULL, 'N/A', '5000', '4181715', '100000', '4609196', 'new member', 'House No 60 Street No 3 Faiz Abad Chichawatni Road Burewala Dist Vehari', 'House No 60 Street No 3 Faiz Abad Chichawatni Road Burewala Dist Vehari', 'Khaliq Memorial Hospital', 'Administer', '067-3355122', 'Faizbad Chichawatni Road Burewala', NULL, 'Wife', 'Nusrat Khalid', '1972-02-10', '36601-6525547-2', 'Dr Khalid .jpeg', NULL, NULL, '2019-11-20 16:06:01', 0, '', 1500, 'eligible'),
(158, 'KarAmat Hussain', 'Malik Nazar Hussain', '1964-02-02', 'B.A L.L.B', 'Married', '', '3017999520', '36601-5294742-9', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', '147 A City Housing Burewala', '147 A City Housing Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'karamat hussain.jpg', NULL, NULL, '2019-11-20 15:56:39', 0, '', 1500, 'eligible'),
(159, 'Khalid Saeed', 'Muhammad Sharif', '1969-05-16', 'F.A', 'Married', 'B+', '3007720842', '36601-9932923-5', NULL, NULL, 'N/A', '5000', '4542620', '100000', '300117', 'new member', 'Street No 2 Iqbal Nagar Marzi Pura Burewala', 'Street No 2 Iqbal Nagar Marzi Pura Burewala', 'Revenue Department', 'Tehsil Office District Vehari', NULL, 'Land Registrar Revenue Parwari', NULL, 'Wife', 'Shahida Kousar', NULL, '36601-6885875-0', 'Usman Ghani_12_Khalid Saeed.jpg', NULL, NULL, '2019-11-20 15:53:32', 0, '', 1500, 'eligible'),
(160, 'Shabab Majid', 'Abdul Majid  Hassan Sheikh', '1993-12-12', 'MBBS', 'Married', 'B+', '3236817990', '36601-7494122-1', 'smk17120@gmail.com', NULL, 'N/A', '5000', '', '100000', '21977866', 'new member', '94 I Black Civil Line Burewala', '94 I Black Civil Line Burewala', 'AZiz Muhammad din Hospital', 'owner', 'Aziz Muhammad Din Hospital Burewala', '3353723', NULL, 'Wife', NULL, NULL, NULL, 'WhatsApp Image 2019-11-10 at 1.02.59 PM.jpeg', NULL, NULL, '2019-11-20 15:13:21', 0, '', 1500, 'eligible'),
(161, 'Muhammad Akram', 'Shaukat Ali', '1963-05-13', 'Matric', 'Single', 'A+', '3015305555', '36601-1589072-3', NULL, NULL, 'N/A', '5000', '4181606', '100000', '4609145', 'new member', 'Canal Garden Main Road H No 14 Burewala District Vehari', 'Block 1st Tarables House 9 Al Khaldia 0095 Kuwait Kuwait', 'Businessman', 'Owner', 'Kuwait', '3015305555', NULL, 'Wife', 'Kousar Parveen', '1966-06-01', '36601-1527227-4', 'Usman Ghani_3_M.Akram.jpg', NULL, NULL, '2019-11-20 15:04:19', 0, '', 1500, 'eligible'),
(162, 'Muhammad Rauf', 'Muhammad Yousaf', '1983-02-10', 'B.S.C', 'Married', 'B+', '3226156402', '36601-1158954-7', 'raufmariner10@hotmail.com', NULL, 'N/A', '5000', '4181774', '100000', '4609147', 'new member', 'Chak No 439/EB Bairky Burewala', 'Chak No 439/EB Bairky Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Zunaira yousaf', '1984-04-13', '36104-6482204-4', 'WhatsApp Image 2019-11-10 at 1.11.55 PM.jpeg', NULL, NULL, '2019-11-20 14:57:33', 0, '', 1500, 'eligible'),
(163, 'Najaf Farooq', 'Ch Rehmat Ullah', '1985-04-10', 'B.A', 'Married', '', '3007590744', '36601-4441653-1', 'Nbrarula@yahoo.com', NULL, 'N/A', '5000', '', '100000', '4609143', 'new member', '128 G Rail Bazar Burewala', '128 G Rail Bazar Burewala', 'Rehmat Crockery', NULL, NULL, '128 G Rail bazar Burewala', NULL, 'Wife', 'Kalsoom Akhtar', '1975-04-04', '36603-6436576-4', 'WhatsApp Image 2019-11-10 at 1.16.46 PM.jpeg', NULL, NULL, '2019-11-20 14:52:34', 0, '', 1500, 'eligible'),
(164, 'Ahtasham Shahid', 'Shahid Mehmood Ahmad', '1991-12-02', 'Matric', 'Married', 'B+', '331777174', '36601-4299531-5', NULL, NULL, 'N/A', '5000', '3894742', '100000', '4609146', 'new member', 'Chak No 40 Mohala New Model Town Burewala Vehari', 'Chak No 90/12L Chichawatni Sahiwal', 'Trader', 'Owner', NULL, 'Shahid & Tahir Traders Chowk 90 Mor', NULL, 'Wife', 'Lubna', '0087-10-10', NULL, 'WhatsApp Image 2019-11-10 at 1.19.34 PM.jpeg', NULL, NULL, '2019-11-20 14:43:01', 0, '', 1500, 'eligible'),
(165, 'Muhammad Umair Khalid', 'Khalid Mehmood Khan', '1988-07-27', 'MBA', 'Single', 'A+', '3006999659', '36601-8249087-9', 'Umair.Khalid58@gmail.com', NULL, 'N/A', '5000', '', '100000', '4609148', 'new member', '9-W Block vehari Bazar Burewala', '9-W Block vehari Bazar Burewala', 'Zyra Enterprises Sme PVt LTd', '9W Block Vehari Bazar Burewala', '067-3370641', '9 W Block Vehari Bazar Burewala', NULL, 'Other', NULL, NULL, NULL, 'Muhammad Umair Khalid .jpeg', NULL, NULL, '2019-11-20 13:52:14', 0, '', 1500, 'eligible'),
(166, 'Haris Naseem', 'Ahmad Naseem Khan', '1991-07-12', 'M.S', 'Single', 'B+', '3006999499', '36601-9272367-9', 'Ranaharisnaeemkhan@hotamail.com', NULL, 'N/A', '5000', '', '100000', '4609149', 'new member', '9-W Block Vehatri Bazar Burewala', '9-W Block Vehatri Bazar Burewala', 'Bashir Begum (MW) Hospital', 'Managing Director', '067-3771786', '259 EB Lahore Road Burewala', NULL, 'Wife', NULL, NULL, NULL, 'WhatsApp Image 2019-11-10 at 1.29.11 PM.jpeg', NULL, NULL, '2019-11-20 13:16:51', 0, '', 1500, 'eligible'),
(167, 'Muhammad Usman Ahmad', 'Nabardar Riaz Ahmad', '1984-02-04', 'Matric', 'Single', NULL, '3007590344', '36601-3682443-5', NULL, NULL, 'N/A', '5000', '3036452', '100000', '3036231', 'new member', 'House No 120 Block D Burewala Distt Vehari', 'House No 120 Block D Burewala Distt Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-20 13:11:09', 0, '', 1500, 'eligible'),
(168, 'Pro Rehmat Ali Sidhu', 'Fateh Muhammad', '1959-01-01', 'M.A English', 'Married', 'B+', '3017998389', '36601-1552840-7', NULL, NULL, 'N/A', '5000', '', '100000', '4181898', 'new member', 'House No 347 Lalazar Canal View Burewala', 'House No 347 Lalazar Canal View Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Nasira Rashid', '1966-05-20', '36601-1500239-0', 'Rehmat Ali.jpeg', NULL, NULL, '2019-11-20 12:09:39', 0, '', 1500, 'eligible'),
(169, 'Allah Dita', 'Dilmeer Ahmed', '1977-01-10', 'B.A', 'Married', '', '00966591445099', '36601-6840417-1', NULL, NULL, 'N/A', '5000', '4181892', '100000', '4579202', 'new member', 'Al Shohda Road Madinah 422,100 Madinah Saudia Arab', 'Khan House Strt 8 Marzi Pura Distt vehari Burewala', 'Al Raqwi Al Tatwar Rest Contracting', 'Owner', '00966591445099', 'Madinah Munawarah', NULL, 'Wife', 'Parveen Shahida', '1979-03-12', '33105-0345287-2', 'Usman Ghani_14_Allah ditta.jpg', NULL, NULL, '2019-11-20 12:03:55', 0, '', 1500, 'eligible'),
(170, 'Khuram Shafiq', 'Muhammad Shafique', '1990-06-10', 'F.A', 'Married', 'AB+', '3006992200', '36601-9450184-1', NULL, NULL, 'N/A', '5000', '4221602', '100000', '4609222', 'new member', 'Strt No 3 Muhalla Iqbal Nagar Marzi Pura Burewala', 'Strt No 3 Muhalla Iqbal Nagar Marzi Pura Burewala', 'Govt Contructor', 'GOVT jOB', '3006992200', NULL, NULL, 'Wife', 'Naila Nazir', '1990-07-25', '36601-9107795-2', 'Khuram Shafiq.jpeg', NULL, NULL, '2019-11-20 11:57:36', 0, '', 1500, 'eligible'),
(171, 'Sh Abrar Ahmad', 'Sh.Abdul Haq', '1957-11-18', 'M.S.C', 'Single', NULL, '3336297457', '36601-6359010-9', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', '165 Gulshane Rehman Burewala (Vehari)', '165 Gulshane Rehman Burewala (Vehari)', 'Retired Bank Officer', 'A.V.P', '3336297457', 'Z.T.B.L', NULL, 'Wife', NULL, NULL, NULL, 'sh Abarar .jpeg', NULL, NULL, '2019-11-20 11:51:57', 0, '', 1500, 'eligible'),
(172, 'Muhammad ishaq', 'Muhammad Ashfaq', '1988-10-15', 'B.A', 'Married', '', '3347201334', '36601-6609185-1', NULL, NULL, 'N/A', '5000', '', '50000', '4181613', 'vehari', '113 Gulshane Rehman Multan Road Burewala', '113 Gulshane Rehman Multan Road Burewala', 'Mevina Restuarant', 'Owner', '067-3352017', 'Quaid e Azam Model Town Burewala Stadium', NULL, 'Wife', 'Muhammad Ashfaq', NULL, '36601-1324025-5', 'ishaq_1.jpg', NULL, NULL, '2019-11-20 11:47:04', 0, '', 1500, 'eligible'),
(173, 'Hussain Haider', 'Muhammad Arshad Shah', '1993-01-06', 'Doctor (MBBS)', 'Single', '', '33471911008', '36601-5263885-9', 'king.hdr@gmail.com', NULL, 'N/A', '5000', '', '50000', '4181605', 'Blood Relation', '96 D Block Burewala Distric Vehari', '96 D Block Burewala Distric Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'ishaq_2_Hussain haider.jpg', NULL, NULL, '2019-11-20 11:41:01', 0, '', 1500, 'eligible'),
(174, 'sardar Zahoor Ahmad Dogar', 'Muhammad Rafique Dogar', '1975-01-11', 'M.A L.L.B', 'Married', 'B+', '3006999445', '36601-2564575-7', 'zahoordogar7@gmail.com', NULL, 'N/A', '5000', '', '50000', '4181717', 'vehari', 'House No 6 Block V Housing Scheme Burewala', 'House No 6 Block V Housing Scheme Burewala', 'Bar Association Burewala', 'Advocate High Court', '3006999445', 'Chamber No 8 Jinah Block', NULL, 'Wife', 'Fouzia Zahoor', '1977-12-05', '36601-1681459-4', 'ishaq_3_Zahoor Ahmad.jpg', NULL, NULL, '2019-11-20 11:38:04', 0, '', 1500, 'eligible'),
(175, 'Amer Habib', 'Muhammad Habib Ullah', '1979-02-02', 'B.A', 'Married', 'B+', '3347786002', '36601-3401182-7', NULL, NULL, 'N/A', '5000', '', '100000', '4181611', 'new member', '114 A Block Burewala', '114 A Block Burewala', 'Tulvan Property Advisor', '65.A Block Burewala', NULL, NULL, NULL, 'Wife', 'Shumaila Amen', '1976-08-15', '36601-5106482-4', 'ishaq_4_Amir habib.jpg', NULL, NULL, '2019-11-20 11:33:51', 0, '', 1500, 'eligible'),
(176, 'Rao Zaheer Ahmad', 'Nazir Ahmad Mehmood', '1992-06-06', 'F.A', 'Married', 'A+', '3366997778', '36601-7613803-7', 'zaheer.rao.77@gmail.com', NULL, 'N/A', '5000', '4181539', '100000', '4181721', 'new member', '180 A Model Town 437 E/B Burewala', '180 A Model Town 437 E/B Burewala', 'Zee Collection', 'Owner', '3366997778', NULL, NULL, 'Wife', 'Maha', '1995-02-24', '42000-545840-2', 'ishaq_5_Zaheer.jpg', NULL, NULL, '2019-11-20 11:29:57', 0, '', 1500, 'eligible'),
(177, 'Waqas Ahmad', 'Mushtaq Ahmad', '1989-04-24', 'Matric', 'Married', '', '3333444913', '36601-9216706-5', NULL, NULL, 'N/A', '5000', '4579300', '100000', '4181716', 'new member', 'House No 129 D Block Burewala Vehari', 'House No 22 New K Block Vehari', 'Waqas and Co Company', 'owner', '0345-7097444', 'waqas and Co Shop 74 /G Galla Mandi', NULL, 'Wife', 'Tabinda Waqas', '1995-08-04', '36302-6720943-6', 'waqas Ahmad .jpeg', NULL, NULL, '2019-11-20 11:03:15', 0, '', 1500, 'eligible'),
(178, 'Sheikh Muhammad Faheem', 'Taj Din', '1988-01-30', 'Matric', 'Married', 'B+', '33347207077', '36601-73378407-1', NULL, NULL, 'N/A', '5000', '4181538', '100000', '4181708', 'new member', 'House no 356 Lalazar Colony Burewala', 'House no 356 Lalazar Colony Burewala', 'Ahmed Commison Shop', '4/G Grain Market Burewala', '33347207077', 'Owner', NULL, 'Wife', 'Madiha Mukhtar', '1988-01-30', '36601-8116058-4', 'Sheikh Muhammad Faheem .jpeg', NULL, NULL, '2019-11-20 10:47:58', 0, '', 1500, 'eligible'),
(179, 'Muhammad Abbas', 'Haji Muhammad ishaq', '1985-01-04', 'B.com', 'Married', '', '3006993296', '36601-2507834-9', 'sheikabbas96@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 5 P Block Burewala', 'House No 5 P Block Burewala', 'Haji Faqir Flour And Ghee Industries', 'Managing Director', '0673355696', '7KM Multan Road Burewala', NULL, 'Wife', 'Doctor Sadia Abbas', '1977-09-27', '36601-3483453-2', 'abbas.jpg', NULL, NULL, '2019-11-20 10:42:58', 0, '', 1500, 'eligible'),
(180, 'Mian Umair salar', 'Mian Zaheer Ahmad', '1986-06-12', 'Graduation', 'Married', 'B+', '3334048181', '36601-3670829-3', 'Umairsalar@gmail.com', NULL, 'N/A', '5000', '4609225', '100000', '4609224', 'new member', 'House No 141 Block A Burewala', 'House No 141 Block A Burewala', 'Salar & Associates', 'Owner', '0673500777', 'Gagoo Garden Main Multan Road Gaggo Mandi Burewala', NULL, 'Wife', 'Dr. Mehreen munir', '1995-06-14', '36603-652639-8', 'Mian Umair Salar .jpeg', NULL, NULL, '2019-11-20 09:48:06', 0, '', 1500, 'eligible'),
(181, 'Mian Iftakhar Sabar', 'Muhammad Hanif', '1975-02-02', 'BA LLB', 'Married', 'AB+', '0300-6991373', '36601-3746316-9', NULL, NULL, 'N/A', '5000', '', '100000', '4609023', 'new member', 'House No 120 P Block Lahore Road Burewala', 'House No 120 P Block Lahore Road Burewala', 'Mian Law Chamber', 'Advocate High Court', '0300-6991373', 'Chamber No 19 B Jinnah Block Courts Burewala', NULL, 'Wife', 'Shabnam Kirm', '1981-04-05', '36601-2545567-6', 'Iftikhar.jpeg', NULL, NULL, '2019-11-25 07:51:53', 0, '', 1500, 'eligible'),
(182, 'Ahmad waqas', 'Malik Muhammad Anwar', '1986-04-03', 'BBA', 'Married', 'AB+', '0333-6282672', '36601-3082233-3', NULL, NULL, 'N/A', '5000', '4392800', '100000', '4609231', 'new member', 'House No 11 A Block Burewala', 'House No 11 A Block Burewala', 'MHA Biotech Private Limited', 'Owner', 'Vehari Bazar Burewala', '0333-6282672', NULL, 'Wife', 'Hafiza Ammara Iftikhar', '1991-02-22', '33102-6738360-8', 'IMG-20191128-WA0038.jpg', NULL, NULL, '2019-11-28 12:50:24', 0, '', 1500, 'eligible'),
(183, 'Muhammad Shahid Ali', 'Muhammad Sadique', '1973-02-11', NULL, 'Married', NULL, '0301-7933636', '36601-3023767-5', NULL, NULL, 'N/A', '5000', '4609191', '100000', '4609190', 'new member', 'House No 318 Str No 8 Muhalla Muhammad Ngar Dist Vehari', 'House No 318 Str No 8 Muhalla Muhammad Ngar Dist Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'Shahid Chohan .jpeg', NULL, NULL, '2019-11-25 07:42:50', 0, '', 1500, NULL),
(184, 'Adnan Ashraf', 'Muhammad Ashraf', '1980-09-07', 'Matric', 'Married', NULL, '0302-6999173', '36601-6265634-1', 'alazizmob@yahoo.com', NULL, 'N/A', '5000', '4609125', '100000', '4609193', 'new member', 'Gulshane Noo 505 Road Burewala', 'Al Aziz Mobile Al Aziz Market Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'Adnan.jpeg', NULL, NULL, '2019-11-25 07:36:48', 0, '', 1500, 'eligible'),
(185, 'Qaiser Waseem', 'Abdul Rehman', '1988-03-11', NULL, 'Married', 'B+', '3006999085', '36601-8015894-3', 'bhattiwasimol@gmail.com', NULL, 'N/A', '5000', '4609126', '100000', '4609192', 'new member', 'Po Khas Chak No 451 EB Burewala', 'Po Khas Chak No 451 EB Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Sadia Saeed', '1986-01-05', '36601-5744973-8', 'Qaiser Waseem.jpeg', NULL, NULL, '2019-11-25 07:25:25', 0, '', 1500, 'eligible'),
(186, 'Muhammad sadiq', 'Liaqat Ali', '1983-08-14', 'M.Com.MA ECo', 'Married', 'AB+', '0321-6999956', '31101-9178950-1', 'sadiq.hailian@gmail.com', NULL, 'N/A', '5000', '4609153', '100000', '4609188', 'new member', 'Chak No 173/9L Main Trikhni Road Ghaziabad Teh Chichawatni Distt Sahiwal', 'House No 23 Fine City Banglow Canal Road Al Habib Super Mall Burewala', 'MCB Bank Limited', 'Assistant Vice Perisident Branch Manager', '067-3771088', 'Ghalla Mandi Burewala`', NULL, 'Wife', 'Fozia Sadiq', '1989-10-10', '36601-3976699-6', 'Usama Arian_3_Sadiq.jpg', NULL, NULL, '2019-11-25 07:20:35', 0, '', 1500, 'eligible'),
(187, 'Fozia Anwar', 'Muhammad Anwar', '1977-12-03', 'M.Phil', 'Married', 'A+', '0333-8006109', '36601-1462260-8', 'foziaanwar87@gmail.com', NULL, 'N/A', '5000', '', '100000', '4609152+4609032', 'new member', 'Vehari Bazar House No 31 x Block Housing Scheme Burewala Distt Vehari', 'Vehari Bazar House No 31 x Block Housing Scheme Burewala Distt Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', 'Muhammad Shafiq Khan', '1972-05-15', '36601-2392518-7', NULL, NULL, NULL, '2019-11-25 07:11:26', 0, '', 1500, 'eligible'),
(188, 'Abdur Razzaq', 'Muhammmad Sardar', '1972-04-15', 'F.S.C', 'Married', 'B+', '0303-9389487', '90403-0167149-1', 'abdurrazzaq287@gmail.com', NULL, 'N/A', '5000', '4609154', '100000', '4609189', 'new member', 'Po khas Chak No 287 E/B Burewala Dist Vehari', 'Fine City Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Lubna Jamil', NULL, '36601-8956038-8', 'Abdur Razaq.jpeg', NULL, NULL, '2019-11-25 07:08:05', 0, '', 1500, 'eligible'),
(189, 'muhammad ijaz sarwar', 'Muhammad Sarwar', '1992-09-07', 'BSC', 'Single', 'B+', '0333-8283167', '36601-2462231-7', NULL, NULL, 'N/A', '5000', '', '100000', '', 'new member', 'House NO 48 Gulshane Ghani Burewala', 'House NO 48 Gulshane Ghani Burewala', 'Pakistan Administrative Service', 'Assistant Commissioner', '0333-8283167', NULL, NULL, 'Other', NULL, NULL, NULL, 'Usama_Arian_2_ijaz[1].jpg', NULL, NULL, '2019-11-25 07:02:07', 0, '', 1500, 'eligible'),
(190, 'Ch Usama ahmad Arian', 'Ch.Faqir Muhammad Arian', '1985-03-27', 'Master', 'Married', 'B+', '0300-5257103', '36601-8049735-1', 'Usmanch@gmail.com', NULL, 'N/A', '5000', '4221510', '100000', '', 'new member', 'House No 55 Block E Burewala', 'House No 55 Block E Burewala', 'Aljannat Dairies', 'Owner', '0300-5257103', 'Chak No 108 EB Burewala', NULL, 'Other', NULL, NULL, NULL, 'Usama_Arian_1[1].jpg', NULL, NULL, '2019-11-25 06:52:32', 0, '', 1500, 'eligible'),
(191, 'Usman Idrees', 'Muhammad Idrees', '2019-08-02', 'F.A', 'Single', 'B+', '0334-7793031', '36601-0853249-7', NULL, NULL, 'N/A', '5000', '0', '100000', '4309237', 'new member', 'Mulatan Road St No 3 Iqbal Nagar Burewala', 'Mulatan Road St No 3 Iqbal Nagar Burewala', 'Government Employ', 'Employ', '0334-7793031', NULL, NULL, 'Other', NULL, NULL, NULL, 'Usman Adrees .jpeg', NULL, NULL, '2019-11-25 06:43:55', 0, '', 1500, 'eligible'),
(192, 'Shakeel Ahmad', 'Ghulam Nabi', '1975-07-02', NULL, 'Married', 'B+', '0300-6993456', '36601_4060837-1', NULL, NULL, 'N/A', '5000', '2879205', '100000', '2879255', 'new member', 'Chak No 461/EB Burewala', 'Gulshan Raza Town Burewala', 'Qasim Palaza', 'Director', '0300-6993456', 'Vehari Bazar Burewala', NULL, 'Wife', 'Farhan Ilyas', '1973-01-01', '36601-9270920-4', 'New Doc 2019-11-09 13.11.36_1.jpg', NULL, NULL, '2019-11-21 19:51:58', 0, '', 1500, 'eligible'),
(193, 'Muhammad Nasir Mehmood', 'Muhammad Akhtar', '1983-01-01', 'Master', 'Married', 'A+', '0300_4572886', '36601-6289566-7', 'Gatewayburewala@Gmail.com', NULL, 'N/A', '5000', '', '100000', '4609235', 'new member', 'House No 218 Gulshane Rehman Multan Road Burewala Distt Vehari', 'House No 218 Gulshane Rehman Multan Road Burewala Distt Vehari', 'Gate way IELTS and study Abroad', 'Director', '067-3359019', '22 A Block Burewala', NULL, 'Wife', 'Noreen Sarwar', '1985-06-26', '36601-88512036-0', 'New Doc 2019-11-09 13.26.25_3.jpg', NULL, NULL, '2019-11-21 19:43:41', 0, '', 1500, 'eligible'),
(194, 'Hassan Mueez', 'Muhammad Mueez', '1990-11-15', 'MSC', 'Married', 'B+', '0300-7099970', '36601-7678501-9', 'Hmueez1@gmail.com', NULL, 'N/A', '5000', '', '50000', '4609208', 'vehari', 'House No 82 D Block Burewala', NULL, 'Al Majeed Rice Mill', 'Owner', '201/EB Burewala', '067-356500', NULL, 'Wife', 'Arfa Majid', '1996-12-19', '36601-9644456-4', 'New Doc 2019-11-10 15.14.27_3.jpg', NULL, NULL, '2019-11-25 07:53:04', 0, '', 1500, 'eligible'),
(195, 'Qasir Nazir', 'Ch Nazir Ahmad', '1964-04-02', NULL, 'Married', NULL, '0300-6993086', '36601-1548107-9', NULL, NULL, 'N/A', '5000', '', '50000', '4609216', 'vehari', 'House No 174 Settlite town Burewala', 'House No 174 Settlite town Burewala', 'Shahid Petrol pump', 'CEO', '067-9200027', 'Chal 201 E/B Burewala', NULL, 'Other', NULL, NULL, NULL, 'New Doc 2019-11-10 15.14.27_4.jpg', NULL, NULL, '2019-11-21 19:30:36', 0, '', 1500, 'eligible'),
(196, 'Pervaiz Ahmad Bhatti', 'Muhammad Shafi', '1960-07-14', 'F.A', 'Married', 'B+', '0321-3300321', '36601-9092107-1', NULL, NULL, 'N/A', '5000', '4181946', '100000', '4609255', 'new member', 'House No 217 satellite Town Burewala', 'House No 217 satellite Town Burewala', 'Pervaiz Ahmad Contructor', 'Owner', '067-3359321', 'House No 217 settlite Town Burewala', NULL, 'Wife', 'Rukhsana kausar', '1962-11-25', '36601-1596332-8', 'New Doc 2019-11-09 13.26.25_2.jpg', NULL, NULL, '2019-11-21 19:24:32', 0, '', 1500, 'eligible'),
(197, 'Qaiser Pervaiz', 'Abdul Rehman', '1984-11-13', 'B A', 'Married', NULL, '0321_5991111', '36601-4747811-5', NULL, NULL, 'N/A', '5000', '', '100000', '4221606', 'new member', '451 EB Bhattian Burewala', '451 EB Bhattian Burewala', 'Stars Acadmy', 'Teacher', '067-3603177', 'A Block Burewala', NULL, 'Wife', 'Maryam Batool', NULL, NULL, 'New Doc 2019-11-09 13.26.25_1.jpg', NULL, NULL, '2019-11-21 19:13:59', 0, '', 1500, 'eligible'),
(198, 'Muhammad Ashraf', 'Abdul Majeed', '1959-08-29', 'F.A', 'Married', 'A+', '0300_6999346', '36601-6354358-5', NULL, NULL, 'N/A', '5000', '4221603', '100000', '4609254', 'new member', 'House No 45 M Block Burewala', 'House No 121 O Block Burewala', 'Al Haj M Ashraf Bhatte', 'Owner', 'House No 121 O Block Burewala', '45 M Block Burewala', NULL, 'Son', '.Ammar Ashraf', '1999-03-18', '36601_6306860-1', 'New Doc 2019-11-09 13.11.36_3.jpg', NULL, NULL, '2019-11-21 19:08:31', 0, '', 1500, 'eligible'),
(199, 'Muhammad Zafar ali', 'Bashir Ahmad', '1988-09-23', 'SSC', 'Married', 'B+', '0334-8989089', '36601-8705583-5', 'Mzafarpk@gmail.com', NULL, 'N/A', '5000', '', '100000', '3894987', 'new member', 'House No 121 O Block Burewala', 'House No 121 O Block Burewala', 'Data Traders', 'Partner', '067_3351089', 'House No 58 Grain Market Burewala', NULL, 'Wife', 'Sadia Ahmad', '1987-01-14', '35202-9816433-2', NULL, NULL, NULL, '2019-11-21 19:01:22', 0, '', 1500, 'eligible'),
(200, 'Muhammad shazaf farooq', 'Muhammad Irshad Khan', '1982-12-24', 'B.SC', 'Married', 'B+', '0301_7994050', '36601-8457557-1', 'Asjadshazaf@gmail.com', NULL, 'N/A', '5000', '4609181', '100000', '4609182', 'new member', 'House No 95 D Block Burewala', 'House No 95 D Block Burewala', 'Shazaf Interprises', 'Director', '067-3770658', 'Office Habib Bank Building Urdu Bazar Lahore', NULL, 'Wife', 'Rubeena Shazaf', '1994-02-17', '35201-1408766-8', 'New Doc 2019-11-09 15.08.32_3.jpg', NULL, NULL, '2019-11-21 18:56:14', 0, '', 1500, 'eligible');
INSERT INTO `members_record` (`membership_id`, `name`, `father_name`, `DOB`, `Qualification`, `MaritaLStatus`, `BloodGroup`, `cell_no`, `Cnic`, `email`, `Landline`, `twitter`, `registration_fee`, `registration_slipNo`, `membership_fee`, `membership_slipNo`, `category`, `Address`, `TemporaryAddress`, `NameOfOrganization`, `Designation`, `BuisnessAddress`, `BuisnessMobile`, `blandline`, `Relation`, `SName`, `SDob`, `SCnic`, `image`, `password`, `created_at`, `updated_at`, `status`, `payments_status`, `service_charges`, `remarks`) VALUES
(201, 'Muhammad Ishaq', 'Mushtaq Ahmad', '1973-01-18', 'Graduation', 'Married', 'A+', '0333-6999399', '42000-0446006-1', 'Makkahtrv999@gmail.com', NULL, 'N/A', '5000', '', '50000', '4609008', 'vehari', 'House No 28 Gulshan e Rehman Phase 1 Burewala Distt Vehari', 'House No 28 Gulshan e Rehman Phase 1 Burewala Distt Vehari', 'Makkah Air Int Travels', 'C.E.O', 'City Gate Market College Road Burewala', '067-3354999', NULL, 'Wife', 'Samreena Arif', '1978-02-02', '34101-2358810-0', 'New Doc 2019-11-10 15.14.27_5.jpg', NULL, NULL, '2019-11-21 18:49:40', 0, '', 1500, 'eligible'),
(202, 'Mian khalid Hussain', 'Muhammad Ramzan', '1971-03-06', 'F.A', 'Married', NULL, '0300-6992210', '36601-1591464-7', NULL, NULL, 'N/A', '5000', '', '50000', '4609009', 'vehari', 'House No 52 New Model Town Burewala', 'House No 52 New Model Town Burewala', 'City Housing Scheme Burewala', 'Director', '0300-6992210', 'Burewala', NULL, 'Wife', 'Jameela Khalid', '1976-10-06', '36601-1030568-4', 'New Doc 2019-11-10 15.14.27_6.jpg', NULL, NULL, '2019-11-21 18:42:07', 0, '', 1500, 'eligible'),
(203, 'Mirza Muhammad Khalid', 'Bashir Ahmad', '1972-01-01', 'F.A', 'Married', NULL, '0300_7592347', '36601-3466333-5', NULL, NULL, 'N/A', '5000', '', '50000', '4609011', 'vehari', 'House No 95 city Housing Scheme', 'House No 95 city Housing Scheme', 'City Housing Scheme Burewala', 'Director', '0300_7592347', 'Burewala', NULL, 'Other', NULL, NULL, NULL, 'New Doc 2019-11-10 15.14.27_7.jpg', NULL, NULL, '2019-11-21 18:37:03', 0, '', 1500, NULL),
(204, 'Khalid Mahmood', 'Muhammad Mushtaq', '1961-10-01', 'BA', 'Married', NULL, '0300-6992063', '36601-8948473_7', NULL, NULL, 'N/A', '5000', '', '50000', '4609012', 'vehari', 'House No 31 Mohalla Model Town Burewala', 'House No 31 Mohalla Model Town Burewala', 'City Housing Scheme Burewala', 'Director', '067-3351089', 'Burewala', NULL, 'Other', NULL, NULL, NULL, 'New Doc 2019-11-10 15.14.27_8.jpg', NULL, NULL, '2019-11-21 18:32:11', 0, '', 1500, 'eligible'),
(205, 'Muhammad Latif', 'Chuadhary Talib Hussain', '1974-01-28', 'BS', 'Married', 'B+', '0300_7879558', '36601-6746729-3', NULL, NULL, 'N/A', '5000', '4609227', '100000', '4609226', 'new member', 'House No 24 New Model Town Burewala', 'House No 24 New Model Town Burewala', 'M.Latif Construction Company', 'C.E.O', '067-3770658', 'Chichawatni Road Opposite Punjab Bank', NULL, 'Wife', 'Faiza Arshad', '1980-06-26', '36601-0147031-2', NULL, NULL, NULL, '2019-11-21 18:24:27', 0, '', 1500, 'eligible'),
(206, 'Muhammad Ghazanfar ali', 'Basheer Ahmad', '1986-07-10', 'MBA', 'Married', 'B+', '0300-6999589', '36601-7772697-3', 'Mghazanfarpk@gmail.com', NULL, 'N/A', '5000', '4392802', '50000', '4609230', 'vehari', 'House no 121 D Block Burewala', 'House no 121 D Block Burewala', 'M/s Data Traders', 'Partner', '067-3351089', '58 G Grain Market Burewala', NULL, 'Wife', 'Hafiza Mehwish', '1987-06-04', '36601-7611475-0', NULL, NULL, NULL, '2019-11-21 18:18:35', 0, '', 1500, 'eligible'),
(207, 'Muhammad Asif Ibrahim', 'Muhammad Ibrahim', '1984-11-15', NULL, 'Married', 'A+', '0321-6990222', '36601-5600749-3', NULL, NULL, 'N/A', '5000', '', '100000', '4609187', 'new member', 'Makan No 21 strt No 3 Block No k Burewala Distt Vehari', 'No 3 Block No k Burewala Distt Vehari', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-21 18:05:54', 0, '', 1500, 'eligible'),
(208, 'Tariq mahmood Bajwa', 'Ch Khadam Hussain Bajwa', '1972-04-01', 'B A', 'Married', 'B+', '0303-4444313', '36601-9953372-9', NULL, NULL, 'N/A', '5000', '4181914', '50000', '4579255', 'vehari', 'Chal No 313/EB Burewala', 'House No 120 Defence block Model town Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'Usman Ghani_13_Tariq Mahmood Bajwa.jpg', NULL, NULL, '2019-11-21 18:02:07', 0, '', 1500, 'eligible'),
(209, 'Muhammad ali Fiaz', 'Sardar Muhammad Abid Azeem', '1993-11-16', 'Engineer', 'Single', 'AB+', '0334_7208150', '36601_2311868-9', NULL, NULL, 'N/A', '5000', '4181730', '100000', '4609197', 'new member', 'HouseNo 27 Azeemabad Burewala', 'HouseNo 27 Azeemabad Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'New Doc 2019-11-09 13.26.25_4.jpg', NULL, NULL, '2019-11-21 17:57:47', 0, '', 1500, 'eligible'),
(210, 'Shahid Javed', 'Muhammad Yaqoob', '1975-04-27', 'F.S.C', 'Married', 'A+', '0300_7594157', '36601-0421109-1', NULL, NULL, 'N/A', '5000', '4609141', '100000', '4609142', 'new member', 'Dogar House ( MPA House) Bangla Kamal Road Burewala', 'Dogar House ( MPA House) Bangla Kamal Road Burewala', 'Al Madina Transport Company', 'Director', '067-3355357', 'Mehboob Petroleum Service', NULL, 'Wife', 'Sajeela Akhter', '1975-01-12', '36601_7940158_4', 'New Doc 2019-11-09 14.13.38_4.jpg', NULL, NULL, '2019-11-21 17:52:43', 0, '', 1500, 'eligible'),
(211, 'Muhammad Amin', 'Muhammad Din', '1965-11-01', 'Under Matric', 'Married', 'B+', '0300_6991089', '36601_1440502_5', NULL, NULL, 'N/A', '5000', '4609168', '100000', '4609165', 'new member', 'Marzi Pura St No 01 Yousaf Block No 3 Burewala', 'Marzi Pura St No 01 Yousaf Block No 3 Burewala', 'Mepco Wapda', 'Lineman', '067-9200027', 'Wapda Complex vehari Bazar Burewala', NULL, 'Wife', 'Zohaib ul Hassan', '2000-11-10', '36601-2555042-3', 'IMG-20191109-WA0074.jpg', NULL, NULL, '2019-11-21 17:44:30', 0, '', 1500, 'eligible'),
(212, 'Muhammad Bilawal Hameed', 'Tariq Mehmood', '1990-03-26', 'M.A Political Science', 'Single', '', '0300-6992292', '36601-0225907-3', 'Bilawalhameedbutt@gmail.com', NULL, 'N/A', '5000', '4609108', '100000', '4609016', 'new member', 'Strt No 1 Aziamabad Burewala Distt Vehari', 'Strt No 1 Aziamabad Burewala Distt Vehari', 'Butt Protein Farm', 'Owner', '0300-6992292', 'Arifwala D/s Pakpattan', NULL, 'Other', NULL, NULL, NULL, 'IMG-20191109-WA0073.jpg', NULL, NULL, '2019-11-21 17:34:58', 0, '', 1500, 'eligible'),
(213, 'Muhammad Imran', 'Mian Khan', '1980-10-10', 'M.Phil', 'Married', 'A+', '0322-7500120', '36501-0319312_5', 'Imranmgc5263@gmail.com', NULL, 'N/A', '5000', '4609007', '100000', '4609015', 'new member', 'Mariyam girls college 8 settlite town Burewala', 'mariyam Girls college Burewala', 'Mariyam girls college Burewala', 'Director', '8 settlite town lahore Road Burewala', '067_3770006', NULL, 'Wife', 'Kalsoom', '1991-11-29', '36603_8335266_6', 'IMG-20191109-WA0072.jpg', NULL, NULL, '2019-11-21 16:27:39', 0, '', 1500, NULL),
(214, 'Hassan Raza', 'Shafqaat Munir', '1989-04-15', 'Matric', 'Married', NULL, '0321-6996997', '36601-5989931-1', NULL, NULL, 'N/A', '5000', '4579096', '100000', '4609172', 'new member', '106 N Burewala', '106 N Burewala', 'Ghosia Iron Store', 'owner', '0321-6996997', 'Vehari Bazar Burewala', NULL, 'Other', NULL, NULL, NULL, 'Hassan Raza.jpg', NULL, NULL, '2019-11-21 15:44:09', 0, '', 1500, 'eligible'),
(215, 'Asim Ali', 'Hashim Ali', '1989-10-04', 'F.A', 'Married', 'B+', '0333-6293080', '36601-9548800-3', NULL, NULL, 'N/A', '5000', '4181680', '100000', '4609171', 'new member', '22 N Block Burewala', '22 N Block Burewala', 'Hashim Jewellers', 'owner', 'H Block Burewala', '0333-6293080', NULL, 'Wife', 'Rubab Saleem', '1993-12-05', '42000-9478441-6', 'Asim .jpeg', NULL, NULL, '2019-11-21 15:40:43', 0, '', 1500, 'eligible'),
(216, 'Bilal Farooq', 'Umer Farooq', '1990-05-04', 'Matric', 'Married', 'B-', '0301-7682050', '36601-7120235-7', NULL, NULL, 'N/A', '5000', '4181681', '100000', '4609136+4609174', 'new member', '12 N Block Burewala', '12 N Block Burewala', 'Naeem Medicine', 'owner', '0301-7682050', 'Burewala', NULL, 'Wife', 'Ayesha', '1990-07-14', '36601-6126451-0', 'Bilal_1.jpg', NULL, NULL, '2019-11-21 15:02:35', 0, '', 1500, 'eligible'),
(217, 'Wajid Sarwar', 'Muhammad Sarwar', '1987-12-12', 'Business Management', 'Married', 'B+', '0308-4258848', '36601-9758307-7', 'wajidsarwar101@gmail.com', NULL, 'N/A', '5000', '4181561', '100000', '4609170', 'new member', 'Chak No 461/EB Burewala', 'Chak No 461/EB Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Azba Shaheen', '1986-03-05', '36601-1796969-2', 'Wajid sarwar .jpeg', NULL, NULL, '2019-11-21 14:56:29', 0, '', 1500, 'eligible'),
(218, 'Rana Shahid Sarwar', 'Rana Muhammad Sarwar', '1975-01-01', 'B.A', 'Married', 'A+', '0300-6998913', '36601-5726614-7', NULL, NULL, 'N/A', '5000', '4579326', '100000', '4609137+4609169', 'new member', 'House No 27 Stadium Road New Model Town Burewala', 'House No 27 Stadium Road New Model Town Burewala', 'Agriculture', 'Farmer', '287 EB / 275 EB', '0300-6998913', NULL, 'Wife', 'Mehwish Rana', '1984-08-10', '36601-8739158-8', 'Rana Shahid .jpeg', NULL, NULL, '2019-11-21 14:46:18', 0, '', 1500, 'eligible'),
(219, 'Asif Nadeem', 'Riaz Muhammad', '1972-03-20', 'B.A', 'Married', 'AB+', '0300-4985101', '36601-7754901-7', NULL, NULL, 'N/A', '5000', '4181796', '100000', '4609186', 'new member', 'House No 94/11 I Block Burewala Distt Vehari', 'House No 94/11 I Block Burewala Distt Vehari', 'Riaz Dentel Clinic', 'Dental Hygienist', '0300-4985101', '94/11 I Block Burewala', NULL, 'Wife', 'Nadia Asif', '1979-07-19', '35302-1906669-0', 'Asif Nadeem.jpeg', NULL, NULL, '2019-11-21 14:40:42', 0, '', 1500, 'eligible'),
(220, 'Muhammad afzal', 'Abdul Haq', '1980-10-15', 'B.A', 'Married', '', '0321-6990416', '36601-4557419-7', 'afzal6990@yahoo.com', NULL, 'N/A', '5000', '469177', '100000', '4609173', 'new member', 'House NO 416 Madina Coloni St No Multan Burewala', 'House NO 416 Madina Coloni St No Multan Burewala', 'Al Fateh Sanitary Store (M.Afzal)', 'Owner', '0321-6990416', 'Al Fateh Sanitary Store (M.Afzal)', NULL, 'Wife', NULL, NULL, NULL, 'muhammad afzal.jpeg', NULL, NULL, '2019-11-21 14:35:50', 0, '', 1500, 'eligible'),
(221, 'Muhammad Nosher khan Anjum', 'Zahoor Ahmad', '1963-03-16', NULL, 'Married', NULL, '0304-9117790', '36601-4110958-7', NULL, NULL, 'N/A', '5000', '', '100000', '4609138', 'new member', 'House No 31 Block Red Town Housing Scheme Burewala', 'House No 31 Block Red Town Housing Scheme Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'Malik Nosher Langryal .jpeg', NULL, NULL, '2019-11-25 13:10:57', 0, '', 1500, 'eligible'),
(222, 'Visha Shahzad', 'Shahzad Mubeen', '1998-09-09', 'MBBS', 'Single', 'B+', '0300-8690419', '36601-4438957-4', NULL, NULL, 'N/A', '5000', '4609132', '50000', '4181720', 'Blood Relation', 'House NO 83 D Block Burewala', 'House NO 83 D Block Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-21 14:02:25', 0, '', 1500, 'eligible'),
(223, 'Sajjad Haider', 'Shafqat Munir', '1987-02-15', 'F.A', 'Married', NULL, '0300-6996997', '36601-5880081-3', NULL, NULL, 'N/A', '5000', '4579095', '50000', '4609173', 'vehari', 'Makan No 106 Block NoBurewala Distt Vehari', 'Makan No 106 Block NoBurewala Distt Vehari', 'Ghosia Iron Store', 'Owner', '0300-6996997', 'Vehari Bazar', NULL, 'Wife', 'Rabia', '1987-02-15', '36601-4094373-2', 'Sajjad Haider .jpg', NULL, NULL, '2019-11-21 14:00:35', 0, '', 1500, 'eligible'),
(224, 'Muhammad Arshad', 'Muhammad Ashiq', '1978-10-22', 'Matric', 'Married', 'A+', '0322-6824969', '36601-7973123-1', NULL, NULL, 'N/A', '5000', '4579480', '100000', '15538900', 'new member', '21F Burewala Dist Vehari', '21F Burewala Dist Vehari', 'Businessman / Properity and shoaping Center', 'Owner', '0322-6824969', 'Gago Mandi Ahmad Garden', NULL, 'Wife', 'Munawar naz', '1980-01-19', '36601-6827170-2', 'New Doc 2019-11-21 20.47.42_1_Arshad.jpg', NULL, NULL, '2019-11-21 13:52:16', 0, '', 1500, 'eligible'),
(225, 'Muhammad Nadeem', 'Muhammad Jamil', '1980-08-31', 'Matric', 'Married', NULL, '0300-3309455', '36601-1605207-9', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 140 Strt No 16 Habib COlony Burewala', 'House No 140 Strt No 16 Habib COlony Burewala', 'Mudasir Cloth House', 'owner', '0300-3309455', 'Luddan Road Burewala', NULL, 'Wife', 'Farzana Nadeem', '1984-01-01', '36402-7664215-6', NULL, NULL, NULL, '2019-11-21 13:44:27', 0, '', 1500, 'eligible'),
(226, 'Sajid Sharif', 'Muhammad Sharif', '1968-02-02', NULL, 'Married', 'B+', '0336-0043500', '36601-8976623-5', 'Sajidatrsco.net', NULL, 'N/A', '5000', '4609258', '100000', '4609257', 'new member', 'Chak No 435/EB Burewala', 'Chak No 435/EB Burewala', 'Bab Fareed', 'CEO', '0336-0043500', '435/EB', NULL, 'Wife', 'Samera Afzal', '1980-01-01', '36601-6136741-6', 'Sajid Sharif .jpeg', NULL, NULL, '2019-11-21 13:41:03', 0, '', 1500, 'eligible'),
(227, 'Sikandar Hayat', 'Maqbool Ahmad', '1992-04-14', 'Graduation', 'Single', 'A-', '0333-6281845', '36601-2704957-7', NULL, NULL, 'N/A', '5000', '4181988', '100000', '4609233', 'new member', 'PO Box CHak No 445-EB Burewala', 'House No 6 Block V Housing Sheme Burewala Dist Vehari', 'Dogar Bricks Company', 'Sole Proprietor', '445.EB Burewala', '0333-6281845', NULL, 'Other', NULL, NULL, NULL, 'Sikanadar Hayat .jpeg', NULL, NULL, '2019-11-21 13:34:48', 0, '', 1500, 'eligible'),
(228, 'Iyaz Haider Khan', 'Inayat Ali', '1972-04-15', 'B.A', 'Married', 'B+', '0333-6283656', '36601-6338681-9', NULL, NULL, 'N/A', '5000', '', '25000', '', 'old member', 'House No 26 D Block Burewala', 'House No 26 D Block Burewala', 'Baban Company', 'owner', '0333-6283656', '39 G Gala Mandi Burewala', NULL, 'Other', NULL, NULL, NULL, 'Iyaz Haider Khan.jpeg', NULL, NULL, '2019-11-21 12:57:11', 0, '', 1500, 'eligible'),
(229, 'izhar Ahmad', 'Abdul Haq', '1965-03-10', 'M.A English', 'Married', 'B+', '0300-9728166', '36601-3862519-3', 'Shaikghee@yahoo.com', NULL, 'N/A', '5000', '4609185', '100000', '4609184', 'new member', 'House No 165 Near masjid Rizwan Gulshan E Rehman Burewala', 'House No 165 Near masjid Rizwan Gulshan E Rehman Burewala', 'Oil & Gas Development Company Limited', 'Sr.Ad', '0723-660683', 'Qadirpur Gas Field Ghotki', NULL, 'Wife', 'Sobia Izhar', '1971-06-08', '36601-0813773-4', NULL, NULL, NULL, '2019-11-21 12:50:14', 0, '', 1500, 'eligible'),
(230, 'Ali Raza', 'Muhammad Ramzan', '1991-03-01', 'F.A', 'Single', 'B+', '0333-3405333', '36601-3831467-5', 'sheikhali.raza123@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'bedminton', 'House No 320 Strt No 9 Muhalla Yaqoobabad Burewala', 'House No 320 Strt No 9 Muhalla Yaqoobabad Burewala', 'Ali City Cable', 'owner', 'Yaqoob Abad', '0333-3405333', NULL, 'Other', NULL, NULL, NULL, 'Ali Raza badmintton .jpeg', NULL, NULL, '2019-11-21 12:39:13', 0, '', 1500, 'eligible'),
(231, 'Sheikh Noor Ul Amin', 'Sheikh Naseer ud din Ahmad', '1994-09-30', 'Mphil.Ms', 'Single', 'A+', '0300-0798949', '36601-6832278-1', 'Nooramin473@gmail.com', NULL, 'N/A', '5000', '', '25000', '', 'bedminton', '23/x Vehari Bazar Burewala', '23/x Vehari Bazar Burewala', 'Almughani Medical Store', 'Owner', '0333-6296792', '23 X Vehari Bazar Burewala', NULL, 'Other', NULL, NULL, NULL, 'Noor Amin .jpeg', NULL, NULL, '2019-11-21 12:30:30', 0, '', 1500, 'eligible'),
(232, 'Zain ul Abdin *Bedminton', 'Abdul Razaq', '1992-04-12', 'F.A', 'Married', '', '0333-3355170', '36601-6692499-1', 'zainali356@yhaoo.com', NULL, 'N/A', '5000', '', '25000', '', 'bedminton', 'House No 36 Block H Burewala', 'House No 36 Block H Burewala', 'Mudasir Cloth House', 'CO Owner', '0333-3355170', 'Rehan Gali Burewala', NULL, 'Other', NULL, NULL, NULL, 'zain .jpeg', NULL, NULL, '2019-11-21 12:25:12', 0, '', 1500, 'eligible'),
(233, 'Hamayun zafar', 'Dr.Zafar Iqbal', '1995-02-03', 'Medical Student', 'Married', 'B+', '0333-6998086', '36601-4653848-9', NULL, NULL, 'N/A', '5000', '3709187', '50000', '2879118', 'Blood Relation', 'House No 12/Y Str B Housing Scheme burewala', 'House No 12/Y Str B Housing Scheme burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'Hamayoun.jpg', NULL, NULL, '2019-11-21 12:07:27', 0, '', 1500, 'eligible'),
(234, 'Ch.Bukhtayar Ahmed Gujjar', 'Ch.Mukhtar Ahmed', '1959-03-10', 'F.A', 'Married', 'B+', '0300-6991405', '36601-1585670-7', NULL, NULL, 'N/A', '5000', '', '50000', '4609017', 'vehari', 'Chak No 405/EB Teh Burewala', '45 Gulshane Rehman Ghani Burewala', 'Five star Developers', 'Director', 'Canal Residencia Canal Road Burewala', '0333-6297405', NULL, 'Wife', 'Naheed Bukhtar', '1963-01-01', '36601-6624054-0', 'New Doc 2019-10-27 10.41.18_5_Bakhtayar gujjar.jpg', NULL, NULL, '2019-11-21 12:04:10', 0, '', 1500, 'eligible'),
(235, 'Muhammad Aslam Khan', 'Rao Noor Muhammad', '1986-12-25', 'Graduation', 'Married', 'AB+', '0333-6287522', '36601-0206474-7', NULL, NULL, 'N/A', '5000', '', '100000', '3894719', 'new member', 'Model Town Chak No 437 EB Post Office Burewala', 'Model Town Chak No 437 EB Post Office Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Nayab Aslam', '1985-10-09', '31104-3188840-4', 'Aslam Khan.jpg', NULL, NULL, '2019-11-21 11:59:41', 0, '', 1500, 'eligible'),
(236, 'Muhammad Sagheer', 'Ch. Noor Muhammad Ramay', '1948-03-05', 'F.A', 'Married', '', '0300-6991399', '36601-9316417-7', NULL, NULL, 'N/A', '5000', '', '50000', '4609109', 'vehari', '501-EB Burewala', 'House No 17 Gulshane Rahim Phase 2 Deffence view Ramay House Burewala', 'Al Ramay Industries', 'MD', '0300-6991399', 'Factory Area Multan Road Burewala', NULL, 'Wife', 'Shamim Akhtar', '1961-03-09', '36601-5943041-4', NULL, NULL, NULL, '2019-11-21 11:53:23', 0, '', 1500, 'eligible'),
(237, 'Muhammad Imran irshad', 'Ch Irshad Ahmad Arian', '0000-00-00', NULL, 'Married', NULL, '0334-7202071', '36601-2311831-5', NULL, NULL, 'N/A', '5000', '4609021', '100000', '4609024', 'new member', '74 A Pcsir I Fase Lahore', '36 E Block Burewala Distt Vehari', 'Advovate High Court', 'lawyer', '0334-7202071', 'Punjab Bar Lahore', NULL, 'Other', NULL, NULL, NULL, 'Imran .jpeg', NULL, NULL, '2019-11-21 11:49:00', 0, '', 1500, 'eligible'),
(238, 'Ch.Irshad Ahmad Arain', 'Ch Sardar Muhammad', '1953-01-01', 'B.A', 'Married', '', '0333-6885051', '36601-5461448-1', NULL, NULL, 'N/A', '5000', '4609020', '100000', '4609022', 'new member', '36 E Block Burewala Distt Vehari', '74 A PC sir I Faze Lahore', 'Madina Group of Industries', 'owner', '067-3352051', 'Burewala', NULL, 'Other', NULL, NULL, NULL, 'Irshad Arian.jpg', NULL, NULL, '2019-11-21 11:41:59', 0, '', 1500, 'eligible'),
(239, 'Muhammad Furqan Yousaf', 'Malik Muhammad Yousaf', '1974-07-08', 'BA LLB', 'Married', 'B+', '0300-6991163', '36601-8335069-1', 'Furqanfyfy@gmail.com', NULL, 'N/A', '5000', '', '100000', '4609167', 'new member', 'Chak No 463/EB Burewala', 'Chak No 463/EB Burewala', 'Tehsil COurts Burewala', 'Lawyer', '0300-6991163', 'Chamber No 7 Masjid Block', NULL, 'Wife', 'Rahila', NULL, NULL, 'Furqan .jpeg', NULL, NULL, '2019-11-21 11:28:50', 0, '', 1500, 'eligible'),
(240, 'Muhammad Khuram Riaz', 'Ch Riaz Ul Haq', '1983-12-31', 'MA.LLB', 'Married', 'B-', '0300-6997692', '36601-1616169-5', NULL, NULL, 'N/A', '5000', '4609166', '100000', '4609175', 'new member', 'House No 80 Block D Burewala Vehari', 'House No 80 Block D Burewala Vehari', 'Tehsil COurt Burewala', 'Tehsil Courts Burewala', 'Chamber No 3-4 C', '0300-6997692', NULL, 'Wife', 'Sumaira Zafar', '1987-12-04', '36603-54499505-8', 'khuram Riaz_1.jpg', NULL, NULL, '2019-11-21 11:20:43', 0, '', 1500, 'eligible'),
(241, 'Ch.Shahid Hussain', 'Haji Zaheer Ahmad', '1971-12-05', 'M.A', 'Married', 'A+', '0300-6993811', '36601-9985660-1', NULL, NULL, 'N/A', '5000', '4181782', '50000', '4609244', 'vehari', 'New Makah Mechanical Industries Multan Road Burewala', NULL, 'New Makah Mechahnical Industries', 'Owner', '067-3354256', 'Multan Road Burewala', NULL, 'Wife', 'Shazia Sharif', '1973-07-26', '36601-4497482-4', NULL, NULL, NULL, '2019-11-25 11:18:11', 0, '', 1500, 'eligible'),
(242, 'Talib Hussain', 'Haji Muhammad Waryam', '1961-01-01', NULL, 'Married', NULL, '', '36601-9872269-7', NULL, NULL, 'N/A', '5000', '', '100000', '4609243', 'new member', 'Post Office Sahoka Bhatti Teh Burewala Dist Vehari', 'House No 125 N Block Burewala', NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, 'Talib.jpg', NULL, NULL, '2019-11-25 11:21:53', 0, '', 1500, NULL),
(243, 'Muhammad Shahzad', 'Muhammad Saleem', '1991-11-26', 'Matric', 'Single', NULL, '0300-6992400', '36601-3120300-3', NULL, NULL, 'N/A', '5000', '4181956', '100000', '4609246', 'new member', 'House NO 70 Str No 1 Mohala Madina Colony Burewala', 'House NO 70 Str No 1 Mohala Madina Colony Burewala', 'Butt Briks', 'owner', '0300-6992400', 'Chak No 517 EB Near New Fruit Market Burewala', NULL, 'Wife', 'Ayesha Shehzad', '1997-11-29', '42201-4661377-0', 'Muhammad shahzad .jpg', NULL, NULL, '2019-11-25 11:32:01', 0, '', 1500, 'eligible'),
(244, 'Muhammad Asif Saeed', 'Muhammad Saeed', '1979-12-01', 'B.A', 'Married', 'B+', '0300-987053', '61101-1953785-7', 'asifsaeed53@gmail.com', NULL, 'N/A', '5000', '4181781', '100000', '4609245', 'new member', 'Chak No 453 EB Lot Burewala', 'Gulshan E Rehman Phase 2', 'S.A.K International', 'Managing Director', '067-3786253', '6km Multan Road Near Faqir Ghee Mills Burewala', NULL, 'Wife', 'ALia Shahid', '1979-05-17', '36601-1541297-4', 'Asif Saeed .jpeg', NULL, NULL, '2019-11-25 11:39:04', 0, '', 1500, 'eligible'),
(245, 'Amir Ghafoor', 'Abdul Ghafoor', '1986-11-27', 'M.B.A', 'Married', 'AB+', '0300-6993416', '36601-5469221-1', 'aamir-Pcl@hotmail.com', NULL, 'N/A', '5000', '4181832', '100000', '4609238', 'new member', 'Street No 4 House No 560 Madina Colony Multan Road Burewala', 'Street No 4 House No 560 Madina Colony Multan Road Burewala', 'Boduwalia Protein Diaryandagri Farm', 'Owner', '0300-6993416', 'Chak No 158 EB Burewala', NULL, 'Wife', 'Mubeen Matloob', '1986-07-27', '36601-1617057-6', 'Javed Iqbal_9_Amir Ghafoor.jpg', NULL, NULL, '2019-11-25 11:45:42', 0, '', 1500, 'eligible'),
(246, 'Abdul gaffar', 'Muhammad Azeem', '1976-12-12', 'M.Phil Pathalogy', 'Married', 'B+', '0300-6992166', '36601-4002508-5', 'digitalpcl@gmail.com', NULL, 'N/A', '5000', '4609066', '100000', '4609256', 'new member', 'Public Laib E Block Burewala', 'Public Laib E Block Burewala', 'Properity & Cilinical Laib', 'Owner', '067-3352166', 'Public Laib E Block Burewala', NULL, 'Wife', 'Rehana Kousar', '1973-03-08', NULL, 'Dr Abdul Ghaffar .jpeg', NULL, NULL, '2019-11-25 11:51:20', 0, '', 1500, 'eligible'),
(247, 'Tayyab Raza', 'Muhammad Sharif', '1986-08-26', 'B.Teh', 'Single', NULL, '0322-6800746', '36601-2726488-1', NULL, NULL, 'N/A', '5000', '4181834', '100000', '4609239', 'new member', 'Madina Colony House No 11', 'Gulshan E Rehman Housing Society', 'Nextage Imperial Consoltant', 'Director', '067-3355297', 'Chungi No 5 Burewala', NULL, 'Other', NULL, NULL, NULL, 'Tayab.jpeg', NULL, NULL, '2019-11-25 11:57:37', 0, '', 1500, 'eligible'),
(248, 'Professor Salahuddin', 'Abdul Latif', '1951-09-15', 'MSC. M.A . B.E.D', 'Married', NULL, '0300-6850024', '31202-9599836-1', NULL, NULL, 'N/A', '0', '', '10000', '3070622', 'Govt officer', 'Near Govt Girls School Sama Sata Tehsil & Dist Bahawalpur', 'Divisional Public School College Road Burewala', 'Divisional Public school', 'Principal', '0300-6850024', 'College Road Burewala', NULL, 'Wife', 'Mrs. Farhat Almas', '1955-05-12', '31202-0243423-8', 'Javed_Iqbal_10_salahudun[1].jpg', NULL, NULL, '2019-11-25 12:02:36', 0, '', 1500, 'eligible'),
(249, 'Muhammad Naeem CH.', 'Ch. Abdul khaliq', '1973-12-20', 'B.A', 'Married', NULL, '0322-3355050', '36601-0947878-9', NULL, NULL, 'N/A', '5000', '', '100000', 'online', 'new member', 'House No 30 Canal Garden Near Punjab College Burewala Dist Vehari', 'House No 30 Canal Garden Near Punjab College Burewala Dist Vehari', 'Ittefaq Protien Farms', 'Owner', '0322-3355050', 'Poultry Farm Machiwal', NULL, 'Other', NULL, NULL, NULL, 'Javed Iqbal_8_Muhammad Naeem.jpg', NULL, NULL, '2019-11-25 12:08:55', 0, '', 1500, 'eligible'),
(250, '', '', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '4609214', '100000', '4609201', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-18 15:26:01', 0, '', 1500, NULL),
(251, 'Dr iqbal ', '', '0000-00-00', NULL, 'Single', NULL, '', NULL, NULL, NULL, 'N/A', '5000', '4609215', '100000', '4609200', 'new member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-18 15:26:01', 0, '', 1500, NULL),
(252, 'Muhammad Imran', 'Habib Ur Rehman', '1979-03-11', 'M.A Islamiat', 'Married', 'AB+', '0300-6917508', '36501-4870424-7', NULL, NULL, 'N/A', '5000', '4609206', '50000', '', 'vehari', '86 O Block Burewala Vehari', '86 O Block Burewala Vehari', 'JR Fabrics', 'owner', '0300-6917508', 'Burewala', NULL, 'Other', NULL, NULL, NULL, 'Imran Sadiqi .jpeg', NULL, NULL, '2019-11-25 12:56:02', 0, '', 1500, 'eligible'),
(253, 'Waseem Akram', 'Muhammad Akram', '1973-04-04', 'F.A', 'Married', 'B+', '0333-6291629', '36601-7507971-9', NULL, NULL, 'N/A', '5000', '4609211', '100000', '4609028', 'new member', 'House NO 160 Block D Burewala Dist Vehari', 'House NO 160 Block D Burewala Dist Vehari', 'Rice Trading', 'Owner', '0333-6291629', 'Burewala', NULL, 'Other', NULL, NULL, NULL, 'Waseem AKram .jpeg', NULL, NULL, '2019-11-25 12:51:41', 0, '', 1500, 'eligible'),
(254, 'Nabeel Sabir CH', 'Ghulam Sarwar Ch.', '1969-08-15', 'M.A. LLB', 'Married', 'A+', '0333-6999123', '36601-6733909-1', 'nabeelsbc@gmail.com', NULL, 'N/A', '5000', '3048496', '100000', '4609203/4609199', 'new member', 'House No 19 W Block Vehari Bazar Road Housing Scheme Burewala', 'House No 19 W Block Vehari Bazar Road Housing Scheme Burewala', 'Pak Housing', 'Director', '0333-6999123', 'Office No 1 Chichawatni Road Burewala', NULL, 'Wife', 'Komal Naveed', NULL, NULL, 'Nabeel .jpeg', NULL, NULL, '2019-11-25 12:49:04', 0, '', 1500, 'eligible'),
(255, 'Abdul Rasheed', 'Adbul Hameed', '1976-04-08', 'Master LLB', 'Married', 'B+', '0333-6999444', '36601-1589087-1', NULL, NULL, 'N/A', '5000', '2875003', '100000', '4609205/4609204', 'new member', 'House No 105 Gulshan e Rehman Burewala', 'House No 129 St 54 G10/3 Islambad', 'Pak Housing', 'Director', '0333-6999444', 'Office No 1 Pak Housing Chichawatni', NULL, 'Wife', 'Saima Rasheed', '1976-08-15', NULL, 'Abdul Rasheed .jpeg', NULL, NULL, '2019-11-25 12:44:45', 0, '', 1500, 'eligible'),
(256, 'Muhammad zafar Abdullah', 'Muhammad Abdullah khan Asem', '1959-06-20', 'M.Phil', 'Married', NULL, '0300-6991686', '36601-9836392-7', NULL, NULL, 'N/A', '5000', '4609210', '100000', '4609252', 'new member', 'House No 19 New Model Town Burewala', 'House No 19 New Model Town Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'iqbal Begum', '1962-01-01', '36601-8815757-2', 'Zafar Abdullah .jpeg', NULL, NULL, '2019-11-25 12:40:30', 0, '', 1500, 'eligible'),
(257, 'Khalid Mahmood', 'Muhammad Yaqoob', '1969-09-15', 'MA.LLB', 'Married', 'A+', '0300-6991257', '36601-0440652-9', NULL, NULL, 'N/A', '5000', '4609139', '100000', '4609140', 'new member', 'Dogar House (MPA House) Bangla Kanal Road Burewala', 'Dogar House (MPA House) Bangla Kanal Road Burewala', 'Aggriculture', 'Farmer', '0300-6991257', 'Burewala', NULL, 'Wife', 'Nabila Khalid', '1974-01-01', '36601-0248154-6', 'Khalid Dogar .jpeg', NULL, NULL, '2019-11-25 12:36:06', 0, '', 1500, 'eligible'),
(258, 'Javaid Iqbal', 'Zafar Hussain', '1957-05-01', 'M.A (Urdu)', 'Married', 'B+', '0300-0767234', '36601-6266009-9', NULL, NULL, 'N/A', '5000', '4609209', '100000', '4609025', 'new member', 'Chak No 225/EB Teh Burewala Dist Vehari', '94/I Block Burewala', 'Maralam Girls College Burewala', 'Director', '0300-0767234', '8 Setlite Town Burewala', NULL, 'Wife', 'Sajida Gilani', '1958-10-23', '36601-8418707-4', 'Javed Iqbal_1.jpg', NULL, NULL, '2019-11-25 12:29:43', 0, '', 1500, 'eligible'),
(259, 'Saif ur Rehman', 'Nazir Ahmad', '1979-08-09', 'MBA', 'Married', 'AB+', '0300-4747740', '36601-1566017-3', 'rehmanur259@gmail.com', NULL, 'N/A', '5000', '', '100000', '', 'New Member', '259/EB Lot# Burewala Dist Vehari', '259/EB Lot# Burewala Dist Vehari', 'Hamdan Communications', 'Propritor', '0300-4747740', '40 M BLock Burewala', NULL, 'Wife', 'Naila Dawood', '1987-01-01', '36601-8069100-8', NULL, NULL, NULL, '2019-11-25 13:06:21', 0, '', 1500, 'eligible'),
(260, 'Waseem Iqbal', 'Muhammad Iqbal', '1984-06-20', 'Metric', 'Married', 'O+', '03007591418', '36601-4896608-5', 'waseemiqbal668@gmail.com', NULL, 'WASEEMIQBAL', '55000', NULL, NULL, NULL, 'Vehari', 'House No 156 M BLock Burewala', 'House No 156 M BLock Burewala', 'waseem khan and Brothers', 'Owner', '96 N Block Burewala', '96 N Block Burewala', '0673-772396', 'Wife', 'Samree Bano', '1985-03-07', '36601-8808645-3', '1574868267-waseem-iqbal.jpg', '$2y$10$ligRAnxV6XjijST5GxfZMupYzrVlGTPhQTdrwVLkpKzkzyZuKPEMu', '2019-11-27 14:24:27', '2019-11-27 14:57:13', 1, NULL, 1500, 'eligible'),
(261, 'Dr.Muhammad Rafiq Shahid', 'Ch.Siraj ud DIn', '12/11/1958', 'MBBS', 'Married', 'B+', '03006999700', '36501-1563199-3', 'drrafiqshahid@gmail.com', NULL, 'rafiqshahid', '205000', NULL, NULL, NULL, 'new member', 'Shahid Medical Center Burewala', 'Shahid Medical Center Burewala', 'Shahid Medical center', 'Doctor', 'Burewala', '0300-699970', '0674-344433', 'Wife', 'Rubina Shahid', '1960-06-01', '36601-3243233-2', 'Rafique shahid_1.jpg', '$2y$10$2R2D.OT/.J2rLjfg4sTAQu.5Yh.ienePElfwSdDPT6T2k1X2SEmDS', '2019-11-25 14:43:16', '2019-11-25 14:52:51', 1, NULL, 1500, 'eligible'),
(262, 'Usman Ahmad Warraich', 'ch Riaz Ahmed Warriach', '1973-11-08', 'Bachalor', 'Married', 'AB+', '0301-2444111', '36601-7534240-5', 'usmanahmadwarriach@gmail.com', NULL, 'Usman ahmad', '105000', NULL, NULL, NULL, 'New Memeber', 'Warraich Town Burewala', 'Warraich Town Burewala', 'City Housing scheme Burewala', 'Owner', '0301-2444111', 'City Housing scheme Burewala', NULL, 'Wife', 'Sarwat Usman', '1970-02-02', '36601-7428194-6', '1574923939-usman-ahmad-warraich.jpeg', '$2y$10$VKew0Ipi7w1LezYV6rjLweGSmEKbE7CSKFDFgESzW4H/diP7Iiyta', '2019-11-28 05:52:19', '2019-11-28 05:55:38', 1, NULL, 1500, 'eligible'),
(263, 'Rana Ehsan Ullah', 'Rana Mehmood ali Khan', '1951-01-03', 'Master', 'Married', 'A+', '0336-4240742', '36202-8527042-3', 'ranaumer@gmail.com', NULL, 'ranaumer', '30000', NULL, NULL, NULL, 'National Hero', 'House No 747 Block E Cannal View Housing Society Lahore', 'House No 747 Block E Cannal View Housing Society Lahore', NULL, NULL, NULL, NULL, NULL, 'Wife', NULL, NULL, NULL, '1574924729-rana-ehsan-ullah.jpeg', '$2y$10$D0JmX72rPd6R9.ofThZu8uiYfiRq.ffMGEK4J8upf5aYC/AZn2qV2', '2019-11-28 06:05:29', '2019-11-28 06:08:07', 1, NULL, 1500, 'eligible'),
(264, 'Salman Shahid', 'Syed Shahid Mehdi Naseem', '1979-05-04', 'Master', 'Married', 'A+', '0300-8449440', '35202-0791775-7', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New Member', '16-1 Civil Line Lahore Road', '39-C Tech Society Lahore', 'Al Syed Petrolium', 'Owner', '042-35774443', 'Burewala', NULL, 'Wife', 'Maliha Qadeer Khan', '1982-02-11', '35202-5370656-6', '1574925390-salman-shahid.jpeg', '$2y$10$74.of6p9pD6SYrXuC5W2K.7F52O.d7kMnyITINp4WGFbtmlSRDfxK', '2019-11-28 06:16:30', '2019-11-28 06:18:59', 1, NULL, 1500, 'eligible'),
(265, 'Sajjad Zahoor', 'Zahoor Ahmad', '25/12/1965', 'Metric', 'Married', 'B+', '0300-6999521', '36601-6528719-9', 'shehraozjutt321@gmail.com', NULL, NULL, '205000', NULL, NULL, NULL, 'New Memeber', 'House No 154-A Block Burewala', 'House No 154-A Block Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Shaista Sajjad', '1990-03-03', NULL, '1574926031-sajjad-zahoor.jpeg', '$2y$10$aoGdFeX5CNnQ1LKkDg.BXOr9ZyH8ekZ5QIeFRlw1cplbCxv.7xPy.', '2019-11-28 06:27:12', '2019-11-28 06:31:13', 1, NULL, 1500, 'eligible'),
(266, 'Arshad Aziz Bhatti', 'Aziz Bhatti', '1979-12-07', 'Metric', 'Married', 'B+', '0300-6999600', '36601-2056082-5', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New Memeber', 'House No 80 O Block Burewala', 'House No 80 O Block Burewala', 'Arshi jewelers', 'Owner', 'H Block Burewala', 'H Block Burewala', NULL, 'Wife', 'Sobia Saeed', '1984-12-01', '36601-9730205-4', '1574926742-arshad-aziz-bhatti.jpeg', '$2y$10$xxw.P4x/8mwB5NExzYQ1feL0BEd/.I9FKMsc0a7cYXxffn9/s60o6', '2019-11-28 06:39:03', '2019-11-28 06:41:16', 1, NULL, 1500, 'eligible'),
(267, 'Qulb A Hassan', 'Muhammad Ali', '1970-12-12', 'Metric', 'Married', 'B+', '0300-8999678', '36601-7475004-7', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New Memeber', 'Chak No 203 EB Teh Burewala Dist Vehari', 'Green Town Strt No 1 House No 4 Lahore Road Burewala', 'Hassan Traders', 'sale Properiter', '57 G Grain Market Burewala', '57 G Grain Market Burewala', NULL, 'Wife', 'Zobia Hassan', '1982-12-03', '36601-4832329-2', '1574927676-qulb-a-hassan.jpeg', '$2y$10$su84rlkN6dZ9u29UT/pXNuW9skaYYPa3AjQb.p76M5vdmST89DRAu', '2019-11-28 06:54:36', '2019-11-28 06:56:16', 1, NULL, 1500, 'eligible'),
(268, 'Adnan Irshad', 'Muhammad Irshad', '1975-05-05', 'Metric', 'Married', 'O+', '0300-6995758', '36601-3471317-5', 'adnanirshadramay@gmail.com', NULL, NULL, '205000', NULL, NULL, NULL, 'New Member', 'Chak No 501 EB Burewala', 'Ramay Market Gulshan Rehman Town Burewala', 'Adnan Corporation', 'owner', '0300-6995758', '19 G Ghalla Mandi Burewala', NULL, 'Wife', 'Shumaila Adnan', NULL, NULL, '1574928993-adnan-irshad.jpeg', '$2y$10$wVNaHR/tPbA6OukM8UPRf.hSsxy3yt0yTJxJX4WJTbCLQazISWqwO', '2019-11-28 07:16:34', '2019-11-28 07:17:35', 1, NULL, 1500, 'eligible'),
(269, 'Faryad Hussain', 'Muhammad Ismail', '1983-06-01', 'Metric', 'Married', 'B+', '0301-7994492', '36601-3794657-3', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New member', 'Madina Colony Street No 8 Marzi Poora Burewala', 'Madina Colony Street No 8 Marzi Poora Burewala', 'Faryad Zari Corporation', 'Sole Proprieter', '0301-7994492', '57 G Grain Market Burewala', NULL, 'Wife', 'Naseem Akhter', '1989-01-01', '36601-7294723-6', 'Faryad.jpeg', '$2y$10$Tw88Iq7gCMGIng8r5NeRLewem16T6OF1sgVfmAgQreZA6eAl0o50O', '2019-11-28 08:35:17', '2019-11-28 08:44:18', 1, NULL, 1500, 'eligible'),
(270, 'Brig Muhammad Aamir Hashmi (SIm) Ret', 'Muhammad Ahmed Syed Hashmi', '1964-12-28', 'M.Phil (IR)', 'Married', 'A+', '0321-2291052', '13101-6654183-7', 'hashmi281264@gmial.com', NULL, NULL, '30000', NULL, NULL, NULL, 'Brig (Army Officer)', 'House No 16 A Block Burewala', 'House No 16 A Block Burewala', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Dr.Uzma Fartima', NULL, NULL, 'Brg Amir.jpeg', '$2y$10$OSNa61s8rG5BvP/JORk5O.XPJWq3QyZyH5BA5J1fD0oHwxwzUJ5MW', '2019-11-28 08:36:44', '2019-11-28 08:39:01', 1, NULL, 1500, 'eligible'),
(271, 'Sheikh Muhammad Naeem', 'Malik Muhammad', '1970-01-01', 'Metric', 'Married', 'B+', '0300-6990943', '36601-1290777-3', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New member', '51/ M Block Burewala', '51/ M Block Burewala', 'A.S Motors', 'Owner', '0300-6990943', 'Joyia Road Burewala', NULL, 'Wife', 'Nasira Parveen', NULL, '36601-0699446-4', 'Naeem.jpeg', '$2y$10$QwM9d7tyiBOOc5zJgkhA0O59PqAEbiY.MvkV/U0iw3zzbB9hSSQUy', '2019-11-28 08:30:05', '2019-11-28 08:41:58', 1, NULL, 1500, 'eligible'),
(272, 'Ch.Nadeem Anwar Arain', 'Muhammad Anwar', '1984-01-26', 'Master', 'Married', 'O+', '0333-8999222', '36601-6806631-9', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New Member', 'Chak NO 453/EB Burewala', 'House No 93 Muhammad', 'Choudhary Traders', 'owner', '57/EB Grain Market Burewala', '57/EB Grain Market Burewala', NULL, 'Wife', 'Sonia Nisar', '1988-10-01', '36601-9757191-8', '1574929826-chnadeem-anwar-arain.jpeg', '$2y$10$I2P3.QtEV7BT8Cw3lNHQfeL4oF2GXY22CL6za5hf8/XgHP8Fvh43a', '2019-11-28 07:30:26', '2019-11-28 07:31:28', 1, NULL, 1500, 'eligible'),
(273, 'Talat Mahmood', 'Mukhtar Ahmad', '1975-03-01', 'Master', 'Married', 'A+', '0333-6999022', '36601-1609605-5', 'hasi3039@gmail.com', NULL, NULL, '205000', NULL, NULL, NULL, 'New member', 'Chak No 261 E/B Burewala', 'Chak No 261 E/B Burewala', 'Baba Traders', 'Owner', 'Chak No 261 E/B Burewala', '067-3603666', NULL, 'Wife', 'Asima Zameer', '1979-11-06', '36601-7521451-4', 'Talat .jpeg', '$2y$10$NamUayoSUBqsCFp/ey9rHegWfiixcoBswFDWFf6wrKeQhU5u2K80y', '2019-11-28 08:25:31', '2019-11-28 08:29:24', 1, NULL, 1500, 'eligible'),
(274, 'Umar Farooq', 'Khalid Farooq', '1985-01-01', 'F.A', 'Single', NULL, '0333-4114222', '36601-5165798-7', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New member', 'Main Road Sadiq Town Burewala', 'Main Road Sadiq Town Burewala', NULL, NULL, NULL, NULL, NULL, 'Father', 'Khalid Farooq', '1957-06-14', '36601-5398268-3', 'Umar Farooq.jpeg', '$2y$10$QpjUqPcar4PxOUfYLbPVTe8FipQLwGDO929pbb2QfI7P1C1/egMja', '2019-11-28 08:20:29', '2019-11-28 08:27:16', 1, NULL, 1500, 'eligible'),
(275, 'Ali Haider', 'Ch Muhammad Ashiq Arain', '2002-06-29', 'Metric', 'Single', 'B+', '0321-6999055', '36601-8648484-1', NULL, NULL, NULL, '105000', NULL, NULL, NULL, 'New member', 'House No 26 P Block Burewala', 'House No 26 P Block Burewala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ALi Haider.jpeg', '$2y$10$j7Yq0FOhr4aMiLpbRf4E7OglsM1mbhnVguWPb2l.sWYQEt7BvfewO', '2019-11-28 08:06:50', '2019-11-28 08:14:28', 1, NULL, 1500, 'eligible'),
(276, 'Muhammad Usman', 'Ch Muhammad Ashiq Arain', '1997-03-04', 'M.B.B.S', 'Single', 'B+', '0333-6999055', '36601-0719394-9', NULL, NULL, NULL, '105000', NULL, NULL, NULL, 'New member', 'House No 26 P Block Burewala', 'House No 26 P Block Burewala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Usman.jpeg', '$2y$10$pinxWqKVUlbKl.fpGbdMpOpx7.tFWfgEBuOM90CpsAxw/x.QDK/X.', '2019-11-28 08:04:22', '2019-11-28 08:17:04', 1, NULL, 1500, 'eligible'),
(277, 'Muhammad Ijaz', 'Muhammad Umer', '1981-01-04', 'Metric', 'Married', 'A+', '0300-6963132', '36601-8138008-7', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New Member', 'Awais Cotton Factory Luddon Road Chak No 461/EB Burewala', 'Al Fareed Cotton Factory Luddon Road Sahooka Teh Burewala', 'Nizami Company', 'owner', '0300-6963132', 'Luddon Road Burewala', NULL, 'Wife', 'Kiran', '0982-02-01', '36601-1496104-4', '1574932062-muhammad-ijaz.jpeg', '$2y$10$TZdIlRWXHBjVInkjYpAAeeUleJAWBnIa4jWRDQEMmQXp4vBGxuYH.', '2019-11-28 08:07:42', '2019-11-28 08:08:40', 1, NULL, 1500, 'eligible'),
(278, 'Saqib Jabbar', 'Abdul Jabbar', '1988-06-16', 'Metric', 'Married', 'A+', '0301-3434246', '36601-1136044-5', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New member', 'House No 219 A Gulshan rehman Umer Block', 'Chugi No 5 Shabbir Town burewala', 'Lucky Dubi Garments', 'owner', '0301-3434246', 'Rahim Yar Khan', NULL, 'Wife', 'Mrs Saqib', '1989-09-05', '31202-3337801-0', 'Saqib jabbar.jpeg', '$2y$10$cuGFFZtpnyDR967CpjBM5uJgPbq3TwBz7/wUhCNY8qXONFNgGZHfa', '2019-11-28 07:59:02', '2019-11-28 08:02:15', 1, NULL, 1500, 'eligible'),
(279, 'Rana Mazhar Mukhtar', 'Rana Mukhtar Ahmad', '1968-01-01', 'Metric', 'Married', NULL, '0300-7720800', '36601-7144167-3', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New member', 'House No 56 Scheme No 3 Civil Park Burewala', 'House No 56 Scheme No 3 Civil Park Burewala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Rana Mazhar Mukhtar .jpeg', '$2y$10$4fO4fNVoYkA8MKJr8B5ew.LVwmX/jMeOYjMQYNftH8I2HIeOcgUrG', '2019-11-28 07:50:14', '2019-11-28 07:56:22', 1, NULL, 1500, 'eligible'),
(280, 'Mubashar Marghub', 'Haji Marhub Ahmad', '1973-08-29', 'Metric', 'Married', NULL, '0301-7933392', '36601-2480666-9', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'new member', 'House No 19 Hameed block new model town burewala', 'House No 19 Hameed block new model town burewala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mubashar  2019-11-28 at 1.48.44 PM.jpeg', '$2y$10$5FmikFH0cTdtqbksYXHDW.kusuXaz/CgTFxcMND2d7YkWr2IV4DHa', '2019-11-28 07:45:14', '2019-11-28 07:52:58', 1, NULL, 1500, NULL),
(281, 'Ch Tanveer Ahmad Ramay', 'Faqeer Ahmad', '1968-02-07', 'Metric', 'Married', NULL, '0331-7211229', '36601-1407248-9', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New member', 'House No 146 Street No 1 Gulshan-e- Rehman burewala', 'House No 146 Street No 1 Gulshan-e- Rehman burewala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CH Tanvir .jpeg', '$2y$10$dFTEAX1bDrWILClKTEEbqOnkuG7g/2cTwNt0VuFGQ3EP1MTW44Ex.', '2019-11-28 07:42:02', '2019-11-28 07:47:19', 1, NULL, 1500, 'eligible'),
(282, 'Bilal Margub', 'Haji Marghub Ahmad', '1991-09-19', 'Metric', 'Married', 'O+', '0331-7980552', '36601-8135765-1', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'New Member', 'House No 19 Hameed Block New Model Town Burewala', 'House No 19 Hameed Block New Model Town Burewala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1574930531-bilal-margub.jpeg', '$2y$10$cGFArtdeh9VGVzbfpD0G2eQqKea4yEf6n5kPsAUI9FaUGrxTVAEaq', '2019-11-28 07:42:12', '2019-11-28 07:43:29', 1, NULL, 1500, 'eligible'),
(283, 'Muhammad Muzammal', 'Haji Marhub Ahmad', '1988-12-14', 'Metric', 'Married', 'A+', '0302-4457766', '36601-3098215-9', NULL, NULL, NULL, '205000', NULL, NULL, NULL, 'new member', 'House No 19 Hameed block new model town burewala', 'House No 19 Hameed block new model town burewala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Muzamal .jpeg', '$2y$10$Obi2Am1s4OjhAa37EHz90uysauOcNSdH1DLimRQhrFcE4Wd0ftdoe', '2019-11-28 07:34:13', '2019-11-28 07:37:20', 1, NULL, 1500, 'eligible'),
(284, 'Jahangir Khalid', 'Mirza Khalid Mehmmod', '1986-01-31', 'F.A', 'Married', 'A+', '0300-6992876', '36601-0794589-1', 'jahangirkhalid3@gmail.com', NULL, NULL, '205000', NULL, NULL, NULL, 'new member', '14/F Block Burewala', '14/F Block Burewala', 'jahangir Iron Store', 'Owner', '0300-6992876', '14/F Block tember Market Burewala', NULL, 'Wife', 'Asmat Iqbal', '1988-06-20', NULL, 'Jahangir.jpeg', '$2y$10$ikWyWhDN/LMyXRZbJFXu9OAFP.pegWODwbeJL2RjeLjT929ltjC0q', '2019-11-28 08:16:41', '2019-11-28 08:21:29', 1, NULL, 1500, 'eligible'),
(285, 'Khalid Zubair Nisar', 'Nisar Ahmad', '1989-11-07', 'Master', 'Married', 'B+', '0321-4444789', '36601-7192089-1', 'khaliddogar7@hotmail.com', NULL, NULL, '205000', NULL, NULL, NULL, 'New member', 'I Block HOuse No 13 Civil Line', 'I Block HOuse No 13 Civil Line', NULL, NULL, NULL, NULL, NULL, 'Wife', 'Zainub Sikandar', '1996-12-24', '36601-0275490-2', 'khalid.jpeg', '$2y$10$24ExcePEgIlBemFwJ7N05ul4zx1zLEkzqGqRicxCk5frPsd95Me9a', '2019-11-28 08:11:08', '2019-11-28 08:22:40', 1, NULL, 1500, 'eligible'),
(300, 'Talha', 'Pervaiz', '1998-03-13', 'Metric', 'Single', 'B+', '0333-6999039', '36601-2966413-3', 'talhapervaiz2338@gmail.com', NULL, NULL, '205000', NULL, NULL, NULL, 'New Member', 'House NO 117 Main Bazar Near National Bank Gaggo BUrewala', 'House No 129 D Block Burewala', 'Muhammad Pervaiz Islam & Sons', 'owner', '067-3500938', 'Grain Market Gaggo', NULL, NULL, NULL, NULL, NULL, '1574928477-talha.jpeg', '$2y$10$iw.P8IrFEapoi2s1V2WpgO6Ed8kG69pSSRAD.rh13eJqeiR2RVaHm', '2019-11-28 07:07:58', '2019-11-28 07:09:15', 1, NULL, 1500, 'eligible');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_08_03_000000_create_users_table', 1),
(2, '2019_08_03_042342_create_countries_table', 1),
(3, '2019_08_03_043804_add_more_fields_to_users_table', 1),
(4, '2019_08_03_100000_create_password_resets_table', 1),
(5, '2019_10_31_044756_create_customers_table', 2),
(6, '2019_11_05_091246_create_employees_table', 2),
(7, '2019_11_05_093522_create_emplyee_posts_table', 2),
(8, '2019_11_06_055822_create_emyloyee_pays_table', 3),
(9, '2019_11_06_081003_create_members_pay_records_table', 4),
(10, '2019_11_06_112301_create_construction_reports_table', 5),
(11, '2019_11_08_094206_create_members_table', 6),
(12, '2019_11_08_215128_create_hotel_report', 7),
(13, '2019_11_20_163648_create_categories_table', 8),
(14, '2019_11_20_164150_create_products_table', 8),
(15, '2019_11_20_164208_create_order_masters_table', 8),
(16, '2019_11_20_164238_create_order_details_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `monthly_charges`
--

CREATE TABLE `monthly_charges` (
  `MC_id` int(11) NOT NULL,
  `mr_id` int(5) NOT NULL,
  `Service_charges` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1500',
  `totalhotelcredit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_master_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_master_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2019-11-20 16:41:55', '2019-11-20 16:41:58'),
(2, 1, 2, 1, '2019-11-20 16:42:12', '2019-11-20 16:42:14'),
(3, 2, 2, 1, '2019-11-20 16:42:45', '2019-11-20 16:42:48'),
(4, 3, 1, 2, '2019-11-20 16:43:00', '2019-11-20 16:43:03'),
(5, 1, 1, 4, '2019-11-23 20:49:13', '2019-11-23 20:49:13'),
(6, 2, 1, 4, '2019-11-23 20:49:13', '2019-11-23 20:49:13'),
(7, 3, 1, 4, '2019-11-23 20:49:13', '2019-11-23 20:49:13');

-- --------------------------------------------------------

--
-- Table structure for table `order_masters`
--

CREATE TABLE `order_masters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `member_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `order_masters`
--

INSERT INTO `order_masters` (`id`, `member_id`, `date_time`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-11-20 22:43:18', '2019-11-20 16:43:21', '2019-11-20 16:43:23'),
(2, 1, '2019-11-20 22:43:36', '2019-11-20 16:43:38', '2019-11-20 16:43:40'),
(3, 2, '2019-11-20 22:44:24', '2019-11-20 16:44:26', '2019-11-20 16:44:29'),
(4, 3, '2019-11-20 22:44:38', '2019-11-20 16:44:40', '2019-11-20 16:44:42'),
(5, 1, '2019-11-20 22:41:55', '2019-11-23 20:49:13', '2019-11-23 20:49:13'),
(6, 1, '2019-11-20 22:41:55', '2019-11-23 20:49:13', '2019-11-23 20:49:13'),
(7, 1, '2019-11-20 22:41:55', '2019-11-23 20:49:13', '2019-11-23 20:49:13');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `discount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `price`, `discount`, `created_at`, `updated_at`) VALUES
(1, 1, 'chicken chili dry glace sauce with rice', 'chicken chili dry glace sauce with rice', 270, NULL, '2019-11-20 16:26:36', '2019-11-20 16:26:39'),
(2, 1, 'chicken manchurian classic sauce with rice', 'chicken manchurian classic sauce with rice', 230, NULL, '2019-11-20 16:28:01', '2019-11-20 16:28:07'),
(3, 3, 'chicken karahi', 'chicken karahi', 550, NULL, '2019-11-20 16:29:07', '2019-11-20 16:29:10'),
(4, 3, 'chicken ginger', 'chicken ginger', 430, NULL, '2019-11-20 16:29:36', '2019-11-20 16:29:38'),
(5, 3, 'chicken jalfrezi', 'chicken jalfrezi', 450, NULL, '2019-11-20 16:30:08', '2019-11-20 16:30:11'),
(6, 3, 'chicken boneless (Full)', 'chicken boneless (Full)', 800, NULL, '2019-11-20 16:31:51', '2019-11-20 16:31:53'),
(7, 3, 'chicken boneless (Half)', 'chicken boneless (Half)', 450, NULL, '2019-11-20 16:32:19', '2019-11-20 16:32:22'),
(8, 4, 'hot and sour soup', 'hot and sour soup', 80, NULL, '2019-11-20 16:33:08', '2019-11-20 16:33:10'),
(9, 2, 'tikka pizza', 'tikka pizza', 799, NULL, '2019-11-20 16:34:05', '2019-11-20 16:34:13'),
(10, 2, 'fujita pizza', 'fujita pizza', 820, NULL, '2019-11-20 16:34:42', '2019-11-20 16:34:45'),
(11, 2, 'corn pizza', 'corn pizza', 850, NULL, '2019-11-20 16:35:13', '2019-11-20 16:35:16'),
(12, 5, 'club sandwich 4 pcs', 'club sandwich 4 pcs', 210, NULL, '2019-11-20 16:36:20', '2019-11-20 16:36:22'),
(13, 5, 'crunchy sandwich 4 pcs', 'crunchy sandwich 4 pcs', 190, NULL, '2019-11-20 16:37:03', '2019-11-20 16:37:06'),
(14, 6, 'chicken loly pops 3 pcs', 'chicken loly pops 3 pcs', 150, NULL, '2019-11-20 16:38:48', '2019-11-20 16:38:51'),
(15, 6, 'chicken hot wings 6 pcs', 'chicken hot wings 6 pcs', 150, NULL, '2019-11-20 16:39:26', '2019-11-20 16:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'hotel_manager', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `admin`, `status`, `remember_token`, `created_at`, `updated_at`, `address`, `city`, `cnic`, `image`, `pincode`, `phone`, `role_id`) VALUES
(2, 'Faizan Chohan', 'admin@gmail.com', NULL, '$2y$10$EcS2Is6ltE2xABgKS8b2R.HSsFYRN.ulzb6vL82Ls4UeqNam5riv2', 1, NULL, NULL, NULL, '2019-11-27 20:23:30', 'New Lahore Road', 'Lahore', '', '', '', '03339777676', 1),
(3, 'Hotel Manager', 'manager@gmail.com', NULL, '$2y$10$EcS2Is6ltE2xABgKS8b2R.HSsFYRN.ulzb6vL82Ls4UeqNam5riv2', 0, NULL, NULL, NULL, NULL, 'New Lahore Road', 'Lahore', '', '', '', '', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `balances`
--
ALTER TABLE `balances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `chart_account_types`
--
ALTER TABLE `chart_account_types`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `chart_of_accounts`
--
ALTER TABLE `chart_of_accounts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `construction_reports`
--
ALTER TABLE `construction_reports`
  ADD PRIMARY KEY (`id`) USING BTREE;
ALTER TABLE `construction_reports` ADD FULLTEXT KEY `material` (`material`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `employees_email_unique` (`email`) USING BTREE,
  ADD UNIQUE KEY `cnic` (`cnic`) USING BTREE;

--
-- Indexes for table `emplyee_posts`
--
ALTER TABLE `emplyee_posts`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `emplyee_posts_title_unique` (`title`) USING BTREE;

--
-- Indexes for table `emyloyee_pays`
--
ALTER TABLE `emyloyee_pays`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `hotel_report`
--
ALTER TABLE `hotel_report`
  ADD PRIMARY KEY (`hb_id`) USING BTREE,
  ADD KEY `membership_id` (`members_id`) USING BTREE,
  ADD KEY `members_id` (`members_id`) USING BTREE;

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `members_email_unique` (`email`) USING BTREE;

--
-- Indexes for table `members_pay_records`
--
ALTER TABLE `members_pay_records`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `members_record`
--
ALTER TABLE `members_record`
  ADD PRIMARY KEY (`membership_id`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `monthly_charges`
--
ALTER TABLE `monthly_charges`
  ADD PRIMARY KEY (`MC_id`) USING BTREE,
  ADD KEY `hb_id` (`mr_id`) USING BTREE;

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `order_masters`
--
ALTER TABLE `order_masters`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `balances`
--
ALTER TABLE `balances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `chart_account_types`
--
ALTER TABLE `chart_account_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `chart_of_accounts`
--
ALTER TABLE `chart_of_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `construction_reports`
--
ALTER TABLE `construction_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `emplyee_posts`
--
ALTER TABLE `emplyee_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `emyloyee_pays`
--
ALTER TABLE `emyloyee_pays`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_report`
--
ALTER TABLE `hotel_report`
  MODIFY `hb_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `members_pay_records`
--
ALTER TABLE `members_pay_records`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=792;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `monthly_charges`
--
ALTER TABLE `monthly_charges`
  MODIFY `MC_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `order_masters`
--
ALTER TABLE `order_masters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `monthly_charges`
--
ALTER TABLE `monthly_charges`
  ADD CONSTRAINT `hb_id` FOREIGN KEY (`mr_id`) REFERENCES `members_record` (`membership_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
