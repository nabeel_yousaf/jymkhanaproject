<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
 *
 *  Admin Routes
 *
 *
 *  */
 
 Route::get('/clear-cache', function() {
  $exitCode = Artisan::call('config:cache');
  return "Cache is cleared";
});
 Route::get('/jwt-secret', function() {
  $exitCode = Artisan::call('jwt:secret');
  return "Secret Key Generated";
});

Auth::routes(['verify'=>true]);
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/hotel-sale-report','HotelSaleReportController@index');
    Route::get('/hotel-sale-report/edit/{order}','HotelSaleReportController@editSale');
    Route::get('/AddhotelBuying','HotelReport@buyingadd')->name('addbuying');
    
    Route::get('/buyingItem','HotelBuyingItemController@index')->name('buyingItem');
    Route::post('/buy-report/print','HotelBuyingItemController@print');
    Route::post('/buy-report/data','HotelBuyingItemController@data');


    Route::get('/create-buying-item','HotelBuyingItemController@create')->name('createBuyingItem');
    Route::post('/store-buying-item','HotelBuyingItemController@store')->name('storeBuyingItem');
    Route::post('/savehotelBuying','HotelBuyingController@buyingsave')->name('savehotelbuying');
    Route::post('/updateKitchItem','HotelBuyingController@updateKitchItem')->name('updateKitchItem');
    Route::resource('/products','ProductsController');
    Route::resource('/categories', 'CategoriesController');
    Route::get('/search-data-hotel','HotelBuyingController@search')->name('searchhotelbuyingreport');
    Route::get('/search-inventory-hotel','HotelBuyingController@inventoryReport')->name('inventoryReport');
    Route::get('/totalBuying','HotelReport@buying')->name('buying');
    Route::get('/totalBuying/edit/{hotel}','HotelReport@buyingEdit')->name('edit.buying');
    Route::post('/totalBuying/edit/{hotel}','HotelReport@buyingUpdate')->name('update.buying');
    Route::post('/totalBuying/delete/{hotel}','HotelReport@buyingDelete')->name('delete.buying');

    Route::get('/inventory','HotelReport@inventory')->name('inventory');
    Route::resource('/construction-reports','ConstructionReportController');
    Route::get('/bill_Num','ConstructionReportController@bill_number');
    
    Route::get('/Addsalerecord','HotelSaleReportController@addhotelsale')->name('Addsalerecord');
    Route::post('/Addsalerecord','HotelSaleReportController@savehotelsale')->name('Savesalerecord');
    Route::post('/updateSaleRecord/{order}','HotelSaleReportController@updateSaleRecord')->name('updateSaleRecord');
    Route::post('/deleteSaleRecord/{order}','HotelSaleReportController@deleteSaleRecord')->name('deleteSaleRecord');
    
    
    Route::get('/savesalerecord','HotelSaleReportController@savehotelsale')->name('savesalerecord');
    
    Route::get('/construction-reports/{id}/delete','ConstructionReportController@delete');
    Route::post('/sale-report/data','HotelSaleReportController@data');
    Route::post('/sale-report/print','HotelSaleReportController@print');
    Route::resource('/construction-material','ConstructionMaterialController');
});

Route::group(['prefix'=>'admin','middleware'=>['auth','admin']],function (){
    Route::get('/', 'AdminController@index')->name('admin_home');
    Route::get('/create-member-ob','DbScriptController@create_member_ob');
    Route::get('/inser-ob-records','DbScriptController@inser_ob_records');
    Route::get('/insert-dimentions-record','DbScriptController@insert_dimentions_record');
    Route::get('/insert-coa-dimensions-record','DbScriptController@insert_coa_dimensions_record');
    Route::get('/mapServiceFeeToSubsFee','DbScriptController@map_service_charges_to_subscription_fee');
    Route::get('/insert-membership-fee-reg-fee','DbScriptController@insert_membership_fee_reg_fee');
    Route::get('/debit-credit-for-service-revenue-dec','DbScriptController@debit_credit_for_service_revenue_dec');
    Route::get('/update-subs-fee','DbScriptController@update_subs_fee');
    Route::get('/insert-oct-nov-fee-vouchers','DbScriptController@insert_oct_nov_fee_vouchers');
    Route::get('/insert-paid-vouchers-record','DbScriptController@insert_paid_vouchers_record');
    Route::get('/get-missing-ids','DbScriptController@get_missing_ids');
    Route::get('/get-zero-fee','DbScriptController@fee_zero');
    Route::get('/voucher-entry-script1','DbScriptController@voucher_entry_script1');
    Route::get('/voucher-entry-script2','DbScriptController@voucher_entry_script2');
    Route::get('/voucher-entry-script3','DbScriptController@voucher_entry_script3');
    Route::get('/set-email-pass-members','DbScriptController@set_email_pass_members');
    Route::get('/insert-joining-date','DbScriptController@insert_joining_date');
    /// Setting Area
    Route::get('/settings', 'AdminController@settings')->name('admin-settings');
    Route::get('/ledger','ManageLedgerReportController@index')->name('ledger-report');
    Route::get('/detailed-ledger','ManageDetailedLedgerReportController@index')->name('detail-ledger-report');
    Route::get('/member-fee-cleared','ManageLedgerReportController@member_fee_cleared')->name('member-fee-cleared');
    Route::get('/member-fee-pending','ManageLedgerReportController@member_fee_pending')->name('member-fee-pending');
    Route::post('/ledger/data','ManageLedgerReportController@data')->name('ledger-data');
    Route::post('/detailed-ledger/data','ManageDetailedLedgerReportController@data')->name('detail-ledger-data');
    Route::post('/ledger/print', 'ManageLedgerReportController@print');
    Route::post('/detailed-ledger/print','ManageDetailedLedgerReportController@print');
    Route::get('/check-pwd','AdminController@chkPassword');
    Route::post('/update-pwd','AdminController@updatAdminPwd');
    Route::get('/show-users','UserController@show')->name('all-users');
    Route::get('delete-user/{id}','UserController@destroy');
    Route::get('delete-member-user/{id}','MemberController@destroy');
    Route::get('/user/{id}','UserController@listByUsers')->name('users');
    Route::match(['put', 'patch'], '/update-user/{id}','UserController@UserUpdate')->name('user-update');
    Route::get('/create-user','UserController@create')->name('create-user');
    Route::post('/register_user','UserController@register');
    Route::resource('/employee-post','EmplyeePostController');
    Route::resource('/employees','EmployeeController');
    Route::resource('/employees-pay','EmyloyeePayController');
    Route::resource('/customers','CustomerController');
    Route::get('/show-customers/{membership_id}','MemberController@customerDetail')->name('customers-detail');
    Route::get('/payment-history','MemberController@PaymentsHistory')->name('payment-history');
    Route::get('/edit/{membership_id}','editprofile@edit')->name('edit-profile');
    Route::post('/edit/{membership_id}','editprofile@update')->name('update-user');
    Route::get('/addbill/{membership_id}','HotelReport@Storebill')->name('Add-billtoMember');
    Route::match(['put', 'patch'], '/customer-user/{members_id}','MemberController@CustomerUpdate')->name('customer-update');
    Route::resource('/memberspayrecord','MembersPayRecordController');
    //voucher entry-voucher.create
    Route::get('/entry-voucher/create','ManageVoucherController@index')->name('voucher.create');
    Route::post('/entry-voucher/store','ManageVoucherController@store')->name('voucher.store');
    Route::post('/entry-voucher/save','ManageVoucherController@save')->name('voucher.save');
    Route::get('/entry-voucher/show','ManageVoucherController@show')->name('voucher.index');
    Route::post('/addVoucher', 'ManageVoucherController@addVoucher');
    Route::post('/editVoucher', 'ManageVoucherController@editVoucher');
    Route::get('entry-voucher/{id}/view', 'ManageVoucherController@viewVoucher');
    Route::get('/edit/voucher/{id}','ManageVoucherController@edit')->name('voucher.edit');
    Route::get('/delete/voucher/{id}','ManageVoucherController@destroy')->name('voucher.delete');


    // sale report



    Route::get('/showhoteltotal','HotelReport@showtotal')->name('showhoteltotal');
    Route::get('/shgenbill','HotelReport@genbills')->name('genbill');
    Route::get('/BillGenrate','HotelReport@Genbill')->name('GenerateBill');


    Route::get('/newmemberfull','MemberController@reportnewmember')->name('newmember');

    Route::get('/bedmintonmem','MemberController@reportbedmintonmember')->name('bedmintonmembers');

    Route::get('/oldmemberreport','MemberController@oldmemberreport')->name('oldmember');
    Route::get('/veharimemberreport','MemberController@veharimemberreport')->name('veharimembers');
    Route::get('/showmember','HotelReport@showbill')->name('searchmemberbill');
    Route::post('/updateimage','editprofile@updateImage')->name('imageget');
    Route::get('/billadd','HotelReport@BillAdd')->name('bill-Add');
    Route::get('/uploadbill','HotelReport@uploadbill')->name('Upload-Bill');

    Route::get('/menu-hotel','hotelreport@menu')->name('hotelmenu');
    Route::get('/search-data','ConstructionReportController@search')->name('construction.search');
    Route::get('/search-by_bill_num','ConstructionReportController@search_by_BillNum')->name('search_by_bill');

    Route::get('/searchmemberforbill','HotelReport@searchmember')->name('searchmemberidinbill');
    Route::post('/search-MEMBERS','membersreport@searchmember')->name('searchmember');
    Route::get('/generatePDF','ConstructionReportController@generatePDF')->name('generatePDF');
    Route::get('/chart-of-accounts','Admin\ChartOfAccountController@index')->name('showChartOfAccount');
    Route::get('/create/chart-of-accounts','Admin\ChartOfAccountController@create')->name('chartOfAccount.create');
    Route::post('/create/chart-of-accounts','Admin\ChartOfAccountController@store')->name('chartOfAccount.store');
    Route::get('/edit/chart-of-accounts/{id}','Admin\ChartOfAccountController@edit')->name('chartOfAccount.edit');
    Route::post('/update/chart-of-accounts/{id}','Admin\ChartOfAccountController@update')->name('chartOfAccount.update');
    Route::get('/delete/chart-of-accounts/{id}','Admin\ChartOfAccountController@delete')->name('chartOfAccount.delete');
    Route::get('/opening-balance/create','BalanceController@index')->name('balance.create');
    Route::post('/opening-balance/store','BalanceController@store')->name('balance.store');
    Route::get('/opening-balance/show','BalanceController@show')->name('balance.index');
    Route::get('delete-balance/{id}','BalanceController@destroy');
    Route::get('/edit/balance/{id}','BalanceController@edit')->name('balance.edit');
    Route::post('/balance/update/{id}','BalanceController@update')->name('balance.update');

    Route::get('/logs','LogsController@index')->name('logs');
    Route::get('/logs-data','LogsController@data')->name('logs-data');

});


/*
 *
 * Manager routes
 *
 * */
Route::group(['prefix'=>'manager','middleware'=>['auth','manager']], function (){
    Route::get('/','Manager\ManagerController@index');
    Route::get('/members/orders',[
        'as' => 'membersOrders',
        'uses' => 'Manager\MemberController@index'
    ]);
    Route::get('/show/product/data/{id}','Manager\MemberController@productShow');
});

/**
 * Member Routes
 */
Route::group(['prefix' => 'member','middleware'=>['auth','member']], function () {
    Route::get('/','Member\DashboardController@index');
    Route::get('/member-ledger', 'Member\MemberLedgerController@index');
});


/**
 * Secertry Routes
 */
Route::group(['prefix' => 'secertry','middleware'=>['auth','secertry']], function () {
    Route::get('/','Secertry\SecertryController@index');
    Route::get('/members/orders',[
        'as' => 'secertryMembersOrders',
        'uses' => 'Secertry\MemberController@index'
    ]);

    Route::get('/settings', 'Secertry\SecertryController@settings')->name('secertry-settings');
    Route::get('/check-pwd','Secertry\SecertryController@chkPassword');
    Route::post('/update-pwd','Secertry\SecertryController@updatSecertryPwd');

    Route::get('/show/product/data/{id}','Secertry\MemberController@productShow');

    Route::get('/logs','Secertry\LogsController@index')->name('secertrylogs');
    Route::get('/pendingApproval','Secertry\editprofile@index')->name('pendingapproval');
    Route::get('/ApproveProfile/{membership_id}','Secertry\editprofile@update')->name('approve-profile');

    Route::get('/voucherApproval','Secertry\ManageVoucherController@show')->name('voucherapproval');
    Route::get('/approveVoucher/{id}','Secertry\ManageVoucherController@addVoucher')->name('approve-voucher');
    Route::get('/voucherApproval/{id}/vieww', 'Secertry\ManageVoucherController@viewVoucher');
    
    Route::get('/show-customers/{membership_id}','Secertry\MemberController@customerDetail')->name('secertrycustomers-detail');
});


/**
 * Operator Routes
 */
Route::group(['prefix' => 'operator','middleware'=>['auth','operator']], function () {
    Route::get('/','Operator\OperatorController@index');
    Route::get('/memberspayrecord','Operator\MembersPayRecordController@index')->name('operatorMemberspayrecord.index');
    Route::get('/memberspayrecord/create','Operator\MembersPayRecordController@create')->name('operatorMemberspayrecord.create');
    Route::get('/memberspayrecord/store','Operator\MembersPayRecordController@store')->name('operatorMemberspayrecord.store');
    Route::get('/show-customers/{membership_id}','Operator\MemberController@customerDetail')->name('operatorcustomers-detail');
    Route::get('/edit/{membership_id}','Operator\editprofile@edit')->name('operatoredit-profile');
    Route::post('/edit/{membership_id}','Operator\editprofile@update')->name('operatorupdate-user');

    Route::get('/payment-history','Operator\MemberController@PaymentsHistory')->name('operatorPayment-history');

    Route::get('/chart-of-accounts','Operator\ChartOfAccountController@index')->name('operatorShowChartOfAccount');
    Route::get('/create/chart-of-accounts','Operator\ChartOfAccountController@create')->name('operatorChartOfAccount.create');
    Route::post('/create/chart-of-accounts','Operator\ChartOfAccountController@store')->name('operatorChartOfAccount.store');
    Route::get('/edit/chart-of-accounts/{id}','Operator\ChartOfAccountController@edit')->name('operatorChartOfAccount.edit');
    Route::post('/update/chart-of-accounts/{id}','Operator\ChartOfAccountController@update')->name('operatorChartOfAccount.update');
    Route::get('/delete/chart-of-accounts/{id}','Operator\ChartOfAccountController@delete')->name('operatorChartOfAccount.delete');
    
    Route::resource('/operator-employee-post','Operator\EmplyeePostController');
    Route::resource('/operator-employees','Operator\EmployeeController');
    Route::resource('/operator-employees-pay','Operator\EmyloyeePayController');
    
    Route::get('/user/{id}','Operator\UserController@listByUsers')->name('operator-users');
    Route::get('/delete-user/{id}','Operator\UserController@destroy');
    Route::match(['put', 'patch'], '/update-user/{id}','Operator\UserController@UserUpdate')->name('operator-user-update');
    
    Route::resource('/operator-construction-reports','Operator\ConstructionReportController');
    Route::get('/operator-bill_Num','Operator\ConstructionReportController@bill_number');
    Route::get('/operator-construction-reports/{id}/delete','Operator\ConstructionReportController@delete');
    Route::resource('/operator-construction-material','Operator\ConstructionMaterialController');
    Route::get('/search-data','Operator\ConstructionReportController@search')->name('operator-construction.search');
    Route::get('/search-by_bill_num','Operator\ConstructionReportController@search_by_BillNum')->name('operator-search_by_bill');
    Route::get('/generatePDF','Operator\ConstructionReportController@generatePDF')->name('operator-generatePDF');
    
    Route::get('/searchmemberforbill','Operator\HotelReport@searchmember')->name('operator-searchmemberidinbill');
    Route::get('/operator-hotel-sale-report','Operator\HotelSaleReportController@index');
    Route::get('/operator-hotel-sale-report/edit/{order}','Operator\HotelSaleReportController@editSale');
    Route::get('/AddhotelBuying','Operator\HotelReport@buyingadd')->name('operator-addbuying');
    Route::get('/search-data-hotel','Operator\HotelBuyingController@search')->name('operator-searchhotelbuyingreport');
    Route::get('/search-inventory-hotel','Operator\HotelBuyingController@inventoryReport')->name('operator-inventoryReport');
    Route::get('/totalBuying','Operator\HotelReport@buying')->name('operator-buying');
    Route::get('/totalBuying/edit/{hotel}','Operator\HotelReport@buyingEdit')->name('edit.operator-buying');
    Route::post('/totalBuying/edit/{hotel}','Operator\HotelReport@buyingUpdate')->name('update.operator-buying');
    Route::post('/totalBuying/delete/{hotel}','Operator\HotelReport@buyingDelete')->name('delete.operator-buying');
    Route::get('/buyingItem','Operator\HotelBuyingItemController@index')->name('operator-buyingItem');
    Route::post('/operator-buy-report/print','Operator\HotelBuyingItemController@print');
    Route::post('/operator-buy-report/data','Operator\HotelBuyingItemController@data');
    Route::post('/operator-sale-report/data','Operator\HotelSaleReportController@data');
    Route::post('/operator-sale-report/print','Operator\HotelSaleReportController@print');
    Route::get('/create-buying-item','Operator\HotelBuyingItemController@create')->name('operator-createBuyingItem');
    Route::post('/store-buying-item','Operator\HotelBuyingItemController@store')->name('operator-storeBuyingItem');
    Route::post('/savehotelBuying','Operator\HotelBuyingController@buyingsave')->name('operator-savehotelbuying');
    Route::post('/updateKitchItem','Operator\HotelBuyingController@updateKitchItem')->name('operator-updateKitchItem');
    Route::get('/Addsalerecord','Operator\HotelSaleReportController@addhotelsale')->name('operator-Addsalerecord');
    Route::post('/Addsalerecord','Operator\HotelSaleReportController@savehotelsale')->name('operator-Savesalerecord');
    Route::post('/updateSaleRecord/{order}','Operator\HotelSaleReportController@updateSaleRecord')->name('operator-updateSaleRecord');
    Route::post('/deleteSaleRecord/{order}','Operator\HotelSaleReportController@deleteSaleRecord')->name('operator-deleteSaleRecord');
    Route::resource('/operator-products','Operator\ProductsController');
    Route::resource('/operator-categories', 'Operator\CategoriesController');
    Route::get('/operator-inventory','Operator\HotelReport@inventory')->name('operator-inventory');
    
    Route::get('/shgenbill','HotelReport@genbills')->name('operator-genbill');
    Route::get('/BillGenrate','Operator\HotelReport@Genbill')->name('operator-GenerateBill');

    Route::get('/entry-voucher/create','Operator\ManageVoucherController@index')->name('operatorvoucher.create');
    Route::post('/entry-voucher/store','Operator\ManageVoucherController@store')->name('operatorvoucher.store');
    Route::post('/entry-voucher/save','Operator\ManageVoucherController@save')->name('operatorvoucher.save');
    Route::get('/entry-voucher/show','Operator\ManageVoucherController@show')->name('operatorvoucher.index');
    Route::post('/addVoucher', 'Operator\ManageVoucherController@addVoucher');
    Route::post('/editVoucher', 'Operator\ManageVoucherController@editVoucher');
    Route::get('/entry-voucher/{id}/view', 'Operator\ManageVoucherController@viewVoucher');
    Route::get('/edit/voucher/{id}','Operator\ManageVoucherController@edit')->name('operatorvoucher.edit');

    Route::get('/ledger','Operator\ManageLedgerReportController@index')->name('operatorledger-report');
    Route::get('/member-fee-cleared','Operator\ManageLedgerReportController@member_fee_cleared')->name('operatormember-fee-cleared');
    Route::get('/member-fee-pending','Operator\ManageLedgerReportController@member_fee_pending')->name('operatormember-fee-pending');
    Route::post('/ledger/data','Operator\ManageLedgerReportController@data')->name('operatorledger-data');
    Route::post('/ledger/print', 'Operator\ManageLedgerReportController@print');
    Route::get('/operator-detailed-ledger','Operator\ManageDetailedLedgerReportController@index')->name('operator-detail-ledger-report');
    Route::post('/operator-detailed-ledger/data','Operator\ManageDetailedLedgerReportController@data')->name('operator-detail-ledger-data');

    Route::post('/search-MEMBERS','Operator\membersreport@searchmember')->name('operatorsearchmember');
    
    Route::get('/operator-voucherApproval','Operator\ManageVoucherApprovalController@show')->name('operator-voucherapproval');
    Route::get('/operator-voucherApproval/{id}/vieww', 'Operator\ManageVoucherApprovalController@viewVoucher');
    Route::get('/show-customers/{membership_id}','Operator\MemberController@customerDetail')->name('operatorcustomers-detail');
    
    Route::get('/pendingApproval','Operator\editprofile@index')->name('operator-pendingapproval');
});


/// Simple User Login /////
Route::get('/customer-login','MemberController@index')->name('customer-login');
Route::get('/customer-register','MemberController@registered')->name('customer-register');
Route::post('/customer_login','MemberController@login');
Route::post('/register_customer','MemberController@register')->name('register_customer');
Route::post('/user_login','UserController@login');
Route::post('/register_user','UserController@register');
Route::get('/logout','CustomerController@logout');

/* FrontEnd Location */
Route::get('/','IndexController@index')->name('/');

Route::get('/insertEmployeeToUserTable', 'UserController@insert');
Route::get('/inserNullUserIdRecord', 'UserController@insertNullUserIdRecord');
Route::get('/createOperator', 'UserController@createOperator');
Route::get('/createSecertry', 'UserController@createSecertry');















