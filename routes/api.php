<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login','API\AuthController@authenticate');

Route::group(['middleware' => 'jwt2.auth'], function () {

    Route::post('/order/processing', 'API\AuthController@orderProcessing');
    Route::get('/category/{member_id}', 'API\AuthController@category');
    Route::get('/product/by/category', 'API\AuthController@productByCategory');
    Route::post('/member-ledger','API\MemberLedgerController@index');
});
Route::post('tab-order-processing','API\TabOrderProcessingController@tab_order_processing');
Route::post('tab-categories','API\TabOrderProcessingController@tab_categories');
Route::post('tab-products','API\TabOrderProcessingController@tab_products');
