<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Gym Khana Burewaala</title>
    @include("manager.layouts.headerScripts")
 @yield('styles')
</head>
<body>
<div class="container-scroller">
@section('header')
@include('manager.layouts.header')
@show
<div class="container-fluid page-body-wrapper">
@section('nav')
@include('manager.layouts.nav')
@show
<div class="main-panel">
<div id="content">
    @yield('content')
</div>
@include('manager.layouts.footer')
@include("manager.layouts.footerScripts")
</div>
</div>
</div>
@stack('jsblock')
</body>
</html>
