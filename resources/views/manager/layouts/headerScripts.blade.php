<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
<!-- iCheck -->
<link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
<!-- Plugin css for this page -->
<!-- endinject -->
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">