@extends('manager.layouts.master')
@section('title','Manager Dashboard')
@section('styles')
    <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Members Order List</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/manager')}}">Home</a></li>
                            <li class="breadcrumb-item active">Members Order List</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="table-responsive">
                <table id="orderList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm">#</th>
                        <th class="th-sm">Member ID
                        </th>
                        <th class="th-sm">Member Name
                        </th>
                        <th class="th-sm">Order Date Time
                        </th>
                        <th class="th-sm">Products
                        </th>
                        <th class="th-sm">Total
                        </th>
                        <th class="th-sm">Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $count = 0; @endphp
                    @foreach($member as $val)
                        @php $count = $count + 1; @endphp
                        <tr>
                            <td>{{$count}}</td>
                            <td>{{$val['member_id']}}</td>
                            <td>{{$val['name']}}</td>
                            <td>{{$val['date_time']}}</td>
                            <td>{{$val['total_products']}}</td>
                            <td>{{array_sum($val['product_price'])}}</td>
                            <td>
                                <a href="{{$val['order_id']}}" style="cursor: pointer;" class="btn btn-outline-info" data-target="#details"
                                   data-name="" data-id="{{$val['order_id']}}"
                                   data-address="" data-branch_description=""
                                   data-toggle="modal"><i class="mdi mdi-pencil mr-1 text-muted"></i>Details</a>
                                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Cancel Order</button>
                      
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </section>
    </div>
    <!-- details modal -->
    <div id="details" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <h4 class="modal-title" style="position: absolute;">Product Details</h4>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row" style="float: right;position: relative; right: 11px;">
                        <button type="button" class="btn btn-outline-primary">Print Invoice</button>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>price</th>
                                <th>Sub Total</th>
                            </tr>
                            </thead>
                            <tbody id="tBody">
                            </tbody>
                        </table>
                    </div>
                    <p>Total = <span id="subTotal"></span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info">Name Paid</button>
                    <button type="button" class="btn btn-outline-success">Debit To Member Account</button>
                </div>
            </div>

        </div>
    </div>
    <!-- details modal -->
    
    <!-- cancel  modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cancel Message to </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label> 
  <textarea class="form-control" id="message-text" rows="7"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
        <button type="button" class="btn btn-primary" data-dismiss="modal">Send Cancellation Message</button>
      </div>
    </div>
  </div>
</div>

    
    <!-- Cancel  modal -->
   
@endsection
@push('jsblock')
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('js/sweetalert.min.js')}}"></script>
    <script>
        $(function () {
            $('#orderList').DataTable();
        });
    </script>
    <script>
        $('#details').on('show.bs.modal', function (event) {
            $('#tBody').html('');
            $('#subTotal').html('');
            var href = $(event.relatedTarget);
            var id = href.data('id');
            var link = '/manager/show/product/data/'+id;
            $.ajax({
                url:link,
                type:'GET',
                success: function (response) {
                    if(response.success === true) {
                            var data = response.data[0].order_details;
                            $('#tBody').html('');
                            var price = [];
                            data.map(function (data) {
                               console.log(data);
                               var html = '<tr>' +
                                   '<td class="product_name">' + data.product.name + '</td>' +
                                   '<td class="quantity">' + data.quantity + '</td>' +
                                   '<td class="price">' + data.product.price + '</td>' +
                                   '<td class="sub_total">' + data.quantity * data.product.price + '</td>' +
                                   '</tr>';
                               price.push(data.quantity * data.product.price);
                               $('#tBody').append(html);
                            });
                            console.log("price");
                            console.log(price);
                            console.log("price");
                            var sum = price.reduce(function(a, b){return a+b;});
                            console.log('sum array values');
                            console.log(sum);
                            $('#subTotal').html(sum);
                        }
                },
                error:function (error) {
                    console.log(error);
                }
            });
        });
    </script>
    <script>
    $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('Cancel Order ' )
  modal.find('.modal-body input').val()
})
    </script>
@endpush
