@extends('operator.layouts.master')
@section('title','List Categories')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
<!-- iCheck -->
<link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
<!-- Plugin css for this page -->
<!-- endinject -->
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detailed Ledger</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
            <form method="post" id="reportForm" action="/operator/operator-detailed-ledger/print" target="_blank" style="display: none;">
                @csrf()
                <input type="hidden" name="coaId" id="coa-id">
                <input type="hidden" name="startDate" id="start-date">
                <input type="hidden" name="endDate" id="end-date">
            </form>
        <div class="row">

            <div class="col-md-3">
                <label>Select Chart of Account</label>
                <div class="form-group">
                    <select class="form-control selectAccount" name="coa" id="coa">
                        
                            <option value="0">All</option>
                        @foreach($accounts as $item)
                            @if($item['membership_id'] != '')
                                <option value="{{ $item['id'] }}" data-parent-id="{{ $item['parent_id'] }}" data-name="{{ $item['name'] }}"> ({{ $item['membership_id'] }}) {{ $item['name'] }}</option>
                            @else
                                <option value="{{ $item['id'] }}" data-parent-id="{{ $item['parent_id'] }}" data-name="{{ $item['name'] }}">{{ $item['name'] }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                    <label>Start Date</label>
                    <input type="date" name="startDate" id="startDate" class="form-control">
            </div>
                <div class="col-md-3">
                    <label for="end-date">End Date</label>
                    <input type="date" name="endDate" id="endDate" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Click button to Print Report</label>
                    <button type="button" class="form-control btn btn-success" id="printReport">Print</button>
                </div>
        </div>
        <div class="container">
            <table class="table table-border" id="ledgerTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>M Id</th>
                        <th>Voucher Date</th>
                        <th>Type</th>
                        <th>Narration</th>
                        <th>Dimension</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                    </tr>
                </thead>
            </table>
        </div>
    </section>

    <!-- /.content -->
</div>
@endsection
@section('jsblock')
<script type="text/javascript">
    @if (Session::has('success'))
            toastr.success("{{Session::get('success')}}");
    @endif
</script>
<script>

    function populateTable() {
        let parentId = $('#coa').find(':selected').data('parent-id');
        let id = $('#coa').find(':selected').val();
        let startDate = '';
        let endDate = '';
        startDate = $('#startDate').val();
        endDate = $('#endDate').val();
        $('#ledgerTable').DataTable({
                "processing": true,
                "searching": false,
                "paging":   false,
                "ordering": false,
                "serverSide": true,
                "ajax": {
                    "url": "/operator/operator-detailed-ledger/data",
                    "type": "POST",
                    "data": {
                        "_token": "{{ csrf_token() }}",
                        "parent-id": parentId,
                        "id": id,
                        "startDate": startDate,
                        "endDate": endDate
                    }
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'm_id', name: 'm_id' },
                    { data: 'voucher_date', name: 'voucher_id' },
                    { data: 'voucher_type', name: 'voucher_type' },
                    { data: 'narration', name: 'narration' },
                    { data: 'dim_name', name: 'dim_name'},
                    { data: 'debit', name: 'debit' },
                    { data: 'credit', name: 'credit' },
                    { data: 'balance', name: 'balance' },
                ]
            });
    }

    $(document).ready(function() {
         $('.selectAccount').select2();
        $(document).on('change','#coa',function() {
            $('#ledgerTable').DataTable().clear().destroy();
            populateTable();
        });
        $(document).on('change','#endDate',function(){
            let startDate = $('#startDate').val();
            let endDate = $('#endDate').val();
            if(startDate == '') {
                return;
            }
            startDate = new Date(startDate);
            endDate = new Date(endDate);
            if(startDate > endDate) {
                return;
            }
            else {
                $('#coa').trigger('change');
            }
        });
        $(document).on('change','#startDate',function(){
            let startDate = $('#startDate').val();
            let endDate = $('#endDate').val();
            if(endDate == '') {
                return;
            }
            startDate = new Date(startDate);
            endDate = new Date(endDate);
            if(startDate > endDate) {
                return;
            }
            else {
                $('#coa').trigger('change');
            }
        });
        $('#coa').trigger('change');
        $(document).on('click','#printReport',function(){
            let coa = $('#coa').val();
            let startDate = $('#startDate').val();
            let endDate = $('#endDate').val();
            $('#coa-id').val(coa);
            $('#start-date').val(startDate);
            $('#end-date').val(endDate);
            $('#reportForm')[0].submit();
        });
    });
    
</script>



@endsection
