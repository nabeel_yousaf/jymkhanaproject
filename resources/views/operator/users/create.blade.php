@extends('operator.layouts.master')
@section('title','Add Brand')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users Add</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <form action="{{url('register_user')}}" method="post">
              @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="name">User Name</label>
                <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}">
                <span class="text-danger">{{$errors->first('name')}}</span>
              </div>
              <div class="form-group">
                <label for="email">User Email</label>
                <input type="text" id="email" name="email" class="form-control" value="{{old('email')}}">
                <span class="text-danger">{{$errors->first('email')}}</span>
              </div>
              <div class="form-group">
                <label for="address">User Address</label>
                <textarea id="address" name="address" class="form-control" rows="3" value="{{old('address')}}"></textarea>
                <span class="text-danger">{{$errors->first('address')}}</span>
              </div>
              <div class="form-group">
                <label for="phone">User Phone</label>
                <input type="text" id="phone" name="phone" class="form-control" value="{{old('phone')}}">
                <span class="text-danger">{{$errors->first('phone')}}</span>
              </div>
              <div class="form-group">
                <label for="cnic">User CNIC</label>
                <input type="text" id="cnic" name="cnic" class="form-control" value="{{old('cnic')}}">
                <span class="text-danger">{{$errors->first('cnic')}}</span>
              </div>
              <div class="form-group">
                <label for="inputStatus">Status</label>
                <select class="form-control custom-select" name="status" value="{{old('status')}}">
                  <option>Select one</option>
                  <option value="1">Active</option>
                  <option value="0">Unactive</option>
                </select>
                <span class="text-danger">{{$errors->first('status')}}</span>
              </div>
              <div class="form-group">
                <label for="image">User Image</label>
                <input type="file" id="image" name="image" class="form-control" value="{{old('image')}}">
                <span class="text-danger">{{$errors->first('image')}}</span>
              </div>
              <div class="form-group">
                <label for="city">City</label>
                <input type="text" id="city" name="city" class="form-control" value="{{old('city')}}">
                <span class="text-danger">{{$errors->first('city')}}</span>
              </div>
               <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                <label for="city">Password</label>
                <input type="password" placeholder="Password" id="password" class="form-control" name="password" value="{{old('password')}}">
                <span class="text-danger">{{$errors->first('password')}}</span>
                </div>
                <div class="col-sm-6">
                <label for="city">Password</label>
                <input type="password" placeholder="Confirm Password" id="password_confirmation" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
                <span class="text-danger">{{$errors->first('password_confirmation')}}</span>
                </div>
                </div>
              </div>
              <div class="form-group">
                <input type="submit" value="Create new User" class="btn btn-success float-right">
              </div>
            </div>
            </form>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>

@endsection
