@extends('operator.layouts.master')
@section('title','List Categories')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">User Data</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Cnic No.</th>
                  <th>City</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th>View</th>
                </tr>
                </thead>
                <tbody>
               
               @foreach($users as $user)
                <tr>
                  <td><img src="{{url('images/',$user->image)}}" alt="" width="70"></td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->cnic}}</td>
                  <td>{{$user->city}}</td>
                  <td>{{$user->address}}</td>
                  <td>
@if ($user->status==0) 
<span class="badge bg-danger">Unactive</span>
@else
<span class="badge bg-success">Active</span>
@endif
</td>
                  <td>
                      <a href="javascript:" rel="{{$user->id}}" rel1="delete-user" class="btn btn-danger btn-sm deleteRecord"><i class="fa fa-trash"></i></a>
                      <a href="{{route('operator-users',$user->id)}}"  class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

    <script type="text/javascript">
        @if (Session::has('success'))

        toastr.success("{{Session::get('success')}}");
        @endif
    </script>
    <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
    <script>
            $(".deleteRecord").click(function () {
               var id=$(this).attr('rel');
               var deleteFunction=$(this).attr('rel1');
               swal({
                   title:'Are you sure?',
                   text:"You won't be able to revert this!",
                   type:'warning',
                   showCancelButton:true,
                   confirmButtonColor:'#3085d6',
                   cancelButtonColor:'#d33',
                   confirmButtonText:'Yes, delete it!',
                   cancelButtonText:'No, cancel!',
                   confirmButtonClass:'btn btn-success',
                   cancelButtonClass:'btn btn-danger',
                   buttonsStyling:false,
                   reverseButtons:true
               },function () {
                  window.location.href="/operator/"+deleteFunction+"/"+id;
               });
            });
        </script>
@endsection
