@extends('operator.layouts.master')
@section('title','All Posts')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Vouchers Approval</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
              <li class="breadcrumb-item active">Vouchers Approval</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vouchers Approval</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>V Id</th>
                  <th>M Id</th>
                  <th>Member Name</th>
                  <th>Date</th>
                  <th>Type</th>
                <th>COA Name</th>
                  <th>Debit</th>
                  <th>Credit</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($voucher as $voucher)
                
                <tr>
                  <td class="id" data-id = "{{$voucher->id}}">{{$voucher->id}}</td>
                  @php
                    $members = App\Member::where('user_id','=',$voucher->user_id)->first();
                  @endphp
                  <td>
                    @if($members != "")
                        {{$members->membership_id}}
                    @else
                        N/A
                    @endif
                  </td>
                  <td>
                    @if($members != "")
                        <a href="{{ route('operatorcustomers-detail',[$members->membership_id]) }}">{{$members->name}}</a>
                    @else
                        N/A
                    @endif
                  </td>
                  <td>{{$voucher->voucher_date}}</td>
                  <td>{{$voucher->voucher_type}}</td>
                <td>{{$voucher->coa_name}}</td>
                  <td>{{$voucher->debit}}</td>
                  <td>{{$voucher->credit}}</td>
                  <td>
                      <span class="badge badge-danger">Not Approved</span>
                  </td>
                  <td>
                      <a href="#" id="viewDetails" class="btn btn-success btn-rounded"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
                @endforeach
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Voucher Details</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" id="voucher_master">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Type</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                <table class="table table-bordered table-striped" id="voucher_detail">
                  <thead>
                    <tr>
                        <th>COA Name</th>
                      <th>Dimension</th>
                      <th>Narration</th>
                      <th>Debit</th>
                      <th>Credit</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->


    <script type="text/javascript">
        @if (Session::has('success'))

        toastr.success("{{Session::get('success')}}");
        @endif
    </script>
    <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
    <script>
            
        $(document).ready(function () {
          $('#example1').on('click', '#viewDetails', function () {
            $("#voucher_detail > tbody").closest('tr').remove();
            
            $('#voucher_detail > tbody').empty();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).closest('tr').find('td.id').attr("data-id");;
            $('#ajaxModel').modal('show');
            $("#voucher_master").closest('tr').remove();
            $("#voucher_detail > tbody").closest('tr').remove();
            $.get('operator-voucherApproval/'+ id +'/vieww', function (data) {
                $("#voucher_master > tbody").html("<tr><td>"+ data.voucher_date +"</td><td>"+ data.voucher_type +"</td></tr>");
                for(var i=0;i<data.length;i++){
                  $("#voucher_master > tbody").html("<tr><td>"+ data[i].voucher_date +"</td><td>"+ data[i].voucher_type +"</td></tr>");
                $("#voucher_detail > tbody").append("<tr class='tr'><td>"+ data[i].coa_name +"</td><td>"+ data[i].dim_name +"</td><td>"+ data[i].narration +"</td><td>"+ data[i].debit +"</td><td>"+ data[i].credit +"</td></tr>");
                }
            })

          });
        });
    </script>
@endsection