@extends('operator.layouts.master')
@section('title','List Categories')
@section('styles')
 <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-colorpicker.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/select2-bootstrap4.min.css')}}">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-duallistbox.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  
@endsection
<style>
    #error_data{
      display:none;
    }
  </style>
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Hotel buying Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"> Hotel Buying Reports</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
   

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Buying  Reports</h3>
            </div>

            <form method="get" id="reportForm" action="{{route('operator-inventoryReport')}}" target="_blank" style="display: none;">
                @csrf()
                <input type="hidden" name="item" id="item-name">
                <input type="hidden" name="startDate" id="start-date">
                <input type="hidden" name="endDate" id="end-date">
            </form>

            <div class="col-md-12">
              <div class="row">
                <div class="col-md-3">
                    <label>Select Chart Items</label>
                    <div class="form-group">
                        <select class="form-control selectAccount select2" name="product" id="product">
                            <option value="0">All</option>
                            @foreach($items as $buying)
                                <option value="{{ $buying['item_name'] }}" > {{ $buying['item_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                        <label>Start Date</label>
                        <input type="date" name="startDate" id="startDate" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="end-date">End Date</label>
                    <input type="date" name="endDate" id="endDate" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Click button to Print Report</label>
                    <button type="button" class="form-control btn btn-success" id="printReport">Print</button>
                </div>
              </div>
                <a class="btn btn-success float-right" href="{{route('operator-addbuying')}}"> Add Bill</a>
            </div>
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <?php
                $tot=0;
                ?>
                <tr>
                <th>ID</th>
                  <th>Hotel Buying Id</th>
                  <th>Vendor</th>
                  <th>Item</th>
                  <th>Quantity</th> 
                  <th>Unit Amount</th> 
                  <th>Total Amount</th> 
                  <th>Type</th>
                  <th>Date</th> 
                </tr>
                </thead>
                <tbody>
               @foreach($allbuyingreport as $buyre)
               <?php
                  $unitprice = (float)$buyre->hotelBuying->total /(float)$buyre->hotelBuying->quantity;
                  $tot = $unitprice * $buyre->used_item ;

               ?>
               <tr>
                  <td>{{ $buyre->id }}</td>
                  <td>{{ $buyre->hotel_buying_id }}</td>
                  <td>{{$buyre->hotelBuying->vendor}}</td>
                  <td>{{$buyre->hotelBuying->hotel_buying_items->item_name}}</td>
                  <td>{{$buyre->used_item}}</td>
                  <td>{{ $unitprice }}</td>
                  <td>{{ $tot }}</td>
                  <td>{{$buyre->hotelBuying->type}}</td>
                  <td>{{$buyre->created_at}}</td>
                </tr>
                @endforeach
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <!-- /.content -->
  </div>
 
@endsection
@section('jsblock')

<script type="text/javascript">
    @if (Session::has('success'))
    toastr.success("{{Session::get('success')}}");

    @endif
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
  
<script> 

  var table1;
      $(function () {
      table1 = $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });

    $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = $('#startDate').val();
        var max = $('#endDate').val();
        var vendor = $('#product').val();

        var createdAt = data[6] || 0; // Our date column in the table
        var materialName = data[3]
 
        if(vendor!=0){
          if(vendor == materialName){
            return true;
          }
          return false;
        }else{

          if (
            (min == "" || max == "") ||
            (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
            ) {
            return true;
            }
            return false;

        }
      }
    );

</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    $(document).on('change','#product',function() {
        console.log("click")
        table1.draw();
    });

    $(document).on('change','#endDate',function(){
        let startDate = $('#startDate').val();
        let endDate = $('#endDate').val();
        if(startDate == '') {
            return;
        }
        startDate = new Date(startDate);
        endDate = new Date(endDate);
        if(startDate > endDate) {
            return;
        }
        else {
            $('#product').trigger('change');
        }
    });

    $(document).on('change','#startDate',function(){
        let startDate = $('#startDate').val();
        let endDate = $('#endDate').val();
        if(endDate == '') {
            return;
        }
        startDate = new Date(startDate);
        endDate = new Date(endDate);
        if(startDate > endDate) {
            return;
        }
        else {
            $('#product').trigger('change');
        }
    });

    $(document).on('click','#printReport',function(){
        console.log("click..."); 
        let item = $('#product').val();
        let startDate = $('#startDate').val();
        let endDate = $('#endDate').val();
        $('#item-name').val(item);
        $('#start-date').val(startDate);
        $('#end-date').val(endDate);
        
        if(startDate == '' && endDate!=''){
          
          return;
          
        }

        if(startDate != '' && endDate==''){
          
          return;
          
        }


        $('#reportForm')[0].submit();


    });


  })

</script>

@endsection
