@extends('operator.layouts.master')
@push('styles')
    <style>
         .hide{
             display:none;
         }
         .show{
             display:block;
         }
         .product_quantity_input{
             border:none;
         }
    </style>
@endpush
@section('title','Add Voucher')

@section('content')

<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create New Sale</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
              <li class="breadcrumb-item active">Add SALE</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Add Hotel Sale</h3>

              <div class="card-tools">
                {{--<a href="#" class="btn btn-info">View Vouchers
                                                  </a>--}}
              </div>
            </div>
            <div class="card-body">
            <form action="#" method="post" id="voucherForm">
              @csrf
              <div class="row">
                <div class="col-lg-4">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="date">Date</label>
                      <input type="date" id="date" name="date" class="form-control" value="{{old('date')}}">
                      <span class="text-danger">{{$errors->first('date')}}</span>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="member_type">Type</label>
                      <select class="form-control selectAccount" id="member_type" name="member_type" >
                        <option value="member">Member</option>
                        <option value="employee">Employee</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-12" id="member_id_div">
                    <div class="form-group">
                      <label for="member_id">Member  Name</label>
                      <select class="form-control selectAccount" id="member_id" name="member_id" >
                        <option selected disabled value="">Select one</option>
                        @foreach($members as $coa) 
                          <option data-name="{{$coa['name']}}" value="{{$coa['membership_id']}}">( {{ $coa['membership_id'] }} ){{$coa['name']}}</option>
                        @endforeach
                      </select>
                      <span class="text-danger">{{$errors->first('coa_id')}}</span>
                    </div>
                  </div>
                  <div class="col-sm-12" id="employee_id_div">
                    <div class="form-group">
                      <label for="employee_id">Employee  Name</label>
                      <select class="form-control selectAccount" id="employee_id" name="employee_id" >
                        <option selected disabled value="">Select one</option>
                        @foreach($employees as $employee) 
                          <option data-name="{{$employee['name']}}" value="{{$employee['membership_id']}}">( {{ $employee['membership_id'] }} ){{$employee['name']}}</option>
                        @endforeach
                      </select>
                      <span class="text-danger">{{$errors->first('coa_id')}}</span>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="coa_id">Products Name</label>
                      <select class="form-control selectAccount" id="selected_product" name="coa_id" value="{{old('membership_id')}}">
                        <option selected disabled value="">Select one</option>
                        @foreach($items     as $coa)
                          <option value="{{$coa['id']}}"  data-price="{{ $coa['price'] }}" data-name="{{ $coa['name'] }}">{{$coa['name']}}</option>
                        @endforeach
                      </select>
                      <span class="text-danger">{{$errors->first('coa_id')}}</span>
                    </div>

                    <div class="form-group">
                      <label for="coa_id">Quantity</label> 
                      <input type="number" id="quantity"  class="form-control" >
                    </div>
                  
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input id="debitVoucher" type="submit" value="Add" name="submit" class="btn btn-success">
                  
                    </div>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="col-sm-12">
                    <div class="table-responsive" style="margin-top: 30px;">
                      <table id="productTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm">Product Name</th>
                            <th class="th-sm">Member Name</th>                          
                            <th class="th-sm">Price</th>
                            <th class="th-sm">Quantity</th>
                            <th class="th-sm">Sub TOTAL</th>
                            <th class="th-sm">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>
                    <div class="col-sm-12 text-right">
                    <input type="submit" id="saveVoucher" value="Save" name="submit" class="btn btn-primary">
                    </div>
                  </div>                  
                </div>
                
                
              </div>
          	</form>
            </div>      
	   	  </div>
      </div>
    </div>
</section>





</div>
@endsection
@section('jsblock')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>

  jQuery(document).ready(function(){
    $('.selectAccount').select2();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    jQuery('#debitVoucher').click(function(e){
      //$(this).prop('disabled', true);
      var _self = $("#selected_product");
      e.preventDefault();
      var member_type = $("#member_type").val();
      var member_id = member_type == 'member' ? $("#member_id").val() : $("#employee_id").val();
      var member_name = member_type == 'member' ? $("#member_id").find(':selected').attr('data-name') : $("#employee_id").find(':selected').attr('data-name');
      var product_name = _self.find(':selected').attr('data-name');
      var product_id = _self.find(':selected').val();
      var product_price = _self.find(':selected').attr('data-price');
      var quantity = $("#quantity").val() == "" ? 1 : $("#quantity").val() ;
      var sub_total = quantity*product_price;
      var new_html = "<tr data-type='"+member_type+"' data-memberId='"+member_id+"' data-price='"+product_price+"' data-name='"+product_name+"' data-id='"+product_id+"'><td>"+product_name+"</td><td>"+member_name+"</td><td>"+product_price+"</td><td class='product_quantity'><input class='hide product_quantity_input' type='number' value="+quantity+"><span class='quantity_display'>"+quantity+"</span></td><td class='sub_total'>"+sub_total+"</td><td><button class='product_edit'>edit</button><button class='product_delete'>delete</button></td></tr>";
      $("#productTable > tbody").append(new_html); 
      return;
      
    });

    // remove item
    $("#productTable").on("click",".product_delete",function(e){
      e.preventDefault();

        $(this).closest('tr').remove();
        console.log($(this).closest('tr'));
    })

    // edit item 
    $("#productTable").on("click",".product_edit",function(e){
      e.preventDefault();

        $(this).closest('tr').find('.product_quantity_input').removeClass('hide').addClass('show');//.show();//.remove();
        $(this).closest('tr').find('.quantity_display').removeClass('show').addClass('hide');
    })

    // update subtotal on quantity change
    $("#productTable").on("change",".product_quantity_input",function(e){
      e.preventDefault();
      console.log("change...");
      console.log($(this).closest('tr').data('price'));
      let product_price = $(this).closest('tr').data('price');
      let quantity = $(this).val();
      let sub_total = quantity*product_price;
      console.log(sub_total);

      $(this).closest('tr').find('.sub_total').html(sub_total); 
      
    });  


    //save voucher 
    $("#saveVoucher").click(function(e){
      e.preventDefault();
        let member_id = $("#coa_id").select2().val();
        let order_date = $("#date").val();
        let products =  [];

          
        $("#productTable tbody tr").each(function(index){ 
          let product_id = $(this).data('id');
          let quantity = $(this).find('.product_quantity_input').val();
          let member_id = $(this).data('memberid');
          let type = $(this).data('type');
          let product  = {"memberable_id":member_id,"memberable_type":type,"product_id":product_id, "quantity":quantity};
          products.push(product);

        });

        

        if(products.length ==0 || order_date.length == 0){
          alert("Invalid Data provided");
          return;
        }

        data = {
          "order_date" : order_date,
          "products" : products
        };
         
        $("#saveVoucher").attr('disabled',true);
        $.ajax({
              url: "{{ route('operator-Addsalerecord') }}",
              method: "POST",
              data: data,
              success: function (response) {
                $("#productTable tbody").html('');
                $("#saveVoucher").attr('disabled',false);

              Swal.fire(
                  'Good job!',
                  'data has been created!',
                  'success'
                )
                  
              },
                 
    
              error: function (response) {
                $("#saveVoucher").attr('disabled',false);

              Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Something went wrong!' 
                })
                  
              }
          });

    })

    $("#employee_id_div").hide();
    $("#member_type").change(function(e){
        let selected_value = $(this).val();
        if(selected_value == 'member'){
          $("#employee_id_div").hide();
          $("#member_id_div").fadeIn();
        }else{
          $("#member_id_div").hide();
          $("#employee_id_div").fadeIn();
        }
    });
            
  });
</script>


@endsection

