<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Gym Khana</title>
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
    <! <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
        <!-- summernote -->
        <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
        <!-- Plugin css for this page -->
        <!-- endinject -->
        <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


</head>

<body>


    <div class="content" id="app">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> SALE  Report</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <!-- Main content -->
                        <div class="invoice p-3 mb-3">
                            <!-- title row -->
                            <div class="row">
                                <div class="col-12">
                                    <h4>
                                        <i class="fas fa-globe"></i> GymKhana Club Burewala. Please Click Ctrl + P to print the report
                                    </h4>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- Table row -->

                            <div class="row">
                                <div class="col-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                             
                                                <th>Member Id</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Date</th>
                                                <th>Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                        $tot = 0;
                    
                    ?>
                                            @foreach ($records as $item)
                                                    <tr>
                                                        
                                                        <td>{{ $item['memberable_id'] }}</td>
                                                        <td>{{ $item['memberable']['name'] }}</td>
                                                        <td>{{ $item['memberable_type'] }}</td>
                                                        <td>{{ $item['date_time'] }}</td>
                                                        @php 
                                                        $order_details = $item['order_details'];
                                                        $total =0;

                                                            foreach($order_details as $order){
                                                                $total += $order['quantity']*$order['product']['price'];
                                                                $tot += $order['quantity']*$order['product']['price'];
                                                            }
                                                            
                                                        @endphp
                                                        
                                                        <td>{{ $total }}</td>
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                       <td>
                                                       <table cellpadding="5" cellspacing="0" class="table table-hover"  style="padding-left:50px;">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Quantity</th>
                                                                <th>Price</th>
                                                                <th>Total Amount</th>
                                                            </tr>
                                                        </thead>
                                                        @foreach($order_details as $order)
                                                            <tr>
                                                                <td>{{ $order['product']['name'] }}</td>
                                                                <td>{{ $order['product']['price'] }}</td>
                                                                <td>{{ $order['quantity'] }}</td>
                                                                <td>{{ $order['quantity']*$order['product']['price'] }}</td>
                                                            </tr>
                                                            
                                                        @endforeach
                                                        </table>
                                                       </td>

                                                    </tr>
                                                    <?php
                 
                        $tot = $tot+ ($item['price'] * $item['quantity']);
                        
                    ?>
                                            @endforeach
                                          
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <div class="row">
                            
                <div class="col-6">
                  <p class="lead">Total Sale</p>

                  <div class="table-responsive">
                    <table class="table">
                      <tbody><tr>
                        <th style="width:50%">Total :</th>

                        <td><h3>{{$tot}}</h3></td>
                      </tr>

                    </tbody></table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
                            
                        </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.invoice -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
    </div>
    <footer style="text-align:center">
        <strong>Copyright &copy; 2019 BitSoftSol.</strong>
        All rights reserved.
    </footer>
    <script src="{{asset('js/app.js')}}"></script>

</body>

</html>
