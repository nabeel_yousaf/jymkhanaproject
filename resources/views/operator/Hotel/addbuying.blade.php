@extends('operator.layouts.master')
@section('title','Add Members Record')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{asset('css/bootstrap-colorpicker.min.css')}}">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('css/select2-bootstrap4.min.css')}}">
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{asset('css/bootstrap-duallistbox.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
<link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add buying</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add hotel Buying Products</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Buying Products</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="add_record_form" action="#">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="buy_date">Date</label>
                                        <input type="date" required name="buy_date" id="buy_date" class="form-control">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="bill_no">Bill No</label>
                                        <input type="text" required name="bill_no" id="bill_no_name" class="form-control">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="vendor">Vendor</label>
                                        <input type="text" name="vendor" required id="vendor_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="month">Item</label>
                                        <select class="form-control" required name='hotel_buying_id' id="hotel_buying_id">
                                            @foreach ($items as $item)
                                                <option value="{{ $item->id }}" data-name="{{ $item->item_name  }}">{{ $item->item_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="month">Quantity</label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text"></span>
                                            </div>
                                            <input type="text" required name="quantity" id="quantity" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="month">Unit</label>
                                        <select class="form-control" name='Unit' id="unit">
                                            <option value="{{old('Unit')}}">Select unit</option>
                                            <option value="Grams">Gram</option>
                                            <option value="Number">Number</option>                                        
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="unit_price">Total Bill Amount</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">PKR</span>
                                            </div>
                                            <input type="text" name="total" id="total" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="credit">Credit</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">PKR</span>
                                            </div>
                                            <input type="text" name="credit" id="credit" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="payment">Payments</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">PKR</span>
                                            </div>
                                            <input type="text" name="payment" id="payment" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->first('payment')}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="Kitchen">Kitchen</label>
                                        <div class="input-group">                                                    
                                            <input type="text" name="Kitchen" id="kitchen" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">Grams</span>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->first('Kitchen')}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="month">Type</label>
                                        <div class="input-group">
                                        <select class="form-control" name="bill_type" id="bill_type">
                                            <option value="{{old('type')}}">Select Type</option>
                                            <option value="Assets">Assets</option>
                                            <option value="Consumable">Consumable</option>
                                        </select>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Type</span>
                                        </div>
                                        </div>
                                        <span class="text-danger">{{$errors->first('type')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Add Record" id="add_record" class="btn btn-success float-right">
                            </div>
                        </form>    
                    </div>
                    
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Buying Table</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>                     
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>BillNo</th>
                                    <th>Vendor</th>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Total Amount</th>
                                    <th>Credit</th>
                                    <th>Payment</th>
                                    <th>kitchen</th> 
                                    <th>Type</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <form action="{{route('operator-savehotelbuying')}}" method="post">
                                <tbody id="savehotelbuying">
                                    
                                </tbody>
                            </form>
                        </table>
                        <br>
                        <button id="save_data" class=" btn btn-primary">save record</button>
                    </div>
                    
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="editModalLabel">Update item</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div> 
        <div class="modal-body">
            <div class="form-group">
                <label for="update_date" class="col-form-label">Date :</label>
                <input type="date" class="form-control" id="update_buy_date">
            </div>
            <div class="form-group">
                <label for="update_bill_no" class="col-form-label">BillNo:</label>
                <input type="text" class="form-control" id="update_bill_no">
            </div>
            <div class="form-group">
                <label for="update_vendor_name" class="col-form-label">Vendor:</label>
                <input type="text" class="form-control" id="update_vendor_name">
            </div>
            <div class="form-group">
                <label for="update_hotel_buying_id">Item</label>
                <select class="form-control" required name='update_hotel_buying_id' id="update_hotel_buying_id">
                    @foreach ($items as $item)
                        <option value="{{ $item->id }}" data-name="{{ $item->item_name  }}">{{ $item->item_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="update_quantity" class="col-form-label">Quantity:</label>
                <input type="text" class="form-control" id="update_quantity">
            </div>
            <div class="form-group">
                <label for="update_unit">Unit</label>
                <select class="form-control" name='Unit' id="update_unit">
                    <option value="{{old('Unit')}}">Select unit</option>
                    <option value="Grams">Gram</option>
                    <option value="Number">Number</option>                                        
                </select>
            </div>
            <div class="form-group">
                <label for="update_total" class="col-form-label">Total Bill Amount:</label>
                <input type="text" class="form-control" id="update_total">
            </div>
            <div class="form-group">
                <label for="update_credit" class="col-form-label">Credit:</label>
                <input type="text" class="form-control" id="update_credit">
            </div>
            <div class="form-group">
                <label for="update_payment" class="col-form-label">Payments:</label>
                <input type="text" class="form-control" id="update_payment">
            </div>
            <div class="form-group">
                <label for="update_kitchen" class="col-form-label">Kitchen:</label>
                <input type="text" class="form-control" id="update_kitchen">
            </div>
             
            <div class="form-group">
                <label for="month">Type</label> 
                <select class="form-control" name="update_bill_type" id="update_bill_type">
                    <option value="Assets">Assets</option>
                    <option value="Consumable">Consumable</option>
                </select>
            </div>

            
             
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="update_changes">Update changes</button>
      </div>
    </div>
  </div>
</div>
<!-- end Edit Modal -->
@endsection
@section('jsblock')

<script type="text/javascript">
    @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
</script>
<script>
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
 
    $("#add_record").click(function(e){
        e.preventDefault();
        let bill_no = $("#bill_no_name").val();
        let buy_date = $("#buy_date").val();
        let vendor_name = $("#vendor_name").val();
        let hotel_buying_id = $("#hotel_buying_id").val();
        let hotel_buying_name = $("#hotel_buying_id").find(':selected').attr('data-name');
        let quantity = $("#quantity").val();
        let unit = $("#unit").val();
        let total = $("#total").val();
        let credit = $("#credit").val();
        let payment = $("#payment").val();
        let kitchen = $("#kitchen").val();
        let bill_type = $("#bill_type").val();

        let data = {
            "bill_no" : bill_no,
            "buy_date" : buy_date,
            "vendor_name" : vendor_name,
            "hotel_buying_id" : hotel_buying_id,
            "hotel_buying_name" : hotel_buying_name,
            "quantity" : quantity,
            "unit" : unit,
            "total" : total,
            "credit" : credit,
            "payment" : payment,
            "kitchen" : kitchen,
            "bill_type" : bill_type
        }

        let newhtml = `<tr 
                        data-bill_no="`+bill_no+`"
                        data-buy_date="`+buy_date+`"
                        data-vendor_name="`+vendor_name+`"
                        data-hotel_buying_id="`+hotel_buying_id+`"
                        data-hotel_buying_name="`+hotel_buying_name+`"
                        data-quantity="`+quantity+`"
                        data-unit="`+unit+`"
                        data-total="`+total+`"
                        data-credit="`+credit+`"
                        data-payment="`+payment+`"
                        data-kitchen="`+kitchen+`"
                        data-bill_type="`+bill_type+`"
                        >
                            <td>`+buy_date+`</td>
                            <td>`+bill_no+`</td>
                            <td>`+vendor_name+`</td>
                            <td>`+hotel_buying_name+`</td>
                            <td>`+quantity+`</td>
                            <td>`+unit+`</td>
                            <td>`+total+`</td>
                            <td>`+credit+`</td>
                            <td>`+payment+`</td>
                            <td>`+kitchen+`</td>
                            <td>`+bill_type+`</td>
                            <td><button class="edit_item">edit</button><button class="remove_item">remove</button></td>
                       </tr> 
                        `;
   

        if(
            bill_no != "" && 
            buy_date != "" && 
            vendor_name!="" && 
            hotel_buying_id!="" && 
            quantity!="" && 
            unit!="" && 
            total!="" &&
            credit!="" &&
            payment!="" &&
            kitchen!="" &&
            bill_type!=""
        ){
            $("#savehotelbuying").append(newhtml);
            console.log(data);
        }else{
            alert("please fill all input fields");
        }            
        



    });   

    $("#savehotelbuying").on("click",".remove_item",function(){
        $(this).closest('tr').remove();
    })

    var active_tr;

    $("#savehotelbuying").on("click",".edit_item",function(){

        let _self = $(this).closest("tr");
        active_tr = _self;
        $("#update_bill_no").val(_self.data('bill_no'));
        $("#update_buy_date").val(_self.data('buy_date'));
        $("#update_vendor_name").val(_self.data('vendor_name'));
        $("#update_hotel_buying_id").val(_self.data('hotel_buying_id'));
        $("#update_quantity").val(_self.data("quantity"));
        $("#update_unit").val(_self.data("unit"));  
        $("#update_total").val(_self.data("total"));
        $("#update_credit").val(_self.data("credit"));
        $("#update_payment").val(_self.data("payment"));
        $("#update_kitchen").val(_self.data("kitchen"));
        $("#update_bill_type").val(_self.data("bill_type"));
        
        $("#editModal").modal('show');
    })

    $("#update_changes").click(function(){
        let bill_no = $("#update_bill_no").val();
        let buy_date = $("#update_buy_date").val();
        let vendor_name = $("#update_vendor_name").val();
        let hotel_buying_id = $("#update_hotel_buying_id").val();
        let hotel_buying_name = $("#update_hotel_buying_id").find(':selected').attr('data-name');
        let quantity = $("#update_quantity").val();
        let unit = $("#update_unit").val();  
        let total = $("#update_total").val();
        let credit = $("#update_credit").val();
        let payment = $("#update_payment").val();
        let kitchen = $("#update_kitchen").val();
        let bill_type = $("#update_bill_type").val();
        console.log(bill_type);

        let newhtml = `<tr 
                        data-bill_no="`+bill_no+`"
                        data-buy_date="`+buy_date+`"
                        data-vendor_name="`+vendor_name+`"
                        data-hotel_buying_id="`+hotel_buying_id+`"
                        data-hotel_buying_name="`+hotel_buying_name+`"
                        data-quantity="`+quantity+`"
                        data-unit="`+unit+`"
                        data-total="`+total+`"
                        data-credit="`+credit+`"
                        data-payment="`+payment+`"
                        data-kitchen="`+kitchen+`"
                        data-bill_type="`+bill_type+`"
                        >
                            <td>`+buy_date+`</td>
                            <td>`+bill_no+`</td>
                            <td>`+vendor_name+`</td>
                            <td>`+hotel_buying_name+`</td>
                            <td>`+quantity+`</td>
                            <td>`+unit+`</td>
                            <td>`+total+`</td>
                            <td>`+credit+`</td>
                            <td>`+payment+`</td>
                            <td>`+kitchen+`</td>
                            <td>`+bill_type+`</td>
                            <td><button class="edit_item">edit</button><button class="remove_item">remove</button></td>
                       </tr> 
                        `;
                        

        if(
            buy_date != "" && 
            bill_no != "" && 
            vendor_name!="" && 
            hotel_buying_id!="" && 
            quantity!="" && 
            unit!="" && 
            total!="" &&
            credit!="" &&
            payment!="" &&
            kitchen!="" &&
            bill_type!=""
        ){
            active_tr.replaceWith(newhtml);    
            $("#editModal").modal('hide');
        }else{
            alert("please fill all input fields");
        }                   

        


    });

    $("#save_data").click(function(){
        let products =  [];
        let _self = $(this);
        _self.attr('disabled', true);
                    
        $("#savehotelbuying tr").each(function(index){ 
            let buy_date = $(this).data('buy_date'); 
            let bill_no = $(this).data('bill_no'); 
            let vendor_name = $(this).data('vendor_name'); 
            let hotel_buying_id = $(this).data('hotel_buying_id'); 
            let quantity = $(this).data('quantity'); 
            let unit = $(this).data('unit'); 
            let total = $(this).data('total'); 
            let credit = $(this).data('credit'); 
            let payment = $(this).data('payment'); 
            let kitchen = $(this).data('kitchen'); 
            let bill_type = $(this).data('bill_type'); 

            let product  = {
            "buy_date" : buy_date,
            "bill_no" : bill_no,
            "vendor_name" : vendor_name,
            "hotel_buying_id" : hotel_buying_id, 
            "quantity" : quantity,
            "unit" : unit,
            "total" : total,
            "credit" : credit,
            "payment" : payment,
            "kitchen" : kitchen,
            "bill_type" : bill_type
            };
 
        products.push(product);

        });

        data = {
                    "products" : products,
                    '_token':'<?php echo csrf_token() ?>'
                };
            
            $.ajax({
                url: "{{route('operator-savehotelbuying')}}",
                method: "POST",
                data: data,
                success: function (response) {
                    _self.attr('disabled', false);
                    $("#savehotelbuying").html('');
                    $("#add_record_form").trigger("reset");    
                    Swal.fire(
                        'Good job!',
                        'data has been created!',
                        'success'
                        )
                    
                },
                error: function (jqXhr, json, errorThrown) {
                    var errors = jqXhr.responseJSON;
                    _self.attr('disabled', false);

                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: errors.message 
                        })
                    
                }
            });
        console.log(products);

    });
  
 
 
   

  })
</script>
@endsection
