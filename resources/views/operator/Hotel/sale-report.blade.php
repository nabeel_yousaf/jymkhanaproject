@extends('operator.layouts.master')
@section('title','List Categories')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
<!-- iCheck -->
<link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
 
<!-- Plugin css for this page -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<!-- endinject -->
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">



@endsection

@push('styles')
<style>
td.details-control {
    background: url('{{ asset("images/details_open.png") }}') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('{{ asset("images/details_close.png") }}') no-repeat center center;
}
</style>
@endpush

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Hotel Sale report</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
            <form method="post" id="reportForm" action="/operator/operator-sale-report/print" target="_blank" style="display: none;">
                @csrf()
                <input type="hidden" name="id" id="product-id">
                <input type="hidden" name="startDate" id="start-date">
                <input type="hidden" name="endDate" id="end-date">
            </form>
            
        
        <div class="container">
            <div class="row">  
                    <div class="col-md-3">
                    <label>Select Chart Items</label>
                    <div class="form-group">
                        <select class="form-control selectAccount" name="product" id="product">
                            <option value="0">All</opt"/operatoron>
                            @foreach($items as $item)
                                <option value="{{ $item['id'] }}" data-price="{{ $item['price'] }}" data-name="{{ $item['name'] }}"> {{ $item['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>                
                <div class="col-md-3">
                    <label for="memberId">Member Id</label>
                    <input type="text" name="memberId" id="memberId" class="form-control">
                </div>
                <div class="col-md-3">
                        <label>Start Date</label>
                        <input type="date" name="startDate" id="startDate" class="form-control">
                </div>            
                <div class="col-md-3">
                    <label for="end-date">End Date</label>
                    <input type="date" name="endDate" id="endDate" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Click button to Print Report</label>
                    <button type="button" class="form-control btn btn-success" id="printReport">Print</button>
                </div>            
                <div class="col-md-3">
                    <label> </label>
                   <a class="form-control btn btn-success" style="margin-top:8px"  href="{{route('operator-Addsalerecord')}}">Add Order</a> 
                </div>
                </div>
            </div>
            <div class="row" style="margin-top:20px">
                <div class="col-lg-12">
                    <table class="table table-border" id="data">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Id</th>
                                <th>Member Id</th>
                                <th>Name</th> 
                                <th>Type</th> 
                                <th>Date</th>   
                                <th>Total Amount</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!-- /.content -->
</div>
@endsection
@section('jsblock')
<script type="text/javascript">
    @if (Session::has('success'))
            toastr.success("{{Session::get('success')}}");
    @endif
</script>
<script>
 

    var table = $('#data').DataTable(
                {
                    "processing": true,
                    "searching": false,
                    "paging":   false,
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        "url": "/operator/operator-sale-report/data",
                        "type": "POST",
                        "data": {
                            "_token": "{{ csrf_token() }}",
                            "startDate": function() { return $('#startDate').val() },
                            "endDate": function() { return $('#endDate').val() },
                            "id": function() { return $('#product').find(':selected').val() },
                            "memberId" : function(){ return $("#memberId").val() }
                            
                        }
                    },
                    columns: [
                        {
                            className: 'details-control',
                            orderable: false,
                            data: null,
                            defaultContent: ''
                        },
                        { data: 'id', name: 'id' },
                        { data: 'memberable_id', name: 'memberable_id' },
                        { data: 'memberable.name', name: 'memberable.name' }, 
                        { data: 'memberable_type', name: 'memberable_type' }, 
                        { data: 'date_time', name: 'date_time'},
                        {
                            "data": "total",
                            "name": "total",
                            "render": function ( data, type, row, meta ) {
                                    let total=0;
                                    order_details = row.order_details
                                    for(order in order_details){ 
                                        total += order_details[order].product.price*order_details[order].quantity;
                                    } 
                                    return total;
                            },
                        },
                        {
                            "data": "actions",
                            "name": "actions",
                            "render": function ( data, type, row, meta ) {
                                    let buttons = "<a href='/operator/operator-hotel-sale-report/edit/"+row.id+"' class='btn btn-primary'>edit</a> <a href='#' data-id='"+row.id+"' class='btn btn-danger order_delete'>delete</a>";
                                    return buttons;
                            },
                        },
                    ],
                    order: [[1, 'asc']],
                });
    
   
    
 
    $(document).ready(function() {
        $('.selectAccount').select2();

        $("#memberId").keyup(function(){
            table.ajax.reload();

        });

        $(document).on('change','#product',function() {
            table.ajax.reload();
        });
        $(document).on('change','#endDate',function(){
            let startDate = $('#startDate').val();
            let endDate = $('#endDate').val();
            if(startDate == '') {
                return;
            }
            startDate = new Date(startDate);
            endDate = new Date(endDate);
            if(startDate > endDate) {
                return;
            }
            else {
                $('#product').trigger('change');
            }
        });
        $(document).on('change','#startDate',function(){
            let startDate = $('#startDate').val();
            let endDate = $('#endDate').val();
            if(endDate == '') {
                return;
            }
            startDate = new Date(startDate);
            endDate = new Date(endDate);
            if(startDate > endDate) {
                return;
            }
            else {
                $('#product').trigger('change');
            }
        });
         
        $(document).on('click','#printReport',function(){
            let product = $('#product').val();
            let startDate = $('#startDate').val();
            let endDate = $('#endDate').val();
            $('#product-id').val(product);
            $('#start-date').val(startDate);
            $('#end-date').val(endDate);
            $('#reportForm')[0].submit();
        });
    });
</script>


<script>
var childEditors = {};  // Globally track created chid editors
$(document).ready(function() {
  
  // Return table with id generated from row's name field
  function format(rowData) {
    return '<table id="' + rowData.id + '" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
           '</table>';  
    }
  
 
  
  
  // Add event listener for opening and closing first level childdetails
  $('#data tbody').on('click', 'td.details-control', function () {
     var tr = $(this).closest('tr');
     var row = table.row( tr );
     var rowData = row.data();
      
     console.log(rowData); 
 
     if ( row.child.isShown() ) {
       // This row is already open - close it
       row.child.hide();
       tr.removeClass('shown');
        
       // Destroy the Child Datatable
       $('#' + rowData.id).DataTable().destroy();
     }
     else {
       // Open this row
       row.child(format(rowData)).show();
       var id = rowData.id;
          
       console.log(id); 

        $('#' + id).DataTable({
          dom: "t",
          data: rowData.order_details,
          columns: [
                { data: 'product.name', name: 'product.name', title:'name' },
                { data: 'product.price', name: 'product.price', title:'price' },
                { data: 'quantity', name: 'quantity', title:'quantity'},
                {
                    "data": "total",
                    "name": "total",
                    "title": "total",
                    "render": function ( data, type, row, meta ) {
                            return row.product.price * row.quantity;
                    },
                }

          ],
          scrollY: '150px',
          select: true,
        });
        
        tr.addClass('shown');
      }
  } );

  $('#data tbody').on("click",".order_delete",function(){
    
        let _self = $(this);

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                
                $.ajax({
                        url: "/operator/deleteSaleRecord/"+ _self.data('id'),
                        method: "POST",
                        success: function (response) {
                        table.ajax.reload();
                        Swal.fire(
                            'Deleted!',
                            'Your order has been deleted.',
                            'success'
                            )
                            
                       },
                       error: function (response) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!' 
                          })
                            
                       }
                   });
            }
        })

  });           

          
} );
</script>
@endsection
