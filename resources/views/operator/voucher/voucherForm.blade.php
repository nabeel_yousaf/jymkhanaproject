@extends('operator.layouts.master')
@section('title','Add Voucher')

@section('content')

<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create New Voucher</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
              <li class="breadcrumb-item active">Create New Voucher</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Add Voucher Record</h3>

              <div class="card-tools">
                {{--<a href="#" class="btn btn-info">View Vouchers
                                                  </a>--}}
              </div>
            </div>

              <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
              </div>
            <form action="{{route('operatorvoucher.save')}}" method="post" id="voucherForm">
              @csrf

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="date">Date</label>
                      <input type="date" id="date" name="date" class="form-control" value="{{old('date')}}">
                      <span class="text-danger">{{$errors->first('date')}}</span>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="vouchertype">Voucher Type</label>
                      <select class="form-control custom-select" id="vouchertype" name="vouchertype" value="{{old('vouchertype')}}">
                        <option selected disabled value="">Select one</option>
                        <option value="Bank Receipt">Bank Receipt</option>
                        <option value="Bank Payment">Bank Payment</option>
                        <option value="Cash Payment">Cash Payment</option>
                        <option value="Cash Receipt">Cash Receipt</option>
                        <option value="Journal Voucher">Journal Voucher</option>
                      </select>
                      <span class="text-danger">{{$errors->first('vouchertype')}}</span>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label for="coa_id">Account Name</label>
                      <select class="form-control selectAccount" id="coa_id" name="coa_id" value="{{old('coa_id')}}">
                        <option selected disabled value="">Select one</option>
                        @foreach($accounts as $coa)
                            @if($coa['membership_id'] != '')
                                <option value="{{$coa['id']}}">( {{ $coa['membership_id'] }} ){{$coa['name']}}</option>
                             @else
                                <option value="{{$coa['id']}}">{{$coa['name']}}</option>
                             @endif
                        @endforeach
                      </select>
                      <span class="text-danger">{{$errors->first('coa_id')}}</span>
                    </div>
                    <div class="form-group">
                      <label for="dimension">Dimension</label>
                      <select class="form-control custom-select" id="dimension" name="dimension" value="{{old('dimension')}}">
                        <option selected disabled value="">Select one</option>
                        @foreach($dim as $dimension)
                          <option value="{{$dimension->id}}">{{$dimension->dim_name}}</option>
                        @endforeach
                      </select>
                      <span class="text-danger">{{$errors->first('dimension')}}</span>
                    </div>

                    <div class="form-group">
                      <label for="naration">Naration</label>
                      <textarea name="naration" id="naration" cols="30" rows="5" class="form-control"></textarea>
                      <span class="text-danger">{{$errors->first('naration')}}</span>
                    </div>

                    <div class="form-group">
                      <label for="amount">Amount</label>
                      <input type="text" name="amount" id="amount" class="form-control" value="{{old('amount')}}">
                      <span class="text-danger">{{$errors->first('amount')}}</span>
                    </div>


                  </div>
                  <div class="col-sm-8">
                    <div class="table-responsive" style="margin-top: 30px;">
                      <table id="userTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm">Account Name</th>
                            <th class="th-sm">Dimension</th>
                            <th class="th-sm">Debit</th>
                            <th class="th-sm">Credit</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>
                    <div class="col-sm-12 text-right">
                    <input type="submit" id="saveVoucher" value="Save" name="submit" class="btn btn-primary">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input style="margin-left: 20px" id="debitVoucher" type="submit" value="Debit" name="submit" class="btn btn-success">
                      <input type="submit" id="creditVoucher" value="Credit" name="submit" class="btn btn-danger">

                    </div>
                  </div>
                </div>
          	</form>

	   	  </div>
      </div>
    </div>
</section>





</div>
@endsection
@section('jsblock')
<script>

  $(document).ready(function(){
    $('.selectAccount').select2();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
              $('#debitVoucher').click(function(e){
                e.preventDefault();
                $.ajax({
                  type: "POST",
                  url: "/operator/addVoucher",
                  data: {
                      date: $('#date').val(),
                      vouchertype: $('#vouchertype').val(),
                      coa_id: $('#coa_id').val(),
                      dimension: $('#dimension').val(),
                      naration: $('#naration').val(),
                      debit: $('#amount').val(),
                      credit: "0"
                  },
                  success: function(result){
                    if($.isEmptyObject(result.error)){
                      $("#userTable > tbody").append("<tr><td>"+ result.coa_id +"</td><td>"+ result.dimension +"</td><td>"+ result.debit +"</td><td>"+ result.credit +"</td></tr>")
                    }
                    else{
                      printErrorMsg(result.error);

                    }
                  }
                });
              });

              $('#creditVoucher').click(function(e){
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/operator/addVoucher",
                    data: {
                      date: $('#date').val(),
                      vouchertype: $('#vouchertype').val(),
                      coa_id: $('#coa_id').val(),
                      dimension: $('#dimension').val(),
                      naration: $('#naration').val(),
                      credit: $('#amount').val(),
                      debit: "0"
                    },
                    success: function(result){
                    if($.isEmptyObject(result.error)){
                      $("#userTable > tbody").append("<tr><td>"+ result.coa_id +"</td><td>"+ result.dimension +"</td><td>"+ result.debit +"</td><td>"+ result.credit +"</td></tr>")
                    }
                    else{
                      printErrorMsg(result.error);

                    }
                  }
                  });
              });

              function printErrorMsg (msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
    });
</script>


@endsection

