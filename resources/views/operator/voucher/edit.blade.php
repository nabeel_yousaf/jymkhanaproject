@extends('operator.layouts.master')
@section('title','Add Voucher')

@section('content')

<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Update Voucher</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
              <li class="breadcrumb-item active">Update Voucher</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Edit Voucher Record</h3>

              <div class="card-tools">
                {{--<a href="#" class="btn btn-info">Edit Vouchers
                                                  </a>--}}
              </div>
            </div>
            <!-- @if($errors->any())
                  @foreach ($errors->all() as $error)
                      <div class="ml-3 text-danger">{{ $error }}</div>
                  @endforeach
              @endif -->

              <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
              </div>
            @foreach($edit as $edit)
            <form action="" method="post" id="voucherForm">
              @csrf
                
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="date">Date</label>
                      <input type="date" id="date" name="date" class="form-control" value="{{$edit->voucher_date}}">
                      <span class="text-danger">{{$errors->first('date')}}</span>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="vouchertype">Voucher Type</label>
                      <select class="form-control custom-select" id="vouchertype" name="vouchertype" value="{{$edit->voucher_type}}">
                        <option selected value="{{$edit->voucher_type}}">{{$edit->voucher_type}}</option>
                        <option value="Bank Receipt">Bank Receipt</option>
                        <option value="Bank Payment">Bank Payment</option>
                        <option value="Cash Payment">Cash Payment</option>
                        <option value="Cash Receipt">Cash Receipt</option>
                        <option value="Journal Voucher">Journal Voucher</option>
                      </select>
                      <span class="text-danger">{{$errors->first('vouchertype')}}</span>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label for="coa_id">Account Name</label>
                      <select class="form-control selectAccount" id="coa_id" name="coa_id" value="">
                        <option selected disabled value="">Select one</option>
                        @foreach($accounts as $coa)
                            @if($coa['membership_id'] != '')
                                <option value="{{$coa['id']}}">( {{ $coa['membership_id'] }} ){{$coa['name']}}</option>
                             @else
                                <option value="{{$coa['id']}}">{{$coa['name']}}</option>
                             @endif
                        @endforeach
                      </select>
                      <span class="text-danger">{{$errors->first('coa_id')}}</span>
                    </div>
                    <div class="form-group">
                      <label for="dimension">Dimension</label>
                      <select class="form-control custom-select" id="dimension" name="dimension" value="">
                        <option selected disabled value="">Select one</option>
                        @foreach($dim as $dimension)
                          <option value="{{$dimension->id}}">{{$dimension->dim_name}}</option>
                        @endforeach
                      </select>
                      <span class="text-danger">{{$errors->first('dimension')}}</span>
                    </div>

                    <div class="form-group">
                      <label for="naration">Naration</label>
                      <textarea name="naration" id="naration" value="" cols="30" rows="5" class="form-control"></textarea>
                      <span class="text-danger">{{$errors->first('naration')}}</span>
                    </div>

                    <div class="form-group">
                      <label for="amount">Amount</label>
                      <input type="text" name="amount" id="amount" class="form-control amount" value="">
                      <span class="text-danger">{{$errors->first('amount')}}</span>
                    </div>
                    <input type="hidden" value="" id="voucher_details_id" name="voucher_details_id">
                    <input type="hidden" value="{{$edit->id}}" id="voucher_masters_id" name="voucher_masters_id">


                  </div>
                  <div class="col-sm-8">
                    <div class="table-responsive" style="margin-top: 30px;">
                      <table id="userTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="th-sm">Account Name</th>
                            <th class="th-sm">Dimension</th>
                            <th class="th-sm">Debit</th>
                            <th class="th-sm">Credit</th>
                            <th class='th-sm'>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($voucher_coa as $key => $v_d)  
                            @if($edit->id == $v_d->voucher_master_id)
                            @php $vdd_id = $v_d->id @endphp
                            @php $vd_id = $v_d->voucher_master_id @endphp
                            @php $naration = $v_d->narration @endphp
                            @php $coa_id = $v_d->coa_id @endphp
                            @php $dim_id = $v_d->dim_id @endphp
                            @php $debit = $v_d->debit @endphp
                            @php $credit = $v_d->credit @endphp
                            @php $name = $v_d->name @endphp
                            @php $voucher_master_id = $v_d->voucher_master_id @endphp
                            @php 
                                if($v_d->debit == 0){
                                    $amount = $v_d->credit;
                                }
                                else{
                                    $amount = $v_d->debit;
                                } 
                            @endphp
                            <tr id="temp_tr" data-vid="{{$vdd_id}}" data-nar="{{$naration}}">
                                <td class="name" data-coaid = "{{$coa_id}}">{{$name}}</td>
                                <td class="dim" data-dimid = "{{$dim_id}}">{{$dim_id}}</td>
                                <td class="deb" data-deb = "{{$debit}}">{{$debit}}</td>
                                <td class="cred" data-cred = "{{$credit}}">{{$credit}}</td>
                                <td><a href="javascript:void(0)" class="btn btn-primary btn-rounded editVoucher"><i class="fa fa-edit"></i></a></td>
                            </tr>
                            @endif
                            
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    <div class="col-sm-12 text-right">
                    <input type="submit" id="saveVoucher" value="Save" name="submit" class="btn btn-primary">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input style="margin-left: 20px" id="debitVoucher" type="submit" value="Debit" name="submit" class="btn btn-success">
                      <input type="submit" id="creditVoucher" value="Credit" name="submit" class="btn btn-danger">

                    </div>
                  </div>
                </div>
          	</form>
            @endforeach
	   	  </div>
      </div>
    </div>
</section>





</div>
@endsection
@section('jsblock')

<script>
        $(document).ready(function () {
            $('#userTable').on('click', '.editVoucher', function(e){
            var coa_id = $(this).closest('tr').find('td.name').attr("data-coaid");
            var narration = $(this).closest('tr').attr("data-nar");
            var dim = $(this).closest('tr').find('td.dim').attr("data-dimid");
            var deb = $(this).closest('tr').find('td.deb').attr("data-deb");
            var cred = $(this).closest('tr').find('td.cred').attr("data-cred");
            var id = $(this).closest('tr').attr("data-vid");
            if (deb == "0.00") {
              var ammount = cred;
            }
            else{
              var ammount = deb;
            }
                $('#coa_id').each(function() {
                $(this).val(coa_id);
                });
                $('#dimension').each(function() {
                $(this).val(dim);
                });
                $("#naration").val(narration);
                $("#amount").val(ammount);
                $("#voucher_details_id").val(id);
              $(this).closest('tr').remove();
            });

        });

    
</script>

<script>

  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });    
                
              $('#debitVoucher').click(function(e){
                
                e.preventDefault();
                var detail_id = $("#voucher_details_id").val();
                  $.ajax({
                  type: "POST",
                  url: "/operator/editVoucher",
                  data: {
                      id: detail_id,
                      coa_id: $('#coa_id').val(),
                      dimension: $('#dimension').val(),
                      naration: $('#naration').val(),
                      debit: $('#amount').val(),
                      credit: "0.00",
                      voucher_master_id: "{{$voucher_master_id}}"
                  },
                  success: function(result){
                    if($.isEmptyObject(result.error)){
                      // $("#temp_tr").closest('tr').remove();
                      $("#userTable > tbody").append("<tr id='temp_tr' data-vid='"+ result.vd_id +"' data-nar='"+ result.naration +"'>" +
                      "<td class='name' data-coaid='"+ result.coa_id +"'>"+ result.coa_name +"</td>" +
                      "<td class='dim' data-dimid='"+ result.dim_id +"'>"+ result.dimension +"</td>" +
                      "<td class='deb' data-deb='"+ result.debit +"'>"+ result.debit +"</td>" +
                      "<td class='cred' data-cred='"+ result.credit +"'>"+ result.credit +"</td>" +
                      "<td><a href='javascript:void(0)' class='btn btn-primary btn-rounded editVoucher'><i class='fa fa-edit'></i></a></td>" +
                      "</tr>");

                      $('#coa_id').each(function() {
                          $(this).val("");
                      });
                      $('#dimension').each(function() {
                        $(this).val("");
                      });
                      $("#naration").val("");
                      $("#amount").val("");
                      $("#voucher_details_id").val("");
                    }
                    else{
                      printErrorMsg(result.error);
                    }
                  }
                });

              });

              $('#creditVoucher').click(function(e){
                e.preventDefault();
                
                var detail_id = $("#voucher_details_id").val();
                  $.ajax({
                    type: "POST",
                    url: "/operator/editVoucher",
                    data: {
                      id: detail_id,
                      coa_id: $('#coa_id').val(),
                      dimension: $('#dimension').val(),
                      naration: $('#naration').val(),
                      credit: $('#amount').val(),
                      debit: "0.00",
                      voucher_master_id: "{{$voucher_master_id}}"
                    },
                    success: function(result){
                      if($.isEmptyObject(result.error)){
                        // $("#temp_tr").closest('tr').remove();
                        $("#userTable > tbody").append("<tr id='temp_tr' data-vid='"+ result.vd_id +"' data-nar='"+ result.naration +"'>" +
                        "<td class='name' data-coaid='"+ result.coa_id +"'>"+ result.coa_name +"</td>" +
                        "<td class='dim' data-dimid='"+ result.dim_id +"'>"+ result.dimension +"</td>" +
                        "<td class='deb' data-deb='"+ result.debit +"'>"+ result.debit +"</td>" +
                        "<td class='cred' data-cred='"+ result.credit +"'>"+ result.credit +"</td>" +
                        "<td><a href='javascript:void(0)' class='btn btn-primary btn-rounded editVoucher'><i class='fa fa-edit'></i></a></td>" +
                        "</tr>");

                        $('#coa_id').each(function() {
                            $(this).val("");
                        });
                        $('#dimension').each(function() {
                          $(this).val("");
                        });
                        $("#naration").val("");
                        $("#amount").val("");
                        $("#voucher_details_id").val("");
                      }
                      else{
                        printErrorMsg(result.error);
                      }
                    }
                  });
              });


              $('#saveVoucher').click(function(e){
                e.preventDefault();

                var master_id = $("#voucher_masters_id").val();
                $.ajax({
                  type: "POST",
                  url: "/operator/entry-voucher/store",
                  data: {
                    id: master_id,
                    date: $('#date').val(),
                    vouchertype: $('#vouchertype').val()
                  },
                  success: function(result){
                    if($.isEmptyObject(result.error)){
                      window.location.href = "{{route('operatorvoucher.index')}}";
                    }
                    else{
                      printErrorMsg(result.error);
                    }
                  }
                });
              });
              
              function printErrorMsg (msg) {
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display','block');
                $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                });
              }
      
  });
</script>

@endsection

