@extends('operator.layouts.master')
@section('title','Account setting')
@section('content')
    <!--breadcrumbs-->

     <ol class="breadcrumb">
          
          <li class="breadcrumb-item active">Setting</li>
        </ol>

    <!--End-breadcrumbs-->
    <div class="container">
             <div class="card card-login mx-auto mt-5">
      <div class="card-body">
            <form  method="post" action="{{url('/admin/update-pwd')}}" name="password_validate" id="password_validate" novalidate="novalidate">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" name="pwd_current" id="pwd_current" class="form-control" placeholder="Current Password" required="required" />
                        <label for="pwd_current">Current Password</label>
                        &nbsp;<span id="chkPwd"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" name="pwd_new" id="pwd_new" class="form-control" placeholder="New password" required="required"/>
                        <label for="pwd_new">New password</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                <input type="password" name="pwdnew_confirm" id="pwdnew_confirm" class="form-control" placeholder="Confirm password" required="required"/>
                <label for="pwdnew_confirm">Confirm password</label>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit" value="Update Password" class="btn btn-block btn-success">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('jsblock')
      <!-- Bootstrap core JavaScript-->
  <script src="{{asset('backend/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('backend/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('backend/jquery-easing/jquery.easing.min.js')}}"></script>




  <script src="../../vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="../../vendors/chart.js/Chart.min.js"></script>
  <script src="../../../../vendors/progressbar.js/progressbar.min.js"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="../../js/off-canvas.js"></script>
  <script src="../../js/hoverable-collapse.js"></script>
  <script src="../../js/template.js"></script>
  <script src="../../js/settings.js"></script>
  <script src="../../js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="../../js/dashboard.js"></script>
@endsection
