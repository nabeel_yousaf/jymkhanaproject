@extends('operator.layouts.master')
@section('title','All Posts')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pending Approval</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
              <li class="breadcrumb-item active">Pending Approval</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Pending Approval</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>Members_id</th>
                                          <th>Image</th>
                                          <th>Name</th>
                                          <th>Father Name</th>
                                          <th>Cnic No.</th>
                                          <th>Mobile</th>
                                          <th>Address</th>
                                          <th>Category</th>
                                          <th>Reg-Fee</th>
                                          <th>Member_fee</th>
                                          <th>Monthly_fee</th>
                                          <th>Remarks</th>
                                          <th>Status</th>
                                          <th>View</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($byCustomer as $customer)
                    <tr>
                      <td>{{$customer->membership_id}}</td>
                      <td><img src="{{url('images/members_image/',$customer->image)}}" class="rounded"
                                                    alt="" width="100"></td> <td>{{$customer->name}}</td>
                      <td>{{$customer->father_name}}</td>
                      <td>{{$customer->Cnic}}</td>
                      <td>{{$customer->cell_no}}</td>
                      <td>{{$customer->Address}}</td>
                      <td>{{$customer->category}}</td>
                      <td>{{$customer->registration_fee}}</td>
                      <td>{{$customer->membership_fee}}</td>
                      <td>{{$customer->monthly_sub_fee}}</td>
                      <td>{{$customer->remarks}}</td>

                      <td>
                        @if ($customer->status==1)
                        <span class="badge bg-warning">unpaid</span>
                        @else
                        <span class="badge bg-success">paid</span>
                        @endif
                      </td>
                      <td>                       
                      <span class="badge bg-danger">Not Approved</span>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>

                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->


    <script type="text/javascript">
        @if (Session::has('success'))

        toastr.success("{{Session::get('success')}}");
        @endif
    </script>
<script>
    $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection
