@extends('operator.layouts.master')
@section('title','List Categories')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
<!-- iCheck -->
<link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
<!-- Plugin css for this page -->
<!-- endinject -->
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All Members Record</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">All Members Record</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
         
            
        <div class="row">
            
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Members Record</h3>
                        
                    </div>    
                    <div class="col-md-12" style="margin-bottom:10px">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Start Date</label>
                                <input type="date" name="startDate" id="startDate" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label>End Date</label>
                                <input type="date" name="endDate" id="endDate" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <button id="searchBtn" style="margin-top:30px" class="form-control btn btn-success" >Search</button> 
                            </div>
                            <div class="col-md-3">
                                <label>Click button to Print Report</label>
                                <button type="button" class="form-control btn btn-success" id="print">Print</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Members_id</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Father Name</th>
                                        <th>Cnic No.</th>
                                        <th>Mobile</th>
                                        <th>Address</th>
                                        <th>Category</th>
                                        <th>Reg-Fee</th>
                                        <th>Member_fee</th>
                                        <th>Monthly_fee</th>
                                        <th>Remarks</th>
                                        <th>Joining Date</th>
                                        <th>Created_at</th>
                                        <th>Status</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($customers as $customer)
                                    <tr>
                                        <td>{{$customer->membership_id}}</td>
                                        <td><img src="{{url('images/members_image/',$customer->image)}}" class="rounded" alt="" width="100"></td>
                                        <td>{{$customer->name}}</td>
                                        <td>{{$customer->father_name}}</td>
                                        <td>{{$customer->Cnic}}</td>
                                        <td>{{$customer->cell_no}}</td>
                                        <td>{{$customer->Address}}</td>
                                        <td>{{$customer->category}}</td>
                                        <td>{{$customer->registration_fee}}</td>
                                        <td>{{$customer->membership_fee}}</td>                                        
                                        <td>{{$customer->monthly_sub_fee}}</td>
                                        <td>{{$customer->remarks}}</td>
                                        <td>{{$customer->JoiningDate}}</td>
                                        <td>{{ $customer->created_at }}</td>
                                        <td>
                                            @if ($customer->status==1)
                                            <span class="badge bg-danger">unpaid</span>
                                            @else
                                            <span class="badge bg-success">paid</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('operatorcustomers-detail',$customer->membership_id)}}"
                                                class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('operatoredit-profile',$customer->membership_id)}}"
                                                class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
 
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <!-- /.content -->
</div>
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="overflow-x: scroll; background: #fff;">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"> Options for Report  </h4>
            </div>
            <div class="modal-body">
            <form action="{{route('operatorsearchmember')}}" method="post">
                {{ csrf_field() }}
                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Members_id</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Father Name</th>
                            <th>Cnic No.</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th>Category</th>
                            <th>Reg-Fee</th>
                            <th>Member_fee</th>
                            <th>Monthly_fee</th>
                            <th>Remarks</th>
                            <th>Status</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td> <input type="checkbox" name="select[]" value="membership_id"></td>
                            <td> <input type="checkbox" name="select[]" value="image"> </td>
                            <td> <input type="checkbox" name="select[]" value="name"> </td>
                            <td> <input type="checkbox" name="select[]" value="father_name"> </td>
                            <td> <input type="checkbox" name="select[]" value="Cnic"></td>
                            <td> <input type="checkbox" name="select[]" value="cell_no"> </td>
                            <td> <input type="checkbox" name="select[]" value="Address"> </td>
                            <td> <input type="checkbox" name="select[]" value="category"> </td>
                            <td> <input type="checkbox" name="select[]" value="registration_fee"></td>
                            <td> <input type="checkbox" name="select[]" value="membership_fee"></td>
                            <td> <input type="checkbox" name="select[]" value="monthly_sub_fee"></td>
                            <td> <input type="checkbox" name="select[]" value="remarks"></td>
                            <td> <input type="checkbox" name="select[]" value="status"></td>

                            <input type="hidden" name="startDate" id="start-date">
                            <input type="hidden" name="endDate" id="end-date">
                        </tr>
                    </tbody>

                </table>
                <button type="submit" class="btn btn-primary">Generate</button>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection
@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->


<script type="text/javascript">
    @if (Session::has('success'))

        toastr.success("{{Session::get('success')}}");
        @endif
</script>
<script>
var table1;
    $(function () {
    table1 = $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script>
    $("#print").click(function () {
        
        let startDate = $('#startDate').val();
        let endDate = $('#endDate').val(); 

        $('#start-date').val(startDate);
        $('#end-date').val(endDate);
        
        $('#ajaxModel').modal('show');
        
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script>
$.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
        var min = $('#startDate').val();
        var max = $('#endDate').val();
        var createdAt = data[12] || 0; // Our date column in the table

        if (
        (min == "" || max == "") ||
        (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
        ) {
        return true;
        }
        return false;
    }
    );
$("#searchBtn").click(function(){
    console.log("click")
    table1.draw();
});
</script>
@endsection
