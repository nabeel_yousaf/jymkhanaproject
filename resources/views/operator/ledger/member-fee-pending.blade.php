@extends('operator.layouts.master')
@section('title','List Categories')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
<!-- iCheck -->
<link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
<!-- Plugin css for this page -->
<!-- endinject -->
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ledger</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container">
                {{-- <div class="col-md-4" style="margin-bottom: 25px">
                        <label>Click button to Print Report</label>
                        <button type="button" class="form-control btn btn-success" id="printReport">Print</button>
                </div> --}}
            <table class="table table-border" id="ledgerTable">
                <thead>
                    <tr>
                        <th>M Id</th>
                        <th>Name</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pending as $item)
                        <tr>
                            <td>{{ $item['m_id'] }}</td>
                            <td>{{ $item['name'] }}</td>
                            <td>{{ $item['debit'] }}</td>
                            <td>{{ $item['credit'] }}</td>
                            <td>{{ $item['balance'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>

    <!-- /.content -->
</div>
@endsection
@section('jsblock')
<script>
    $(document).ready(function() {
        $('#ledgerTable').DataTable();
    });
</script>
@endsection
