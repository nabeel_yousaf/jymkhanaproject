@extends('operator.layouts.master')
@section('title','Add New Employee')
@section('styles')
  <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-colorpicker.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/select2-bootstrap4.min.css')}}">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-duallistbox.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create New Employee</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
              <li class="breadcrumb-item active">Create New Employee</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Add Employee Record</h3>

              <div class="card-tools">
                <a href="#" class="btn btn-info">View Employess
                  </a>
              </div>
            </div>
            <form action="{{route('operator-employees.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="name">Full Name</label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}">
                        <span class="text-danger">{{$errors->first('name')}}</span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="custom_id">Custom Id</label>
                        <input type="text" id="custom_id" name="custom_id" class="form-control" value="{{old('custom_id')}}">
                        <span class="text-danger">{{$errors->first('custom_id')}}</span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control" value="{{old('email')}}">
                        <span class="text-danger">{{$errors->first('email')}}</span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Phone No.</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-phone"></i></span>
                          </div>
                          <input type="text" class="form-control" name="phone" data-inputmask="'mask': ['999-999-9999[999]', '+099 999  99[9]-99']" data-mask="" im-insert="true" value="{{old('phone')}}">
                        </div>
                        <!-- /.input group -->
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="cnic">CNIC</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-credit-card"></i></span>
                          </div>
                          <input type="text" name="cnic" class="form-control" value="{{old('cnic')}}" data-inputmask="'mask': ['99999-9999999-9', '+099 999  99[9]-99']" data-mask="" im-insert="true">
                        </div>
                        <span class="text-danger">{{$errors->first('cnic')}}</span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="salary">Salary</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-credit-card"></i></span>
                          </div>
                          <input type="text" name="salary" class="form-control" value="{{old('salary')}}">
                        </div>
                        <span class="text-danger">{{$errors->first('salary')}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                      
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="inputStatus">Status</label>
                        <select class="form-control custom-select" name="status" value="{{old('status')}}">
                          <option>Select one</option>
                          <option value="1">Active</option>
                          <option value="0">Unactive</option>
                        </select>
                        <span class="text-danger">{{$errors->first('status')}}</span>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="address">Designation</label>
                        <select class="form-control custom-select" name="employee_post_id" value="{{old('status')}}">
                          <option>Select one</option>
                          @foreach($posts as $post)
                          <option value="{{$post->id}}">{{$post->title}}</option>
                          @endforeach
                        </select>
                        <span class="text-danger">{{$errors->first('address')}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-6 mt-4">
                      <div class="form-group">
                        <input type="file" class="custom-file-input" name="image" id="customFile">
                          <label class="custom-file-label" for="customFile">Choose Image</label>
                          <span class="text-danger">{{$errors->first('image')}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="city">City</label>
                          <input type="text" id="city" name="city" class="form-control" value="{{old('city')}}">
                          <span class="text-danger">{{$errors->first('city')}}</span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="address">User Address</label>
                          <input type="text" id="address" name="address" class="form-control" value="{{old('address')}}">
                          <span class="text-danger">{{$errors->first('address')}}</span>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label for="city">Password</label>
                        <input type="password" placeholder="Password" id="password" class="form-control" name="password" value="{{old('password')}}">
                        <span class="text-danger">{{$errors->first('password')}}</span>
                      </div>
                      <div class="col-sm-6">
                        <label for="city">Password</label>
                        <input type="password" placeholder="Confirm Password" id="password_confirmation" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
                        <span class="text-danger">{{$errors->first('password_confirmation')}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Add New Employee" class="btn btn-success float-right">
                  </div>
                </div>
            </form>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@push('jsblock')
  
  <script type="text/javascript">
      @if (Session::has('success'))
      toastr.success("{{Session::get('success')}}");

      @endif
  </script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

    

    })
  </script>
@endpush
