@extends('operator.layouts.master')
@section('title','Admin Dashboard')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
    <!-- Plugin css for this page -->
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Chart Of Accounts</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
                            <li class="breadcrumb-item active">Chart Of Accounts</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container">
                <a href="{{ route('operatorChartOfAccount.create') }}" class="btn btn-outline-primary">Create New</a>
                <br><br>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{session()->get('message')}}
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif
                <div class="table-responsive">
                    <table id="orderList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="th-sm">#</th>

                            <th class="th-sm">Name
                            </th>
                            <th class="th-sm">Description
                            </th>
                            <th class="th-sm">Type
                            </th>
                            <th class="th-sm">Is Parent
                            </th>
                            <th class="th-sm">Parent
                            </th>
                            <th class="th-sm">Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $chartOfAccount)
                            @php
                                $is_parent = '';
                                if ($chartOfAccount->is_parent > 0) {
                                    $is_parent = "Yes";
                                } else {
                                    $is_parent = "No";
                                }
                                $parent = '';
                                if ($chartOfAccount->parent_id != 0) {
                                    $acc = \App\ChartOfAccount::where('id','=',$chartOfAccount->parent_id)->first();
                                    if($acc) {
                                        $parent = $acc->name;
                                    }
                                    else {
                                        $parent = "NA";
                                    }
                                } else {
                                    $parent = "NA";
                                }
                            @endphp
                        <tr>
                            <td>{{$chartOfAccount->id}}</td>
                            <td>{{ $chartOfAccount->name }}</td>
                            <td>{{$chartOfAccount->description}}</td>
                            <td>{{$chartOfAccount->type}}</td>
                            <td>{{$is_parent}}</td>
                            <td>{{$parent}}</td>
                            @if($chartOfAccount->id != 54 && $chartOfAccount->id != 871 && $chartOfAccount->id != 870)
                               <td>
                                    <a href="{{route('operatorChartOfAccount.edit',[$chartOfAccount->id])}}" class="btn btn-sm btn-outline-success">Edit</a>
                                    /
                                    <a href="{{route('operatorChartOfAccount.delete',[$chartOfAccount->id])}}" class="btn btn-sm btn-outline-danger">Delete</a>
                                </td>
                            @else
                                <td>No Action</td>
                            @endif
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('jsblock')


    <script>
        $(function () {
            $('#orderList').DataTable();
        });
    </script>
@endsection
