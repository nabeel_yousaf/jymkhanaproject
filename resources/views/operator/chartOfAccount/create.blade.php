@extends('operator.layouts.master')
@section('title','Operator Dashboard')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
    <!-- Plugin css for this page -->
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Chart Of Accounts</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/operator')}}">Home</a></li>
                            <li class="breadcrumb-item active">Chart Of Accounts</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container">
                <h2>Create New Chart Of Account</h2>
                <br>
                <form action="{{route('operatorChartOfAccount.store')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Select Parent Account</label>
                                <select name="parent_id" class="form-control">
                                    <option selected value="0">New Parent</option>
                                    @foreach($chartAccount as $parent)
                                        <option value="{{$parent->id}}">{{$parent->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Chart Of Account Name</label>
                                <input type="text" name="name" class="form-control" required placeholder="Enter Chart of Account Name">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Chart Of Account Description</label>
                                <input type="text" name="description" class="form-control" required placeholder="Enter Chart of Account Description">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Select Chart of Account Type</label>
                                <select name="is_parent" class="form-control" required>
                                    <option selected disabled="disabled" value="0">chart of account type</option>
                                    @foreach($accountType as $type)
                                        <option value="{{$type->id}}">{{$type->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            <button class="btn btn-outline-success" type="submit" name="submit">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('jsblock')
    
    <script>
        $(function () {
            $('#orderList').DataTable();
        });
    </script>
@endsection
