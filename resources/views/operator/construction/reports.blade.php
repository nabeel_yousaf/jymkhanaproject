<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Gym Khana</title>
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!
  <!-- Theme style  -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


  </head>
<body>


  <div class="content" id="app">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Construction Reports</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> GymKhana Club Burewala. Press Cntr + P to print the report
                  </h4>
                </div>
                <!-- /.col -->
            </div>

              <!-- Table row -->

              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Vendor</th>
                        <th>Material</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Total Amount</th>
                        <th>Credit</th>
                        <th>Payments</th>
                        <th>Labour</th>
                        <th>No. Labour</th>
                        <th>Labour Payments</th>
                        <th>Skills</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach(Session::get('reportsData')  as $data)
                        <tr>
                          <td>{{$data->Vendor}}</td>
                          <td>{{ $data['constructionmaterial']['mat_name'] }}</td>
                          <td>{{$data->qty}}</td>
                          <td>{{$data->unit_price}}</td>
                          <td>{{$data->total}}</td>
                          <td>{{$data->credit}}</td>
                          <td>{{$data->payment}}</td>
                          <td>{{$data->labour}}</td>
                          <td>{{$data->no_labour}}</td>
                          <td>{{$data->labour_payments}}</td>
                          <td>{{$data->skills}}</td>
                          <td>{{$data->saved_at}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">

                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Amount Due </p>

                  <div class="table-responsive">
                    <table class="table">
                      <tbody><tr>
                        <th style="width:50%">Total:</th>

                        <td>{{number_format(Session::get('sum1'))}}</td>
                      </tr>
                      <tr>
                        <th>Total Credit</th>
                        <td>{{number_format(Session::get('sum2'))}}</td>
                      </tr>
                      <tr>
                        <th>Total Payments:</th>
                        <td>{{number_format(Session::get('sum3'))}}</td>
                      </tr>
                      <tr>
                        <th>Total Labour Payments:</th>
                        <td>{{number_format(Session::get('sum4'))}}</td>
                      </tr>
                    </tbody></table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
</div>
<footer class="main-footer">
    <strong>Copyright &copy; 2019 Softech.</strong>
    All rights reserved.
  </footer>
<script src="{{asset('js/app.js')}}"></script>

</body>
</html>
