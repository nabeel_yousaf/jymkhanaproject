@extends('operator.layouts.master')
@section('title','List Categories')
@section('styles')
 <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-colorpicker.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/select2-bootstrap4.min.css')}}">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-duallistbox.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">

@endsection
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Construction Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Construction Reports</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
    
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Search Construction Reports </h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <form method="get" id="reportForm" action="{{route('operator-construction.search')}}" target="_blank" style="display: none;">
                    @csrf()
                    <input type="hidden" name="material" id="material-name">
                    <input type="hidden" name="billNo" id="bill-no">
                    <input type="hidden" name="startDate" id="start-date">
                    <input type="hidden" name="endDate" id="end-date">
                </form>
                <div class="col-md-3">
                    <label>Select Chart Items</label>
                    <div class="form-group">
                        <select class="form-control selectAccount select2" name="product" id="product">
                            <option value="0">All</option>
                            @foreach($constructionMaterial as $item)
                                <option value="{{ $item->mat_name }}" > {{ $item->mat_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Search By Bill No</label>
                    <div class="form-group">
                      <input type="number" id="billNo" name="bill_Num" class="form-control" placeholder="search by Bill number">
                    </div>
                </div>
                <div class="col-md-3">
                        <label>Start Date</label>
                        <input type="date" name="startDate" id="startDate" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="end-date">End Date</label>
                    <input type="date" name="endDate" id="endDate" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Click button to Print Report</label>
                    <button type="button" class="form-control btn btn-success" id="printReport">Print</button>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Construction Reports</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-responsive table-bordered table-striped">
                <thead>
                <tr>
                  <th>Bill No</th>
                  <th>Vendor</th>
                  <th>Material</th>
                  <th>Quantity</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                  <th>Credit</th>
                  <th>Payments</th>
                  <th>Labour</th>
                  <th>No. Labour</th>
                  <th>Labour Payments</th>
                  <th>Skills</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               @foreach($constructionReport as $construction)
               <tr>
                  <td>{{$construction->bill_num}}</td>
                  <td>{{$construction->Vendor}}</td>
                  <td>{{$construction->mat_name}}</td>
                  <td>{{$construction->qty}}</td>
                  <td>{{round($construction->unit_price, 2)}}</td>
                  <td>{{number_format($construction->total)}}</td>
                  <td>{{number_format($construction->credit)}}</td>
                  <td>{{number_format($construction->payment)}}</td>
                  <td>{{$construction->labour}}</td>
                  <td>{{$construction->no_labour}}</td>
                  <td>{{number_format($construction->labour_payments)}}</td> 
                  <td>
                      {{$construction->skills}}
                  </td>
                  <td>{{ Carbon\Carbon::parse($construction->created_at) }}</td>
                  <td>

                        <a href="/operator-construction-reports/{{$construction->id}}/edit" class="btn btn-sm btn-outline-success">Edit</a>
                        /
                  <a href="/operator-construction-reports/{{$construction->id}}/delete" class="btn btn-sm btn-outline-danger">Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <!-- /.content -->
  </div>
@endsection
@section('jsblock')
<!-- Select2 -->

 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
  
  <script>
  var table1;
      $(function () {
      table1 = $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  </script>


<script> 
    $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {

        var min = $('#startDate').val();
        var max = $('#endDate').val();
        var product = $('#product').val();
        var bill = $("#billNo").val();


        var createdAt = data[12] || 0; // Our date column in the table
        var productName = data[2]
        var billDetail = data[0]

        console.log(min);  
        console.log(max);  
        console.log(createdAt);  
        console.log('--------------------');  

        if(bill !=""){
          if( billDetail.indexOf(bill) >=0 ){
            return true;
          }
          return false;
        }

        if(product!=0){
          if(product == productName){
            return true;
          }
          return false;
        }else{

          if (
            (min == "" || max == "") ||
            (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
            ) {
            return true;
            }
            return false;

        }
      }
    );
 

    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2();
      $('#product').select2();

      
      $(document).on('change','#product',function() {
          console.log("click")
          table1.draw();
      });

      $(document).on('change','#endDate',function(){
          let startDate = $('#startDate').val();
          let endDate = $('#endDate').val();
          if(startDate == '') {
              return;
          }
          startDate = new Date(startDate);
          endDate = new Date(endDate);
          if(startDate > endDate) {
              return;
          }
          else {
              $('#product').trigger('change');
          }
      });

      $("#billNo").keyup(function(){
        $('#product').trigger('change');
      });

      $(document).on('change','#startDate',function(){
          let startDate = $('#startDate').val();
          let endDate = $('#endDate').val();
          if(endDate == '') {
              return;
          }
          startDate = new Date(startDate);
          endDate = new Date(endDate);
          if(startDate > endDate) {
              return;
          }
          else {
              $('#product').trigger('change');
          }
      });

      $(document).on('click','#printReport',function(){
          let product = $('#product').val();
          let startDate = $('#startDate').val();
          let endDate = $('#endDate').val();
          let billNo = $("#billNo").val();
          $('#material-name').val(product);
          $('#start-date').val(startDate);
          $('#end-date').val(endDate);
          $('#bill-no').val(billNo);
          $('#reportForm')[0].submit();
      });

      

    })
</script>
@endsection
