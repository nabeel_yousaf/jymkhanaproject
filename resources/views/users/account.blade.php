@extends('frontEnd.layouts.master')
@section('title','My Account Page')
@section('slider')
@endsection
@section('content')
<div class="banner_bottom_agile_info">
    <div class="container">
        @if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif
        <div class="row">
                <div class="col-sm-1 in-gp-tl">
                </div>
                <div class="col-sm-4 in-gp-tl">

                        <div class="modal-body modal-body-sub_agile">
                                <div class="modal_body_left modal_body_left1">
                                <h3 class="agileinfo_sign">Update Profile</h3>
                                <form action="{{url('/update-profile',$user_login->id)}}" method="post">

                                @csrf
                                @method('PUT')
                                <div class="styled-input agile-styled-input-top {{$errors->has('name')?'has-error':''}}"">
                                    <input type="text" name="name" id="name" value="{{$user_login->name}}" placeholder="Name">

                                    <span class="text-danger">{{$errors->first('name')}}</span>
                                </div>
                                <div class=" {{$errors->has('address')?'has-error':''}}">
                                    <input type="text" value="{{$user_login->address}}" name="address" id="address" placeholder="Address">

                                    <span class="text-danger">{{$errors->first('address')}}</span>
                                </div>
                                <div class="styled-input {{$errors->has('city')?'has-error':''}}">
                                        <input type="text"  name="city" value="{{$user_login->city}}" id="city" placeholder="City">
                                        <span class="text-danger">{{$errors->first('city')}}</span>
                                    </div>
                                    <div class="styled-input {{$errors->has('state')?'has-error':''}}">
                                            <input type="text"  name="state" value="{{$user_login->state}}" id="state" placeholder="State">
                                            <span class="text-danger">{{$errors->first('state')}}</span>
                                        </div>
                                <div class="styled-input">
                                                <select name="country" id="country">
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->country_name}}" {{$user_login->country==$country->country_name?' selected':''}}>{{$country->country_name}}</option>
                                                    @endforeach
                                                </select>
                                </div>
                                <div class="styled-input {{$errors->has('pincode')?'has-error':''}}">
                                        <input type="text" name="pincode" value="{{$user_login->pincode}}" id="pincode" placeholder="Pincode">
                                        <span class="text-danger">{{$errors->first('pincode')}}</span>
                                    </div>
                                    <div class="styled-input {{$errors->has('mobile')?'has-error':''}}">
                                        <input type="text" name="mobile" value="{{$user_login->mobile}}" id="mobile" placeholder="Mobile">
                                        <span class="text-danger">{{$errors->first('mobile')}}</span>
                                    </div>
                                <input type="submit" value="Update Profile">
                            </form>

                             <div class="clearfix"></div>

                            </div>
        </div>
                </div><!--/login form-->

            <div class="col-sm-2">
                    <h3 class="agileinfo_sign text-center">OR</h3>
            </div>
            <div class="col-sm-4 cin-gp-tb">
                    <div class="modal-body modal-body-sub_agile">
                                <div class="modal_body_left modal_body_left1">
                            <h3 class="agileinfo_sign">Update Password</h3>
                    <form action="{{url('/update-password',$user_login->id)}}" method="post" class="form-horizontal">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        {{method_field('PUT')}}
                        <div class="styled-input {{$errors->has('password')?'has-error':''}}">
                            <input type="password" name="password" id="password" placeholder="Old Password">
                            @if(Session::has('oldpassword'))
                                <span class="text-danger">{{Session::get('oldpassword')}}</span>
                            @endif
                        </div>
                        <div class="styled-input {{$errors->has('newPassword')?'has-error':''}}">
                            <input type="password"  name="newPassword" id="newPassword" placeholder="New Password">
                            <span class="text-danger">{{$errors->first('newPassword')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('newPassword_confirmation')?'has-error':''}}">
                            <input type="password"  name="newPassword_confirmation" id="newPassword_confirmation" placeholder="Confirm Password">
                            <span class="text-danger">{{$errors->first('newPassword_confirmation')}}</span>
                        </div>
                        <input type="submit" value="Update Password">
                    </form>
                </div><!--/sign up form-->
            </div>
        </div></div>
    </div></div>
    <div style="margin-bottom: 20px;"></div>
@endsection
