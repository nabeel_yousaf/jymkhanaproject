<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<title>JymKhana | Home</title>
<link rel='stylesheet' href="{{asset('assets/css/styles.min.css')}}" type='text/css'/>
<link rel='stylesheet' href="{{asset('assets/css/genericons.css')}}" type='text/css'/>
<link rel='stylesheet'href="{{asset('assets/css/style.css')}}" type='text/css'/>
<link rel='stylesheet'href="{{asset('assets/css/custom-style.css')}}" type='text/css'/>
<link rel='stylesheet'href="{{asset('assets/css/public.css')}}" type='text/css'/>
<link rel='stylesheet'href="{{asset('assets/css/tooltip.min.css')}}" type='text/css'/>
<link rel='stylesheet' href="{{asset('css/responsiveslides.css')}}" type='text/css'/>
<link rel='stylesheet' href="{{asset('css/demo.css')}}" type='text/css' media='all' />
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

<style>

</style>
</head>
<body class="home page-template-default tribe-theme-twentyfourteen masthead-fixed slider">
<div id="page" class="hfeed site">
<header id="masthead" class="site-header" role="banner">
<div class="header-main">
<h1 class="site-title"><a href="index.html" rel="home">GymKhana</a></h1>

<nav id="primary-navigation" class="site-navigation primary-navigation desktop" role="navigation">

<a class="screen-reader-text skip-link" href="#content">Skip to content</a>
<div class="menu-menu-1-container"><ul id="menu-menu-1" class="nav-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4"><a href="{{ route('/') }}">Home</a></li>
<li  class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{ route('login') }}">Member Login</a></li>
<li  class="menu-item menu-item-type-custom menu-item-object-custom"><a href="{{route('customer-register')}}">Member SignUp</a></li>
</ul></div> </nav>

<nav class="navbar navbar-expand-lg navbar-light bg-light mobile" style="display: none;">
  <a class="navbar-brand" href="index.html" style="margin-left: -75px;"><strong>GymKhana</strong></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('/') }}">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('login') }}">Member Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('customer-register')}}">Member SignUp</a>
      </li>
    </ul>
    
  </div>
</nav>
</div>

</header>
<div id="main" class="site-main">
<div id="main-content" class="main-content">
<!-- banner -->
<!--content-->
<div class="content">
@section('slider')
    @include('frontEnd.layouts.slider')
@show
    <!-- //banner -->

@yield('content')
</div>
@include('frontEnd.layouts.category_menu')
<!-- footer -->
@include('frontEnd.layouts.footer')
<!-- //footer -->


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
