<div id="featured-content" class="featured-content" style="background-image: url('assets/fourteen-colors/pattern-dark-inverse.svg');width:100%;z-index: 10;">
<div class="featured-content-inner">
<div class="callbacks_container">
      <ul class="rslides" id="slider4">
        <li>
          <img src="{{asset('assets/images/SLIDER-1.jpg')}}" alt="">
          <p class="caption">GymKhana View</p>
        </li>
        <li>
          <img src="{{asset('assets/images/SLIDER-2.jpg')}}" alt="">
          <p class="caption">GymKhana view 2</p>
        </li>
        <li>
          <img src="{{asset('assets/images/SLIDER-3.jpg')}}" alt="">
          <p class="caption">GymKhana View 3 </p>
        </li>
        <li>
          <img src="{{asset('assets/images/SLIDER-4.jpg')}}" alt="">
          <p class="caption">GymKhana View 4</p>
        </li>
        <li>
          <img src="{{asset('assets/images/SLIDER-5.jpg')}}" alt="">
          <p class="caption">GymKhana View 5</p>
        </li>
      </ul>
    </div>

</div>
</div>
