@extends('frontEnd.layouts.master')
@section('title','Online Shopping in Pakistan at the best prices | Fari Couture')
@section('slider')
@endsection
@section('content')
<div class="page-head_agile_info_w3l">
		<div class="container">
			<h3>About <span>Us </span></h3>
			<!--/w3_short-->
				 <div class="services-breadcrumb">
						<div class="agile_inner_breadcrumb">

						   <ul class="w3_short">
								<li><a href="http://127.0.0.1:8000">Home</a><i>|</i></li>
								<li>About Us</li>
							</ul>
						 </div>
				</div>
	   <!--//w3_short-->
	</div>
</div>
<div class="banner_bottom_agile_info">
	    <div class="container">
			<div class="agile_ab_w3ls_info">

				 <div class="col-md-12 ab_pic_w3ls_text_info">
				    <h5>Our Terms And<span> Condition</span> </h5>
					<p>The Company may amend this Agreement and/or the Privacy Policy at any time by posting a revised version on the Site. All updates and amendments shall be notified to you via posts on website or through e-mail. The revised version will be effective at the time we post it on the Site, and in the event you continue to use our Site, you are impliedly agreeing to the revised TERMS AND CONDITIONS and Privacy Policy expressed herein.</p>
                    <p>In addition, if the revised version of this Agreement includes a Substantial Change, we will provide you with 30 days’ prior notice of such Substantial Change as per the Notification Preferences provided by you.</p>
                    <p>Please read these terms and conditions carefully. These terms & conditions, as modified or amended from time to time, are a binding contract between the company and you. If you visit, use, or shop at the site (or any future site operated by the company, you accept these terms and conditions). In addition, when you use any current or future services of the company or visit or purchase from any business affiliated with the company or third party vendors, whether or not included in the site, you will also be subject to the guidelines and conditions applicable to such service or merchant. If these conditions are inconsistent with such guidelines and conditions, such guidelines and conditions will prevail.</p>
                    <p>If this Terms and Conditions conflicts with any other document, the Terms and Conditions will prevail for the purposes of usage of the Site. As a condition of purchase, the Site requires your permission to send you administrative and promotional emails. We will send you information regarding your account activity and purchases, as well as updates about our products and promotional offers. You can opt out of our promotional emails anytime by clicking the UNSUBSCRIBE link at the bottom of any of our email correspondences. Please see our Privacy Policy for details. We shall have no responsibility in any manner whatsoever regarding any promotional emails or SMS/MMS sent to you. The offers made in those promotional emails or SMS/MMS shall be subject to change at the sole discretion of the Company and the Company owes no responsibility to provide you any information regarding such change. By placing an order, you make an offer to us to purchase products you have selected based on standard Site restrictions, Merchant specific restrictions, and on the terms and conditions stated below. You are required to create an account in order to purchase any product from the Site. This is required so we can provide you with easy access to print your orders and view your past purchases.</p>
				</div>
				  <div class="clearfix"></div>
			</div>
		 </div>
    </div>


	<!-- /we-offer -->
		<div class="sale-w3ls">
			<div class="container">
				<h6>We Offer Flat <span>40%</span> Discount</h6>

				<a class="hvr-outline-out button2" href="single.html">Shop Now </a>
			</div>
		</div>
	<!-- //we-offer -->
<!--/grids-->
<div class="coupons">
		<div class="coupons-grids text-center">
			<div class="w3layouts_mail_grid">
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<i class="fa fa-truck" aria-hidden="true"></i>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>FREE SHIPPING</h3>
						<p>Lorem ipsum dolor sit amet, consectetur</p>
					</div>
				</div>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<i class="fa fa-headphones" aria-hidden="true"></i>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>24/7 SUPPORT</h3>
						<p>Lorem ipsum dolor sit amet, consectetur</p>
					</div>
				</div>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<i class="fa fa-shopping-bag" aria-hidden="true"></i>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>MONEY BACK GUARANTEE</h3>
						<p>Lorem ipsum dolor sit amet, consectetur</p>
					</div>
				</div>
					<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<i class="fa fa-gift" aria-hidden="true"></i>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>FREE GIFT COUPONS</h3>
						<p>Lorem ipsum dolor sit amet, consectetur</p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>

		</div>
</div>


@endsection
