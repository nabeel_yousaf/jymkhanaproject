@extends('frontEnd.layouts.master')
@section('title', $title)
@section('slider')
@endsection
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-sm-12">

                <div class="features_items"><!--features_items-->
                <h2 class="title text-center" style="padding-top:10px;"><b>Your Results</b></h2>
                @if($products->count()>0)
                <p class="title count text-center">{{ $products->count()}} products found!</p>
                @else
                <p class="title count text-center">No products found!</p>
                @endif
                    <ol class="breadcrumb">
                    <li><a href="{{url('http://127.0.0.1:8000')}}">Home</a></li>
                    <li><span class="fa fa-angle-right"></span> </li>
        <li class="active">{{$qurey}}</li>
        </ol>
@if ($products->count()>0)


                    @foreach($products as $product)

                    <div class="col-md-3 product-men">
                            <div class="men-pro-item simpleCart_shelfItem">
                                <div class="men-thumb-item">
                                    <img src="{{url('products/medium/',$product->image)}}" alt="" class="pro-image-front">
                                    <img src="{{url('products/medium/',$product->image)}}" alt="" class="pro-image-back">
                                        <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                <a href="{{url('/',$product->slug)}}" class="link-product-add-cart">Quick View</a>
                                            </div>
                                        </div>
                                        <?php
                                    $date1 = $product->created_at;

                                   $strip = new DateTime($date1->format('Y-m-d'));
                                    $date2 = new DateTime(date("Y-m-d"));
                                   $days=date_diff($strip,$date2);
                                  $abc= $days->format('%R%a days');
                                  if($abc<=7)
                                  {
                                echo"<span class='product-new-top'>New</span>";
                                  }
                                  else
                                  {
                                    echo"<span class='product-new-top' style='display:none;'>$abc</span>";
                                  }
                                 ?>

                                </div>
                                <div class="item-info-product ">
                                <h4><a href="{{url('/',$product->slug)}}">{{ str_limit($product->p_name,35) }}</a></h4>
                                    <div class="info-product-price">
                                            @if ($product->discount_price=='Null')
                                            <span class="item_price">PKR. {{number_format($product->price)}}</span>
                                            <del>{{number_format($product->discount_price)}}</del>

                                            @else
                                            <span class="item_price">PKR. {{number_format($product->discount_price)}}</span>
                                            <del>{{number_format($product->price)}}</del>
                                            @endif
                                    </div>

                                </div>
                            </div>
                        </div>

                    @endforeach
                    @else
                    <h2 class="text-center">No Product Found!</h2>

@endif
                    <div class="row">
                            <div class="col-md-12" style="text-align:center;">
                            {{$products->links()}}
                            </div>
                       </div>
                </div><!--features_items-->
            </div>
        </div>
    </div>
@endsection
