@extends('frontEnd.layouts.master')
@section('title','Buy '. $byCate->name .' Wear Online in Pakistan | Fari Couture')
@section('slider')
@endsection
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-sm-12">

                <div class="features_items"><!--features_items-->

            @if($byCate!="")
                <h2 class="title text-center" style="padding-top:1%;">{{$byCate->name}}</h2>
                @if($list_product->count()>0)
                <p class="title count text-center">{{ $list_product->count()}} products found!</p>
                @else
                <p class="title count text-center">No products found!</p>
                @endif

            @endif
            <ol class="breadcrumb" style="text-align:center;">

                <li><a href="{{url('/')}}" title="{{'Home'}}">Home</a></li>
                <li><span class="fa fa-angle-right"></span> </li>
                <?php
    $categories=DB::table('categories')->where([['status',1],['id',$byCate->parent_id]])->get();
                ?>
@foreach($categories as $category)

<li>
<a href="{{route('cats',$category->slug)}}" title="{{$category->name}}">{{$category->name}}</a>
</li>
 <li><span class="fa fa-angle-right"></span> </li>
@endforeach
<li class="active" title="{{$byCate->name}}">{{$byCate->name}}</li>
</ol>

                    @foreach($list_product as $product)



                    <div class="col-md-3 product-men">
                            <div class="men-pro-item simpleCart_shelfItem">
                                <div class="men-thumb-item">
                                    <img src="{{url('products/medium/',$product->image)}}" alt="" class="pro-image-front">
                                    <img src="{{url('products/medium/',$product->image)}}" alt="" class="pro-image-back">
                                        <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                <a href="{{url('/',$product->slug)}}" class="link-product-add-cart">Quick View</a>
                                            </div>
                                        </div>
                                      <span class='product-new-top'>New</span>


                                </div>
                                <div class="item-info-product ">
                                <h4><a href="{{url('/',$product->slug)}}">{{ str_limit($product->p_name,35) }}</a></h4>
                                    <div class="info-product-price">
                                            @if ($product->discount_price=='Null')
                                            <span class="item_price">PKR. {{number_format($product->price)}}</span>
                                            <del>{{number_format($product->discount_price)}}</del>

                                            @else
                                            <span class="item_price">PKR. {{number_format($product->discount_price)}}</span>
                                            <del>{{number_format($product->price)}}</del>
                                            @endif
                                    </div>

                                </div>
                            </div>
                        </div>

                    @endforeach
                    <div class="row">
                            <div class="col-md-12" style="text-align:center;">
                            {{$list_product->links()}}
                            </div>
                       </div>
                </div><!--features_items-->
            </div>
        </div>
    </div>
@endsection
