<!doctype html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Burewala Gymkhana</title>

<!--META-->
<meta name="viewport" content="width=device-width initial-scale=1.0">

<!--FONTS-->
<link href='https://fonts.googleapis.com/css?family=Raleway:900,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
<!--CSS-->
<link rel="stylesheet" type="text/css" href="css/grid.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="css/layout.css">
<link rel="stylesheet" type="text/css" href="css/style2.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
        
<!--JS-->
<script type="text/javascript">
    var embedCode = '<iframe src="https://www.youtube.com/embed/-CpvBYE-rd8?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>'
</script>
<script src="js/modernizr.custom.js"></script>
</head>

<body>
<!-- Paste this code after body tag -->
		 <div class="se-pre-con">
         		<div class="cssload-square">
	<div><div><div><div><div></div></div></div></div></div>
	<div><div><div><div><div></div></div></div></div></div>
</div>
<div class="cssload-square cssload-two">
	<div><div><div><div><div></div></div></div></div></div>
	<div><div><div><div><div></div></div></div></div></div>
</div>
         </div>
 	<!-- Ends -->
	<div class="homebanner" id="home">
		<div class="container">
            <h1 class="os-animation" data-os-animation="bounceIn" data-os-animation-delay="0.2s">Wellcome to Burewala Gymkhana</h1>
            <span class="line os-animation" data-os-animation="bounceIn" data-os-animation-delay="0.4s"></span>
    		<p class="os-animation" data-os-animation="bounceIn" data-os-animation-delay="0.6s">“I believe that every human has a finite amount of heartbeats. I don't intend to waste any of mine running around doing exercises.”<br>
</p>
    		
            <a href="#about" class="os-animation" data-os-animation="bounceIn" data-os-animation-delay="0.8s"><img src="images/mouse.png" alt=""/></a>
    	</div>
    </div>
    
    <div class="intro" id="about">
    	<div class="container">
    	<h2 class="os-animation" data-os-animation="zoomIn" data-os-animation-delay="0.3s">Introduction</h2>
        <span class="line2 os-animation" data-os-animation="rollIn" data-os-animation-delay="0.5s"></span>
        <p class="os-animation" data-os-animation="zoomIn" data-os-animation-delay="0.7s">A gym - physical exercises and activities performed inside, often using equipment, especially when done as a subject at school. Gymnasium is a large room with equipment for exercising the body and increasing strength or a club where you can go to exercise and keep fit.

A gym is a gymnasium, also known as health club and fitness centre. Gymnasiums have moved away just being a location for gymnastics. Where they had gymnastics apparatus such as bar bells, parallel bars, jumping boards and running path etc.

If you are looking to join a gymnastics club, please see gymnastics.

A health club, fitness centre is now commonly referred to as a gym.</p>	</div>
    
    
    </div>
  
    
    <div class="work" id="work">
    	<div class="container os-animation" data-os-animation="pulse" data-os-animation-delay="0.5s">
    		<h2>Some Of our Services</h2>
            <span class="line2"></span>
            
            
            <ul id="filters" class="clearfix">               
			<li><span class="filter active" data-filter="app card icon logo web">All</span></li>
			<li><span class="filter" data-filter="app">Main Building</span></li>
			<li><span class="filter" data-filter="card">Sports Facilities</span></li>
			<li><span class="filter"  class="fas fa-coffee"data-filter="icon">Restaurant</span></li>
            <li><span class="filter" data-filter="logo">PoolSide Building</span></li>
            
			<li><span class="filter" data-filter="logo">Others</span></li>
		</ul>

		<div class="clearfix" id="portfoliolist">
			 
						
			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="images/portfolios/app/w1.jpg" alt="" />
					<div class="imgDescription2">
						<h1>MOBILE UI</h1>
                        <p>Google Mail</p>
						<div class="orangeline"></div>
					</div>
				</div>
			</div>			
			
            <div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">			
					<img src="images/portfolios/app/w2.jpg" alt="" />
					<div class="imgDescription2">
						<h1>MOBILE UI</h1>
                        <p>Google Mail</p>
						<div class="orangeline"></div>
					</div>
				</div>
			</div>
            
			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="images/portfolios/card/w3.jpg" alt="" />
					<div class="imgDescription2">
						<h1>MOBILE UI</h1>
                        <p>Google Mail</p>
						<div class="orangeline"></div>
					</div>
				</div>
            </div>	
            <div class="portfolio icons" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="images/portfolios/icon/w7.jpg" alt="" />
					<div class="imgDescription2">
						<h1>MOBILE UI</h1>
                        <p>Google Mail</p>
						<div class="orangeline"></div>
					</div>
				</div>
			</div>	
			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="images/portfolios/card/w6.jpg" alt="" />
					<div class="imgDescription2">
						<h1>MOBILE UI</h1>
                        <p>Google Mail</p>
						<div class="orangeline"></div>
					</div>
				</div>
			</div>	
			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="images/portfolios/card/w4.jpg" alt="" />
					<div class="imgDescription2">
						<h1>MOBILE UI</h1>
                        <p>Google Mail</p>
						<div class="orangeline"></div>
					</div>
				</div>
			</div>	
            
            <div class="portfolio icon" data-cat="icon">
				<div class="portfolio-wrapper">			
					<img src="images/portfolios/icon/w5.jpg" alt="" />
					<div class="imgDescription2">
						<h1>MOBILE UI</h1>
                        <p>Google Mail</p>
						<div class="orangeline"></div>
					</div>
				</div>
			</div>	
			
			<div class="portfolio logo" data-cat="logo">
				<div class="portfolio-wrapper">			
					<img src="images/portfolios/logo/w6.jpg" alt="" />
					<div class="imgDescription2">
						<h1>MOBILE UI</h1>
                        <p>Google Mail</p>
						<div class="orangeline"></div>
					</div>
				</div>
			</div>																																							

			
		</div>
        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Show me more</button></a>
    	</div>
    </div>
    
 
    


    
    <div class="intro">
    	<div class="container">
        <div class="row">
        	<div class="col-lg-12">
            <h2 class="os-animation" data-os-animation="zoomIn" data-os-animation-delay="0.3s">get in touch</h2>
            <span class="line2 os-animation" data-os-animation="zoomIn" data-os-animation-delay="0.4s"></span>
            <p class="os-animation" data-os-animation="zoomIn" data-os-animation-delay="0.5s">We ALways Get in Touch With Our Members.Our Members Is our Family .For Get in Touch,Join us  In  our  Office. Or Send Your Inquiry Here</p></div>
            <Button><a href="#contact" class="os-animation" data-os-animation="flipInX" data-os-animation-delay="0.5s">Send Your INQUIRY.</a></Button>
       </div>
        
        </div>
        </div>
    
    <div class="contactus clearfix" id="contact">
    	<div class="left clearfix os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.5s">
    		<div class="datacon">
            	<h3>Our Location</h3>
                <p>Main Multan Road <br>
Opposite To CityCenter<br>
Burewala<br>
Gymkhana</p>
	<a href="https://www.facebook.com/GymKhanaClubBurewala"><i class="fa fa-twitter" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
            </div>
    	</div>
        <div class="right clearfix os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0.5s">
    		<div class="fielddata">
            	<form>
            		<input type="text" name="Name" placeholder="Name">
                	<input type="email" name="Email" placeholder="Email">
                    <textarea placeholder="Message"></textarea>
                    <a href="#">Send</a>
                </form>
            </div>
    	</div>
    </div>

<!--  comments !--> 

<!--  comments !-->
    <footer>
    	<div class="container">
    		<a href="#"><img src="images/logo.png" alt=""/></a>
        	<p>2019 © <a href="#">Burewala Gymkhana</a>  All Rights Reserved.</p>
            <p>Designed bY<a href="">SoftTech IT $ web solutions</a></p>
        </div>
    </footer>
    
    
	<nav id="bt-menu" class="bt-menu">
				<a href="#" class="bt-menu-trigger"><span>Menu</span></a>
				<ul>
					<li><a href="{{route('customer-register')}}" class="bt-icon"><i class="fa fa-plus-square" aria-hidden="true"></i>SignUP</a></li>
					<li><a href="#home" class="bt-icon"><i class="fa fa-home" aria-hidden="true"></i> Home  </a></li>
					<li><a href="#about" class="bt-icon"><i class="fa fa-info-circle" aria-hidden="true"></i>Intro</a></li>
					<li><a href="#work" class="bt-icon"><i class="fa fa-briefcase" aria-hidden="true"></i>Services</a></li>
                    <li><a href="#contact" class="bt-icon"><i class="fa fa-phone" aria-hidden="true"></i>Contact</a></li>
                    <li><a href="Gallery" class="bt-icon"><i class="fas fa-clone" aria-hidden="true"></i>Gallery</a></li>
				
				</ul>
			</nav>
 </body>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/borderMenu.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/script.js"></script>
</html>
