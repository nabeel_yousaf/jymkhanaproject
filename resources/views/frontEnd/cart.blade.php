@extends('frontEnd.layouts.master')
@section('title', 'Cart | Fari Couture')
@section('slider')
@endsection
@section('content')
<div class="banner_bottom_agile_info" style="padding-bottom:0px;">
    <div class="container">

        <h2 class="typoh2">CART</h2>
        @if(Session::has('message'))
                <div class="alert alert-success text-center" role="alert">
                    {{Session::get('message')}}
                </div>
            @endif
        <div class="bs-docs-example">
            <table class="table">
                <thead>
                    <tr>
                        <th class="image">Item</th>
                        <th class="description"></th>
                        <th class="price">Price</th>
                        <th class="quantity">Quantity</th>
                        <th class="total">Total</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                        @foreach($cart_datas as $cart_data)



                        <?php
                            $image_products=DB::table('products')->select('image')->where('id',$cart_data->products_id)->get();
                        ?>

                        <tr>
                            <td class="cart_product">
                                @foreach($image_products as $image_product)
                                    <a href=""><img src="{{url('products/small',$image_product->image)}}" alt="" style="width: 100px;"></a>
                                @endforeach
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{$cart_data->product_name}}</a></h4>
                                <p>{{$cart_data->product_code}} | {{$cart_data->size}}</p>
                            </td>
                            <td class="cart_price">
                                <p>PKR {{number_format($cart_data->price)}}</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <a class="cart_quantity_up btn btn-success" href="{{url('/cart/update-quantity/'.$cart_data->id.'/1')}}" style="border-radius:0;border: none;"> + </a>
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{$cart_data->quantity}}" autocomplete="off" size="2" style="padding-left:25px;" disabled>
                                    @if($cart_data->quantity>1)
                                        <a class="cart_quantity_down btn btn-danger" href="{{url('/cart/update-quantity/'.$cart_data->id.'/-1')}}" style="border-radius:0;border: none;"> - </a>
                                    @endif
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">PKR. {{number_format($cart_data->price*$cart_data->quantity)}}</p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete" href="{{url('/cart/deleteItem',$cart_data->id)}}" ><i class="fa fa-times"></i></a>
                            </td>
                        </tr>



                    @endforeach
                    @if ($cart_datas->count()<=0)

                <tr>
                    <td colspan="5">
                        <h4 class="text-center">There is no item in your cart!</h4>
                    </td>
                </tr>
                    <tr>
                       <td colspan="5" class="text-center">
                       <a class="btn btn-default check_out" href="{{url('/')}}" style="background:#000000;color: #FFFFFF;border-radius:0;margin-top:15px;border: none;padding: 5px 15px;font-weight: normal;letter-spacing: 1px;">Continue To Shopping</a>
                       </td>
                </tr>
                @endif



                </tbody>
            </table>
        </div>
        <style>
                .total_area span {
                float: right;
            }
                #do_action .total_area, #do_action .chose_area {
                border: 1px solid #E6E4DF;
                color: #696763;
                padding: 30px 25px 30px 0;
                margin-bottom: 80px;
            }
                ul li {
                list-style: none;
            }
                 .total_area ul li {
                background: #E6E4DF;
                color: #696763;
                margin-top: 10px;
                padding: 7px 20px;
                   }
                </style>
    </div></div>
    <div class="banner_bottom_agile_info" style="padding-top:0px;">
        <div class="container" style="border-top:1px solid #E6E4DF;">
            <h2 class="typoh2">What would you like to do next?</h2>
   <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
   <div class="row">
        <div class="col-lg-6 in-gp-tl">

            <div class="chose_area" style="padding: 20px;">
                <form action="{{url('/apply-coupon')}}" method="post" role="form">

                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="Total_amountPrice" value="{{$total_price}}">
                    @if(Session::has('message_coupon'))
                    <span class="text-danger" style="font-size:11px;">
                            {{Session::get('message_coupon')}}
                    </span>
                    @endif
                    <div class="input-group">

                    <input type="text" class="form-control" name="coupon_code" id="coupon_code" placeholder="Promotion By Coupon" style="border-radius:0;padding:5px;">

                    <span class="input-group-btn">
                            @if ($cart_datas->count()>0)
                    <button type="submit" class="btn btn-default" type="button" style="background:#000000; border: 0 none;
                    border-radius:0; color:#ffffff;font-weight: normal;letter-spacing: 1px;">Apply</button>
                           @else
                           <button type="submit" class="btn btn-default" type="button" style="background:#000000; border: 0 none;
                           border-radius:0; color:#ffffff;font-weight: normal;letter-spacing: 1px;" disabled>Apply</button>
                           @endif
                    </span>

                            <div class="controls {{$errors->has('coupon_code')?'has-error':''}}">

                                    <span class="text-danger">{{$errors->first('coupon_code')}}</span>
                                </div>
                        </div>
                </form>
            </div>
        </div>
        <div class="col-lg-6 in-gp-tl">
            @if(Session::has('message_apply_sucess'))
                <div class="alert alert-success text-center" role="alert">
                    {{Session::get('message_apply_sucess')}}
                </div>
            @endif
            <div class="total_area" style="margin-top:3.7%;">
                <ul>
                    @if(Session::has('discount_amount_price'))
                        <li>Sub Total <span>PKR {{$total_price}}</span></li>
                        <li>Coupon Discount (Code : {{Session::get('coupon_code')}})
                        <span>PKR {{Session::get('discount_amount_price')}}</span>
                        </li>
                        <li>Total <span>PKR {{$total_price-Session::get('discount_amount_price')}}</span></li>
                    @else
                        <li>Total <span>PKR: {{number_format($total_price)}}</span></li>
                    @endif
                </ul>
<div>
        @if ($cart_datas->count()>0)
    <a class="btn btn-default check_out" href="{{url('/check-out')}}" style="background:#000000;color: #FFFFFF;border-radius:0;margin-top:15px;border: none;padding: 5px 15px;font-weight: normal;letter-spacing: 1px;">Check Out</a>
    @else
    <a class="btn btn-default disabled check_out" href="{{url('/check-out')}}" style="background:#000000;color: #FFFFFF;border-radius:0;margin-top:15px;border: none;padding: 5px 15px;font-weight: normal;letter-spacing: 1px;">Check Out</a>
    @endif
</div>
            </div>
        </div>
    </div>
        </div>
    </div>
@endsection


