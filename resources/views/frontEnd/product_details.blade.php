@extends('frontEnd.layouts.master')
@section('title', $detail_product->p_name)
@section('slider')
@endsection
@section('content')
<!-- //banner-top -->
<!-- Modal1 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
                <div class="modal-body modal-body-sub_agile">
                <div class="col-md-8 modal_body_left modal_body_left1">
                <h3 class="agileinfo_sign">Size <span>Chart</span></h3>
                <img src="{{url('products/chart',$detail_product->chart)}}" alt=" "/>
                <div class="clearfix"></div>

                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <!-- //Modal content-->
    </div>
</div>
<!-- //Modal1 -->
<div class="page-head_agile_info_w3l">
		<div class="container">
			<h3>Single <span>Page </span></h3>
			<!--/w3_short-->
				 <div class="services-breadcrumb">
						<div class="agile_inner_breadcrumb">

						   <ul class="w3_short">
                           <li><a href="{{url('/')}}">Home</a><i>|</i></li>
								<li>{{$detail_product->p_name}}</li>
							</ul>
						 </div>
				</div>
	   <!--//w3_short-->
	</div>
</div>

  <!-- banner-bootom-w3-agileits -->
<div class="banner-bootom-w3-agileits">

	<div class="container">
            @if(Session::has('message'))
                    <div class="alert alert-success text-center" role="alert">
                       <b> {{Session::get('message')}}</b>
                    </div>
                @endif
	     <div class="col-md-4 single-right-left ">
        <div class="grid images_3_of_2">
    <div class="flexslider">

        <ul class="slides">


	@forelse($imagesGalleries as $imagesGallery)
	<li data-thumb="{{url('products/small',$imagesGallery->image)}}">
    <div class="thumb-image">
     <img src="{{url('products/medium',$imagesGallery->image)}}" data-imagezoom="true" class="img-responsive">
     </div>
    </li>
    @empty
    <li data-thumb="{{url('products/small',$detail_product->image)}}">
        <div class="thumb-image">
         <img src="{{url('products/medium',$detail_product->image)}}" data-imagezoom="true" class="img-responsive">
         </div>
        </li>

			@endforelse

        </ul>
        <div class="clearfix"></div>
    </div>
     </div>
		</div>
		<div class="col-md-8 single-right-left simpleCart_shelfItem">
                <form action="{{route('addToCart')}}" method="post" role="form">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="products_id" value="{{$detail_product->id}}">
                <input type="hidden" name="product_name" value="{{$detail_product->p_name}}">
                <input type="hidden" name="product_code" value="{{$detail_product->p_code}}">
                <input type="hidden" name="product_color" value="{{$detail_product->p_color}}">
                <input type="hidden" name="quantity" value="{{$totalStock}}">

					<h3>{{$detail_product->p_name}}</h3>
					<p>
                        <span id="dynamic_price">RKR. {{number_format($detail_product->discount_price)}}</span>
                        <del id="dynamic_discount_price">{{number_format($detail_product->price)}}</del>

                        <input type="hidden" name="price" value="{{ $detail_product->price}}" id="dynamicPriceInput">

                    </p>

					<div class="description">
						<h5><a href="#" data-toggle="modal" data-target="#myModal">SIZE CHART</a></h5>

					</div>
					<div class="color-quality">
						<div class="color-quality-right">
                            <div class="row">
                                <div class="col-sm-3" style="justify-content: center;align-items: center; text-align:center;">
							<h5><b>Size:</b></h5>
                <select name="size" id="idSize" class="frm-field required sect">
                    <option value="">Select Size</option>
                        @foreach($detail_product->attributes as $attrs)

                        <option value="{{$detail_product->id}}-{{$attrs->size}}">{{ucfirst($attrs->size)}}({{$attrs->stock}})</option>


                    @endforeach
                </select>
                    </div>
                                <div class="col-sm-3" style="justify-content: center;align-items: center; text-align:center;">
                            <h5><b>Quantity:</b></h5>
                            <select name="quantity" id="idQuantity" class="frm-field required sect">
                                <option value="">Quantity</option>
                                @php

                                    for($i=1; $i<=$totalStock; $i++)
                                    {
                                echo"<option value='$i' style='text-center:center;'>$i</option>";

                                    }
                                @endphp
                            </select>
                        </div>
                        <div class="col-sm-3" style="justify-content: center;align-items: center; text-align:center;">
                            <h5><b>Available Qty:</b></h5>
                            <input type="text" name="qty" value="{{$totalStock}}" id="inputStock" class="frm-field required sect" style="width:30%; text-align:center;background-color:#fff;border:none;" disabled/>
                        </div>
						</div>
					</div>
					<div class="occasional">
                        <div class="row">
                            <div class="col-sm-4">
                        <h5><b>Quantity:</b>
                            @if($totalStock>0)
                                <span id="availableStock">In Stock</span>
                            @else
                                <span id="availableStock">Out of Stock</span>
                            @endif
                        </h5>
                            </div>
                            <div class="col-sm-4">
                        <h5><b>Condition:</b>
                                <span>New</span>
                        </h5>
                            </div>
                            <div class="col-sm-4">
						<h5><b>Brand:</b>
                            @foreach($proBrand as $brand)

							<span>{{$brand->name}}</span>
							@endforeach
					</h5>
                            </div>
						<div class="clearfix"> </div>
					</div></div>
     <div class="occasion-cart">





            @if($totalStock>0)
            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
            <fieldset>
            <input type="submit" name="submit" value="Add to cart" id="buttonAddToCart" class="button">
        </fieldset>
    </div>
            @endif

        </div>
					</div>
        </form>

					<ul class="social-nav model-3d-0 footer-social w3_agile_social single_page_w3ls">
                    <li class="share"><b>Short Description:</b> </li><br>
                    <li class="">{!!$detail_product->description!!}</li>
                    </ul>


		      </div>
	 			<div class="clearfix"> </div>
				<!-- /new_arrivals -->
                <div class="responsive_tabs_agileits">
                        <div id="horizontalTab">
                                <ul class="resp-tabs-list">
                                    <li>Description</li>
                                    <li>Brand Info</li>
                                </ul>
                            <div class="resp-tabs-container">
                            <!--/tab_one-->
                               <div class="tab1">

                                    <div class="single_page_agile_its_w3ls">
                                            {!!$detail_product->description!!}

                                    </div>
                                </div>
                                <!--//tab_one-->

<div class="tab3">

                                    <div class="single_page_agile_its_w3ls">
                                            {!!$detail_product->brand->description!!}
                                    </div>
</div>
                            </div>
                        </div>
                    </div>
            <!-- //new_arrivals -->
                  <!--/slider_owl-->

<div class="w3_agile_latest_arrivals">
    <h3 class="wthree_text_info">Featured <span>Arrivals</span></h3>
     <?php $countChunk=0;?>
        @foreach($relateProducts->chunk(3) as $chunk)
            <?php $countChunk++; ?>
            <div class="item<?php if($countChunk==1){ echo' active';} ?>">
                @foreach($chunk as $item)
                            <div class="col-md-3 product-men single">
                                    <div class="men-pro-item simpleCart_shelfItem">
                                        <div class="men-thumb-item">
                                            <img src="{{url('/products/medium',$item->image)}}" alt="" class="pro-image-front">
                                            <img src="{{url('/products/medium',$item->image)}}" alt="" class="pro-image-back">
                                                <div class="men-cart-pro">
                                                    <div class="inner-men-cart-pro">
                                                        <a href="{{url('/',$item->slug)}}" class="link-product-add-cart" title="{{$item->p_name}}">Quick View</a>
                                                    </div>
                                                </div>
                                                <span class="product-new-top">New</span>

                                        </div>
                                        <div class="item-info-product ">
                                            <h4><a href="{{url('/',$item->slug)}}">{{$item->p_name}}  </a></h4>
                                            <div class="info-product-price">
                                                <span class="item_price">PKR.{{$item->price}}</span>
                                                <del>PKR {{$item->discount_price}}</del>
                                            </div>

                                        </div>
                                    </div>
                            </div>
                            @endforeach
                        </div>
                    @endforeach


                            <!--//slider_owl-->
                    </div>
                    </div>
                         </div>
        <!--//single_page-->
        <!--/grids-->
        <div class="coupons">
                <div class="coupons-grids text-center">
                    <div class="w3layouts_mail_grid">
                        <div class="col-md-3 w3layouts_mail_grid_left">
                            <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                                <i class="fa fa-truck" aria-hidden="true"></i>
                            </div>
                            <div class="w3layouts_mail_grid_left2">
                                <h3>FREE SHIPPING</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class="col-md-3 w3layouts_mail_grid_left">
                            <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                                <i class="fa fa-headphones" aria-hidden="true"></i>
                            </div>
                            <div class="w3layouts_mail_grid_left2">
                                <h3>24/7 SUPPORT</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class="col-md-3 w3layouts_mail_grid_left">
                            <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                            </div>
                            <div class="w3layouts_mail_grid_left2">
                                <h3>MONEY BACK GUARANTEE</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                            </div>
                        </div>
                            <div class="col-md-3 w3layouts_mail_grid_left">
                            <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                                <i class="fa fa-gift" aria-hidden="true"></i>
                            </div>
                            <div class="w3layouts_mail_grid_left2">
                                <h3>FREE GIFT COUPONS</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                </div>
        </div>

@endsection


