@extends('secertry.layouts.master')
@section('title','Secertry Dashboard')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
<!-- iCheck -->
<link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
<!-- Plugin css for this page -->
<!-- endinject -->
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/secertry')}}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
                <div class="col-md-3">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            @php

                            $count_items=DB::table('members_record')->get();@endphp
                            <h3>{{ $count_items->count() }}</h3>

                            <p>Members Registered</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="{{route('memberspayrecord.index')}}" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>RS. {{ number_format($data['memberDrAmount'],2) }}</h3>
                            <p>Fee Receivable </p>
                        </div>
                        <a href="secertry/ledger" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>RS. {{ number_format($data['memberCrAmount'],2) }}</h3>
                            <p>Recieved</p>
                        </div>
                        <a href="secertry/member-fee-cleared" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>RS. {{ number_format(($data['memberDrAmount']-$data['memberCrAmount']),2) }}</h3>
                            <p>Pending</p>
                        </div>
                        <a href="secertry/member-fee-pending" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <h4>Bank Account Transaction Details</h4>
            <table class="table table-border" id="ledgerTable">
                <thead>
                    <tr>
                        <th>M Id</th>
                        <th>Voucher Date</th>
                        <th>Type</th>
                        <th>Narration</th>
                        <th>Dimension</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($bankData['data'] as $item)
                        <tr>
                            <td>{{ $item['m_id'] }}</td>
                            <td>{{ $item['voucher_date'] }}</td>
                            <td>{{ $item['voucher_type'] }}</td>
                            <td>{{ $item['narration'] }}</td>
                            <td>{{ $item['dim_name'] }}</td>
                            <td>{{ $item['debit'] }}</td>
                            <td>{{ $item['credit'] }}</td>
                            <td><b>{{ $item['balance'] }}</b></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- <div class="row">
                <div class="small-box bg-warning  w-25 p-3  border border-primary">
                    <div class="inner">
                        @php
                        $count_employees=DB::table('members_record')->where('category','bedminton')->get();
                        @endphp
                        <h3>{{ $count_employees->count() }}</h3>

                        <p>bedminton</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{route('bedmintonmembers')}}" class="small-box-footer">More info <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
                <div class="p-4 mb-2  w-25 p-3  bg-danger text-white">
                    <div class="inner">
                        @php
                        $count_employees=DB::table('members_record')->where('category','old member')->get();
                        @endphp
                        <h3>{{ $count_employees->count() }}</h3>
                        <p>Old Member</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{route('oldmember')}}" class="small-box-footer">More info <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div> --}}
        </div>



</div>
</div>




</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>


@endsection
