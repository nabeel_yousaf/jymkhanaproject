@extends('backEnd.layouts.master')
@section('title','All Posts')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Vouchers</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/admin')}}">Home</a></li>
              <li class="breadcrumb-item active">All Vouchers</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vouchers Data</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Voucher ID</th>
                  <th>Date</th>
                  <th>Type</th>
                  <th>Debit</th>
                  <th>Credit</th>
                  <th>View</th>
                </tr>
                </thead>
                <tbody>
                @foreach($voucher as $voucher)
                
                <tr>
                  <td class="id">{{$voucher->id}}</td>
                  <td>{{$voucher->voucher_date}}</td>
                  <td>{{$voucher->voucher_type}}</td>
                  <td>{{$voucher->debit}}</td>
                  <td>{{$voucher->credit}}</td>
                  <td>
                      <a href="{{route('voucher.edit',$voucher->id)}}" class="btn btn-primary btn-rounded"><i class="fa fa-edit"></i></a>
                      <a href="{{route('voucher.delete',$voucher->id)}}" class="btn btn-danger btn-rounded"><i class="fa fa-trash"></i></a>
                      <a href="#" id="viewDetails" class="btn btn-success btn-rounded"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
                @endforeach
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Voucher Details</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" id="voucher_master">
                  <thead>
                    <tr>
                      <th>Voucher Date</th>
                      <th>Voucher Type</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                <table class="table table-bordered table-striped" id="voucher_detail">
                  <thead>
                    <tr>
                      <th>Dimension</th>
                      <th>Narration</th>
                      <th>Debit</th>
                      <th>Credit</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->


    <script type="text/javascript">
        @if (Session::has('success'))

        toastr.success("{{Session::get('success')}}");
        @endif
    </script>
    <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
    <script>
            
        $(document).ready(function () {
          $('#example1').on('click', '#viewDetails', function () {
            $("#voucher_detail > tbody").closest('tr').remove();
            
            $('#voucher_detail > tbody').empty();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $(this).closest('tr').find('td.id').text();;
            $('#ajaxModel').modal('show');
            $("#voucher_master").closest('tr').remove();
            $("#voucher_detail > tbody").closest('tr').remove();
            $.get(id +'/view', function (data) {
                $("#voucher_master > tbody").html("<tr><td>"+ data.voucher_date +"</td><td>"+ data.voucher_type +"</td></tr>");
                for(var i=0;i<data.length;i++){
                  $("#voucher_master > tbody").html("<tr><td>"+ data[i].voucher_date +"</td><td>"+ data[i].voucher_type +"</td></tr>");
                $("#voucher_detail > tbody").append("<tr class='tr'><td>"+ data[i].dim_id +"</td><td>"+ data[i].narration +"</td><td>"+ data[i].debit +"</td><td>"+ data[i].credit +"</td></tr>");
                }
            })

          });
        });
    </script>
@endsection
