@extends('backEnd.layouts.master')
@section('title','Add Members Record')
@section('styles')
  <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-colorpicker.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/select2-bootstrap4.min.css')}}">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-duallistbox.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Construction Reports</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Create Construction Reports</li>
            </ol>
          </div>
          <div class="col-sm-12">
            @if(Session::has('Success'))
              <p class="alert alert-success">{{ Session::get('Success') }}</p>
            @endif
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Construction Reports</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <form action="{{route('construction-reports.store')}}" method="post">
              @csrf
            <div class="card-body">

              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                  <label for="bill_num">Bill Number</label>
                  <div class="input-group">
                    <input type="text" id="bill_num" name="bill_num" value="" class="form-control">
                  </div>
                  <span class="text-danger">{{$errors->first('qty')}}</span>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                  <label for="bill_num">Vendor</label>
                  <div class="input-group">
                    <input type="text" id="Vendor" name="Vendor" value="" class="form-control">
                  </div>
                  <span class="text-danger">{{$errors->first('Vendor')}}</span>
                  </div>
                </div>
                  <div class="col-sm-4">
              <div class="form-group">
                <label for="customer_id">Select Material</label>
                <select name="material" class="form-control">
                    <option value="" selected disabled>Select Option</option>
                      @foreach($constructionMaterial as $construction)
                      <option value="{{$construction->id}}">{{$construction->mat_name}}</option>
                      @endforeach
                </select>
                <span class="text-danger">{{$errors->first('material')}}</span>
              </div>
            </div>
                  <div class="col-sm-4">
              <div class="form-group">
                <label for="month">Qty</label>
                <div class="input-group">

                    <input type="text" id="qty" name="qty" value="" class="form-control">
                  </div>
                  <!-- /.input group -->
                <span class="text-danger">{{$errors->first('qty')}}</span>
              </div></div></div>
              <div class="row">
                  <div class="col-sm-3">
              <div class="form-group">
                <label for="unit_price">Unit Price</label>
              <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">PKR</span>
                  </div>
                  <input type="text" id="unit_price" name="unit_price" value="" class="form-control">
                  <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                  </div>

                </div>
                <span class="text-danger">{{$errors->first('unit_price')}}</span>
              </div></div>

              <div class="col-sm-3">
              <div class="form-group">
                <label for="grand_total">Grand Total</label>
              <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">PKR</span>
                  </div>
                  <input type="text" id="grand_total" name="grand_total" value="" class="form-control" readonly>
                  <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                  </div>

                </div>
                <span class="text-danger">{{$errors->first('credit')}}</span>
              </div></div>

              <div class="col-sm-3">
              <div class="form-group">
                <label for="payment">Payments</label>
              <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">PKR</span>
                  </div>
                  <input type="text" id="payments" name="payment" value="" class="form-control">
                  <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                  </div>

                </div>
                 <span class="text-danger">{{$errors->first('payment')}}</span>
              </div></div>

              <div class="col-sm-3">
              <div class="form-group">
                <label for="credit">Credit</label>
              <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">PKR</span>
                  </div>
                  <input type="text" id="credits" value="" name="credit" class="form-control" readonly>
                  <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                  </div>

                </div>
                <span class="text-danger">{{$errors->first('credit')}}</span>
              </div></div>
              </div>

              <div class="row">
                  <div class="col-sm-6">
              <div class="form-group">
                <label for="labour">Labour</label>
              <div class="input-group">

                  <input type="text" name="labour" class="form-control">

                </div>
                <span class="text-danger">{{$errors->first('labour')}}</span>
              </div></div>
              <div class="col-sm-6">
              <div class="form-group">
                <label for="inputStatus"> No. Of Labour</label>
              <div class="input-group">
                  <input type="text" name="no_labour" class="form-control">


                </div>
                 <span class="text-danger">{{$errors->first('no_labour')}}</span>
              </div></div>
              </div>

              <div class="row">
                  <div class="col-sm-6">
              <div class="form-group">
                <label for="labour_payments">Labour Payments</label>
              <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">PKR</span>
                  </div>
                  <input type="text" name="labour_payments" class="form-control">
                  <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                  </div>

                </div>
                <span class="text-danger">{{$errors->first('labour_payments')}}</span>
              </div></div>
              <div class="col-sm-6">
              <div class="form-group">
                <label for="inputStatus">Skills</label>
              <div class="input-group">
                  <input type="text" name="skills" class="form-control">

                </div>
                 <span class="text-danger">{{$errors->first('skills')}}</span>
              </div></div>
              </div>
              <div class="form-group">
                <input type="submit" value="Save Record" class="btn btn-success float-right">
              </div>
            </div>
            </form>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('jsblock')

  <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
$(document).ready(function(){
    $('#qty').keyup(calculate);
    $('#unit_price').keyup(calculate);
    $('#payments').keyup(calculate);
});
function calculate(e)
{
    $('#grand_total').val($('#qty').val() * $('#unit_price').val());
    $('#credits').val($('#grand_total').val() - $('#payments').val());
}
</script>
@endsection
