@extends('backEnd.layouts.master')
@section('title','Edit Category')
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-md-12 grid-margin">
          <div class="card bg-white">
            <div class="card-body d-flex align-items-center justify-content-between">
              <h4 class="mt-1 mb-1">Update Category!</h4>
              <a class="btn btn-info d-none d-md-block" href="">View Brands</a>
            </div>
          </div>
      </div>
    </div>
    <div class="col-12 grid-margin">
            <div class="card">
              <div class="card-body">
                <form class="form-sample"  method="post" action="{{route('brands.update',$edit_brands->id)}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        {{method_field("PUT")}}
                  <div class="row">
                    <div class="col-md-7">
                            <div class="form-group {{$errors->has('name')?' has-error':''}}">
                        <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{$edit_brands->name}}" required placeholder="Name">
                <span class="text-danger" style="color: red;">{{$errors->first('name')}}</span>
                <p class="small" style="font-size: 11px;">{{config('app.url')}}/<span>{{$edit_brands->slug}}</span></p>
                <span class="text-danger" id="chCategory_name" style="color: red;font-size:11px;">{{$errors->first('name')}}</span>
                <input type="hidden" name="slug" id="slug" value="{{$edit_brands->slug}}">
                            </div>
                    </div>
                  </div>

    <div class="row">
    <div class="col-md-6">
            <div class="form-group">
@if($edit_brands->image!='')
<div class="card">
        <div class="card-body">
            <input type="file" name="image" class="dropify" data-default-file="{{url('brands',$edit_brands->image)}}" />
            <a href="javascript:" rel="{{$edit_brands->id}}" rel1="delete_image" class="btn btn-danger btn-sm deleteRecord">Remove</a>
        </div>
        </div>
        @else

        <div class="card">
            <div class="card-body">
                <input type="file" name="image" class="dropify" data-default-file="{{url('brands/noimage.jpg')}}" />

            </div>
            </div>
                @endif

        </div>
            </div>
    </div>

                  <div class="row form-group">
                <div class="col-lg-12">

                <h4 class="card-title">Description</h4>
            <textarea name="description" id="editor">{{$edit_brands->description}}</textarea>

                </div>
                  </div>
                  <div class="row form-group">
                        <div class="col-sm-9">
                  <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" name="status" id="status" value="1" {{($edit_brands->status==0)?'':'checked'}}>
                      Enable
                    <i class="input-helper"></i></label>
                  </div>
                  </div></div>
                  <button type="submit" class="btn btn-primary mr-2">Update</button>

                </form>
              </div>
            </div>
          </div>
  </div>
@endsection
@section('jsblock')

<script>
    $(function(){
        $('#name').on('keyup', function(){
            var url = slugify($(this).val());
            $('#url').html(url);
            $('#slug').val(url);
        });
    })
    </script>


<script>
    ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .then( editor => {
                    console.log( editor );
            } )
            .catch( error => {
                    console.error( error );
            } );
</script>
<script>
        $(".deleteRecord").click(function () {
            var id=$(this).attr('rel');
            var deleteFunction=$(this).attr('rel1');
            swal({
                title:'Are you sure?',
                text:"You won't be able to revert this!",
                type:'warning',
                showCancelButton:true,
                confirmButtonColor:'#3085d6',
                cancelButtonColor:'#d33',
                confirmButtonText:'Yes, delete it!',
                cancelButtonText:'No, cancel!',
                confirmButtonClass:'btn btn-success',
                cancelButtonClass:'btn btn-danger',
                buttonsStyling:false,
                reverseButtons:true
            },function () {
                window.location.href="/admin/"+deleteFunction+"/"+id;
            });
        });
    </script>
     <script src="{{asset('js/toastr.min.js')}}"></script>
     <script type="text/javascript">
         @if (Session::has('success'))

         toastr.success("{{Session::get('success')}}");
         @endif
     </script>
@endsection
