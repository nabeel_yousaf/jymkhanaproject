@extends('backEnd.layouts.master')
@section('title','User Info')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{url('images/members/medium',$byCustomer->image)}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{($byCustomer->name)}}</h3>

                <p class="text-muted text-center">Members</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Status</b> 
                    @php
                    if ($byCustomer->status==0)
                    {
                      echo("<a class='float-right'>Unactive</a>");
                    }
                    else
                    {
                      echo("<a class='float-right'>Active</a>");
                    }
                    @endphp
                  </li>
                  <li class="list-group-item">
                    <b>City</b> <a class="float-right">{{($byCustomer->city)}}</a>
                  </li>
                </ul>
<form action="{{route('customer-update',$byCustomer->id)}}" method="post">
  @csrf
                    @method('PUT')
  <input type="hidden" name="id" value="{{$byCustomer->id}}">
                <button type="submit" class="btn btn-primary btn-block @php
                    if ($byCustomer->status==0)
                    {
                      echo("");
                    }
                    else
                    {
                      echo("disabled");
                    }
                    @endphp "><b>Update Status</b></button>
                    </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><h2>Member Information</h2></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    
                    <div class="post">
 <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Email</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->email)}}</p>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>CNIC NO.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->cnic)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Address.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->address)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Phone No.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->mobile)}}</p>
                        </div>
                      </div>
                    </div>
                    <!-- /.post -->
                  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection
@section('jsblock')
  
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>
@endsection
