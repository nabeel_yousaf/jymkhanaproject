@extends('backEnd.layouts.master')
@section('title','Add Post')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Users Post</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/admin')}}">Home</a></li>
              <li class="breadcrumb-item active">Create New Post</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Create New Posts</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <form action="{{route('employee-post.store')}}" method="post">
              @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="name">Post Title</label>
                <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}">
                <span class="text-danger">{{$errors->first('title')}}</span>
              </div>

               <div class="form-group">
                <label for="name">Post Descriptions</label>
               <div class="mb-3">
                <textarea class="textarea" name="description" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$errors->first('description')}}</textarea>
              </div>
                <span class="text-danger">{{$errors->first('description')}}</span>
              </div>
              <div class="form-group">
                <input type="submit" value="Create new Post" class="btn btn-success float-right">
              </div>
            </div>
            </form>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('jsblock')

<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
<!-- overlayScrollbars -->

 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>

@endsection
