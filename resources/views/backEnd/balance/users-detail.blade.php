@extends('backEnd.layouts.master')
@section('title','Dashboard')
@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/admin')}}">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{url('images/medium',$byUser->image)}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{($byUser->name)}}</h3>

                <p class="text-muted text-center">Customer</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Status</b> 
                    @php
                    if ($byUser->status==0)
                    {
                      echo("<a class='float-right'>Unactive</a>");
                    }
                    else
                    {
                      echo("<a class='float-right'>Active</a>");
                    }
                    @endphp
                  </li>
                  <li class="list-group-item">
                    <b>City</b> <a class="float-right">{{($byUser->city)}}</a>
                  </li>
                </ul>
<form action="{{route('user-update',$byUser->id)}}" method="post">
  @csrf
                    @method('PUT')
  <input type="hidden" name="id" value="{{$byUser->id}}">
                <button type="submit" class="btn btn-primary btn-block @php
                    if ($byUser->status==0)
                    {
                      echo("");
                    }
                    else
                    {
                      echo("disabled");
                    }
                    @endphp "><b>Update Status</b></button>
                    </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Full Detail</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    
                    <div class="post">
 <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Email</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byUser->email)}}</p>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>CNIC NO.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byUser->cnic)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Address.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byUser->address)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Phone No.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byUser->mobile)}}</p>
                        </div>
                      </div>
                    </div>
                    <!-- /.post -->
                  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection
@section('jsblock')
   
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>
@endsection
