@extends('backEnd.layouts.master')
@section('title','Account setting')

@section('styles')
  <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endsection

@section('content')
    <!--breadcrumbs-->
<div class="content-wrapper">
 
     <ol class="breadcrumb">
          
          <li class="breadcrumb-item active">Setting</li>
        </ol>

    <!--End-breadcrumbs-->
    <div class="container">
             <div class="card card-login mx-auto mt-5">
      <div class="card-body">
            <form  method="post" action="{{url('/admin/update-pwd')}}" name="password_validate" id="password_validate" novalidate="novalidate">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" name="pwd_current" id="pwd_current" class="form-control" placeholder="Current Password" required="required" />
                        <label for="pwd_current">Current Password</label>
                        &nbsp;<span id="chkPwd"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" name="pwd_new" id="pwd_new" class="form-control" placeholder="New password" required="required"/>
                        <label for="pwd_new">New password</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                <input type="password" name="pwdnew_confirm" id="pwdnew_confirm" class="form-control" placeholder="Confirm password" required="required"/>
                <label for="pwdnew_confirm">Confirm password</label>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit" value="Update Password" class="btn btn-block btn-success">
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
@section('jsblock')
      <!-- Bootstrap core JavaScript-->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('backend/jquery-easing/jquery.easing.min.js')}}"></script>




  <script src="../../vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="../../vendors/chart.js/Chart.min.js"></script>
  <script src="../../../../vendors/progressbar.js/progressbar.min.js"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="../../js/off-canvas.js"></script>
  <script src="../../js/hoverable-collapse.js"></script>
  <script src="../../js/template.js"></script>
  <script src="../../js/settings.js"></script>
  <script src="../../js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="../../js/dashboard.js"></script>
@endsection


@section('jsblock2')
 
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->



@endsection


