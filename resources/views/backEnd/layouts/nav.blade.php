  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/admin')}}" class="brand-link">
      <img src="{{asset('images/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Burewala GymKhana</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('images/default.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"></a>
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
             <p>{{ Auth::user()->name }}</p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin-settings')}}" class="nav-link">
                  <p>Settings</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('logout') }}" class="nav-link" onclick="event.document.getElementById('logout-form').submit();">
                  <p>{{ __('Logout') }}</p>
                </a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </li>


            </ul>
          </li>
        </ul>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview menu-open">
            <a href="{{url('/admin')}}" class="nav-link active">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas  fa-address-card"></i>
              <p>
                Member's Record
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
           
              </li>
              <li class="nav-item">
                <a href="{{route('memberspayrecord.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-street-view"></i>
                  <p>View All Members</p>
                </a>
              </li>
              
            </ul>
          </li>

            <li class="nav-item has-treeview">
                <a href="{{route('showChartOfAccount')}}" class="nav-link">
                    <i class="nav-icon fas fa-industry"></i>
                    <p>Chart Of Accounts</p>
                </a>
            </li>
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-street-view"></i>
              <p>
                Employee
                <i class="fas fa-angle-left right"></i>
              </p> 
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('employee-post.create')}}" class="nav-link">
                  <i class="far fa-plus-square nav-icon"></i>
                  <p>Add Post</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('employee-post.index')}}" class="nav-link">
                  <i class="far fa-folder nav-icon"></i>
                  <p>View All Posts</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('employees.create')}}" class="nav-link">
                  <i class="far fa-plus-square nav-icon"></i>
                  <p>Add Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('employees.index')}}" class="nav-link">
                  <i class="far fa-folder nav-icon"></i>
                  <p>View Employees</p>
                </a>
              </li>


            </ul>
          
         <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                Empolyee Pay Record
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('employee-post.create')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Record</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('employee-post.index')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View All Record</p>
                </a>
              </li>
            </ul>
          </li> -->

           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="nav-icon fas fa-hammer"></i><p>Construction Reports<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item"><a href="{{route('construction-reports.create')}}" class="nav-link"><i class="far fa-plus-square nav-icon"></i><p>Add Record</p></a>
              </li>
              <li class="nav-item">
                <a href="{{route('construction-reports.index')}}" class="nav-link"><i class="far fa-folder nav-icon"></i><p>View All Record</p></a>
              </li>

              <li class="nav-item">
                <a href="{{url('bill_Num')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Search By Bill Number</p></a>
              </li>

              <li class="nav-item">
                <a href="{{route('construction-material.index')}}" class="nav-link"><i class="fas fa-toolbox"></i><p>Construction Materials</p></a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="nav-icon fas fa-coffee"></i><p>Hotel Management<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('buying')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Buying</p>
                </a>
                </li>

            <li class="nav-item">
              <a href="/categories" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Menu Categories</p>
              </a>
              </li>
              <li class="nav-item">
                <a href="/products" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Menu Products</p>
                </a>
                </li>
              <li class="nav-item">
                <a href="{{route('buyingItem')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Buying Item</p>
                </a>
                </li>
                <li class="nav-item">
                    <a href="/hotel-sale-report" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Hotel Sale Rerport</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/inventory" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Inventory</p>
                    </a>
                </li>

              {{-- <li class="nav-item">
              <a href="{{route('bill-Add')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Billing</p>
              </a>
              </li>

                <li class="nav-item">
                  <a href="" class="nav-link"><i class="far fa-circle nav-icon"></i><p>View All Record</p></a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link"><i class="far fa-circle nav-icon"></i><p>hotel menu</p></a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Categories Management</p></a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Menu Mangement</p></a>
                </li>
                <li class="nav-item">
                  <a href="" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Order Mangement</p></a>
                </li> --}}
              </ul>
          </li>



        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tree"></i><p>Monthly Bill<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('GenerateBill')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>Generate Bill</p></a>
              </li>

            </ul>
          </li>
<!--myVoucher-->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                Accounting
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('voucher.create')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Entry Voucher form</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('voucher.index')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View All Vouchers</p>
                </a>
              </li>
              <li class="nav-item">
                    <a href="/admin/ledger" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Ledger Report</p>
                    </a>
                  </li>
            <li class="nav-item">
                <a href="/admin/detailed-ledger" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Detailed Ledger Report</p>
                </a>
              </li>


            </ul>

          </li>
          <li class="nav-item has-treeview">
                <a href="{{route('logs')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Logs</p>
                </a>
            </li>





      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
