@extends('backEnd.layouts.master')
@section('title','List Categories')
@section('content')
<div class="content-wrapper">
        <div class="row">
          <div class="col-md-12 grid-margin">
              <div class="card bg-white">
                <div class="card-body d-flex align-items-center justify-content-between">
                  <h4 class="mt-1 mb-1">All Categories!</h4>
                <a href="{{route('category.create')}}" class="btn btn-info d-none d-md-block">Add Category</a>
                </div>
              </div>
          </div>
        </div>
        <div class="col-12 grid-margin">
                <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Data table</h4>
                          <div class="row">
                            <div class="col-12">
                              <div class="table-responsive">
                                    <table id="order-listing" class="table">
                                            <thead>
                                              <tr>
                                                  <th>ID #</th>
                                                  <th>Name</th>
                                                  <th>Main Category</th>
                                                  <th>Slug</th>
                                                  <th>Created At</th>
                                                  <th>Status</th>
                                                  <th>Actions</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                    @foreach($categories as $category)
                                                    <?php
                                                        $parent_cates = DB::table('categories')->select('name')->where('id',$category->parent_id)->get();
                                                    ?>
                                              <tr>
                                                  <td>{{$category->id}}</td>
                                                  <td>{{$category->name}}</td>
                                                  <td>
                                                      @foreach($parent_cates as $parent_cate)
                                                        {{$parent_cate->name}}
                                                    @endforeach</td>
                                                  <td>{{$category->slug}}</td>
                                                  <td>{{$category->created_at->diffForHumans()}}</td>
                                                  <td>
                                                  <label class="badge {{($category->status==0)?'badge-danger badge-pill':'badge-info badge-pill'}}">{{($category->status==0)?' Disabled':'Enable'}}</label>
                                                  </td>
                                                  <td>
                                                        <a href="{{route('category.edit',$category->id)}}" class="btn btn-primary btn-rounded"><i class="mdi mdi-square-edit-outline mx-0"></i></a>
                                                        <a href="javascript:" rel="{{$category->id}}" rel1="delete-category" class="btn btn-danger btn-rounded deleteRecord"><i class="mdi mdi-delete mx-0"></i></a>
                                                  </td>
                                              </tr>
                                              @endforeach
                                            </tbody>
                                          </table>
                        </div>
                      </div>
              </div>
      </div>
@endsection
@section('jsblock')

      <script type="text/javascript">
          @if (Session::has('success'))

          toastr.success("{{Session::get('success')}}");
          @endif
      </script>
    <script>
            $(".deleteRecord").click(function () {
               var id=$(this).attr('rel');
               var deleteFunction=$(this).attr('rel1');
               swal({
                   title:'Are you sure?',
                   text:"You won't be able to revert this!",
                   type:'warning',
                   showCancelButton:true,
                   confirmButtonColor:'#3085d6',
                   cancelButtonColor:'#d33',
                   confirmButtonText:'Yes, delete it!',
                   cancelButtonText:'No, cancel!',
                   confirmButtonClass:'btn btn-success',
                   cancelButtonClass:'btn btn-danger',
                   buttonsStyling:false,
                   reverseButtons:true
               },function () {
                  window.location.href="/admin/"+deleteFunction+"/"+id;
               });
            });
        </script>
@endsection
