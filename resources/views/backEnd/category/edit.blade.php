@extends('backEnd.layouts.master')
@section('title','Edit Category')
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-md-12 grid-margin">
          <div class="card bg-white">
            <div class="card-body d-flex align-items-center justify-content-between">
              <h4 class="mt-1 mb-1">Add Category!</h4>
              <a class="btn btn-info d-none d-md-block" href="{{route('category.index')}}">View Categories</a>
            </div>
          </div>
      </div>
    </div>
    <div class="col-12 grid-margin">
            <div class="card">
              <div class="card-body">
                <form class="form-sample"  method="post" action="{{route('category.update',$edit_category->id)}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        {{method_field("PUT")}}
                  <div class="row">
                    <div class="col-md-7">
                            <div class="form-group {{$errors->has('name')?' has-error':''}}">
                        <label for="name">Name</label>
                        <span class="text-danger" id="chCategory_name" style="color: red;">{{$errors->first('name')}}</span>
                <input type="text" class="form-control" name="name" id="name" value="{{$edit_category->name}}" required placeholder="Name">
                <span class="text-danger" style="color: red;">{{$errors->first('name')}}</span>
                <p class="small" style="font-size: 11px;">{{config('app.url')}}/<span id="url"></span></p>

                <input type="hidden" name="slug" id="slug" value="{{$edit_category->slug}}">
                            </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                          <label for="title">Main Category</label>

                          <select class="form-control" name="parent_id" id="parent_id">
                            {{--@foreach($cate_levels as $key=>$value)
                                <option value="{{$key}}" {{($edit_category->parent_id==$key)?' selected':''}}>{{$value}}</option>
                            @endforeach--}}

                            @foreach($cate_levels as $key=>$value)
                                <option value="{{$key}}"{{($edit_category->parent_id==$key)?' selected':''}}>{{$value}}</option>
                                <?php
                                if($key!=0){
                                    $subCategory=DB::table('categories')->select('id','name')->where('parent_id',$key)->get();
                                    if(count($subCategory)>0){
                                        foreach ($subCategory as $subCate){
                                            echo '<option value="'.$subCate->id.'">&nbsp;&nbsp;--'.$subCate->name.'</option>';
                                        }
                                    }
                                }
                                ?>
                            @endforeach
                        </select>

                        </div>
                      </div>
                  </div>

                  <div class="row form-group">
                <div class="col-lg-12">

                <h4 class="card-title">Description</h4>
            <textarea name="description" id="editor">{{$edit_category->description}}</textarea>

                </div>
                  </div>
                  <div class="row form-group">
                        <div class="col-sm-9">
                  <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" name="status" id="status" value="1" {{($edit_category->status==0)?'':'checked'}}>
                      Enable
                    <i class="input-helper"></i></label>
                  </div>
                  </div></div>
                  <button type="submit" class="btn btn-primary mr-2">Update</button>

                </form>
              </div>
            </div>
          </div>
  </div>
@endsection
@section('jsblock')

<script>
    $(function(){
        $('#name').on('keyup', function(){
            var url = slugify($(this).val());
            $('#url').html(url);
            $('#slug').val(url);
        });
    })
    </script>

<script>
    ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .then( editor => {
                    console.log( editor );
            } )
            .catch( error => {
                    console.error( error );
            } );
</script>
<script type="text/javascript">
    @if (Session::has('success'))

    toastr.success("{{Session::get('success')}}");
    @endif
</script>
@endsection
