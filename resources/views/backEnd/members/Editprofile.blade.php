@extends('backEnd.layouts.master')
@section('title','User Info')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

            <!-- Profile Image -->
           @if(session('success'))
            <div class="alert alert-success">{{session('success')}} </div>
           @endif
            <!-- /.card -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
              <div class="col-md-12">
              <div class="form-group mt-4">
                    <!-- <label for="customFile">Custom File</label> -->



                <h3 class="profile-username text-center">{{($byCustomer->name)}}</h3>
<form  action="{{route('update-user',$byCustomer->membership_id)}}" class="btn btn-primary" method="POST" enctype="multipart/form-data">
@csrf
<div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{url('images/members_image/',$byCustomer->image)}}"
                       alt="User profile picture">

                </div>
            </div>
<div class="custom-file">
<input type="file" value="{{old('image')}}" class="custom-file-input" name="image" id="image">
<label class="custom-file-label" for="customFile">Choose Image</label>

                    </div>
                    <span class="text-danger">{{$errors->first('Image')}}</span>
                  </div>
</div>
               </div>
              <!-- /.card-body -->


<button type="submit" class="btn btn-primary">Update profile</button>


          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><h2>Member Information</h2></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">

                    <div class="post">
 <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Name</strong></p>
                        </div>
                        <div class="col-sm-6">
                         <input type="text" name="name" class="form-control" id="name" value="{{($byCustomer->name)}}"></p></b></h1>
                        </div>
                     </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Father_name</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type="text" name="fname" class="form-control" id="fname" value="{{($byCustomer->father_name)}}"></p></b>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Marital Status</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <select class="form-control" class="form-control" id="maritalstatus"name="maritalstatus">
                         <option value="{{ old('MaritaLStatus')}}">@if($byCustomer->MaritaLStatus) {{$byCustomer->MaritaLStatus}}@else Select Marital Status @endif</option>
                          <option value="single">Single</option>
                          <option value="married">Married</option>
                          <option value="divorced">Divorced</option>
                        </select>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Blood Group</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <select class="form-control" class="form-control" name="blood_group" id="blood_group">
                          <option value="{{old('blood_group')}}" selected>@if($byCustomer->BloodGroup) {{$byCustomer->BloodGroup}}@else Select Blood Group @endif</option>
                          <option value="O+">O Positive</option>
                          <option value="A+">A Positive</option>
                          <option value="B+">B Positive</option>
                          <option value="AB+">AB Positive</option>
                          <option value="O-">O Negative</option>
                          <option value="A-">A Negative</option>
                          <option value="B-">B Negative</option>
                          <option value="AB-">AB Negative</option>
                        </select>
                        <span class="text-danger">{{$errors->first('BloodGroup')}}</span>

                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Email</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="Email" id="Email" class="form-control" value="{{($byCustomer->email)}}"></p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Password</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='password' class="form-control" name="password" id="password"></p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Categories</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="Category" class="form-control" id="Category" value="{{($byCustomer->category)}}">

                        <span class="text-danger">{{$errors->first('category')}}</span>
                        </p></b>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>CNIC NO.</strong></p>
                        </div>
                        <div class="col-sm-6">

                        <input type='text' name="cnic" class="form-control" id="cnic" value="{{($byCustomer->Cnic)}}">

                        <span class="text-danger">{{$errors->first('Cnic')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Permanent Address.</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="address" class="form-control" id="address" value="{{($byCustomer->Address)}}">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Temporary Address.</strong></p>
                        </div>
                        <div class="col-sm-6">

                        <input type='text' name="taddress" class="form-control" id="taddress"  value="{{($byCustomer->TemporaryAddress)}}">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Mobile No.</strong></p>
                        </div>
                        <div class="col-sm-6">

                        <input type='text' name="Cell_no" class="form-control" id="Cell_no" value="{{($byCustomer->cell_no)}}">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Date Of Birth</strong></p>
                        </div>
                        <input type='date' name="dob" class="form-control" id="dob" value="{{($byCustomer->DOB)}}">

                        <span class="text-danger">{{$errors->first('DOB')}}</span>
                        </div>
                      </div>

<div class="row">
                        <div class="col-sm-6">
                          <p><strong>JoiningDate</strong></p>
                        </div>
                        <input type='date' name="dob" class="form-control" id="dob" value="{{($byCustomer->JoiningDate)}}">

                        <span class="text-danger">{{$errors->first('JoiningDate')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                            <p><strong>Remarks</strong></p>
                        </div>
                       <div class="col-sm-3">
                           <select class="form-control" class="form-control" name="remarks" id="remarks">
                               <option value="{{old('remarks')}}" selected>@if($byCustomer->remarks) {{$byCustomer->remarks}}@else Select Remarks @endif</option>
                               <option value="in eligible">In Eligible</option>
                               <option value="eligible">Eligible</option>
                           </select>
                       </div>
                        </div>
                    </div>
                  <br>


                       <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Qualification</strong></p>
                        </div>
                        <div class="col-sm-6">

                        <input type='text' name="qualification" class="form-control" id="qualification" value="{{($byCustomer->Qualification)}}">

                        <span class="text-danger">{{$errors->first('Qualification')}}</span>
                        </div>
                      </div>

<div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">Buisness Information</h3>
              </div>
              <div class="card-body">


              <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Name of Organization</strong></p>
                        </div>
                        <div class="col-sm-6">

                        <input type='text' name="Name_of_Organization" class="form-control" id="Name_of_Organization" value="{{($byCustomer->NameOfOrganization)}}">

                        <span class="text-danger">{{$errors->first('NameOfOrganization')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Designation</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="designation" class="form-control" id='designation' value="{{($byCustomer->Designation)}}">

                        <span class="text-danger">{{$errors->first('Designation')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Phone_Number</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="businessmobile" class="form-control" id="buisnessmobile" value="{{($byCustomer->BuisnessMobile)}}">

                        <span class="text-danger">{{$errors->first('BuisnessMobile')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Buisness Address</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="buisnessadress" class="form-control" id="businessaddress" value="{{($byCustomer->BuisnessAddress)}}">

                        <span class="text-danger">{{$errors->first('BusinessAddress')}}</span>
                        </div>
                      </div>
                      </div>
               <!---Asad--->

<div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">Fees Information</h3>
              </div>
              <div class="card-body">


              <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Registration Fee</strong></p>
                        </div>
                        <div class="col-sm-6">

                        <input type='text' name="registration_fee" class="form-control" id="registration_fee" value="{{($byCustomer->registration_fee)}}">

                        <span class="text-danger">{{$errors->first('registration_fee')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Membership Fee</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="membership_fee" class="form-control" id='membership_fee' value="{{($byCustomer->membership_fee)}}">

                        <span class="text-danger">{{$errors->first('membership_fee')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>monthly Subcription fee</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="monthly_sub_fee" class="form-control" id="monthly_sub_fee" value="{{($byCustomer->monthly_sub_fee)}}">

                        <span class="text-danger">{{$errors->first('monthly_sub_fee')}}</span>
                        </div>
                      </div>

                      </div>



<!--asad--->



<div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">FAMILY INFORMATION</h3>
              </div>
              <div class="card-body">

              <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Relation</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <select class="form-control" name="relation" id="relation">
                         <option value="{{old('Relation')}}">@if($byCustomer->Relation) {{$byCustomer->Relation}} @else Select Relation @endif</option>
                         <option value="Wife">Wife</option>
                          <option value="Son">Son</option>
                          <option value="Daughter">Daughter</option>
                          <option value="Mother">Mother</option>
                          <option value="Father">Father</option>
                          <option value="Other">Other</option>
                        </select>
                        <span class="text-danger">{{$errors->first('Relation')}}</span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Name</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' class="form-control" name="sname" id="sname" value="{{($byCustomer->SName)}}">

                        <span class="text-danger">{{$errors->first('SName')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Cnic</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='text' name="scnic" class="form-control" id="scnic" value="{{($byCustomer->SCnic)}}">

                        <span class="text-danger">{{$errors->first('Scnic')}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Dob</strong></p>
                        </div>
                        <div class="col-sm-6">
                        <input type='date' name="sdob" class="form-control" id="SDob" value="{{($byCustomer->SDob)}}">

                        <span class="text-danger">{{$errors->first('SDob')}}</span>
                        </div>
                      </div>
                    <!-- /.post -->
                  </div>

                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

</form>
@endsection
@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
</script>
@endsection
