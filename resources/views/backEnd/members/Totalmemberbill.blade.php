@extends('backEnd.layouts.master')
@section('title','List Categories')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
{{ csrf_field() }}
<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header"  dir="rtl" style="align:center">
              <h3 class="card-title">Search Hotel  Reports</h3>

              
            </div>
            <form action="{{route('showrecord')}}" method="get">

            <div class="card-body">
                  <div class="col-sm-6">
              <div class="form-group">
                  <label>Report</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"></span>
                    </div>
                    <input type="text" name="searchrerporttext" class="form-control" >
                  </div>  
            </div>
 <div class="form-group">
                 <input type="submit" value="Report" class="btn btn-success float-left">
                </div>
          </div>
            </div>
            </form>
            
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    
      </div>
    </section>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Members Record</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">All Hotel Record</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Hotel Record</h3>
              
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            
              <table id="example1" class="table table-bordered table-striped">
              
                <thead>
                <tr>
               
                  <th>M_id</th>
                  <th>Image</th>
                  <th>name</th>
                  <th>Bill_Amount</th>
                  <th>Discount</th> 
                  <th>credit</th>
                  <th>Paid</th>
                  <th>Bill_no</th>
                 
                  <th>DATE</th>
                </tr>
                </thead>
                <tbody>
               @foreach($record as $customer)
               <tr>
               <td>{{$customer->members_id}}</td>
               <td><img src="{{url('images/members_image/',$customer->image)}}" class="rounded"alt="" width="100"></td>
  
               <td>{{$customer->name}}
                
                  <td>{{$customer->Billamount}}</td>
                  <td>{{$customer->Discount}}</td>
                  <td>{{$customer->Credit}}</td>
                  <td>{{$customer->Paid}}</td>
                  <td>{{$customer->Bill_no}}</td>
                  <td>{{$customer->date}}</td>
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    
    <!-- /.content -->
  </div>
@endsection

@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->


<!-- AdminLTE App -->


    <script type="text/javascript">
        @if (Session::has('success'))

        toastr.success("{{Session::get('success')}}");
        @endif
    </script>
    <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
    <script>
            $(".deleteRecord").click(function () {
               var id=$(this).attr('rel');
               var deleteFunction=$(this).attr('rel1');
               swal({
                   title:'Are you sure?',
                   text:"You won't be able to revert this!",
                   type:'warning',
                   showCancelButton:true,
                   confirmButtonColor:'#3085d6',
                   cancelButtonColor:'#d33',
                   confirmButtonText:'Yes, delete it!',
                   cancelButtonText:'No, cancel!',
                   confirmButtonClass:'btn btn-success',
                   cancelButtonClass:'btn btn-danger',
                   buttonsStyling:false,
                   reverseButtons:true
               },function () {
                  window.location.href="/admin/"+deleteFunction+"/"+id;
               });
            });
        </script>
@endsection
