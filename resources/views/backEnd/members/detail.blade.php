@extends('backEnd.layouts.master')
@section('title','User Info')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{url('images/members_image/',$byCustomer->image)}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{($byCustomer->name)}}</h3>

                <p class="text-muted text-center">Members</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Status</b> 
                    @php
                    if ($byCustomer->status==1)
                    {
                      echo("<a class='float-right'>Unpaid</a>");
                      {{}}
                    }
                    else
                    {
                      echo("<a class='float-right'>paid</a>");
                    }
                    @endphp
                </ul>
<form action="{{route('customer-update',$byCustomer->membership_id)}}" method="post">
  @csrf
                    @method('PUT')
  <input type="hidden" name="membership_id" value="{{$byCustomer->membership_id}}">
                <button type="submit" class="btn btn-primary btn-block @php
                    if ($byCustomer->status==1)
                    {
                      echo("");
                    }
                    else
                    {
                      echo("disabled");
                    }
                    @endphp "><b>Update Status</b></button>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><h2>Member Information</h2></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    
                    <div class="post">
 <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Name</strong></p>
                        </div>
                        <div class="col-sm-6">
                         <b><h1> <p>{{($byCustomer->name)}}</p></b></h1>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Father_name</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->father_name)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Marital Status</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->	MaritaLStatus)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Blood Group</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->	BloodGroup)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Email</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->email)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Categories</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->category)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>JoiningDate</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->JoiningDate)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>CNIC NO.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->Cnic)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Permanent Address.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->Address)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Temporary Address.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->Temporaryaddress)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Mobile No.</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->cell_no)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Date Of Birth</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->DOB)}}</p>
                        </div>
                      </div>
                   
                     
                      
                       <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Qualification</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->Qualification)}}</p>
                        </div>
                      </div>
                                           
<div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">Buisness Information</h3>
              </div>
              <div class="card-body">
              
                 
              <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Name of Organization</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->NameOfOrganization)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Designation</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->Designation)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Phone_Number</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->BuisnessMobile)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Buisness Address</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->Buisnessddress)}}</p>
                        </div>
                      </div>
                      </div>
                      
<div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">FAMILY INFORMATION</h3>
              </div>
              <div class="card-body">
              
              <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Relation</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->Relation)}}</p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Name</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->SName)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Cnic</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->SCnic)}}</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <p><strong>Dob</strong></p>
                        </div>
                        <div class="col-sm-6">
                          <p>{{($byCustomer->SDob)}}</p>
                        </div>
                      </div>
                    <!-- /.post -->
                  </div>
                  
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection
@section('jsblock')
  
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>
@endsection

