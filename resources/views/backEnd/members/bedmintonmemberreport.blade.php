<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Gym Khana</title>
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


  </head>
<body>


  <div class="content" id="app">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Bedminton  Members Reports</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> GymKhana Club Burewala.
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">
                  From
                  <address>
                    <strong>{{ Auth::user()->name }}</strong><br>
                   Address:<b>Main Multan Road Burewala</b> <br>
                  Name:   Burewala Gymkhana<br>
                    <p>Email:  burewalagymclub.com</p>
                  </address>
                </div>
              </div>
              <!-- /.row -->

              <!-- Table row -->

              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>

                  <th>M_id</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Father Name</th>
                  <th>Cell</th>
                  <th>Reg Fee</th>
                  <th>Reg SlipNo</th>
                  <th>Member Fee</th>
                  <th>Member SlipNo</th>
                  <th>Category</th>
                  <th>Status</th>
                     </tr>
                    </thead>
                    <tbody>



                    @foreach($bedmem as $types)

                    <tr>

                      <td>{{$types->membership_id}}</td>

                      <td><img src="{{url('images/members_image/',$types->image)}}" class="rounded"alt="" width="100"></td>
                      <td>{{$types->name}}</td>
                      <td>{{$types->father_name}}</td>
                      <td>{{$types->cell_no}}</td>
                      <td>{{$types->registration_fee}}</td>
                      <td>{{$types->registration_slipNo}}</td>
                      <td>{{$types->membership_fee}}</td>
                      <td>{{$types->membership_slipNo}}</td>
                      <td>{{$types->category}}</td>
                      <td>{{$types->status}}</td>
                    </tr>
                       @endforeach

                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">

                <!-- /.col -->
                <div class="col-6">


                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                      <tr>
                        <th>Total Members:</th>
                        <td><h3>{{$bedmem->count()}}</td>
                      </tr>

                      <tr>
                        <th>Total Registration Fee:</th>
                        <td><h3>{{$bedmem->sum('registration_fee')}}</td>
                      </tr>
                      <tr>
                        <th>Total Membership Fee Fee:</th>
                        <td><h3>{{$bedmem->sum('membership_fee')}}</td>
                      </tr>
                    </tbody></table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
               <div class="col-12">
               <a href="" @click.prevent="printme" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                  <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                    Payment
                  </button>
                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
</div>
<footer class="main-footer">
    <strong>Copyright &copy; 2019 BitSoft.</strong>
    All rights reserved.
  </footer>
<script src="{{asset('js/app.js')}}"></script>


</body>
</html>
