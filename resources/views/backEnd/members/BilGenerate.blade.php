<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Gym Khana</title>
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


  </head>

<body>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}

table#t01 tr:nth-child(odd) {
 background-color: #fff;
}
tr#01{
  background-color: #eee;
}
th, td {
  padding: 5px;
  text-align: left;
}
</style>

<div class="content" id="app">
  <!-- Content Header (Page header) -->

  <section class="">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <!-- Main content -->
          <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
              <div class="col-12">
                <h4>
                  <i class="fas fa-globe"></i> GymKhana Club Burewala.

                  <p>Multan Road Burewala</p>

                </h4>
              </div>
              <!-- /.col -->
            </div>
            <table style="width:100%">

<br/>
<br/><br/>

            Bank Copy:
  <tr>
    <td rowspan="2"><img src=".\A.jpeg" alt="" border="3" height=100 width=300></img></td>
    <td colspan="2" align="center"><h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Title:Burewala Gym Khana</h1></td>


    <td></td>
  </tr>
  <tr>
    <td   style="width:55%"><b><h3>AC/NO:</h3></b></th>
    <td  style="width:18%"><b> B/O Housing Scheme Burewala 0787</b></td>
    <td style="width:20%" ><h3>{{ date('d-m-Y') }}</h3></th>
  </tr>
  <tr id="01">
    <td style="width:15%" bgcolor="CDC8C6"> <b>Name Of Member:</b></td>
    <td bgcolor="CDC8C6">{{($recod->name)}}</td>
    <td bgcolor="CDC8C6"><b>Members Id</b></td>
    <td bgcolor="CDC8C6"> {{($recod->membership_id)}}</td>
  </tr>
  <tr >
    <td><b>Developement Charge:</b></td>
    <td></td>
    <td rowspan="4" colspan="2"></td>

  </tr>
  <tr >
    <td><b>Sports/Game/Events:</b></td>
    <td></td>


  </tr>
  <tr >
    <td><b>Tea House Bills:</b></td>

    <td><b></b></td>

  </tr>
  <tr >
    <td><b>Subscription:</b></td>
    <td></td>


  </tr>
  <tr >
    <td><b>Outstanding:</b></td>
    <td></td>
    <td><b>Cash/cheque/DD/Insta.No</b></td>
    <td></td>
  </tr>
  <tr >
    <td><b>Others:</b></td>
    <td></td>
    <td><b>Bank Name:</b></td>
    <td></td>
  </tr>
  <tr >
    <td><b>Total Payable:</b></td>
  <td>{{ $pending_balance }}</td>
    <td><b>Branch Name:</b></td>
    <td></td>
  </tr>
  <tr >
    <td  bgcolor="CDC8C6" style="width:20%"><b><h1>Amounts in Words:</h1></b></td>
    <td bgcolor="CDC8C6">{{ $amount_in_words }}</td>
    <td ><h1><b>Amount</h1></b></td>
    <td></td>
  </tr>
</table>
<br/><br/><br/><br/><br/><br/>
<hr width="20%"  align="left"></hr>
<p></p>
<table class="label">
</table>

            <div class="row">

              <!-- /.col -->
              <div class="col-6">


                <div class="table-responsive">

                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
             <div class="col-12">
             <a href="" @click.prevent="printme" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                  Payment
                </button>
                <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                  <i class="fas fa-download"></i> Generate PDF
                </button>
              </div>
            </div>
          </div>
          <!-- /.invoice -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
</div>



<footer class="main-footer">
    <strong>Copyright &copy; 2019 BitSoft.</strong>
    All rights reserved.
  </footer>
<script src="{{asset('js/app.js')}}"></script>


</body>
</html>
