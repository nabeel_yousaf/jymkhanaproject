<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Gym Khana</title>
  <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>
  <div class="content" id="app">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> Members Reports</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> GymKhana Club Burewala.
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <!-- /.row -->

              <!-- Table row -->

              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>

                      <tr> 
                        @if(is_array($select))
                          @if(in_array('membership_id', $select))
                              <td>membership_id</td>                             
                          @endif
                          @if(in_array('image', $select))
                              <td>image</td>                             
                          @endif
                          @if(in_array('name', $select))
                              <td>name</td>                             
                          @endif
                          @if(in_array('father_name', $select))
                              <td>father_name</td>                             
                          @endif
                          @if(in_array('cell_no', $select))
                              <td>cell_no</td>                             
                          @endif
                          @if(in_array('registration_fee', $select))
                              <td>registration_fee</td>                             
                          @endif
                          @if(in_array('membership_fee', $select))
                              <td>membership_fee</td>                             
                          @endif
                          @if(in_array('Cnic', $select))
                              <td>Cnic</td>                             
                          @endif
                          @if(in_array('Address', $select))
                              <td>Address</td>                             
                          @endif
                          @if(in_array('category', $select))
                              <td>category</td>                             
                          @endif
                          @if(in_array('service_charges', $select))
                              <td>service_charges</td>                             
                          @endif
                          @if(in_array('remarks', $select))
                              <td>remarks</td>                             
                          @endif
                          @if(in_array('status', $select))
                              <td>status</td>                             
                          @endif
                        @else
                        <th>M_id</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Cell</th>
                        <th>Reg Fee</th>
                        <th>Membership_fee</th>
                        <th>monthly Fee</th>
                        <th>Cnic</th>
                        <th>Address</th>
                        <th>Category</th>
                        <th>Service Charges</th>
                        <th>Remarks</th>
                        @endif
                      </tr>

                    </thead>
                    <tbody>
                    @if($select)
                        @foreach($type as $types)
                            <tr>
                            @if(isset($types->membership_id))
                            <td>{{$types->membership_id}}</td>
                            @endif
                            @if(isset($types->image))
                                <td><img src="{{url('images/members_image/',$types->image)}}" onerror="this.src='/images/default.png'" class="rounded"alt="" width="100"></td>
                            @endif
                            @if(isset($types->name))
                            <td>{{$types->name}}</td>
                            @endif
                            @if(isset($types->father_name))
                            <td>{{$types->father_name}}</td>
                            @endif
                            @if(isset($types->cell_no))
                            <td>{{$types->cell_no}}</td>
                            @endif
                            @if(isset($types->registration_fee))
                            <td>{{$types->registration_fee}}</td>
                            @endif
                            @if(isset($types->membership_fee))
                            <td>{{isset($types->membership_fee)}}</td>
                            @endif
                            @if(isset($types->Cnic))
                            <td>{{isset($types->Cnic) ? $types->Cnic : 'no Cnic added yet'}}</td>
                            @endif
                            @if(isset($types->Address))
                            <td>{{$types->Address}}</td>
                            @endif
                            @if(isset($types->category))
                            <td>{{$types->category}}</td>
                            @endif
                            @if(isset($types->service_charges))
                            <td>{{$types->service_charges}}</td>
                            @endif
                            @if(isset($types->remarks))
                            <td>{{isset($types->remarks) ? $types->remarks : 'no remarks'}}</td>
                            @endif
                            @if(isset($types->status))
                            <td>
                                @if ($types->status==1)
                                <span class="badge bg-danger">unpaid</span>
                                @else
                                <span class="badge bg-success">paid</span>
                                @endif
                            </td>
                            @endif
                            </tr>
                        @endforeach
                        @else
                            @foreach($type as $types)
                                <tr>
                                <td>{{$types->membership_id}}</td>
                                <td><img src="{{url('images/members_image/',$types->image)}}" onerror="this.src='/images/default.png'" class="rounded"alt="" width="100"></td>
                                <td>{{$types->name}}</td>
                                <td>{{$types->father_name}}</td>
                                <td>{{$types->cell_no}}</td>
                                <td>{{$types->registration_fee}}</td>
                                <td>{{$types->membership_fee}}</td>
                                <td>{{isset($types->Cnic) ? $types->Cnic : 'no Cnic added yet'}}</td>
                                <td>{{$types->Address}}</td>
                                <td>{{$types->category}}</td>
                                <td>{{$types->service_charges}}</td>
                                <td>{{isset($types->remarks) ? $types->remarks : 'no remarks'}}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">

                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Amount Due </p>

                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                      <tr>
                        <th style="width:50%">Total Number of Members</th>
                        <td><h3>{{ $types->count() }}</h3></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Total Membership Fee</th>
                        <td><h3>{{ $allMember->sum('membership_fee') }}</h3></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Total Registration Fee</th>
                        <td><h3>{{ $allMember->sum('registration_fee') }}</h3></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Total Monthly Fee</th>
                        <td><h3>{{ $allMember->sum('monthly_sub_fee') }}</h3></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Total Monthly Fee</th>
                        <td><h3>{{ $allMember->sum('monthly_sub_fee') }}</h3></td>
                      </tr>

                      <tr>
                        <th style="width:50%">Total Unpaid Member</th>
                        <td><h3>{{ $allMember->where('status',0)->count('monthly_sub_fee') }}</h3></td>
                      </tr>

                      <tr>
                        <th style="width:50%">Total Paid member</th>
                        <td><h3>{{ $allMember->where('status',1)->count('monthly_sub_fee') }}</h3></td>
                      </tr>

                    </tbody></table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
               <div class="col-12">
               <a href="" @click.prevent="printme" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                  <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                    Payment
                  </button>
                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
</div>
<footer class="main-footer">
    <strong>Copyright &copy; 2019 BitSoft.</strong>
    All rights reserved.
  </footer>
<script src="{{asset('js/app.js')}}"></script>


</body>
</html>
