<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Gym Khana</title>
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
    <! <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
        <!-- summernote -->
        <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
        <!-- Plugin css for this page -->
        <!-- endinject -->
        <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


</head>

<body>


    <div class="content" id="app">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Ledger Report</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <!-- Main content -->
                        <div class="invoice p-3 mb-3">
                            <!-- title row -->
                            <div class="row">
                                <div class="col-12">
                                    <h4>
                                        <i class="fas fa-globe"></i> GymKhana Club Burewala. Please Click Ctrl + P to print the report
                                    </h4>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- Table row -->

                            <div class="row">
                                <div class="col-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                    <th>M Id</th>
                                                    <th>Voucher Date</th>
                                                    <th>Type</th>
                                                    <th>Narration</th>
                                                    <th>Dimension</th>
                                                    <th>Debit</th>
                                                    <th>Credit</th>
                                                    <th>Balance</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($records as $item)
                                                    <tr>
                                                        <td>{{ $item['m_id']}}</td>
                                                        <td>{{ $item['voucher_date'] }}</td>
                                                        <td>{{ $item['voucher_type'] }}</td>
                                                        <td>{{ $item['narration'] }}</td>
                                                        <td>{{ $item['dim_name'] }}</td>
                                                        <td>{{ $item['debit'] }}</td>
                                                        <td>{{ $item['credit'] }}</td>
                                                        <td>{{ $item['balance'] }}</td>
                                                    </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

              <div class="row">
<!-- /.col -->
       
                
                <!-- /.col -->
              </div>
              <!-- /.row -->

                            <!-- /.row -->
                        </div>
                        <!-- /.invoice -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
    </div>
    <footer style="text-align:center">
        <strong>Copyright &copy; 2019 BitSoftSol.</strong>
        All rights reserved.
    </footer>
    <script src="{{asset('js/app.js')}}"></script>


</body>

</html>
