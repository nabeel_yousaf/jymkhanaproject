@extends('backEnd.layouts.master')
@section('title','Add Members Record')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="{{asset('css/bootstrap-colorpicker.min.css')}}">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('css/select2-bootstrap4.min.css')}}">
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{asset('css/bootstrap-duallistbox.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
<link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit buying</h1> 

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add hotel Buying Products</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Buying Products</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="update_record_form" method="post" action="{{ route('update.buying', ['hotel'=> $hotel->id]) }}">
                                @csrf   
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="buy_date">Date</label>
                                        <input type="date" required name="buy_date" id="buy_date" value="{{ Carbon\Carbon::parse($hotel->buy_date)->format('Y-m-d') }}" class="form-control">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="bill_no">Bill No</label>
                                        <input type="text" required name="bill_no" id="bill_no" value="{{ $hotel->bill_no }}" class="form-control">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="vendor">Vendor</label>
                                        <input type="text" name="vendor" required id="vendor" value="{{ $hotel->vendor }}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="month">Item</label>
                                        <select class="form-control" required name='hotel_buying_id' id="hotel_buying_id">
                                            @foreach ($items as $item)
                                                <option value="{{ $item->id }}" {{ $item->id == $hotel->hotel_buying_id ? 'selected' : '' }} data-name="{{ $item->item_name  }}">{{ $item->item_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="month">Quantity</label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text"></span>
                                            </div>
                                            <input type="text" required name="quantity" id="quantity" value="{{ $hotel->quantity }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="month">Unit</label>
                                        <select class="form-control" name='Unit' id="unit">
                                            <option value="{{old('Unit')}}">Select unit</option>
                                            <option value="Grams" {{ $hotel->Unit == 'Grams'? 'selected' : '' }}>Gram</option>
                                            <option value="Number" {{ $hotel->Unit == 'Number'? 'selected' : '' }}>Number</option>                                        
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="unit_price">Total Bill Amount</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">PKR</span>
                                            </div>
                                            <input type="text" name="total" id="total" value="{{ $hotel->total }}" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="credit">Credit</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">PKR</span>
                                            </div>
                                            <input type="text" name="credit" id="credit" value="{{ $hotel->credit }}" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="payment">Payments</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">PKR</span>
                                            </div>
                                            <input type="text" name="payment" id="payment" value="{{ $hotel->payment }}" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->first('payment')}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="Kitchen">Kitchen</label>
                                        <div class="input-group">                                                    
                                            <input type="text" name="Kitchen" id="kitchen" value="{{ $hotel->Kitchen }}" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text">Grams</span>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->first('Kitchen')}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="month">Type</label>
                                        <div class="input-group">
                                        <select class="form-control" name="type" id="bill_type">
                                            <option value="{{old('type')}}">Select Type</option>
                                            <option value="Assets" {{ $hotel->type == 'Assets'? 'selected' : '' }}>Assets</option>
                                            <option value="Consumable" {{ $hotel->type == 'Consumable'? 'selected' : '' }}>Consumable</option>
                                        </select>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Type</span>
                                        </div>
                                        </div>
                                        <span class="text-danger">{{$errors->first('type')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Update Record" id="add_record" class="btn btn-success float-right">
                            </div>
                        </form>    
                    </div>
                    
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        </div>
        
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

 
@endsection
@section('jsblock')

<script type="text/javascript">
    @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
</script>
<script>
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  
  
   

  })
</script>
@endsection
