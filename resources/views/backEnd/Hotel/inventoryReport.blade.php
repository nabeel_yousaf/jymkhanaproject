<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Gym Khana</title>
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


  </head>
<body>


  <div class="content" id="app">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>hotel Reports</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> GymKhana Club Burewala.
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">
                  From
                  <address>
                    <strong>{{ Auth::user()->name }}</strong><br>
                   Address:<b>Main Multan Road Burewala</b> <br>
                  Name:   Burewala Gymkhana<br>
                    <p>Email:  burewalagymclub.com</p>
                  </address>
                </div>
              </div>
              <!-- /.row -->

              <!-- Table row -->

              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Vendor</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Total Amount</th>                   
                            <th>Type</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>


                    <?php
                        $totalSale = 0;
                    ?>
                    @if(count($reportsDatahotel) > 0)
                      @foreach($reportsDatahotel as $types)
                        <?php
                        $to1=(float)$types->quantity - (float)$types->Kitchen;
                        $unitprice = (float)$types->hotelBuying->total /(float)$types->hotelBuying->quantity;
                        $tot = $unitprice * $types->used_item ;
                        $totalSale += $tot;
                        ?>
                        <tr>
                          <td>{{$types->id}}</td>
                          <td>{{$types->hotelBuying->vendor}}</td>
                          <td>{{$types->hotelBuying->hotel_buying_items->item_name}}</td>
                          <td>{{$types->used_item}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$types->Unit}}</td>
                          <td>{{ $unitprice }}</td>  
                          <td>{{$tot}}</td>
 
                          <td>{{$types->hotelBuying->type}}</td>
                          <td>{{$types->created_at}}</td>
                        </tr>
                         
                      @endforeach
                    @endif   
                   
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              
            <div class="row">
                <div class="col-6">
                    <p class="lead">Total Sale</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th style="width: 50%;">Total :</th> 
                                    <td><h3>{{ $totalSale }}</h3></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
               
<!-- /.invoice -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
               <div class="col-12">
               <a href="" @click.prevent="printme" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
</div>
<script src="{{asset('js/app.js')}}"></script>


</body>
</html>
