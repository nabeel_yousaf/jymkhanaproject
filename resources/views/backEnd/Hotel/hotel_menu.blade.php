@extends('backEnd.layouts.master')
@section('title','List Categories')
@section('styles')
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  <!-- iCheck -->
  <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" id="bootstrap-css">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('css/summernote-bs4.css')}}">
  <!-- Plugin css for this page -->
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Menu Record</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">All Menu Record</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Hotel Menu</h3>
              <form action="" method="get" class="btn btn" width="70"   >
              <button class="btn btn-primary" style='width:100px;margin:0 50%;position:relative;left:1050px;'>Print</button>
             </form>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            
              <table id="example1" class="table table-bordered table-striped">
              
                <thead>
                <tr>
                 
                  <th>Menu Name</th>
                  <th>Category</th>
                  <th>Price</th> 
                  <th>Weight</th>
                  
            
                 
                </tr>
                </thead>
                <tbody>
               @foreach($hotelmenu as $menu)
               <tr>
                  <td>{{$menu->menu_name}}</td>
                  <td>{{$menu->name}}</td>
                  <td>{{$menu->price}}</td>
                  <td>{{menu->Weight}}</td>
                
                  <td>
                      <a href="javascript:" rel="" rel1="delete-user" class="btn btn-danger btn-sm deleteRecord"><i class="fa fa-trash"></i></a>
                      <a href="{{route('customers-detail',$customer->membership_id)}}"  class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                  <a href="{{route('edit-profile',$customer->membership_id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit">Edit</i></a> 
               
                  
                  </td>
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    
    <!-- /.content -->
  </div>
@endsection
@section('jsblock')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

    <script type="text/javascript">
        @if (Session::has('success'))

        toastr.success("{{Session::get('success')}}");
        @endif
    </script>
    <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
    <script>
            $(".deleteRecord").click(function () {
               var id=$(this).attr('rel');
               var deleteFunction=$(this).attr('rel1');
               swal({
                   title:'Are you sure?',
                   text:"You won't be able to revert this!",
                   type:'warning',
                   showCancelButton:true,
                   confirmButtonColor:'#3085d6',
                   cancelButtonColor:'#d33',
                   confirmButtonText:'Yes, delete it!',
                   cancelButtonText:'No, cancel!',
                   confirmButtonClass:'btn btn-success',
                   cancelButtonClass:'btn btn-danger',
                   buttonsStyling:false,
                   reverseButtons:true
               },function () {
                  window.location.href="/admin/"+deleteFunction+"/"+id;
               });
            });
        </script>
@endsection
7y4