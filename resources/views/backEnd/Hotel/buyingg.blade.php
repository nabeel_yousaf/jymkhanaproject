@extends('backEnd.layouts.master')
@section('title','List Categories')
@section('styles')
 <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-colorpicker.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/select2-bootstrap4.min.css')}}">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-duallistbox.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" id="bootstrap-css">
  
@endsection
<style>
    #error_data{
      display:none;
    }
  </style>
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Hotel buying Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"> Hotel Buying Reports</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
   

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Buying  Reports</h3>
            </div>

            <form method="get" id="reportForm" action="{{route('searchhotelbuyingreport')}}" target="_blank" style="display: none;">
                @csrf()
                <input type="hidden" name="item" id="item-name">
                <input type="hidden" name="startDate" id="start-date">
                <input type="hidden" name="endDate" id="end-date">
            </form>

            <div class="col-md-12">
              <div class="row">
                <div class="col-md-3">
                    <label>Select Chart Items</label>
                    <div class="form-group">
                        <select class="form-control selectAccount select2" name="product" id="product">
                            <option value="0">All</option>
                            @foreach($items as $buying)
                                <option value="{{ $buying['item_name'] }}" > {{ $buying['item_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                        <label>Start Date</label>
                        <input type="date" name="startDate" id="startDate" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="end-date">End Date</label>
                    <input type="date" name="endDate" id="endDate" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Click button to Print Report</label>
                    <button type="button" class="form-control btn btn-success" id="printReport">Print</button>
                </div>
              </div>
                <a class="btn btn-success float-right" href="{{route('addbuying')}}"> Add Bill</a>
            </div>
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <?php
                $tot=0;
                ?>
                <tr>
                  <th>ID</th>
                  <th>Bill No</th>
                  <th>Vendor</th>
                  <th>Item</th>
                  <th>Quantity</th>
                  <th>Unit</th>
                  <th>Total Amount</th>
                  <th>Credit</th>
                  <th>Payment</th>
                  <th>kitchen</th>
                  <th>Remaining</th>
                   <th>Type</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               @foreach($allbuyingreport as $buyre)
               <?php
               $tot=(float)$buyre->quantity - (float)$buyre->Kitchen;
               ?>
               <tr>
                  <td>{{ $buyre->id }}</td>
                  <td>{{ $buyre->bill_no }}</td>
                  <td>{{$buyre->vendor}}</td>
                  <td>{{$buyre->item_name}}</td>
                  <td>{{$buyre->quantity}}</td>
                  <td>{{$buyre->Unit}}</td>
                  <td>{{$buyre->total}}</td>
                  <td>{{$buyre->credit}}</td>
                  <td>{{$buyre->payment}}</td>
                  <td>{{$buyre->Kitchen}}</td>
                  <td>{{$tot}}</td>
                  <td>{{$buyre->type}}</td>
                  <td>{{$buyre->buy_date}}</td>
                  <td> 
                    <a href="{{ url('totalBuying/edit/'.$buyre->id ) }}" data-id="{{$buyre->id}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                    <a href="#" data-id="{{$buyre->id}}" data-rem="{{ $tot }}" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i></a>
                    <button data-id="{{$buyre->id}}" data-rem="{{ $tot }}" class="btn btn-warning btn-sm btn-kitchen">Sell</button>
                  </td>
                </tr>
                @endforeach
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <!-- /.content -->
  </div>


  <!-- Modal for sell kitchen -->
  <div class="modal" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Send To Kitchen</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <div class="alert alert-warning" id="error_data" role="alert">
        </div>
          <div class="form-group">
            <label for="used-kitchen" class="col-form-label">Used in Kitched: (value should be from 0 to <span id="modal_used_kitchen"></span>)</label>
            <input type="number" class="form-control" id="used-kitchen">
            <input type="hidden" class="form-control" id="hotel-buying-id">
          </div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="update-send-kitchen">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal for sell kitched -->
@endsection
@section('jsblock')

 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
  
  <script> 

  var table1;
      $(function () {
      table1 = $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });

    $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = $('#startDate').val();
        var max = $('#endDate').val();
        var vendor = $('#product').val();

        var createdAt = data[12] || 0; // Our date column in the table
        var materialName = data[2]

        console.log(min);  
        console.log(max);  
        console.log(createdAt);  
        console.log('--------------------');  

        if(vendor!=0){
          if(vendor == materialName){
            return true;
          }
          return false;
        }else{

          if (
            (min == "" || max == "") ||
            (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
            ) {
            return true;
            }
            return false;

        }
      }
    );

  </script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    $(document).on('change','#product',function() {
        console.log("click")
        table1.draw();
    });

    $(document).on('change','#endDate',function(){
        let startDate = $('#startDate').val();
        let endDate = $('#endDate').val();
        if(startDate == '') {
            return;
        }
        startDate = new Date(startDate);
        endDate = new Date(endDate);
        if(startDate > endDate) {
            return;
        }
        else {
            $('#product').trigger('change');
        }
    });

    $(document).on('change','#startDate',function(){
        let startDate = $('#startDate').val();
        let endDate = $('#endDate').val();
        if(endDate == '') {
            return;
        }
        startDate = new Date(startDate);
        endDate = new Date(endDate);
        if(startDate > endDate) {
            return;
        }
        else {
            $('#product').trigger('change');
        }
    });

    $(document).on('click','#printReport',function(){
        console.log("click...");
        let item = $('#product').val();
        let startDate = $('#startDate').val();
        let endDate = $('#endDate').val();
        $('#item-name').val(item);
        $('#start-date').val(startDate);
        $('#end-date').val(endDate);
        
        if(startDate == '' && endDate!=''){
          
          return;
          
        }

        if(startDate != '' && endDate==''){
          
          return;
          
        }


        $('#reportForm')[0].submit();


    });

    var kitchenRemaning = 0;

    $(".btn-kitchen").click(function(){
      var _self = $(this);
      console.log(_self.data('id'));
      console.log(_self.data('rem'));
      kitchenRemaning = _self.data('rem');
      $("#used-kitchen").val(0)
      $("#hotel-buying-id").val(_self.data('id'));
      $("#modal_used_kitchen").html(kitchenRemaning)
      let usedKitchen = $("#used-kitchen").val();
      if(kitchenRemaning<usedKitchen || kitchenRemaning == 0){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'no more Item is remaning'
        })

      }else{
        $('#myModal').modal('show')

      } 
    });
 
    $("#used-kitchen").change(function(){
      let usedKitchen = $(this).val();
      
      if(kitchenRemaning<usedKitchen || kitchenRemaning == 0){
        $("#update-send-kitchen").attr('disabled',true);
      }else{
        $("#update-send-kitchen").attr('disabled',false);
      }


    })

    $("#update-send-kitchen").click(function(){

      let update_value = $("#used-kitchen").val();
      let hotel_buying_id = $("#hotel-buying-id").val();

      let data = {
        'used_item': update_value,
        'hotel_buying_id': hotel_buying_id,
        '_token':'<?php echo csrf_token() ?>'
      };

      console.log(data);
      
      $.ajax({
          type:'POST',
          url:'/updateKitchItem',
          data:data,
          success:function(data) {
            location.reload();
          },
          error :function( data ) {
            console.log(data)
            if( data.status === 422 ) {
              var errors = $.parseJSON(data.responseText);
              console.log(errors)
              $("#error_data").fadeIn(500).html(errors.message)

            }
          }
      });

    })

    $(".btn-delete").click(function(e){
      e.preventDefault();
      let hotel = $(this).data('id');

      let data = {
        'hotel': hotel,
        '_token':'<?php echo csrf_token() ?>'
      };
 
      Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            $.ajax({
                type:'POST',
                url:'/totalBuying/delete/'+ hotel,
                data:data,
                success:function(data) {
                  location.reload();
                },
                error :function( data ) {
                  console.log(data)
                  if( data.status === 422 ) {
                    var errors = $.parseJSON(data.responseText);
                    console.log(errors)
                    $("#error_data").fadeIn(500).html(errors.message)

                  }
                }
            });

            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
          }
        })
      

    });


  })
</script>
@endsection
