@extends('frontEnd.layouts.master')
@section('title','Checkout')
@section('slider')
@endsection
@section('content')
<div class="container-fluid">
        {{-- <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Library</a></li>
                <li class="active">Data</li>
            </ol> --}}
</div>
    <div class="container">
        @if(Session::has('message'))
            <div class="alert alert-success text-center" role="alert">
                {{Session::get('message')}}
            </div>
        @endif
        <div class="row">

                <form action="{{url('/submit-checkout')}}" method="post" class="form-horizontal">
                    <div class="col-sm-4 col-sm-offset-1">
                <div class="modal-body modal-body-sub_agile">
                        <div class="modal_body_left modal_body_left1">



                    <div class="login-form"><!--login form-->
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <h3>Billing To</h3>
                        <div class="styled-input {{$errors->has('billing_name')?'has-error':''}}">
                            <input type="text"  name="billing_name" id="billing_name" value="{{$user_login->name}}" placeholder="Billing Name">
                            <span class="text-danger">{{$errors->first('billing_name')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('billing_address')?'has-error':''}}">
                            <input type="text"  value="{{$user_login->address}}" name="billing_address" id="billing_address" placeholder="Billing Address">
                            <span class="text-danger">{{$errors->first('billing_address')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('billing_city')?'has-error':''}}">
                            <input type="text"  name="billing_city" value="{{$user_login->city}}" id="billing_city" placeholder="Billing City">
                            <span class="text-danger">{{$errors->first('billing_city')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('billing_state')?'has-error':''}}">
                            <input type="text"  name="billing_state" value="{{$user_login->state}}" id="billing_state" placeholder=" Billing State">
                            <span class="text-danger">{{$errors->first('billing_state')}}</span>
                        </div>
                        <div class="styled-input">
                            <select name="billing_country" id="billing_country" >
                                @foreach($countries as $country)
                                    <option value="{{$country->country_name}}" {{$user_login->country==$country->country_name?' selected':''}}>{{$country->country_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="styled-input {{$errors->has('billing_pincode')?'has-error':''}}">
                            <input type="text"  name="billing_pincode" value="{{$user_login->pincode}}" id="billing_pincode" placeholder=" Billing Pincode">
                            <span class="text-danger">{{$errors->first('billing_pincode')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('billing_mobile')?'has-error':''}}">
                            <input type="text"  name="billing_mobile" value="{{$user_login->mobile}}" id="billing_mobile" placeholder="Billing Mobile">
                            <span class="text-danger">{{$errors->first('billing_mobile')}}</span>
                        </div>
                        <label>
                                <input type='checkbox'  name="checkme" id="checkme">
                                <span></span>
                                Shipping Address same as Billing Address
                              </label>

                    </div><!--/login form-->
                </div></div></div>
                <div class="col-sm-1">

                </div>
                <div class="col-sm-4">
                        <div class="modal-body modal-body-sub_agile">
                                <div class="modal_body_left modal_body_left1">

                    <div class="signup-form"><!--sign up form-->
                        <h3>Shipping To</h3>
                        <div class="styled-input {{$errors->has('shipping_name')?'has-error':''}}">
                            <input type="text"  name="shipping_name" id="shipping_name" value="" placeholder="Shipping Name">
                            <span class="text-danger">{{$errors->first('shipping_name')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('shipping_address')?'has-error':''}}">
                            <input type="text"  value="" name="shipping_address" id="shipping_address" placeholder="Shipping Address">
                            <span class="text-danger">{{$errors->first('shipping_address')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('shipping_city')?'has-error':''}}">
                            <input type="text"  name="shipping_city" value="" id="shipping_city" placeholder="Shipping City">
                            <span class="text-danger">{{$errors->first('shipping_city')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('shipping_state')?'has-error':''}}">
                            <input type="text"  name="shipping_state" value="" id="shipping_state" placeholder="Shipping State">
                            <span class="text-danger">{{$errors->first('shipping_state')}}</span>
                        </div>
                        <div class="styled-input">
                            <select name="shipping_country" class="country" id="shipping_country" >
                                @foreach($countries as $country)
                                    <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="styled-input {{$errors->has('shipping_pincode')?'has-error':''}}">
                            <input type="text"  name="shipping_pincode" value="" id="shipping_pincode" placeholder="Shipping Pincode">
                            <span class="text-danger">{{$errors->first('shipping_pincode')}}</span>
                        </div>
                        <div class="styled-input {{$errors->has('shipping_mobile')?'has-error':''}}">
                            <input type="text"  name="shipping_mobile" value="" id="shipping_mobile" placeholder="Shipping Mobile">
                            <span class="text-danger">{{$errors->first('shipping_mobile')}}</span>
                        </div>
                        <input type="submit" value="CheckOut" style="float:right">
                    </div><!--/sign up form-->
                </div>
            </div></div>
            </form>
        </div>
    </div>
    <div style="margin-bottom: 20px;"></div>
@endsection
