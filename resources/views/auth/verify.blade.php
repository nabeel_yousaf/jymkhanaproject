@extends('layouts.app')

@section('content')

<div class="banner_bottom_agile_info">
	    <div class="container">
			<div class="agile_ab_w3ls_info">

				 <div class="col-md-12 ab_pic_w3ls_text_info">
                    <h5 class="text-center">Verify Your Email <span>Address</span> </h5>
                    @if (session('resent'))
                        <div class="alert alert-success text-center" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
					<p class="text-center">{{ __('Before proceeding, please check your email for a verification link.') }}
                            {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}"><strong>{{ __('click here to request another') }}</strong></a>.</p>

				</div>
				  <div class="clearfix"></div>
			</div>
		 </div>
    </div>
@endsection
