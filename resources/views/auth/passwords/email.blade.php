@extends('layouts.app')
@section('title', 'Rest Password')
@section('content')
<div class="banner_bottom_agile_info ">
        <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-5 in-gp-tl " style=" position: relative; left: 28%; ">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
        <div class="modal-body modal-body-sub_agile">
                            <div class="modal_body_left modal_body_left1">
                            <h3 class="agileinfo_sign text-center">Reset <span>Password</span></h3>
                                        <form action="{{ route('password.email') }}" method="post">
                                            @csrf
                                <div class="styled-input agile-styled-input-top">
                                    <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter Your Email" required>

                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback text-danger" role="alert" style="width:100%;">
                                        <p>{{ $errors->first('email') }}</p>
                                    </span>
                                @endif
                                </div><br>
                                <input type="submit" value="{{ __('Send Password Reset Link') }}" style="text-align:center;">
                            </form>

                             <div class="clearfix"></div>

                            </div>
        </div>
            </div>

</div>
        </div>
@endsection
