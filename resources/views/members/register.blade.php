<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Burewala GymKhana | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-colorpicker.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/select2-bootstrap4.min.css')}}">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-duallistbox.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">

</head>
<div class="wrapper" background-color: lightblue;>

  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">

          <div class="col-sm-12 text-center">
            <div class="register-logo">
    <a href="../../index2.html"><b>GymKhana Burewala</b></a>
  </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

<form action="{{route('register_customer')}}" method="POST" enctype="multipart/form-data">
    @csrf
        <div class="row">
          <div class="col-md-12">

            <div class="card card-info">
             <div class="card-header">
                <h3 class="card-title">PERSONAL INFORMATION</h3>
              </div>
              <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <label for="full_name">Members ID</label>
                    <input  height: 150px; type="number" class="form-control" name="membership_id" id="membership_id" placeholder="Enter Members Id" value="" value=""  >
                    <span class="text-danger">{{$errors->first('membership_id')}}</span>
                    </div>
                  </div></div>


                <div class="row">
                   <div class="col-md-6">
                <div class="form-group">
                    <label for="full_name">Full Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="{{old('full_name')}}">
                    <span class="text-danger">{{$errors->first('name')}}</span>
                  </div></div>

                   <div class="col-md-6">
                  <div class="form-group">
                    <label for="father_name">Father Name</label>
                    <input type="text" class="form-control" name="father_name" id="father_name" placeholder="Father Name" value="{{old('father_name')}}">
                    <span class="text-danger">{{$errors->first('father_name')}}</span>
                  </div></div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                        <label>Qualification</label>
                        <select class="form-control" name="Qualification" value="{{old('Qualification')}}">
                          <option>Metric</option>
                          <option>Bachalor</option>
                          <option>Master</option>
                        </select>
                        <span class="text-danger">{{$errors->first('Qualification')}}</span>
                      </div>
                </div>
                   <div class="col-md-3">
                  <div class="form-group">
                 <label> Category</label>
                        <input type='text' class="form-control" name='category' id='category' value="{{old('category')}}">

                        <span class="text-danger">{{$errors->first('category')}}</span>
                      </div>
                  </div>
            <div class="col-md-3">
                <div class="form-group">
                  <label>Date Of Birth:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="text" name="DOB" value="{{old('DOB')}}" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                  </div>
                  <span class="text-danger">{{$errors->first('DOB')}}</span>
                </div></div>
                </div>

                <!-- phone mask -->
                <div class="row">
                   <div class="col-md-6">
                <div class="form-group">
                  <label>Mobile</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-mobile"></i></span>
                    </div>
                    <input type="number" value="{{old('cell_no')}}" class="form-control" name="cell_no" data-inputmask="'mask': ['9999-9999[999]', '99 999  99[9]-99']" data-mask="" im-insert="true" value="">
                  </div>
                  <span class="text-danger">{{$errors->first('cell_no')}}</span>
                </div></div>
                 <div class="col-md-6">
                <div class="form-group">
                  <label>Land Line</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                    </div>
                    <input type="text" value="{{old('LanDline')}}" class="form-control" name="buisness_land_line" data-inputmask="'mask': ['9999-9999[99]', '99 999  99[9]-99']" data-mask="" im-insert="true" value="">
                  </div>
                  <span class="text-danger">{{$errors->first('Landline')}}</span>
                </div></div>
                </div>
                <div class="row">
                   <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" name="email" value="{{old('email')}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
                    <span class="text-danger">{{$errors->first('email')}}</span>
                  </div></div>
                   <div class="col-md-6">
                  <div class="form-group">
                    <label for="twitter">Twitter</label>
                    <input type="text" class="form-control" value="{{old('twitter')}}" name="twitter" id="twitter" placeholder="Twitter">
                    <span class="text-danger">{{$errors->first('twitter')}}</span>
                  </div></div>
              </div>
              <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="Cnic">CNIC</label>
                <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-credit-card"></i></span>
                </div>
                <input type="text" value="{{old('Cnic')}}" name="Cnic" class="form-control" value="" data-inputmask="'mask': ['99999-9999999-9', '+099 999  99[9]-99']" data-mask="" im-insert="true">
                  </div>
                <span class="text-danger">{{$errors->first('Cnic')}}</span>
              </div></div>
              <div class="col-md-6">
              <div class="form-group">
                    <label for="customFile">Select Image</label>

                    <div class="custom-file">
                      <input type="file" value="{{old('Image')}}" class="custom-file-input" name="Image" id="customImage">
                      <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                    <span class="text-danger">{{$errors->first('Image')}}</span>
                  </div>
</div></div>
<div class="row">
                     <div class="col-md-6">
                  <div class="form-group">
                        <label>Marital Status</label>
                        <select class="form-control" name="MaritaLStatus">
                         <option value="{{old('MaritaLStatus')}}">Select Marital Status</option>
                          <option value="single">Single</option>
                          <option value="married">Married</option>
                          <option value="divorced">Divorced</option>
                        </select>
                        <span class="text-danger">{{$errors->first('MaritaLStatus')}}</span>
                      </div></div>
                       <div class="col-md-6">
                       <div class="form-group">
                        <label>Blood Group</label>
                        <select class="form-control" name="BloodGroup">
                          <option value="{{old('BloodGroup')}}">Select Blood Group</option>
                          <option value="O+">O Positive</option>
                          <option value="A+">A Positive</option>
                          <option value="B+">B Positive</option>
                          <option value="AB+">AB Positive</option>
                          <option value="AB-">AB Negative</option>
                          <option value="O-">O Negative</option>
                          <option value="A-">A Negative</option>
                          <option value="B-">B Negative</option>
                          <option value="AB-">AB Negative</option>
                        </select>
                        <span class="text-danger">{{$errors->first('BloodGroup')}}</span>
                      </div></div>
                </div>
                <div class="form-group">
                    <label for="temporary_address">Temporary Address</label>
                    <input type="text" value="{{old('TemporaryAddress')}}" name="TemporaryAddress" class="form-control" id="temporary_address" placeholder="Temporary Address">
                    <span class="text-danger">{{$errors->first('TemporaryAddress')}}</span>
                  </div>
                  <div class="form-group">
                    <label for="permanent_address">Permanent Address</label>
                    <input type="text" value="{{old('Address')}}" name="Address" class="form-control" id="permanent_address" placeholder="Permanent Address">
                    <span class="text-danger">{{$errors->first('Address')}}</span>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" value="{{old('password')}}" name="password" class="form-control" id="password" placeholder="Password">
                    <span class="text-danger">{{$errors->first('password')}}</span>
                  </div></div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm Password">
                    <span class="text-danger">{{$errors->first('password_confirmation')}}</span>
                  </div>
                  </div>
                  </div>
              </div>
            </div>


          <div class="col-md-12">

            <div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">BUSINESS INFORMATION</h3>
              </div>
              <div class="card-body">
                <div class="form-group">
                    <label for="name_of_organization">Name Of Organization</label>
                    <input type="text" name="NameOfOrganization" class="form-control" id="NameOfOrganization" value="{{old('name_of_organization')}}" placeholder="Name Of Organization">
                    <span class="text-danger">{{$errors->first('NameOfOrganization')}}</span>
                  </div>
                  <div class="form-group">
                    <label for="business_address">Business Address</label>
                    <input type="text" value="{{old('BuisnessAddress')}}" name="BuisnessAddress" class="form-control" id="business_address" placeholder="Business Address">
                    <span class="text-danger">{{$errors->first('BuisnessAddress')}}</span>
                  </div>
                  <div class="form-group">
                    <label for="designation">Designation</label>
                    <input type="text" value="{{old('Designation')}}" class="form-control" name="Designation"  placeholder="Designation" >
                    <span class="text-danger">{{$errors->first('Designation')}}</span>
                  </div>
           <div class="row">
                   <div class="col-md-6">
                <div class="form-group">
                  <label>Land Line</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-mobile"></i></span>
                    </div>
                    <input type="text" value="{{old('BuisnessMobile')}}" class="form-control" name="BuisnessMobile" data-inputmask="'mask': ['9999-9999[99]', '99 999  99[9]-99']" data-mask="" im-insert="true" value="">
                  </div>
                  <span class="text-danger">{{$errors->first('BuisnessMobile')}}</span>
                </div></div>
                 <div class="col-md-6">
                <div class="form-group">
                  <label>Landline</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-fax"></i></span>
                    </div>
                    <input type="text" value="{{old('blandline')}}" class="form-control" name="blandline" data-inputmask="'mask': ['9999-9999[99]', '99 999  99[9]-99']" data-mask="" im-insert="true" value="">
                  </div>
                  <span class="text-danger">{{$errors->first('blandline')}}</span>
                </div></div>
                </div>
              </div>

            </div>
          </div>



<div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">FAMILY INFORMATION</h3>
              </div>
              <div class="card-body">
              <div class="row" id="familyInfo">
                     <div class="col-md-2">
                  <div class="form-group">
                        <label>Relation</label>
                        <select class="form-control" name="Relation[]">
                         <option value="{{old('Relation[]')}}">Select Relation</option>
                          <option value="Wife">Wife</option>
                          <option value="Son">Son</option>
                          <option value="Daughter">Daughter</option>
                          <option value="Mother">Mother</option>
                          <option value="Father">Father</option>
                          <option value="Other">Others.</option>

                        </select>
                        <span class="text-danger">{{$errors->first('Relation')}}</span>
                      </div></div>

                   <div class="col-md-3">
                 <div class="form-group">
                    <label for="spouse_name">Name</label>
                    <input type="text" value="{{old('SName[]')}}" name="SName[]" class="form-control" id="SName" placeholder="Name">
                    <span class="text-danger">{{$errors->first('SName')}}</span>
                  </div>
                  </div>

                   <div class="col-md-3">
                <div class="form-group">
                  <label>Date Of Birth:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="date" value="{{old('SDob[]')}}" id="SDob" name="SDob[]" class="form-control" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" im-insert="false">
                  </div>
                  <span class="text-danger">{{$errors->first('SDob')}}</span>
                </div></div>
                <div class="col-md-3">
                        <div class="form-group">
                                <label for="exampleInputEmail1">CNIC NO.</label>
                              <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-credit-card"></i></span>
                            </div>
                            <input type="text" value="{{old('SCnic[]')}}" id="SCnic" name="SCnic[]" class="form-control" value="" data-inputmask="'mask': ['99999-9999999-9', '+099 999  99[9]-99']" data-mask="" im-insert="true">
                              </div>
                              <span class="text-danger">{{$errors->first('SCnic')}}</span>
                              </div>
                </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Add More</label>
                                <button type="button" class="btn btn-success addMore">Add</button>
                        </div>
                    </div>
              </div>
              <div id="addInfo">

              </div>
              <div class="card-footer">
                  <button type="submit" class="btn btn-block bg-gradient-success btn-flat">REGISTER</button>                </div>
            </div>
          </div>
          <!-- /.col (right) -->
        </div>
        <!-- /.row -->
    </form>
      </div><!-- /.container-fluid -->
    </section>
    </section>
    <form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">

    <strong>Copyright &copy; 2019 BitSoft</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('js/select2.full.min.js')}}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{asset('js/jquery.bootstrap-duallistbox.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('js/moment.min.js')}}"></script>
<script src="{{asset('js/jquery.inputmask.bundle.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('js/daterangepicker.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('js/bootstrap-colorpicker.min.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Bootstrap Switch -->
<script src="{{asset('js/bootstrap-switch.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('js/demo.js')}}"></script>
<!-- Page script -->
<script src="{{asset('js/toastr.min.js')}}"></script>
 <script type="text/javascript">
        @if (Session::has('success'))
        toastr.success("{{Session::get('success')}}");

        @endif
    </script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    $(document).on('click','.addMore',function(){
        $("#familyInfo").clone().appendTo("#addInfo");
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>

</body>
</html>
