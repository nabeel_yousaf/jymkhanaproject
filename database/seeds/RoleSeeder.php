<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = \DB::table('roles')->get();
        if(count($roles) == 0 || count($roles) == '') {
            \DB::table('roles')->insert([
                'role_name' => 'admin'
            ]);
            \DB::table('roles')->insert([
                'role_name' => 'hotel_manager'
            ]);
        }
    }
}
