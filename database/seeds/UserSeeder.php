<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //admin user
        $admin = \DB::table('users')->where('role_id','=',1)->first();
        if(empty($admin)) {
            \DB::table('users')->insert([
                'name' => 'Faizan Chohan',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('123456789'),
                'admin' => 1,
                'address' => 'New Lahore Road',
                'city' => 'Lahore',
                'cnic' => '',
                'image' => '',
                'pincode' => '',
                'phone' => '03339777676',
                'role_id' => 1,
            ]);
        }
        $manager = \DB::table('users')->where('role_id','=',2)->first();
        if(empty($manager)){
        //hotel manager
            \DB::table('users')->insert([
                'name' => 'Hotel Manager',
                'email' => 'manager@gmail.com',
                'password' => bcrypt('123456789'),
                'admin' => 0,
                'address' => 'New Lahore Road',
                'city' => 'Lahore',
                'cnic' => '',
                'image' => '',
                'pincode' => '',
                'phone' => '',
                'role_id' => 2,
            ]);
        }
    }
}
