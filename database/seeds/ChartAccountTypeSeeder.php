<?php

use Illuminate\Database\Seeder;

class ChartAccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('chart_account_types')->insert([
            'name' => 'Capital'
        ]);
        \DB::table('chart_account_types')->insert([
            'name' => 'Assets'
        ]);
        \DB::table('chart_account_types')->insert([
            'name' => 'Liabilities'
        ]);
        \DB::table('chart_account_types')->insert([
            'name' => 'Expense'
        ]);
        \DB::table('chart_account_types')->insert([
            'name' => 'Revenue'
        ]);
    }
}
