<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHotelBuyingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_buyings', function (Blueprint $table) {
            $table->dropColumn('item');
            $table->unsignedBigInteger('hotel_buying_id')->unsigned();
            $table->foreign('hotel_buying_id')->references('id')->on('hotel_buying_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_buyings');

    }
}
