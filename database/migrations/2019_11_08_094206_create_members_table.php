<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Unique_idM');
            $table->string('full_name');
            $table->string('father_name');
            $table->string('qualification');
            $table->string('dob');
            $table->string('mobile');
            $table->string('land_line');
            $table->string('twitter');
            $table->string('member_cnic');
            $table->string('marital_status');
            $table->string('blood_group');
            $table->string('temporary_address');
            $table->string('permanent_address');
            $table->string('name_of_organization');
            $table->string('designation');
            $table->string('business_address');
            $table->string('business_land_line');
            $table->string('business_fax');
            $table->string('spouse_name');
            $table->string('spouse_dob');
            $table->string('spouse_cnic');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('password_confirmation');
            $table->string('image');
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
