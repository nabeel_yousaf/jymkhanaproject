<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructionReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('material');
            $table->integer('qty');
            $table->float('unit_price');
            $table->float('total');
            $table->float('credit');
            $table->float('payment');
            $table->string('labour');
            $table->integer('no_labour');
            $table->float('labour_payments');
            $table->string('skills');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('construction_reports');
    }
}
