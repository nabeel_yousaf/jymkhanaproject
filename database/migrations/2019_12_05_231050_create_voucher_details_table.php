<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('voucher_master_id')->unsigned();
            $table->foreign('voucher_master_id')->references('id')->on('voucher_masters')->onDelete('cascade');
            $table->unsignedBigInteger('coa_id')->unsigned();
            $table->foreign('coa_id')->references('id')->on('chart_of_accounts')->onDelete('cascade');
            $table->string('narration')->nullable();
            $table->decimal('debit');
            $table->decimal('credit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vocher_details');
    }
}
