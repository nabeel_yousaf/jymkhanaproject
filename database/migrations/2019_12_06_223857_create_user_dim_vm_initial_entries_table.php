<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDimVmInitialEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_dim_vm_initial_entries', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            
            $table->bigInteger('dim_id')->unsigned();
            $table->foreign('dim_id')->references('id')->on('dimensions')->onDelete('cascade');
            
            $table->bigInteger('voucher_master_id')->unsigned();
            $table->foreign('voucher_master_id')->references('id')->on('voucher_masters')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_dim_vm_initial_entries');
    }
}
