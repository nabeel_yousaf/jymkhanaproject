<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_report', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('material');
            $table->integer('qty');
            $table->float('unit_price');
            $table->float('total');
            $table->string('PeymentMethod');
            $table->float('Profit');
            $table->float('Loss');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_report');
    }
}
