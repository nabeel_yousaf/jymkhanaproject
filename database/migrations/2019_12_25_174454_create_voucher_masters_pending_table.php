<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherMastersPendingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_masters_pending', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('voucher_type',['Bank Receipt','Bank Payment','Cash Payment', 'Cash Receipt','Journal Voucher']);
            $table->date('voucher_date');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->enum('status',['approved', 'not approved'])->default('approved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_masters_pending');
    }
}
