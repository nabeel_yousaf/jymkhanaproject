<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartOfAccountDimensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chart_of_account_dimensions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('coa_id')->unsigned();
            $table->bigInteger('dim_id')->unsigned();
            $table->foreign('coa_id')->references('id')->on('chart_of_accounts')->onDelete('cascade');
            $table->foreign('dim_id')->references('id')->on('dimensions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chart_of_account_dimensions');
    }
}
