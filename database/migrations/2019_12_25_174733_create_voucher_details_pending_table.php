<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherDetailsPendingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_details_pending', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('voucher_master_id')->unsigned();
            $table->foreign('voucher_master_id')->references('id')->on('voucher_masters')->onDelete('cascade');
            $table->bigInteger('coa_id')->unsigned();
            $table->foreign('coa_id')->references('id')->on('chart_of_accounts')->onDelete('cascade');
            $table->string('narration')->nullable();
            $table->decimal('debit');
            $table->decimal('credit');
            $table->bigInteger('dim_id')->nullable()->unsigned();
            $table->foreign('dim_id')->references('id')->on('dimensions')->onDelete('cascade');
            $table->enum('status',['approved', 'not approved'])->default('approved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_details_pending');
    }
}
