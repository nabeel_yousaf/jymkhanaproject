<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelBuyingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_buyings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vendor');
            $table->string('item');
            $table->string('quantity');
            $table->decimal('total');
            $table->decimal('credit');
            $table->decimal('payment'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_buyings');
    }
}
