<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsedKitchen extends Model
{
    protected $fillable = ['id','hotel_buying_id','used_item'];

    
    public function hotelBuying()
    {
        return $this->hasOne(HotelBuying::class,'id','hotel_buying_id');
    }
    
}