<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Member extends Authenticatable implements JWTSubject
{
    protected $guard = 'members_record';

    protected $hidden = [
        'password'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

//    public function __construct() {
//        parent::__construct(); // Don't forget this, you'll never know what's being done in the constructor of the parent class you extended
//    }

    protected $table='members_record';
    protected $primaryKey='membership_id';
    protected $fillable = ['id','membership_id','service_charges','remarks','category','name','father_name','MaritaLStatus','membership_fee','JoiningDate','membership_slipNo','registration_slipNo','BloodGroup','registration_fee', 'Qualification','monthly_sub_fee','DOB','cell_no','land_line','twitter','Cnic','Relation','TemporaryAddress','Address','NameOfOrganization','Designation','BuisnessAddress','BuisnessMobile','blandline','Relation','SName','SDob','SCnic','email','password','image'];
    public function memberspayrecord()
   {
       return $this->belongsTo(MembersPayRecord::class);
   }
    public function registered(){
        return view('members_record.register');
    }

    public function orders()
    { 
        return $this->morphOne(OrderMaster::class,'memberable_id','membership_id');
    }
}
