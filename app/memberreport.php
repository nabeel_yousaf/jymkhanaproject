<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class memberreport extends Model
{
    protected $table='members_record';
    protected $primaryKey='id';
    protected $fillable = ['membership_id','name','father_name','cell_no','registration_Fee','registration_slipNo','membership_fee','membership_slipNo','category','status'];
}

