<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    const IS_ADMIN_ROLE = 1;
    const IS_MANAGER_ROLE = 2;
    const IS_MEMBER_ROLE = 3;
    const IS_SECERTRY_ROLE = 4;
    const IS_OPERATOR_ROLE = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','admin','address','city','image','status','cnic','pincode','mobile','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin() {
        if(Auth::user()->role_id == 1) {
            return true;
        }
        else {
            return false;
        }
    }
    public function isManager(){
        if(Auth::user()->role_id == 2) {
            return true;
        }
        else {
            return false;
        }
    }
    public function isMember() {
        if(Auth::user()->role_id == 3) {
            return true;
        }
        else {
            return false;
        }
    }
    public function isSecertry() {
        if(Auth::user()->role_id == 4) {
            return true;
        }
        else {
            return false;
        }
    }
    public function isOperator(){
        if(Auth::user()->role_id == 5) {
            return true;
        }
        else {
            return false;
        }
    }
}
