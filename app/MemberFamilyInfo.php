<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberFamilyInfo extends Model
{
    protected $fillable = ['member_id','cnic','relation','name','dob'];
}
