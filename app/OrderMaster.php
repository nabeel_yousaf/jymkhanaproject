<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderMaster extends Model
{
    protected $fillable = ['memberable_id','memberable_type','date_time'];

    /**
     * Get the owning memberable model.
     */
    public function memberable()
    {
        return $this->morphTo();
    }

    public function order_details()
    {
        return $this->hasMany(OrderDetail::class,'order_master_id','id');
    }

    public static function sale_report($product_id, $draw, $startDate,$endDate , $memberId) {
        $data = [];
        $data['draw'] = $draw;

        $final_data = OrderMaster::with(['memberable:membership_id,name,email','order_details.product']);

        if($product_id == 0) {
            // request with out product id and date filter
            if($startDate != '' && $endDate != '') {
                $final_data->where('date_time', '>=', $startDate)
                           ->where('date_time', '<=', $endDate);
            }
            
        }
        else {
            if($startDate == '' && $endDate == '') {
             
                $final_data->whereHas('order_details', function($q) use ($product_id) {
                                $q->where('product_id', '=', $product_id);
                            }) 
                            ->with(['order_details'=> function($q) use ($product_id) {
                                $q->where('product_id', '=', $product_id)->with('product');
                            }]);

            }
            else {
                
                $final_data ->whereHas('order_details', function($q) use ($product_id) {
                                $q->where('product_id', '=', $product_id);
                            }) 
                            ->with(['order_details'=> function($q) use ($product_id) {
                                $q->where('product_id', '=', $product_id)->with('product');
                            }]) 
                            ->where('date_time', '>=', $startDate)
                            ->where('date_time', '<=', $endDate);
            }
        }

        if($memberId){
            $final_data->where('member_id',$memberId);
        }

        $data['data'] = $final_data->get();
        return $data;
    }
}
