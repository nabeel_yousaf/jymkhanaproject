<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembersPayRecord extends Model
{
    protected $table='members_pay_records';
    protected $primaryKey='id';
    protected $fillable=['member_id','month','monthly_payment','Unique_id','father_name','Type'];

    public function customer(){
        return $this->hasMany(Member::class);
    }
}
