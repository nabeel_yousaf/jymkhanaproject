<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelBuyingItem extends Model
{
    protected $fillable = ['item_name','item_desc'];

    public static function buy_report($product_id, $draw, $startDate,$endDate){
        
        $data = [];
        $data['draw'] = $draw;

        $final_data = HotelBuyingItem::select('id','item_name','item_desc','created_at','updated_at');

        if($product_id == 0) {
            // request with out product id and date filter
            if($startDate != '' && $endDate != '') {
                $final_data->where('updated_at', '>=', $startDate)
                           ->where('updated_at', '<=', $endDate);
            }
            
        }
        else {
            if($startDate == '' && $endDate == '') {
             
                $final_data->where('id',$product_id);

            }
            else {
                
                $final_data->where('id',$product_id)
                            ->where('updated_at', '>=', $startDate)
                            ->where('updated_at', '<=', $endDate);
            }
        }
        $data['data'] = $final_data->get();
        return $data;

        
    }
}
