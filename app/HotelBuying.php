<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelBuying extends Model
{
    protected $fillable = ['vendor','bill_no','buy_date','hotel_buying_id','quantity','Unit','total','credit','payment','Kitchen','type'];

    public function hotel_buying_items()
    {
        return $this->hasOne('App\HotelBuyingItem','id','hotel_buying_id');
    }
}
