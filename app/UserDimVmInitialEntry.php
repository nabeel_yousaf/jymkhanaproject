<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDimVmInitialEntry extends Model
{
    protected $fillable = ['user_id','voucher_master_id','dim_id'];
}
