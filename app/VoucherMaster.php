<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherMaster extends Model
{
    protected $fillable = ['voucher_type','voucher_date'];
}
