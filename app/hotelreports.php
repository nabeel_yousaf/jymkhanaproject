<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class hotelreport extends Model
{
    protected $table='hotel_report';
    protected $primaryKey='hb_id';
    protected $fillable = ['hb_id','M_Id','Billamount','Discount','Credit','Paid','Bill_no','date'];

}
