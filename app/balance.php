<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ChartOfAccount;

class balance extends Model
{
    //
    protected $fillable = [
        'coa_id', 'description', 'debit','credit'
    ];
    public function charts(){
        return $this->hasMany('App\ChartOfAccount');
    }
}
