<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherDetail extends Model
{
    protected $fillable = ['voucher_master_id', 'coa_id','narration','debit','credit','dim_id'];
}
