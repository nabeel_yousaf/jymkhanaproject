<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employeePost extends Model
{
    protected $table='emplyee_posts';
    protected $primaryKey='id';
    protected $fillable=['title','description'];
}
