<?php

namespace App\Http\Controllers;

use App\User;
use App\Member;
use App\ChartOfAccount;
use App\Audit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Image;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index(){
        return view('users.login');
    }

    public function show(){
         $menu_active=1;
        $users=DB::table('users')->where('admin', '=', null)->get();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Show All Users";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('backEnd.users.index',compact('menu_active','users'));
    }

    public function listByUsers($id){

         $byUser=User::where('id',$id)->first();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "List By Users";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.users.users-detail',compact('byUser'));
    }
      public function create(){
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Create User";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.users.create');
    }
    public function register(Request $request){
        $this->validate($request,[
            'name'=>'required|string|max:255',
             'email'=>'required|string|email|unique:users,email',
             'address'=>'required',
             'phone'=>'required',
             'city'=>'required',
             'cnic'=>'required',
             'status'=>'required',
             'image'=>'required',
             'password'=>'required|min:6|confirmed',
         ]);
         $input_data=$request->all();
         if($request->file('image')){
            $image=$request->file('image');
            if($image->isValid()){
                $fileName=time().'-'.str_slug($input_data['name'],"-").'.'.$image->getClientOriginalExtension();
                $large_image_path=public_path('images/'.$fileName);
                $medium_image_path=public_path('images/medium/'.$fileName);
                //Resize Image
                Image::make($image)->save($large_image_path);
                Image::make($image)->resize(255,350)->save($medium_image_path);
                $input_data['image']=$fileName;
            }
        }
        //dd($input_data);
         $input_data['password']=Hash::make($input_data['password']);
    User::create($input_data);
      Session::flash('success','Register Successfully !');

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Register User";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }
    public function login(Request $request){
        $input_data=$request->all();
        if(Auth::attempt(['email'=>$input_data['email'],'password'=>$input_data['password']])){
            Session::put('frontSession',$input_data['email']);
            return redirect('/viewcart');
        }else{
            return back()->with('message','Account is not Valid!');
        }
    }
    public function logout(){
        Auth::logout();
        Session::forget('frontSession');
        return redirect('/');
    }
    public function account(){
        $countries=DB::table('countries')->get();
        $user_login=User::where('id',Auth::id())->first();
        return view('users.account',compact('countries','user_login'));
    }
    public function updateprofile(Request $request,$id){
        $this->validate($request,[
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'mobile'=>'required',
        ]);
        $input_data=$request->all();
        DB::table('users')->where('id',$id)->update(['name'=>$input_data['name'],
            'address'=>$input_data['address'],
            'city'=>$input_data['city'],
            'state'=>$input_data['state'],
            'country'=>$input_data['country'],
            'pincode'=>$input_data['pincode'],
            'mobile'=>$input_data['mobile']]);
        return back()->with('message','Your Profile Updated Successfully!');

    }
    public function updatepassword(Request $request,$id){
        $oldPassword=User::where('id',$id)->first();
        $input_data=$request->all();
        if(Hash::check($input_data['password'],$oldPassword->password)){
            $this->validate($request,[
               'newPassword'=>'required|min:6|max:12|confirmed'
            ]);
            $new_pass=Hash::make($input_data['newPassword']);
            User::where('id',$id)->update(['password'=>$new_pass]);
            return back()->with('message','Your Password Updated Successfully!');
        }else{
            return back()->with('oldpassword','Old Password is Inconrrect!');
        }
    }
    public function destroy($id)
    {

        $delete=User::findOrFail($id);

        $image_large=public_path().'/images/'.$delete->image;
        $image_medium=public_path().'/images/medium/'.$delete->image;
        if($delete->delete()){
            unlink($image_large);
            unlink($image_medium);
        }
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "User Deleted";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back()->with('message','Delete Success!');
    }

    public function insert()
    {
        $member_record = Member::all();

        foreach ($member_record as $all_records) {
            $member_id = $all_records->membership_id;
            $member_name = $all_records->name;
            $member_email = $all_records->email;
            $member_address = $all_records->Address;


            if (empty($member_email)) {
                $member_email = "member" . $member_id . "@gmail.com";
            }
            elseif (User::where('email', $member_email)->first()) {
                $member_email = "member" . $member_id . "@gmail.com";
            }
            else{
                $member_email = $all_records->email;
            }
            $user = User::create([
                'name' => $member_name,
                'email' => $member_email,
                'password' => "123456",
                'admin' => 0,
                'address' => $member_address,
                'role_id' => 3,
            ]);
            $lastInsertedId= $user->id;

            $member = Member::where('membership_id', $member_id)->first();
            $member->user_id = $lastInsertedId;
            $member->save();

            ChartOfAccount::create([
                'user_id' => $lastInsertedId,
                'name' => $member_name . " A/R",
                'description' => "N/A",
                'type' => "Assets",
                'is_parent' => 0,
                'parent_id' => 54,
            ]);
        }

    }

    public function insertNullUserIdRecord() {
        $member_record = Member::where('user_id','=',null)->get();

        foreach ($member_record as $all_records) {
            $member_id = $all_records->membership_id;
            $member_name = $all_records->name;
            $member_email = $all_records->email;
            $member_address = $all_records->Address;


            if (empty($member_email)) {
                $member_email = "member" . $member_id . "@gmail.com";
            }
            elseif (User::where('email', $member_email)->first()) {
                $member_email = "member" . $member_id . "@gmail.com";
            }
            else{
                $member_email = $all_records->email;
            }
            $user = User::create([
                'name' => $member_name,
                'email' => $member_email,
                'password' => "123456",
                'admin' => 0,
                'address' => $member_address,
                'role_id' => 3,
            ]);
            $lastInsertedId= $user->id;

            $member = Member::where('membership_id', $member_id)->first();
            $member->user_id = $lastInsertedId;
            $member->save();

            ChartOfAccount::create([
                'user_id' => $lastInsertedId,
                'name' => $member_name . " A/R",
                'description' => "N/A",
                'type' => "Assets",
                'is_parent' => 0,
                'parent_id' => 54,
            ]);
        }
        echo "Inserted";
    }
    public function createOperator()
    {
        $password = Hash::make('123456');
        User::create([
            'name' => 'operator',
            'email' => 'operator@jymkhana.com',
            'password' => $password,
            'role_id' => '4',
        ]);
    }
    public function createSecertry()
    {
        $password = Hash::make('123456');
        User::create([
            'name' => 'secertry',
            'email' => 'secertry@jymkhana.com',
            'password' => $password,
            'role_id' => '5',
        ]);
    }
}
