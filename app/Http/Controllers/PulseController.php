<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Charts\PulseChart;
use App\Http\Controller;
use App\Pulse;
use App\Member;
use App\MembersPayRecord;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Image;

class PulseController extends Controller
{
     public function index(){
        $pulse=Pulse::all();
        $chart=new PulseChart;
        $chart->labels(['One','two','three']);
        $chart->dataset('my dataset is 1','line',[1,2,3,4]);
        return view('backEnd.index',compact('pulse','chart'));
    }
}
