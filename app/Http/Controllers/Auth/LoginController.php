<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/admin';
    public function redirectTo()
    {
        $user = Auth::user();
        if($user->role_id == User::IS_ADMIN_ROLE)
        {
            return '/admin';
        } else if($user->role_id == User::IS_MANAGER_ROLE){
            return '/manager';
        } else if($user->role_id == User::IS_SECERTRY_ROLE){
            return '/secertry';
        } else if($user->role_id == User::IS_OPERATOR_ROLE){
            return '/operator';
        }
        else {
            return '/member';
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
