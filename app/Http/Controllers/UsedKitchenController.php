<?php

namespace App\Http\Controllers;

use App\UsedKitchen;
use Illuminate\Http\Request;

class UsedKitchenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsedKitchen  $usedKitchen
     * @return \Illuminate\Http\Response
     */
    public function show(UsedKitchen $usedKitchen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsedKitchen  $usedKitchen
     * @return \Illuminate\Http\Response
     */
    public function edit(UsedKitchen $usedKitchen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsedKitchen  $usedKitchen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsedKitchen $usedKitchen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsedKitchen  $usedKitchen
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsedKitchen $usedKitchen)
    {
        //
    }
}
