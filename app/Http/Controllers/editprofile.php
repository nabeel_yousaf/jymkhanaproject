<?php

namespace App\Http\Controllers;

use App\UserDimVmInitialEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Member;
use App\VoucherMaster;
use App\VoucherDetail;
use App\ChartOfAccount;
use App\Dimension;
use Image;
use App\User;
use Auth;
use App\Audit;

class editprofile extends Controller
{
    public function edit($id)
    {
        $byCustomer=Member::where('membership_id',$id)->first();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Edit Profile";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.members.Editprofile',compact('byCustomer'));

    }

    public function update(Request $request,$id){
        $this->validate($request,[

        'image'=>'nullable|image|mimes:png,jpg,jpeg|max:2000'
        ]);
        if($request->hasfile('image')){
            $image=$request->file('image');
            $fileName = $request->file('image')->getClientOriginalName();
            $path = 'images/members_image';
            // create instance
            $img = Image::make($request->file('image'));
            $img->resize(300,250);
            $request->file('image')->move($path,$fileName);

        }
        $member = Member::where('membership_id', $id)->first();
          Member::where('membership_id', $id)->update([
              'name'=>isset($request->name) ? $request->name : $member->name,
              'father_name'=>isset($request->fname) ? $request->fname : $member->father_name,
              'MaritaLStatus'=>isset($request->maritalstatus) ? $request->maritalstatus : $member->MaritaLStatus,
              'BloodGroup'=>isset($request->blood_group) ? $request->blood_group : $member->BloodGroup,
              'email'=>isset($request->Email) ? $request->Email : $member->email,
              'password'=>isset($request->password) ? Hash::make($request->password) : $member->password,
              'category'=>isset($request->Category) ? $request->Category : $member->category,
              'Cnic'=>isset($request->cnic) ? $request->cnic : $member->Cnic,
              'Address'=>isset($request->address) ? $request->address : $member->Address,
              'TemporaryAddress'=>isset($request->taddress) ? $request->taddress : $member->TemporaryAddress,
              'cell_no'=>isset($request->Cell_no) ? $request->Cell_no : $member->cell_no,
              'DOB'=>isset($request->dob) ? $request->dob : $member->DOB,
              'Qualification'=>isset($request->qualification) ? $request->qualification : $member->Qualification,

              'registration_fee'=>isset($request->registration_fee) ? $request->registration_fee : $member->registration_fee,

              'membership_fee'=>isset($request->membership_fee) ? $request->membership_fee : $member->membership_fee,

              'monthly_sub_fee'=>isset($request->monthly_sub_fee) ? $request->monthly_sub_fee : $member->monthly_sub_fee,

              'NameOfOrganization'=>isset($request->Name_of_Organization) ? $request->Name_of_Organization : $member->NameOfOrganization,
              'image'=>isset($fileName) ? $fileName : $member->image,
              'Designation'=>isset($request->designation) ? $request->designation : $member->Designation,
              'BuisnessAddress'=>isset($request->businessmobile) ? $request->businessmobile : $member->BuisnessAddress,
              'BuisnessMobile'=>isset($request->buisnessadress) ? $request->buisnessadress : $member->BuisnessMobile,
              'Relation'=>isset($request->relation) ? $request->relation : $member->Relation,
              'SName'=>isset($request->sname) ? $request->sname : $member->SName,
              'SCnic'=>isset($request->scnic) ? $request->scnic : $member->SCnic,
              'SDob'=>isset($request->sdob) ? $request->sdob : $member->SDob,
              'remarks'=>isset($request->remarks) ? $request->remarks : $member->remarks,
          ]);


          //Check if first users entry has already been created
        $user = User::where('id','=',$member->user_id)->first();
        $chartAccount=ChartOfAccount::where('user_id',$member->user_id)->first();
        if($user) {
            $initial_entry = UserDimVmInitialEntry::where('user_id','=',$user->id)->get();
            // if($member->created_at > '2019-12-03') {
            //     if(count($initial_entry) > 0) {
            //         //Deleting Voucher Entries if already exist;
            //         //Edit For Membership Fee
            //         $dimension = Dimension::where('dim_name','Membership Fee')->first();
            //         $int_dim = $initial_entry->where('dim_id','=',$dimension->id)->first();
            //         if($int_dim) {
            //             $vm = VoucherMaster::find($int_dim->voucher_master_id);
            //             $vd = VoucherDetail::where('voucher_master_id','=',$int_dim->voucher_master_id);
            //             if($vm) {
            //                 $vm->delete();
            //             }
            //             if($vd) {
            //                 $vd->delete();
            //             }
            //         }

            //         //Edit For Registration fee
            //         $dimension = Dimension::where('dim_name','Registration Fee')->first();
            //         $int_dim = $initial_entry->where('dim_id','=',$dimension->id)->first();
            //         if($int_dim) {
            //             $vm = VoucherMaster::find($int_dim->voucher_master_id);
            //             $vd = VoucherDetail::where('voucher_master_id','=',$int_dim->voucher_master_id);
            //             if($vm) {
            //                 $vm->delete();
            //             }
            //             if($vd) {
            //                 $vd->delete();
            //             }
            //         }
            //         //Edit For Monthly Subscription Fee;
            //         $dimension = Dimension::where('dim_name','Monthly Subscription Fee')->first();
            //         $int_dim = $initial_entry->where('dim_id','=',$dimension->id)->first();
            //         if($int_dim) {
            //             $vm = VoucherMaster::find($int_dim->voucher_master_id);
            //             $vd = VoucherDetail::where('voucher_master_id','=',$int_dim->voucher_master_id);
            //             if($vm) {
            //                 $vm->delete();
            //             }
            //             if($vd) {
            //                 $vd->delete();
            //             }
            //         }
            //     }
            // }
                //Check if this master and detail id record again need to be set;
                // $membership_fee = isset($request->membership_fee) ? $request->membership_fee : $member->membership_fee;
                // if($membership_fee != 0) {
                //     $dimension=Dimension::where('dim_name','Membership Fee')->first();
                //     //Debit
                //     $voucherMaster = VoucherMaster::create([
                //         'voucher_type'=>'Journal Voucher',
                //         'voucher_date'=>date('Y-m-d'),
                //     ]);
                //     //First Time Entry Membership Dimension;
                //     UserDimVmInitialEntry::create([
                //            'user_id' => $user->id,
                //            'dim_id' => $dimension->id,
                //             'voucher_master_id' => $voucherMaster->id,
                //     ]);
                //     VoucherDetail::create([
                //         'coa_id'=>$chartAccount->id,
                //         'voucher_master_id'=>$voucherMaster->id,
                //         'narration'=>'Voucher Initialized',
                //         'debit'=>$membership_fee,
                //         'credit'=>0,
                //         'dim_id'=>$dimension->id,
                //     ]);
                //     VoucherDetail::create([
                //         'coa_id'=>871,
                //         'voucher_master_id'=>$voucherMaster->id,
                //         'narration'=>'Voucher Initialized',
                //         'debit'=>0,
                //         'credit'=>$membership_fee,
                //         'dim_id'=>$dimension->id,
                //     ]);
                // }
                // $registration_fee = isset($request->registration_fee) ? $request->registration_fee : $member->registration_fee;
                // if($registration_fee != 0) {
                //     $dimension=Dimension::where('dim_name','Registration Fee')->first();
                //     $voucherMaster = VoucherMaster::create([
                //         'voucher_type'=>'Journal Voucher',
                //         'voucher_date'=>date('Y-m-d'),
                //     ]);
                //     //First Time Entry Registration Dimension;
                //     UserDimVmInitialEntry::create([
                //             'user_id' => $user->id,
                //             'dim_id' => $dimension->id,
                //             'voucher_master_id' => $voucherMaster->id,
                //     ]);
                //     VoucherDetail::create([
                //         'coa_id'=>$chartAccount->id,
                //         'voucher_master_id'=>$voucherMaster->id,
                //         'narration'=>'Voucher Initialized',
                //         'debit'=>$registration_fee,
                //         'credit'=>0,
                //         'dim_id'=>$dimension->id,
                //     ]);
                //     VoucherDetail::create([
                //         'coa_id'=>871,
                //         'voucher_master_id'=>$voucherMaster->id,
                //         'narration'=>'Voucher Initialized',
                //         'debit'=>0,
                //         'credit'=>$registration_fee,
                //         'dim_id'=>$dimension->id,
                //     ]);
                // }
                // $monthly_sub_fee = isset($request->monthly_sub_fee) ? $request->monthly_sub_fee : $member->monthly_sub_fee;
                // if($monthly_sub_fee != 0) {
                //     $dimension = Dimension::where('dim_name','Monthly Subscription Fee')->first();
                //     $voucherMaster = VoucherMaster::create([
                //         'voucher_type'=>'Journal Voucher',
                //         'voucher_date'=>date('Y-m-d'),
                //     ]);
                //     //First Time Entry Monthly Subscription Fee Dimension;
                //     UserDimVmInitialEntry::create([
                //             'user_id' => $user->id,
                //             'dim_id' => $dimension->id,
                //             'voucher_master_id' => $voucherMaster->id,
                //     ]);
                //     VoucherDetail::create([
                //         'coa_id'=>$chartAccount->id,
                //         'voucher_master_id'=>$voucherMaster->id,
                //         'narration'=>'Voucher Initialized',
                //         'debit'=>$monthly_sub_fee,
                //         'credit'=>0,
                //         'dim_id'=>$dimension->id,
                //     ]);
                //     VoucherDetail::create([
                //         'coa_id'=>871,
                //         'voucher_master_id'=>$voucherMaster->id,
                //         'narration'=>'Voucher Initialized',
                //         'debit'=>0,
                //         'credit'=>$monthly_sub_fee,
                //         'dim_id'=>$dimension->id,
                //     ]);
                // }
        }

    Session::flash('success','update Successfully !');
    $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Update Profile";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect('admin/memberspayrecord');
    }

}
