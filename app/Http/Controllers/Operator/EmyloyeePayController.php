<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;

use App\EmyloyeePay;
use App\Audit;
use Auth;
use Illuminate\Http\Request;

class EmyloyeePayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmyloyeePay  $emyloyeePay
     * @return \Illuminate\Http\Response
     */
    public function show(EmyloyeePay $emyloyeePay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmyloyeePay  $emyloyeePay
     * @return \Illuminate\Http\Response
     */
    public function edit(EmyloyeePay $emyloyeePay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmyloyeePay  $emyloyeePay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmyloyeePay $emyloyeePay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmyloyeePay  $emyloyeePay
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmyloyeePay $emyloyeePay)
    {
        //
    }
}
