<?php

namespace App\Http\Controllers\Operator;

use App\Member;
use App\User;
use App\MembersPayRecord;
use App\ChartOfAccount;
use App\MemberFamilyInfo;
use App\Audit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Image;
use Illuminate\Support\Facades\Session;

class MemberController extends Controller
{
    public function registered()
    {
        return view('members.register');
    }
    public function PaymentsHistory()
    {
        $allPaymentRecord = DB::table('members_record')
            ->join('members_pay_records', 'members_pay_records.member_id', '=', 'membership_id')
            ->select('members_record.*', 'members_pay_records.*')
            ->get();

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Payments History";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('operator.members.payments', compact('allPaymentRecord'));
    }

    public function reportnewmember()
    {
        $newmem = DB::table('members_record')
            ->where('Category', 'new member')->get();

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Report New Member";
            $audit_create->route = $routeName;
            $audit_create->save();

        return view('backEnd.members.newmemberreport', compact('newmem'));
    }
    public function reportbedmintonmember()
    {
        $bedmem = DB::table('members_record')
            ->where('Category', 'bedminton')->get();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Report Bedminton Member";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.members.bedmintonmemberreport', compact('bedmem'));
    }



    public function veharimemberreport()
    {
        $veharimem = DB::table('members_record')
            ->where('Category', 'vehari')->get();

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Vehari Member Report";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('backEnd.members.veharimemberreport', compact('veharimem'));
    }


    public function oldmemberreport()
    {
        $oldmem = DB::table('members_record')
            ->where('Category', 'old member')->get();

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Old Member Report";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('backEnd.members.oldmemberreport', compact('oldmem'));
    }
    public function register(Request $request)
    {
        $this->validate($request, [
            'membership_id' => 'required|unique:members_record,membership_id',
            'category' => 'required',
            'name' => 'required|string|max:25',
            'email' => 'required|string|email|unique:members_record,email',
            'father_name' => 'required',
            'Qualification' => 'nullable',
            'registration_fee' => 'nullable',
            'DOB' => 'nullable',
            'cell_no' => 'required',
            'land_line' => 'nullable',
            'twitter' => 'nullable',
            'Cnic' => 'nullable',
            'MaritaLStatus' => 'nullable',
            'BloodGroup' => 'nullable',
            'temporary_address' => 'nullable',
            'Address' => 'nullable',
            'NameOfOrganization' => 'nullable',
            'designation' => 'nullable',
            'business_address' => 'nullable',
            'business_land_line' => 'nullable',
            'business_fax' => 'nullable',
            'Relation' => 'nullable',
            'spouse_name' => 'nullable',
            'spouse_dob' => 'nullable',
            'spouse_cnic' => 'nullable',
            'image' => 'nullable|image|mimes:png,jpg,jpeg|max:2000',
            'password' => 'required|min:6|confirmed',
        ]);
        $input_data = $request->all();
        if($request->get('Relation') != '') {
            $input_data['Relation'] = null;
            $input_data['spouse_name'] = null;
            $input_data['spouse_dob'] = null;
            $input_data['spouse_cnic'] = null;
            $input_data['Relation'] = $request->get('Relation')[0];
            $input_data['spouse_name'] = $request->get('SName')[0];
            $input_data['spouse_dob'] = $request->get('SDob')[0];
            $input_data['spouse_cnic'] = $request->get('SCnic')[0];
        }
        $member_name = $input_data['name'];
        if ($request->file('Image')) {
            $image = $request->file('Image');
            if ($image->isValid()) {
                $fileName = time() . '-' . str_slug($input_data['name'], "-") . '.' . $image->getClientOriginalExtension();
                $path = 'images/members_image';
                // create instance
                $img = Image::make($request->file('Image'));
                $img->resize(300, 250);
                $request->file('Image')->move($path, $fileName);
                $input_data['image'] = $fileName;
            }
        }
        $input_data['password'] = Hash::make($input_data['password']);
        $member_id = $input_data['membership_id'];
        $user = User::create([
            'name' => $input_data['name'],
            'email' => $input_data['email'],
            'password' => $input_data['password'],
            'address' => $input_data['Address'],
            'role_id' => 3,
            'admin' => 0,
        ]);
        $input_data['user_id'] = $user->id;
        Member::create($input_data);

        $lastInsertedId = $user->id;
        ChartOfAccount::create([
            'user_id' => $lastInsertedId,
            'name' => $member_name . " A/R",
            'description' => "N/A",
            'type' => "Assets",
            'is_parent' => 0,
            'parent_id' => 54,
        ]);

        if($request->get('Relation') != ''){
            $countrows = count($request->get('Relation'));

            for ($i=0; $i < $countrows; $i++) {
                MemberFamilyInfo::create([
                    'member_id' => $member_id,
                    'relation' => $request->get('Relation')[$i],
                    'name' => $request->get('SName')[$i],
                    'dob' => $request->get('SDob')[$i],
                    'cnic' => $request->get('SCnic')[$i]
                ]);
            }
        }

        Session::flash('success', 'Register Successfully !');

        return view('frontEnd.index');
    }


    public function customerDetail($id)
    {
        $byCustomer = Member::where('membership_id', $id)->first();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Customer Detail";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('operator.members.detail', compact('byCustomer'));
    }
    public function CustomerUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'membership_id',
        ]);
        $id = $request->input('membership_id');
        $status = 0;
        $result = DB::table('members_record')
            ->where('membership_id', $id)
            ->update([
                'status' => $status,
            ]);
        Session::flash('success', 'User are Active Now !');
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Customer Update";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('members.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete related
        $member = Member::where('membership_id', '=', $id)->delete();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Member Deleted";
        $audit_create->route = $routeName;
        $audit_create->save();
        return collect([
            'success' => true
        ]);
    }
}
