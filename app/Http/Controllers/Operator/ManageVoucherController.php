<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;
use App\VoucherDetail;
use App\VoucherMaster;
use App\VoucherDetailPending;
use App\VoucherMasterPending;
use App\ChartOfAccount;
use App\Dimension;
use App\Member;
use App\Audit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Session;
use Validator;
use Auth;
use Illuminate\Support\Facades\Redirect;

class ManageVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $coa = ChartOfAccount::all()->where('is_parent' ,'=', 0);
        $accounts = array();
        foreach ($coa as $key => $value) {
            if($value->user_id != 1) {
                //User is not admin;
                $member = Member::where('user_id','=',$value->user_id)->first();
                if($member){
                    $accounts[] = ['id' => $value->id, 'name'=>$value->name, 'membership_id'=>$member->membership_id];
                }
            }
            else {
                $accounts[] = ['id' => $value->id, 'name'=>$value->name, 'membership_id'=> ''];
            }
        }
        $dim = Dimension::all();
        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Voucher create";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('operator.voucher.voucherForm', compact('coa','dim','accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date'=>'required',
            'vouchertype'=>'required',
        ]);
        if($validator->fails()) {

            return Redirect::back()->withErrors($validator->errors());
        }
        Session::forget('voucher_master_id');
        return redirect()->route('operatorvoucher.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date'=>'required',
            'vouchertype'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        Session::forget('voucher_master_id');
        $id = $request->id;
        $voucherMaster = VoucherMaster::where('id', $id)->first();
        $voucherMaster->voucher_type = $request->vouchertype;
        $voucherMaster->voucher_date = $request->date;
        $voucherMaster->save();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Voucher Saved";
        $audit_create->route = $routeName;
        $audit_create->save();

        return response()->json();
    }

    public function addVoucher(Request $request){

        $validator = Validator::make($request->all(), [
            'date'=>'required',
            'vouchertype'=>'required',
            'coa_id'=>'required',
            'debit'=>'required',
            'credit'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        if (Session::has('voucher_master_id')) {
            $master_id = Session::get('voucher_master_id');
            $dimension_name = null;
            if(isset($request->dimension)) {
                $dimension_name = $request->dimension;
                $accountName = ChartOfAccount::where('id' , $request->coa_id)->first();
                $account_name = $accountName->name;
            }
            else {
                $account_name = 'N/A';
            }
            $voucherDetail = new VoucherDetailPending();
            $voucherDetail->voucher_master_id = $master_id;
            $voucherDetail->coa_id = $request->coa_id;
            $voucherDetail->narration = $request->naration;
            $voucherDetail->debit = $request->debit;
            $voucherDetail->credit = $request->credit;
            $voucherDetail->dim_id = $request->dimension;

            $voucherDetail->save();

            $data = array('coa_id' => $account_name, 'dimension' => $dimension_name, 'debit' => $voucherDetail->debit, 'credit' => $voucherDetail->credit, );

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Voucher Added";
            $audit_create->route = $routeName;
            $audit_create->save();

            return response()->json($data);
        }
        else{
            $voucherMaster = new VoucherMasterPending();
            $voucherMaster->voucher_type = $request->vouchertype;
            $voucherMaster->voucher_date = $request->date;

            $voucherMaster->save();
            $master_id = $voucherMaster->id;
            Session::put('voucher_master_id', $master_id);
            $dimension_name = null;
            if(isset($request->dimension)) {
                $dimension_name = $request->dimension;
                $accountName = ChartOfAccount::where('id' , $request->coa_id)->first();
                $account_name = $accountName->name;
            }
            else {
                $account_name = 'N/A';
            }
            $voucherDetail = new VoucherDetailPending();
            $voucherDetail->voucher_master_id = $master_id;
            $voucherDetail->coa_id = $request->coa_id;
            $voucherDetail->narration = $request->naration;
            $voucherDetail->debit = $request->debit;
            $voucherDetail->credit = $request->credit;
            $voucherDetail->dim_id = $request->dimension;

            $voucherDetail->save();

            $data = array('coa_id' => $account_name, 'dimension' => $dimension_name, 'debit' => $voucherDetail->debit, 'credit' => $voucherDetail->credit, );

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Voucher Added";
            $audit_create->route = $routeName;
            $audit_create->save();

            return response()->json($data);
        }

    }


    public function editVoucher(Request $request){

        $validator = Validator::make($request->all(), [
            'coa_id'=>'required',
            'debit'=>'required',
            'credit'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        if (Session::has('voucher_master_id')) {
            $master_id = Session::get('voucher_master_id');

            if (isset($request->id)) {
                $id = $request->id;
                $voucherDetail = VoucherDetail::where('id',$id)->first();
                $voucherDetail->voucher_master_id = $master_id;
                $voucherDetail->coa_id = $request->coa_id;
                $voucherDetail->narration = $request->naration;
                $voucherDetail->debit = $request->debit;
                $voucherDetail->credit = $request->credit;
                $voucherDetail->dim_id = $request->dimension;

                $voucherDetail->save();
                $vd_id = $voucherDetail->id;

                $accountName = ChartOfAccount::where('id' , $request->coa_id)->first();
                $account_id = $accountName->id;
                $account_name = $accountName->name;

                $dimensionName = Dimension::where('id' , $request->dimension)->first();
                $dimension_id = $dimensionName->id;
                $dimension_name = $dimensionName->dim_name;

                $data = array('vd_id' => $vd_id, 'naration' =>$voucherDetail->narration, 'coa_name' => $account_name, 'coa_id' => $account_id, 'dim_id' => $dimension_id, 'dimension' => $dimension_name, 'debit' => $voucherDetail->debit, 'credit' => $voucherDetail->credit, 'c_id' => $voucherDetail->coa_id, 'naration' => $voucherDetail->narration );

                return response()->json($data);
            }
            else{

                $voucherDetail = new VoucherDetail();
                $voucherDetail->voucher_master_id = $master_id;
                $voucherDetail->coa_id = $request->coa_id;
                $voucherDetail->narration = $request->naration;
                $voucherDetail->debit = $request->debit;
                $voucherDetail->credit = $request->credit;
                $voucherDetail->dim_id = $request->dimension;

                $voucherDetail->save();
                $vd_id = $voucherDetail->id;

                $accountName = ChartOfAccount::where('id' , $request->coa_id)->first();
                $account_id = $accountName->id;
                $account_name = $accountName->name;

                $dimensionName = Dimension::where('id' , $request->dimension)->first();
                $dimension_id = $dimensionName->id;
                $dimension_name = $dimensionName->dim_name;

                $data = array('vd_id' => $vd_id, 'naration' =>$voucherDetail->narration, 'coa_name' => $account_name, 'coa_id' => $account_id, 'dim_id' => $dimension_id, 'dimension' => $dimension_name, 'debit' => $voucherDetail->debit, 'credit' => $voucherDetail->credit, 'c_id' => $voucherDetail->coa_id, 'naration' => $voucherDetail->narration );

                return response()->json($data);
            }

        }
        else{

            if (isset($request->id)) {
                $id = $request->id;
                $master_id = $request->voucher_master_id;
                Session::put('voucher_master_id', $master_id);

                $voucherDetail = VoucherDetail::where('id',$id)->first();
                $voucherDetail->voucher_master_id = $master_id;
                $voucherDetail->coa_id = $request->coa_id;
                $voucherDetail->narration = $request->naration;
                $voucherDetail->debit = $request->debit;
                $voucherDetail->credit = $request->credit;
                $voucherDetail->dim_id = $request->dimension;

                $voucherDetail->save();
                $vd_id = $voucherDetail->id;

                $accountName = ChartOfAccount::where('id' , $request->coa_id)->first();
                $account_id = $accountName->id;
                $account_name = $accountName->name;

                $dimensionName = Dimension::where('id' , $request->dimension)->first();
                $dimension_id = $dimensionName->id;
                $dimension_name = $dimensionName->dim_name;

                $data = array('vd_id' => $vd_id, 'naration' =>$voucherDetail->narration, 'coa_name' => $account_name, 'coa_id' => $account_id, 'dim_id' => $dimension_id, 'dimension' => $dimension_name, 'debit' => $voucherDetail->debit, 'credit' => $voucherDetail->credit, 'c_id' => $voucherDetail->coa_id, 'naration' => $voucherDetail->narration );

                return response()->json($data);
            }
            else{

                $master_id = $request->voucher_master_id;
                Session::put('voucher_master_id', $master_id);

                $voucherDetail = new VoucherDetail();
                $voucherDetail->voucher_master_id = $master_id;
                $voucherDetail->coa_id = $request->coa_id;
                $voucherDetail->narration = $request->naration;
                $voucherDetail->debit = $request->debit;
                $voucherDetail->credit = $request->credit;
                $voucherDetail->dim_id = $request->dimension;

                $voucherDetail->save();
                $vd_id = $voucherDetail->id;

                $accountName = ChartOfAccount::where('id' , $request->coa_id)->first();
                $account_id = $accountName->id;
                $account_name = $accountName->name;

                $dimensionName = Dimension::where('id' , $request->dimension)->first();
                $dimension_id = $dimensionName->id;
                $dimension_name = $dimensionName->dim_name;

                $data = array('vd_id' => $vd_id, 'naration' =>$voucherDetail->narration, 'coa_name' => $account_name, 'coa_id' => $account_id, 'dim_id' => $dimension_id, 'dimension' => $dimension_name, 'debit' => $voucherDetail->debit, 'credit' => $voucherDetail->credit, 'c_id' => $voucherDetail->coa_id, 'naration' => $voucherDetail->narration );

                return response()->json($data);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(VoucherMaster $VoucherMaster)
    {
        //return view('backEnd.balance.index', compact('bal'));
        $voucher = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
            ->where('is_active' ,'=', 1)
            ->select('voucher_masters.*', DB::raw('SUM(voucher_details.debit) as debit'), DB::raw('SUM(voucher_details.credit) as credit'))
            ->groupBy('voucher_details.voucher_master_id')
            ->get();

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "View All Voucher";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('operator.voucher.index', compact('voucher'));
    }

    public function viewVoucher($id)
    {
        $master_record = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
            ->where('is_active' ,'=', 1)
            ->where('voucher_masters.id' , $id)
            ->where('voucher_details.voucher_master_id' , $id)
            ->select('voucher_masters.*', 'voucher_details.dim_id', 'voucher_details.narration', 'voucher_details.debit', 'voucher_details.credit')
            ->get();

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "View Voucher Detail";
            $audit_create->route = $routeName;
            $audit_create->save();
        return response()->json($master_record);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = VoucherMaster::all()->where('id','=',$id );
        $voucher_detail = VoucherDetail::all();
        $voucher_coa = DB::table('chart_of_accounts')
            ->join('voucher_details', 'voucher_details.coa_id', '=', 'chart_of_accounts.id')
            ->select('chart_of_accounts.name', 'voucher_details.*')
            ->get();
        $coa = ChartOfAccount::all()->where('is_parent' ,'=', 0);

        $accounts = array();
        foreach ($coa as $key => $value) {
            if($value->user_id != 1) {
                //User is not admin;
                $member = Member::where('user_id','=',$value->user_id)->first();
                if($member){
                    $accounts[] = ['id' => $value->id, 'name'=>$value->name, 'membership_id'=>$member->membership_id];
                }
            }
            else {
                $accounts[] = ['id' => $value->id, 'name'=>$value->name, 'membership_id'=> ''];
            }
        }
        $dim = Dimension::all();

        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Voucher Edit";
            $audit_create->route = $routeName;
            $audit_create->save();

        return view('operator.voucher.edit', compact('coa','dim','accounts','voucher_coa','edit','voucher_detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
