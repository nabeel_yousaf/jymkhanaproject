<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;
use App\memberreport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Audit;
use App\Member;

class membersreport extends Controller
{
    public function searchmember(Request $request) {
        $data=$request->all();
        
        $startDate = $request->startDate;
        $endDate = $request->endDate;
 
        $type = new Member(); 
        $allMember = Member::all(); 
        $select = $request->input('select');
        if(isset($select)) {
            $type=Member::select($select);  
        }
        
        if($startDate && $endDate){
            $type->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate);

            $allMember = Member :: where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)->get();

        }
 

        $type = $type->get(); 
 

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Print Button Clicked";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('operator.members.report',compact('type', 'select', 'allMember'));

    }
}
