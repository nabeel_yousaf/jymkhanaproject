<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\hotel_buying;
use Illuminate\Database\MySqlConnection;
use Session;
use Illuminate\Support\Facades\DB;
use App\Audit;
use App\HotelBuying;
use App\UsedKitchen;
use Illuminate\Validation\Rule;
use Auth;

class HotelBuyingController extends Controller
{

    public function buyingsave(Request $request){
 
        $request->validate([
            'products' => 'required|array'
        ]);
       
        $products = $request->products;
        
         
        
        foreach($products as $product){
              
                if(
                    $product['bill_no'] == "" ||
                    $product['vendor_name'] == "" || 
                    $product['hotel_buying_id'] == "" || 
                    $product['quantity'] == "" ||
                    $product['unit'] == "" ||
                    $product['total'] == "" ||
                    $product['credit']== "" ||
                    $product['payment'] == "" ||
                    $product['kitchen'] == "" ||
                    $product['bill_type']== ""
                ){
                    return response()->json([

                        'error' => true, 
                        'message' => 'Input Fields can not be empty'

                    ], 422);
                } 
        }
       
 
        foreach($products as $product){

            $hotelbuying = HotelBuying::create([
                'vendor'          => $product['vendor_name'],
                'bill_no'        => $product['bill_no'],            
                'buy_date'        => $product['buy_date'],            
                'quantity'        => $product['quantity'],            
                'Unit'            => $product['unit'],
                'hotel_buying_id' => $product['hotel_buying_id'],
                'total'           => $product['total'],
                'payment'         => $product['payment'],
                'credit'          => $product['credit'],
                'Kitchen'         => $product['kitchen'],
                'type'            => $product['bill_type']
            ]);
    
            if($request->Kitchen && $hotelbuying){
                UsedKitchen::create([
                    'hotel_buying_id' => $hotelbuying->id, 
                    'used_item' => $product['kitchen']
                ]);
            }
        }
            
 
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Save Hotel Buying";
        $audit_create->route = $routeName;
        $audit_create->save();
        return response()->json([
            'error' => false,
            'message' => 'New Data has been created'

        ]);

    }

    public function updateKitchItem(Request $request){

        $used_item = $request->used_item;
        $hotel_buying_id = $request->hotel_buying_id;

        $this->validate($request,[
            'used_item'=>'required',
            'hotel_buying_id'=>'required|exists:hotel_buyings,id',
        ]);

        $hotelbuying = HotelBuying::find($request->hotel_buying_id);

        $remnaning_item = $hotelbuying->total - $hotelbuying->Kitchen;

        if($request->used_item > $remnaning_item){
            return response()->json(array(
                'success' => false,
                'message' => 'used item value is gretor then available item',
                'errors' => ['used_item'=> ['used item value is gretor then available item']]
            ), 422);
        }

        $hotelbuying->Kitchen += $request->used_item;
        $hotelbuying->save();

        if($request->used_item && $hotelbuying){
            UsedKitchen::create([
                'hotel_buying_id' => $hotelbuying->id, 
                'used_item' => $request->used_item
            ]);
        }

        return response()->json(array(
            'success' => true,
            'message' => 'data has been updated',
            'errors' => null
        ));
        



    }

    public function search(Request $request){
          
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $item  = $request->get('item');

        $reportsDatahotel=HotelBuying::join('hotel_buying_items','hotel_buyings.hotel_buying_id','=','hotel_buying_items.id');
            

        if(!$item){
            if($startDate != '' && $endDate != '') {
                $reportsDatahotel->where('hotel_buyings.buy_date', '>=', $startDate)
                ->where('hotel_buyings.buy_date', '<=', $endDate);
            }
             
        }else{ 

            if($startDate == '' && $endDate == '') {
             
                $reportsDatahotel->where('hotel_buying_items.item_name',$item);

            }
            else {
                
                $reportsDatahotel->where('hotel_buying_items.item_name',$item)
                ->where('hotel_buyings.buy_date', '>=', $startDate)
                ->where('hotel_buyings.buy_date', '<=', $endDate);
            }
        }      

        // $reportsDatahotel=DB::table('hotel_buyings')->where('created_at','like','%'.$from_date.'%')->get();
        
        $reportsDatahotel =$reportsDatahotel->select('hotel_buyings.*','hotel_buying_items.item_name')->get();
 

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Search Hotel Buying Report";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('operator.Hotel.Buyingreport',compact('reportsDatahotel'));

    }

    public function inventoryReport(Request $request){
          
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $item  = $request->get('item');

        $reportsDatahotel = UsedKitchen::with('hotelBuying.hotel_buying_items');

        if(!$item){
            if($startDate != '' && $endDate != '') {
                $reportsDatahotel->where('created_at', '>=', $startDate)
                ->where('created_at', '<=', $endDate);
            }
             
        }else{ 

            if($startDate == '' && $endDate == '') {
             
                $reportsDatahotel->whereHas('hotelBuying.hotel_buying_items', function($q) use ($item) {
                    $q->where('item_name', '=', $item);
                });

            }
            else {
                
                $reportsDatahotel->whereHas('hotelBuying.hotel_buying_items', function($q) use ($item) {
                    $q->where('item_name', '=', $item);
                })
                ->where('created_at', '>=', $startDate)
                ->where('created_at', '<=', $endDate);
            }
        }      

        // $reportsDatahotel=DB::table('hotel_buyings')->where('created_at','like','%'.$from_date.'%')->get();
        
        $reportsDatahotel =$reportsDatahotel->get();
  

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Search Hotel Buying Report";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('operator.Hotel.inventoryReport',compact('reportsDatahotel'));

    }

}
