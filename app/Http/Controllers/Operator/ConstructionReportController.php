<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;

use App\ConstructionReport;
use App\ConstructionMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use Psy\Command\ShowCommand;
use Session;
use App\Audit;
use Auth;

use function Psy\sh;

class ConstructionReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $constructionReport=ConstructionReport::all();
        $sql = "Select construction_reports.*, construction_material.mat_name from construction_reports,construction_material where construction_reports.material= construction_material.id;";
        
        $constructionMaterial=ConstructionMaterial::all();
        $constructionReport= DB::select($sql);
       // print_r($constructionReport);

        return view('operator.construction.index', ['constructionReport' => $constructionReport,'constructionMaterial' => $constructionMaterial]);

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Construction Report";
        $audit_create->route = $routeName;
        $audit_create->save();
       // return view('operator.construction.index',['constructionReport' => $constructionReport]);
        
    }

    public function bill_number()
    {
       // $constructionReport=ConstructionReport::all();
        $sql = "Select construction_reports.*, construction_material.mat_name from construction_reports,construction_material where construction_reports.material= construction_material.id;";
        
        
        $constructionReport= DB::select($sql);
       // print_r($constructionReport);

        

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Construction Report";
        $audit_create->route = $routeName;
        $audit_create->save();
       // return view('operator.construction.index',['constructionReport' => $constructionReport]);
        return view('operator.construction.search_by_bill', ['constructionReport' => $constructionReport]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $constructionMaterial=ConstructionMaterial::all();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Construction Report Create";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('operator.construction.create', compact('constructionMaterial'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
             'material'=>'required',
             'Vendor'=>'required',
             'qty'=>'required',
             'unit_price'=>'required',
             'credit'=>'required',
             'payment'=>'required',
            //  'labour'=>'required',
            //  'no_labour'=>'required',
            //  'labour_payments'=>'required',
            //  'skills'=>'required',
        ]);
        $created_date=now();
        $save_date=$created_date->format('d/m/Y');
        $bill_num=$request->input('bill_num');
        $Vendor=$request->input('Vendor');
        $material=$request->input('material');
        $qty=$request->input('qty');
        $unit_price=$request->input('unit_price');
        $credit=$request->input('credit');
        $payment=$request->input('payment');
        $labour=$request->input('labour');
        $no_labour=$request->input('no_labour');
        $labour_payments=$request->input('labour_payments');
        $skills=$request->input('skills');
        $total=$unit_price*$qty;
        DB::table('construction_reports')->insert(
        ['bill_num' => $bill_num,'Vendor'=>$Vendor,'material' => $material,'qty'=>$qty,'unit_price'=>$unit_price,'credit'=>$credit,'payment'=>$payment,'labour'=>$labour,'labour_payments'=>$labour_payments,'no_labour'=>$no_labour,'skills'=>$skills,'total'=>$total,'skills'=>$skills,'saved_at'=>$save_date,'created_at'=>now(),'updated_at'=>now()]
        );
        //dd($save_date);
        Session::flash('Success','You Successfully Added The Report !');
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Construction Report Store";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConstructionReport  $constructionReport
     * @return \Illuminate\Http\Response
     */
    public function show(ConstructionReport $constructionReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConstructionReport  $constructionReport
     * @return \Illuminate\Http\Responseg
     */
    public function edit(ConstructionReport $constructionReport)
    {
        $existing = ConstructionReport::find($constructionReport->id);

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Construction Report Edit";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('operator.construction.edit-report',compact('existing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConstructionReport  $constructionReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConstructionReport $constructionReport)
    {
        $this->validate($request,[
            'material'=>'required',
            'qty'=>'required',
            'unit_price'=>'required',
            'credit'=>'required',
            'payment'=>'required',
            // 'labour'=>'required',
            // 'no_labour'=>'required',
            // 'labour_payments'=>'required',
            // 'skills'=>'required',
       ]);
       $created_date=now();
       $save_date=$created_date->format('d/m/Y');
       $bill_num=$request->input('bill_num');
       $material=$request->input('material');
       $qty=$request->input('qty');
       $unit_price=$request->input('unit_price');
       $credit=$request->input('credit');
       $payment=$request->input('payment');
       $labour=$request->input('labour');
       $no_labour=$request->input('no_labour');
       $labour_payments=$request->input('labour_payments');
       $skills=$request->input('skills');
       $total=$unit_price*$qty;
       DB::table('construction_reports')->where('id',$constructionReport->id)->update(
       ['bill_num' => $bill_num,'material' => $material,'qty'=>$qty,'unit_price'=>$unit_price,'credit'=>$credit,'payment'=>$payment,'labour'=>$labour,'labour_payments'=>$labour_payments,'no_labour'=>$no_labour,'skills'=>$skills,'total'=>$total,'skills'=>$skills,'saved_at'=>$save_date,'created_at'=>now(),'updated_at'=>now()]
       );
       Session::flash('success','You Successfully Updated Record !');

       $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Construction Report Updated";
        $audit_create->route = $routeName;
        $audit_create->save();

       return redirect('/operator-construction-reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConstructionReport  $constructionReport
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }

    public function delete($id) {
        $record = ConstructionReport::find($id);
        if($record) {
            $record->delete();
            Session::flash('success','You Successfully Updated Record !');
            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Construction Report Deleted";
            $audit_create->route = $routeName;
            $audit_create->save();
            return redirect('/operator/operator-construction-reports');
        }
        else {
            Session::flash('success','Error!');
            return redirect()->back();
        }

    }
 /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConstructionReport  $constructionReport
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
 
            $from_date = $request->get('from_date');

            $startDate = $request->get('startDate');
            $endDate = $request->get('endDate');
            $material  = $request->get('material');
            $billnum  = $request->get('billNo');
    
 
            $reportsData= ConstructionReport::with('constructionmaterial');

            if($billnum){
                $reportsData->where('bill_num','like','%'.$billnum.'%');
            }else{
                
                if(!$material){
                    if($startDate != '' && $endDate != '') {
                        $reportsData->where('created_at', '>=', $startDate)
                        ->where('created_at', '<=', $endDate);
                    }
                     
                }else{ 
        
                    if($startDate == '' && $endDate == '') {
                     
                        $reportsData->whereHas('constructionmaterial', function($q) use ($material) {
                            $q->where('mat_name', '=', $material);
                        });
        
                    }
                    else {
                        
                        $reportsData->whereHas('constructionmaterial', function($q) use ($material) {
                            $q->where('mat_name', '=', $material);
                        })
                        ->where('created_at', '>=', $startDate)
                        ->where('created_at', '<=', $endDate);
                    }
                } 

            }
 
            $reportsData = $reportsData->get();

            $sum1=$reportsData->sum('total');
            $sum2=$reportsData->sum('credit');
            $sum3=$reportsData->sum('payment');
            $sum4=$reportsData->sum('labour_payments');


            Session::put('reportsData', $reportsData);
            Session::put('sum1', $sum1);
            Session::put('sum2', $sum2);
            Session::put('sum3', $sum3);
            Session::put('sum4', $sum4);

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();

            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Search Construction Report";
            $audit_create->route = $routeName;
            $audit_create->save();
            return view('operator.construction.reports');
        }


public function search_by_BillNum(Request $request){
$bill_Num = $request->get('bill_Num');

//$reportsData=DB::table('construction_reports')->where('saved_at','like','%'.$from_date.'%')->get();
$sql = "Select construction_reports.*, construction_material.mat_name from construction_reports,construction_material where construction_reports.material= construction_material.id And construction_reports.bill_num='$bill_Num' ;";
        
        
$reportsData= DB::select($sql);
$sum1=DB::table('construction_reports')->where('bill_num',$bill_Num)->sum('total');
$sum2=DB::table('construction_reports')->where('bill_num',$bill_Num)->sum('credit');
$sum3=DB::table('construction_reports')->where('bill_num',$bill_Num)->sum('payment');
$sum4=DB::table('construction_reports')->where('bill_num',$bill_Num)->sum('labour_payments');
 Session::put('reportsData', $reportsData);
 Session::put('sum1', $sum1);
 Session::put('sum2', $sum2);
 Session::put('sum3', $sum3);
 Session::put('sum4', $sum4);

 $logged_in_user = Auth::user();
 $logged_in_user = $logged_in_user->id;
 $routeName = url()->current();

 $audit_create = new Audit();
 $audit_create->user_id = $logged_in_user;
 $audit_create->action = "Search Construction Report";
 $audit_create->route = $routeName;
 $audit_create->save();
    return view('operator.construction.reports');
}
public function generatePDF()
    {
        $data = ['title' => 'Laravel 5.8 HTML to PDF'];
        $pdf = PDF::loadView('operator.construction.reports', $data);

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Generate PDF";
        $audit_create->route = $routeName;
        $audit_create->save();
        return $pdf->download('demonutslaravel.pdf');
    }
}
