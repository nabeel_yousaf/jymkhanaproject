<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;

use App\HotelBuyingItem;
use Illuminate\Http\Request;

class HotelBuyingItemController extends Controller
{
    public function index() {
        $items = HotelBuyingItem::all();
        return view('operator.Hotel.buying-item',compact('items'));
    }
    public function create() {
        return view('operator.Hotel.buying-item-create');
    }
    public function store(Request $request) {
        $this->validate($request,[
            'item_name'=>'required',
            'item_desc'=>'required',
       ]);
       HotelBuyingItem::create([
            'item_name' => $request->get('item_name'),
            'item_desc' => $request->get('item_desc')
       ]);
       return redirect(route('operator-buyingItem'));
    }

    public function data(Request $request) {
        $draw = $request->get('draw');
        $product_id = $request->get('id');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $data = HotelBuyingItem::buy_report($product_id,$draw,$startDate,$endDate);
        echo json_encode($data);
    }

    public function print(Request $request) {
        
        $draw = $request->get('draw');
        $product_id = $request->get('id');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $data = HotelBuyingItem::buy_report($product_id,$draw,$startDate,$endDate);
        $records = $data['data'];
        return view('operator.Hotel.print-buy-report',compact('records'));
    }
}
