<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;

use App\ChartOfAccount;
use App\Dimension;
use App\Member;
use App\VoucherMaster;
use Illuminate\Http\Request;
use Auth;
use App\Audit;

use function Psy\sh;

class ManageDetailedLedgerReportController extends Controller
{
    public function index()
    {
        $coa = ChartOfAccount::where('parent_id','=',0)->get();
        $accounts = array();
        foreach ($coa as $key => $value) {
            if($value->user_id != 1) {
                //User is not admin;
                $member = Member::where('user_id','=',$value->user_id)->first();
                if($member) {
                    $accounts[] = ['id' => $value->id, 'parent_id' => $value->parent_id, 'name'=>$value->name, 'membership_id'=>$member->membership_id];
                }
            }
            else {
                $accounts[] = ['id' => $value->id,'parent_id' => $value->parent_id, 'name'=>$value->name, 'membership_id'=> ''];
            }
            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Detailed Ledger Report";
            $audit_create->route = $routeName;
            $audit_create->save();
        }
        return view('operator.detailedLedger.index', compact('accounts'));
    }

    public function data(Request $request)
    {
        $draw = $request->get('draw');
        $parend_id = $request->get('parent-id');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $id = $request->get('id');
        $data = ChartOfAccount::detail_ledger($draw,$id,$parend_id,$startDate,$endDate);
        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Ledger Data";
            $audit_create->route = $routeName;
            $audit_create->save();
        echo json_encode($data);
        
    }


}
