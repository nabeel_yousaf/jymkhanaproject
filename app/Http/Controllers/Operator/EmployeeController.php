<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;

use App\balance;
use App\employee;
use App\employeePost;
use App\EmyloyeePay;
use App\Audit;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Image;
use Session;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_active=1;
        $count=1;
        $employees=employee::with('pay')->get();
        
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
 
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "All Employees";
        $audit_create->route = $routeName;
        $audit_create->save();
         return view('operator.employees.index', compact('employees','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu_active=1;
        $posts=employeePost::all();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Employee Create";
        $audit_create->route = $routeName;
        $audit_create->save();
         return view('operator.employees.create', compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'custom_id'=>'required',
            'email'=>'required|string|email|unique:employees,email',
            'address'=>'required',
            'phone'=>'required',
            'salary'=>'required',
            'city'=>'required',
            'cnic'=>'required',
            'status'=>'required',
            'image'=>'required',
            'password'=>'required|min:6|confirmed',
        ]);

        $input_data=$request->all();
        $employee = employee::create($input_data);

        $employeeSalary = new EmyloyeePay();
        $employeeSalary->employee_id = $employee->membership_id;
        $employeeSalary->paid_salary = $request->salary;
        $employeeSalary->save();
    
        Session::flash('success','You Successfully Added new employee !');
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Employee Store";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(employee $employee)
    {
        //
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Employee Show";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(employee $employee)
    {
        //
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Employee Edit";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employee $employee)
    {
        //
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Employee Update";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(employee $employee)
    {
        //
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Employee Delete";
        $audit_create->route = $routeName;
        $audit_create->save();
    }
}
