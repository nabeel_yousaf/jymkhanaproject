<?php

namespace App\Http\Controllers;

use App\Customer;
use App\MembersPayRecord;
use App\Audit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Image;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
     public function registered(){
        
        return view('members.register');
    }
    public function PaymentsHistory(){
        $allPaymentRecord= DB::table('customers')
            ->join('members_pay_records', 'members_pay_records.customer_id', '=', 'customers.id')
            ->select('customers.*', 'members_pay_records.*')
            ->get();

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
                
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Customer Payment Records";
            $audit_create->route = $routeName;
            $audit_create->save();
         return view('backEnd.members.payments', compact('allPaymentRecord'));
    }

    public function register(Request $request){
        $this->validate($request,[
            'name'=>'required|string|max:255',
             'email'=>'required|string|email|unique:customers,email',
             'address'=>'required',
             'phone'=>'required',
             'city'=>'required',
             'cnic'=>'required',
             'status'=>'required',
             'image'=>'required',
             'password'=>'required|min:6|confirmed',
         ]);
         $input_data=$request->all();
         if($request->file('image')){
            $image=$request->file('image');
            if($image->isValid()){
        $fileName=time().'-'.str_slug($input_data['name'],"-").'.'.$image->getClientOriginalExtension();
        $large_image_path=public_path('images/members/'.$fileName);
        $medium_image_path=public_path('images/members/medium/'.$fileName);
        //Resize Image
        Image::make($image)->save($large_image_path);
        Image::make($image)->resize(255,350)->save($medium_image_path);
        $input_data['image']=$fileName;
            }
        }
        //dd($input_data);
         $input_data['password']=Hash::make($input_data['password']);
        Customer::create($input_data);
      Session::flash('success','Register Successfully !');
      $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Customer Register";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }

    public function customerDetail($id)
    {
       $byCustomer=Customer::where('id',$id)->first();
       $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Customer Detail";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.members.detail',compact('byCustomer'));
    }
    public function CustomerUpdate(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'id',
        ]);
        $id=$request->input('id');
        $status=1;
        $result = DB::table('customers')
    ->where('id', $id)
    ->update([
        'status' => $status,
    ]);
      Session::flash('success','User are Active Now !');
      $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Customer Update";
        $audit_create->route = $routeName;
        $audit_create->save();
     return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $count=1;
        $customers=customer::all();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "All Customers";
        $audit_create->route = $routeName;
        $audit_create->save();
         return view('backEnd.members.index', compact('customers','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $customers=customer::all();
         $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Create Customer";
        $audit_create->route = $routeName;
        $audit_create->save();
         return view('backEnd.members.index', compact('customers','count'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function logout(Customer $customer)
    {
        
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Logout";
        $audit_create->route = $routeName;
        $audit_create->save();

        Auth::logout();
        return redirect('/login');
    }
}
