<?php

namespace App\Http\Controllers;

use App\MembersPayRecord;
use App\Member;
use Session;
use Illuminate\Http\Request;
use App\Audit;
use Auth;

class MembersPayRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_active=1;
        $count=1;
        $customers=Member::all();
        
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Member Pay Record";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('backEnd.members.index', compact('customers','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers=Member::all();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Member Pay Record Create";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.members.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'member_id'=>'required',
            'month'=>'required',
            'monthly_payment'=>'required',
            'Unique_id'=>'required',
        
        ]);
        $data=$request->all();
        //dd($data);
        MembersPayRecord::create($data);
        Session::flash('success','You Successfully Member Record !');

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Member Pay Record Store";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MembersPayRecord  $membersPayRecord
     * @return \Illuminate\Http\Response
     */
    public function show(MembersPayRecord $membersPayRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MembersPayRecord  $membersPayRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(MembersPayRecord $membersPayRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MembersPayRecord  $membersPayRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MembersPayRecord $membersPayRecord)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MembersPayRecord  $membersPayRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(MembersPayRecord $membersPayRecord)
    {
        //
    }
}
