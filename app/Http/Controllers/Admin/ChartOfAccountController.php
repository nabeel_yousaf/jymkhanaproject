<?php

namespace App\Http\Controllers\Admin;

use App\ChartAccountType;
use App\ChartOfAccount;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Audit;
use Auth;

class ChartOfAccountController extends Controller
{
    public function index()
    {
        //$data = ChartOfAccount::all();
        $data = ChartOfAccount::all();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Show Chart of Accounts";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view("backEnd.chartOfAccount.index",[
            'data' => $data
        ]);
    }

    public function create(){
        $chartAccountType = ChartAccountType::all();
        $chartAccount = ChartOfAccount::all();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Chart of Accounts Create";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view("backEnd.chartOfAccount.create",[
            'accountType' => $chartAccountType,
            'chartAccount' => $chartAccount
        ]);
    }

    public function store(Request $request){
        $accountType = ChartAccountType::where('id','=',$request->is_parent)->first();
        $parent_id = ChartOfAccount::where('id','=',$request->parent_id)->first();
        $is_parent = $request->is_parent;
        if(isset($parent_id)) {
            $is_parent = 0;
        }
        ChartOfAccount::create([
            'user_id' => 1,
            'name' => $request->name,
            'description' => $request->description,
            'type' => $accountType->name,
            'is_parent' => $is_parent,
            'parent_id' => isset($request->parent_id) ? $request->parent_id : 0,
        ]);

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Chart of Accounts Store";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->route('showChartOfAccount')->with('message','account chart created successfully');
    }

    public function edit($id){
        $chartAccountType = ChartAccountType::all();
        $chartAccount = ChartOfAccount::all();
        $data = ChartOfAccount::where('id','=',$id)->first();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Chart of Accounts Edit";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view("backEnd.chartOfAccount.edit",[
            'data' => $data,
            'accountType' => $chartAccountType,
            'chartAccount' => $chartAccount,
        ]);
    }

    public function update(Request $request , $id){
        $accountType = ChartAccountType::where('id','=',$request->is_parent)->first();
        $parent_id = ChartOfAccount::where('id','=',$request->parent_id)->first();
        $is_parent = $request->is_parent;
        if(isset($parent_id)) {
            $is_parent = 0;
        }
        ChartOfAccount::where('id','=',$id)->update([
            'user_id' => 1,
            'name' => $request->name,
            'description' => $request->description,
            'type' => $accountType->name,
            'is_parent' => $is_parent,
            'parent_id' => isset($request->parent_id) ? $request->parent_id : 0,
        ]);

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Chart of Accounts Update";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->route('showChartOfAccount')->with('message','account chart updated successfully');
    }

    public function delete($id){
        $delete1 = ChartOfAccount::where('id','=',$id)->first();
        if(!is_null($delete1)){
            $delete = ChartOfAccount::where('id','=',$delete1->parent_id)->first();
            if(is_null($delete))
            {
                $delete = ChartOfAccount::where('parent_id','=',$delete1->id)->delete();
            } else {
                $delete = ChartOfAccount::where('id','=',$delete->parent_id)->delete();
            }
        }
        ChartOfAccount::where('id','=',$id)->delete();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Chart of Accounts Delete";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back()->with('error','account chart deleted successfully');
    }
}
