<?php

namespace App\Http\Controllers;

use App\OrderMaster;
use App\OrderDetail;
use App\Product;
use App\Member;
use App\employee;
use Illuminate\Http\Request;
use Carbon\Carbon;
use function Psy\sh;
use Illuminate\Support\Collection; 

class HotelSaleReportController extends Controller
{
    public function index(Request $request) {
        // $final_data = OrderMaster::with(['memberable:membership_id,name,email','order_details.product'])->first();
        // dd($final_data);
        $items = Product::all();
        return view('backEnd.Hotel.sale-report',compact('items'));
    }
    
    public function data(Request $request) {
        $draw = $request->get('draw');
        $product_id = $request->get('id');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $memberId = $request->get('memberId');
        $data = OrderMaster::sale_report($product_id,$draw,$startDate,$endDate,$memberId);
        echo json_encode($data);
    }

    public function addhotelsale(Request $request){

        $items     = Product::all();
        $members   = Member::all(); 
        $employees = employee::all(); 
    
        return view('backEnd.Hotel.AddSaleRecord',compact('items','members','employees'));

    }

    public  function savehotelsale(Request $request){

        $this->validate($request,[
            'order_date'=>'required',
            'products' => 'required'
        ]);
 
        $collection = new Collection($request->products); 
        $collection =  $collection->groupBy('memberable_type');

        $employees = isset($collection['employee']) ? new Collection($collection['employee']) : null; 
        $members   = isset($collection['member']) ? new Collection($collection['member']) : null;

        $employees = isset($employees) ? $employees->groupBy('memberable_id') : null;
        $members    = isset($members)    ? $members->groupBy('memberable_id')    : null;

         
        // first create members order
        if($members){
            foreach($members as $key=>$orders){
                $orderMaster = new OrderMaster();
                $orderMaster->memberable_id = $key;    
                $orderMaster->memberable_type = 'member';    
                $orderMaster->date_time = Carbon::parse($request->order_date);    
                $orderMaster->save();
    
                $order_detail = array();
                foreach($orders as $product){
                    $data['product_id'] = $product['product_id'];
                    $data['quantity'] = $product['quantity'];
                    $data['order_master_id'] = $orderMaster->id;
                    $data['created_at'] = Carbon::parse($request->order_date);
                    $data['updated_at'] = Carbon::parse($request->order_date);
                    array_push($order_detail,$data);
                }
                
                OrderDetail::insert($order_detail);
            } 
        }

        // create employee order
        if($employees){
            foreach($employees as $key=>$orders){
                $orderMaster = new OrderMaster();
                $orderMaster->memberable_id = $key;    
                $orderMaster->memberable_type = 'employee';    
                $orderMaster->date_time = Carbon::parse($request->order_date);    
                $orderMaster->save();
    
                $order_detail = array();
                foreach($orders as $product){
                    $data['product_id'] = $product['product_id'];
                    $data['quantity'] = $product['quantity'];
                    $data['order_master_id'] = $orderMaster->id;
                    $data['created_at'] = Carbon::parse($request->order_date);
                    $data['updated_at'] = Carbon::parse($request->order_date);
                    array_push($order_detail,$data);
                }
                
                OrderDetail::insert($order_detail);
            }
        } 

        
        

        $response['success'] = true;
        $response['error'] = false;
        $response['message'] = 'data created!';

        return $response;


    }

     

    public function editSale(Request $request, OrderMaster $order){
        $items = Product::all();
        $members   = Member::all(); 
        $employees = employee::all();  
        
        return view('backEnd.Hotel.editSale', compact('items','members','employees','order'));
    }

    public function updateSaleRecord(Request $request, OrderMaster $order){

        $this->validate($request,[
            'order_date'=>'required',
            'products' => 'required'
        ]);
 
        $collection = new Collection($request->products); 
        $collection =  $collection->groupBy('memberable_type');

        $employees = isset($collection['employee']) ? new Collection($collection['employee']) : null; 
        $members   = isset($collection['member']) ? new Collection($collection['member']) : null;

        $employees = isset($employees) ? $employees->groupBy('memberable_id') : null;
        $members    = isset($members)    ? $members->groupBy('memberable_id')    : null;

         
        // first create members order
        if($members){
            foreach($members as $key=>$orders){
                $orderMaster = new OrderMaster();
                $orderMaster->memberable_id = $key;    
                $orderMaster->memberable_type = 'member';    
                $orderMaster->date_time = Carbon::parse($request->order_date);    
                $orderMaster->save();
    
                $order_detail = array();
                foreach($orders as $product){
                    $data['product_id'] = $product['product_id'];
                    $data['quantity'] = $product['quantity'];
                    $data['order_master_id'] = $orderMaster->id;
                    $data['created_at'] = Carbon::parse($request->order_date);
                    $data['updated_at'] = Carbon::parse($request->order_date);
                    array_push($order_detail,$data);
                }
                
                OrderDetail::insert($order_detail);
            } 
        }

        // create employee order
        if($employees){
            foreach($employees as $key=>$orders){
                $orderMaster = new OrderMaster();
                $orderMaster->memberable_id = $key;    
                $orderMaster->memberable_type = 'employee';    
                $orderMaster->date_time = Carbon::parse($request->order_date);    
                $orderMaster->save();
    
                $order_detail = array();
                foreach($orders as $product){
                    $data['product_id'] = $product['product_id'];
                    $data['quantity'] = $product['quantity'];
                    $data['order_master_id'] = $orderMaster->id;
                    $data['created_at'] = Carbon::parse($request->order_date);
                    $data['updated_at'] = Carbon::parse($request->order_date);
                    array_push($order_detail,$data);
                }
                
                OrderDetail::insert($order_detail);
            }
        } 

        //delete previoud record
        $order->delete(); 
        

        $response['success'] = true;
        $response['error'] = false;
        $response['message'] = 'data created!';

        return $response;


    }

    public function deleteSaleRecord(Request $request, OrderMaster $order){

        //first delete all items
        OrderDetail::where('order_master_id',$order->id)->delete();
        
        $order->delete();

        $response['success'] = true;
        $response['error'] = false;
        $response['message'] = 'order deleted!';

        return $response;

    }

    public function print(Request $request) {

        $draw = $request->get('draw');
        $product_id = $request->get('id');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $memberId = $request->get('memberId');
        $data = OrderMaster::sale_report($product_id,$draw,$startDate,$endDate,$memberId);
        $records = $data['data'];
        return view('backEnd.Hotel.print-sale-report',compact('records'));
    }
}
