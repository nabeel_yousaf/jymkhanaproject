<?php

namespace App\Http\Controllers\Secertry;

use App\OrderMaster;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;
use Auth;
use App\Audit;

class MemberController extends Controller
{
    public function index()
    {
        $data = OrderMaster::with(['member' => function($q){
            $q->select('membership_id','name');
            },'order_details','order_details.product'
            ])->whereHas('member', function ($q){
                $q->select('membership_id','name');
            })->get();
        $arr = [];
        foreach($data as $val) {
            $values= array(
                'order_id' => $val->id,
                'member_id'=>$val->member['membership_id'],
                'name'=>$val->member['name'],
                'date_time' => $val->date_time,
                'total_products' => count($val->order_details),
                'product_price' => [],
            );
            foreach($val->order_details as $details) {
                $price = Product::where('id','=',$details->product_id)->first();
                array_push($values['product_price'],$price->price);
            }
            array_push($arr,$values);
        }
        return view("secertry.memberOrderList.index",[
            'data' => $data,
            'member' => $arr
        ]);
    }

    public function productShow($id)
    {
        $orderMaster = OrderMaster::with(['order_details','order_details.product'])->where('id','=',$id)->get();
        return collect([
            'success' => true,
            'data' => $orderMaster
        ]);
    }
    
    public function customerDetail($id)
    {
        $byCustomer = Member::where('membership_id', $id)->first();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Customer Detail";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('secertry.members.detail', compact('byCustomer'));
    }
}
