<?php

namespace App\Http\Controllers\Secertry;
use App\Http\Controllers\Controller;

use App\UserDimVmInitialEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Member;
use App\MemberPending;
use App\VoucherMaster;
use App\VoucherDetail;
use App\ChartOfAccount;
use App\Dimension;
use Image;
use App\User;
use Auth;
use App\Audit;

class editprofile extends Controller
{
    public function index(){
        $byCustomer=MemberPending::all();
        return view('secertry.pendingApproval.index',compact('byCustomer'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[

        'image'=>'nullable|image|mimes:png,jpg,jpeg|max:2000'
        ]);
        if($request->hasfile('image')){
            $image=$request->file('image');
            $fileName = $request->file('image')->getClientOriginalName();
            $path = 'images/members_image';
            // create instance
            $img = Image::make($request->file('image'));
            $img->resize(300,250);
            $request->file('image')->move($path,$fileName);

        }
        $member = MemberPending::where('membership_id', $id)->first();

            

            Member::where('membership_id', $id)->update([
                'membership_id'=> $member->membership_id,
                'name'=>isset($request->name) ? $request->name : $member->name,
                'father_name'=>isset($request->fname) ? $request->fname : $member->father_name,
                'MaritaLStatus'=>isset($request->maritalstatus) ? $request->maritalstatus : $member->MaritaLStatus,
                'BloodGroup'=>isset($request->blood_group) ? $request->blood_group : $member->BloodGroup,
                'email'=>isset($request->Email) ? $request->Email : $member->email,
                'password'=>isset($request->password) ? Hash::make($request->password) : $member->password,
                'category'=>isset($request->Category) ? $request->Category : $member->category,
                'Cnic'=>isset($request->cnic) ? $request->cnic : $member->Cnic,
                'Address'=>isset($request->address) ? $request->address : $member->Address,
                'TemporaryAddress'=>isset($request->taddress) ? $request->taddress : $member->TemporaryAddress,
                'cell_no'=>isset($request->Cell_no) ? $request->Cell_no : $member->cell_no,
                'DOB'=>isset($request->dob) ? $request->dob : $member->DOB,
                'Qualification'=>isset($request->qualification) ? $request->qualification : $member->Qualification,
  
                'registration_fee'=>isset($request->registration_fee) ? $request->registration_fee : $member->registration_fee,
  
                'membership_fee'=>isset($request->membership_fee) ? $request->membership_fee : $member->membership_fee,
  
                'monthly_sub_fee'=>isset($request->monthly_sub_fee) ? $request->monthly_sub_fee : $member->monthly_sub_fee,
                'user_id'=>$member->user_id,
                'NameOfOrganization'=>isset($request->Name_of_Organization) ? $request->Name_of_Organization : $member->NameOfOrganization,
                'image'=>isset($fileName) ? $fileName : $member->image,
                'Designation'=>isset($request->designation) ? $request->designation : $member->Designation,
                'BuisnessAddress'=>isset($request->businessmobile) ? $request->businessmobile : $member->BuisnessAddress,
                'BuisnessMobile'=>isset($request->buisnessadress) ? $request->buisnessadress : $member->BuisnessMobile,
                'Relation'=>isset($request->relation) ? $request->relation : $member->Relation,
                'SName'=>isset($request->sname) ? $request->sname : $member->SName,
                'SCnic'=>isset($request->scnic) ? $request->scnic : $member->SCnic,
                'SDob'=>isset($request->sdob) ? $request->sdob : $member->SDob,
                'remarks'=>isset($request->remarks) ? $request->remarks : $member->remarks,
                'approval'=> "approved",
            ]);

            MemberPending::where('membership_id', '=', $id)->delete();
        

        Session::flash('success','update Successfully !');
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Update Profile";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }

}
