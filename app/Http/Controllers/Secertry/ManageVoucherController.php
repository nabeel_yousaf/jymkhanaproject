<?php

namespace App\Http\Controllers\Secertry;
use App\Http\Controllers\Controller;
use App\VoucherDetail;
use App\VoucherMaster;
use App\VoucherDetailPending;
use App\VoucherMasterPending;
use App\ChartOfAccount;
use App\Dimension;
use App\Member;
use App\Audit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Session;
use Validator;
use Auth;

class ManageVoucherController extends Controller
{
   
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date'=>'required',
            'vouchertype'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        Session::forget('voucher_master_id');
        $id = $request->id;
        $voucherMaster = VoucherMaster::where('id', $id)->first();
        $voucherMaster->voucher_type = $request->vouchertype;
        $voucherMaster->voucher_date = $request->date;
        $voucherMaster->save();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Voucher Saved";
        $audit_create->route = $routeName;
        $audit_create->save();

        return response()->json();
    }


    public function addVoucher($id){


        $master_record = VoucherMasterPending::where('id',$id)->first();

        $voucherMaster = new VoucherMaster();
        $voucherMaster->voucher_type = $master_record->voucher_type;
        $voucherMaster->voucher_date = $master_record->voucher_date;
        $voucherMaster->save();
        
        $master_record_id = $voucherMaster->id;

        VoucherMasterPending::where('id','=',$id)->delete();

        $detail_record = VoucherDetailPending::where('voucher_master_id', $master_record->id)->get();

        foreach ($detail_record as $detail_record) {
            $voucherDetail = new VoucherDetail();
        $voucherDetail->voucher_master_id = $master_record_id;
        $voucherDetail->coa_id = $detail_record->coa_id;
        $voucherDetail->narration = $detail_record->narration;
        $voucherDetail->debit = $detail_record->debit;
        $voucherDetail->credit = $detail_record->credit;
        $voucherDetail->dim_id = $detail_record->dim_id;
        $voucherDetail->save();

        VoucherDetailPending::where('id','=',$detail_record->id)->delete();
        }
        

        return redirect()->back();

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(VoucherMaster $VoucherMaster)
    {
        //return view('backEnd.balance.index', compact('bal'));
        $voucher = VoucherMasterPending::join('voucher_details_pending', 'voucher_masters_pending.id', '=', 'voucher_details_pending.voucher_master_id')
            ->join('chart_of_accounts','chart_of_accounts.id','voucher_details_pending.coa_id')
            ->where('voucher_masters_pending.is_active' ,'=', 1)
            ->select('chart_of_accounts.user_id','voucher_masters_pending.*','chart_of_accounts.name as coa_name', DB::raw('SUM(voucher_details_pending.debit) as debit'), DB::raw('SUM(voucher_details_pending.credit) as credit'))         
            ->groupBy('voucher_details_pending.voucher_master_id')   
            ->get();
            
        
        
            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "View All Voucher";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('secertry.voucherApproval.index', compact('voucher'));
    }

    public function viewVoucher($id)
    {
        $master_record = VoucherMasterPending::join('voucher_details_pending', 'voucher_masters_pending.id', '=', 'voucher_details_pending.voucher_master_id')
            ->join('chart_of_accounts','chart_of_accounts.id','voucher_details_pending.coa_id')
            ->join('dimensions','dimensions.id','voucher_details_pending.dim_id')
            ->where('is_active' ,'=', 1)
            ->where('voucher_masters_pending.id' , $id)
            ->where('voucher_details_pending.voucher_master_id' , $id)
            ->select('chart_of_accounts.user_id', 'voucher_masters_pending.*', 'dimensions.dim_name', 'chart_of_accounts.name as coa_name', 'voucher_details_pending.narration', 'voucher_details_pending.debit', 'voucher_details_pending.credit')
            ->get();
        
            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "View Voucher Detail";
            $audit_create->route = $routeName;
            $audit_create->save();
        return response()->json($master_record);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = VoucherMaster::all()->where('id','=',$id );
        $voucher_detail = VoucherDetail::all();
        $voucher_coa = DB::table('chart_of_accounts')
            ->join('voucher_details', 'voucher_details.coa_id', '=', 'chart_of_accounts.id')
            ->select('chart_of_accounts.name', 'voucher_details.*')
            ->get();
        $coa = ChartOfAccount::all()->where('is_parent' ,'=', 0);

        $accounts = array();
        foreach ($coa as $key => $value) {
            if($value->user_id != 1) {
                //User is not admin;
                $member = Member::where('user_id','=',$value->user_id)->first();
                if($member){
                    $accounts[] = ['id' => $value->id, 'name'=>$value->name, 'membership_id'=>$member->membership_id];
                }
            }
            else {
                $accounts[] = ['id' => $value->id, 'name'=>$value->name, 'membership_id'=> ''];
            }
        }
        $dim = Dimension::all();

        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Voucher Edit";
            $audit_create->route = $routeName;
            $audit_create->save();

        return view('operator.voucher.edit', compact('coa','dim','accounts','voucher_coa','edit','voucher_detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
}
