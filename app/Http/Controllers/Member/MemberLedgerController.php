<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Auth;
class MemberLedgerController extends Controller
{
    public function index() {
    $user_id = Auth()->user()->id;
    $openingbal = DB::table('balances')->join('chart_of_accounts','balances.coa_id','=','chart_of_accounts.id')
        ->where('chart_of_accounts.user_id','=',$user_id)->select(
            'balances.*','chart_of_accounts.name')->get();

        return view('member.ledger.index',compact('openingbal'));
    }
}
