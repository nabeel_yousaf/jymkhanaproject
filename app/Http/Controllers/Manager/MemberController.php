<?php

namespace App\Http\Controllers\Manager;

use App\OrderMaster;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    public function index()
    {
        $data = OrderMaster::with(['member' => function($q){
            $q->select('membership_id','name');
            },'order_details','order_details.product'
            ])->whereHas('member', function ($q){
                $q->select('membership_id','name');
            })->get();
        $arr = [];
        foreach($data as $val) {
            $values= array(
                'order_id' => $val->id,
                'member_id'=>$val->member['membership_id'],
                'name'=>$val->member['name'],
                'date_time' => $val->date_time,
                'total_products' => count($val->order_details),
                'product_price' => [],
            );
            foreach($val->order_details as $details) {
                $price = Product::where('id','=',$details->product_id)->first();
                array_push($values['product_price'],$price->price);
            }
            array_push($arr,$values);
        }
        return view("manager.memberOrderList.index",[
            'data' => $data,
            'member' => $arr
        ]);
    }

    public function productShow($id)
    {
        $orderMaster = OrderMaster::with(['order_details','order_details.product'])->where('id','=',$id)->get();
        return collect([
            'success' => true,
            'data' => $orderMaster
        ]);
    }
}
