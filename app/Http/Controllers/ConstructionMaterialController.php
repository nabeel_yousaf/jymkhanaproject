<?php

namespace App\Http\Controllers;

use App\ConstructionMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Audit;
use Auth;

class ConstructionMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $constructionMaterial=ConstructionMaterial::all();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Construction Material";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.construction.material_create', compact('constructionMaterial'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'mat_name'=>'required',
        ]);
        $material=$request->input('mat_name');
        DB::table('construction_material')->insert(
            ['mat_name' => $material,'created_at'=>now(),'updated_at'=>now()]
        );
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Construction Material Store";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConstructionMaterial  $constructionMaterial
     * @return \Illuminate\Http\Response
     */
    public function show(ConstructionMaterial $constructionMaterial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConstructionMaterial  $constructionMaterial
     * @return \Illuminate\Http\Response
     */
    public function edit(ConstructionMaterial $constructionMaterial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConstructionMaterial  $constructionMaterial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConstructionMaterial $constructionMaterial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConstructionMaterial  $constructionMaterial
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConstructionMaterial $constructionMaterial)
    {
        //
    }
}
