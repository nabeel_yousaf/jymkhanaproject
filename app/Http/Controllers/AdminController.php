<?php

namespace App\Http\Controllers;

use App\ChartOfAccount;
use App\Constants;
use App\User;
use App\VoucherDetail;
use App\VoucherMaster;
use App\Audit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index(){
       // return "Under Development";
       $menu_active=1;
        if(Auth::check()) {
            $memRecVoucher = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
            ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
            ->where('chart_of_accounts.parent_id','=',54)
            ->where('voucher_masters.is_active', '=', 1)
            ->selectRaw('sum(voucher_details.debit) as debit_sum,sum(voucher_details.credit) as credit_sum')->get();
            $memRevVoucher = ChartOfAccount::join('voucher_details','chart_of_accounts.id','=','voucher_details.coa_id')
            ->where('chart_of_accounts.parent_id','=',870)
            ->selectRaw('sum(voucher_details.debit) as debit_sum,sum(voucher_details.credit) as credit_sum')->get();
            $bankAccount = ChartOfAccount::where('id','=',Constants::$bank_acc_id)->first();
            $bankData = ChartOfAccount::ledger(1,$bankAccount->parent_id,0,'','');
            $data['memberDrAmount'] = $memRecVoucher[0]['debit_sum'];
            $data['memberCrAmount'] = $memRecVoucher[0]['credit_sum'];
            $data['memberAcountName'] = 'Members A/R';
            $data['revenueDrAmount'] = $memRevVoucher[0]['debit_sum'];
            $data['revenueCrAmount'] = $memRevVoucher[0]['credit_sum'];
            $data['revenueAcountName'] = 'Service Revenue';
            return view('backEnd.index',compact('menu_active','data','bankData'));
            }
      else {
            return redirect('login');
        }
    }
    public function settings(){
        $menu_active=0;
        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Admin Settings";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('backEnd.layouts.setting',compact('menu_active'));
    }
    public function chkPassword(Request $request){
        $data=$request->all();
        $current_password=$data['pwd_current'];
        $email_login=Auth::user()->email;
        $check_pwd=User::where(['email'=>$email_login])->first();
        if(Hash::check($current_password,$check_pwd->password)){
            echo "true"; die();
        }else {
            echo "false"; die();
        }
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Check Password";
        $audit_create->route = $routeName;
        $audit_create->save();
    }
    public function updatAdminPwd(Request $request){
        $data=$request->all();
        $current_password=$data['pwd_current'];
        $email_login=Auth::user()->email;
        $check_password=User::where(['email'=>$email_login])->first();
        if(Hash::check($current_password,$check_password->password)){
            $password=bcrypt($data['pwd_new']);
            User::where('email',$email_login)->update(['password'=>$password]);
            return redirect('/admin/settings')->with('message','Password Update Successfully');
        }else{
            return redirect('/admin/settings')->with('message','InCorrect Current Password');
        }
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Update Password";
        $audit_create->route = $routeName;
        $audit_create->save();
    }





    /*public function login(Request $request){
        if($request->isMethod('post')){
            $data=$request->input();
            if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password'],'admin'=>'1'])){
                echo 'success'; die();
            }else{
                return redirect('admin')->with('message','Account is Incorrect!');
            }
        }else{
            return view('backEnd.login');
        }
    }*/
}
