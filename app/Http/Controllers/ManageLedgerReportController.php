<?php

namespace App\Http\Controllers;

use App\ChartOfAccount;
use App\Dimension;
use App\Member;
use App\VoucherMaster;
use Illuminate\Http\Request;
use Auth;
use App\Audit;

use function Psy\sh;

class ManageLedgerReportController extends Controller
{
    public function index()
    {
        $coa = ChartOfAccount::all();
        $accounts = array();
        foreach ($coa as $key => $value) {
            if($value->user_id != 1) {
                //User is not admin;
                $member = Member::where('user_id','=',$value->user_id)->first();
                if($member) {
                    $accounts[] = ['id' => $value->id, 'parent_id' => $value->parent_id, 'name'=>$value->name, 'membership_id'=>$member->membership_id];
                }
            }
            else {
                $accounts[] = ['id' => $value->id,'parent_id' => $value->parent_id, 'name'=>$value->name, 'membership_id'=> ''];
            }
            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Ledger Report";
            $audit_create->route = $routeName;
            $audit_create->save();
        }
        return view('backEnd.ledger.index', compact('accounts'));
    }

    public function data(Request $request)
    {
        $draw = $request->get('draw');
        $parend_id = $request->get('parent-id');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $id = $request->get('id');
        $data = ChartOfAccount::ledger($draw,$id,$parend_id,$startDate,$endDate);
        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Ledger Data";
            $audit_create->route = $routeName;
            $audit_create->save();
        echo json_encode($data);
        
    }

    public function member_fee_cleared() {
        $balance = ChartOfAccount::member_balance();
        $recieved = $balance['recieved'];
        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Member Fee Cleared";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('backEnd.ledger.member-fee-cleared',compact('recieved'));
    }

    public function member_fee_pending() {
        $balance = ChartOfAccount::member_balance();
        $pending = $balance['pending'];
        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Member Fee Pending";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('backEnd.ledger.member-fee-pending',compact('pending'));
    }

    public function print(Request $request) {
        $coa_id = $request->coaId;
        $start_date = $request->startDate;
        $end_date = $request->endDate;
        $account = ChartOfAccount::where('id','=',$coa_id)->first();
        if($account) {
            $parend_id = $account->parent_id;
        }
        $data = ChartOfAccount::ledger(1,$coa_id,$parend_id,$start_date,$end_date);
        $records = $data['data'];

        $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
            
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Ledger Print";
            $audit_create->route = $routeName;
            $audit_create->save();
        
        return view('backEnd.ledger.print',compact('records'));
    }
}
