<?php

namespace App\Http\Controllers;
use App\Member;
use Illuminate\Http\Request;
use App\hotelreports;
use App\hotel_buying;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\MySqlConnection;
use Illuminate\Support\Facades\Session;
use App\Audit;
use App\ChartOfAccount;
use App\HotelBuying;
use App\HotelBuyingItem;
use App\UsedKitchen;
use Auth;
use TNkemdilim\MoneyToWords\Converter;


class HotelReport extends Controller
{
    public function BillAdd(){
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Add Bill";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.Hotel.index');

    }
    public function searchmember(Request $request){
        $id = $request->get('membership');
        $reportsData=DB::table('members_record')->where('membership_id','=',$id)->get();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Search Member Id in Bill";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('backEnd.members.bill',compact('reportsData'));
    }

    public function Storebill(Request $request,$id){
        $result = $request->get('members_id');

        $reportsData1=DB::table('members_record')->where('membership_id','=',$id)->get();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Add Bill to member";
        $audit_create->route = $routeName;
        $audit_create->save();

        return View('backEnd.members.billadd',compact('reportsData1'));
    }
    public function buying(){

        $allbuyingreport=HotelBuying::all();

        $allbuyingreport=HotelBuying::join('hotel_buying_items','hotel_buyings.hotel_buying_id','=','hotel_buying_items.id')
        ->select('hotel_buyings.*','hotel_buying_items.item_name')->get();
        $logged_in_user = Auth::user();
        $items = HotelBuyingItem::all();
        
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "All Buying report";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('backEnd.Hotel.buyingg',compact('allbuyingreport','items'));

    }

    public function buyingEdit(HotelBuying $hotel){

        $allbuyingreport=HotelBuying::all();
        $items = HotelBuyingItem::all();
        
        return view('backEnd.Hotel.editBuying', compact('allbuyingreport','items','hotel'));
    }

    public function buyingUpdate(HotelBuying $hotel, Request $request){

        $request->validate([
            'vendor'=>'required',
            'buy_date'=>'required',
            'bill_no'=>'required',
            'quantity'=>'required',
            'Unit'=>'required',
            'total'=>'required',
            'quantity'=>'required',
            'credit'=>'required',
            'payment'=>'required',
            'Kitchen'=>'required',
            'type'=>'required',
            'hotel_buying_id'=>'required'
        ]);

        $hotel->vendor = $request->vendor;
        $hotel->buy_date = $request->buy_date;
        $hotel->bill_no = $request->bill_no;
        $hotel->quantity = $request->quantity;
        $hotel->Unit = $request->Unit;
        $hotel->total = $request->total;
        $hotel->quantity = $request->quantity;
        $hotel->credit = $request->credit;
        $hotel->payment = $request->payment;
        $hotel->Kitchen = $request->Kitchen;
        $hotel->type = $request->type;
        $hotel->hotel_buying_id = $request->hotel_buying_id;

        $hotel->save();

        Session::flash('success','You Successfully Updated The Hotel Buying!');
        return redirect()->back();
    }

    public function buyingDelete(HotelBuying $hotel){
        $hotel->delete();
    }

    public function inventory(){
        $allbuyingreport=HotelBuying::all();
        

        $allbuyingreport = UsedKitchen::with('hotelBuying.hotel_buying_items')->get();
 
        // dd($allbuyingreport);
        $logged_in_user = Auth::user();
        $items = HotelBuyingItem::all();
        
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "All Buying report";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('backEnd.Hotel.inventory',compact('allbuyingreport','items'));
    }   

    public function Menu(){

        $hotelmenu=DB::table('hotel_menu')
            ->join('categories','hotel_menu.cate_id','=','categories.cat_id')->get();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Hotel Menu";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('backEnd.Hotel.hotelmenu',compact('hotelmenu'));

    }
    public function buyingsave(Request $request){

        $this->validate($request,[
            'HB_Material'=>'required',
            'Quantity'=>'required',
            'Total'=>'required',
            'Credits'=>'required',
            'Peyment'=>'required',

        ]);
        $created_date=now();
        $save_date=$created_date->format('d/m/Y');
        $material=$request->input('material');
        $qty=$request->input('qty');
        $total_price=$request->input('unit_price');
        $credit=$request->input('credit');
        $payment=$request->input('payment');

        DB::table('hotel_buying')->insert(
        ['HB_Material' => $material,'Quantity'=>$qty,'Total'=>$unit_price,'Credits'=>$credit,'Peyment'=>$payment,'created_at'=>$save_date]
        );
        Session::flash('success','You Successfully Add The Post !');
        return redirect()->back();

    }
    public function buyingadd(){

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Add Buying";
        $audit_create->route = $routeName;
        $audit_create->save();
        $items = HotelBuyingItem::all();
        
        return view('backEnd.Hotel.addbuying',compact('items'));
}
    // public function uploadbill(Request $request){
    // $created=now();

    //     return view('backEnd.Hotel.addbuying');
    // }
    public function uploadbill(Request $request)
    {

        $created=now();

        $save_date=$created->format('d/m/Y');
        $first_name = $request->input('title');
        $last_name = $request->input('billamount');

        $srch = $request->input('srch');


        $totbl = $request->input('totbl');
        $city_name = $request->input('discount');
        $email = $request->input('credit');
        $email1 = $request->input('paid');
        $email2= $request->input('bill-no');
        DB::table('hotel_report')->insert(['members_id'=>$first_name,'Billamount'=>$last_name,'Service Charges'=>$srch,'Total'=>$totbl,'Discount'=>$city_name,'Credit'=>$email,'Paid'=>$email1,'Bill_no'=>$email2,'date'=>$save_date,'created_at'=>now(),'updated_at'=>now()]);
        Session::flash('success','Register Successfully !');

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Upload Bill";
        $audit_create->route = $routeName;
        $audit_create->save();

        return redirect()->back();
    }
    public function showtotal()
    {

        $recod=DB::table('hotel_report')
            ->join('members_record','hotel_report.members_id','=','members_record.membership_id')->get();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Show Hotel Total";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.members.hotelRecords',compact('recod'));
    }
    public function genbills(Request $request)
    {
        $converter = new Converter("Rupees", "paisa");
        $membership_id = $request->get('genbbill');
        $recod = Member::where('membership_id','=',$membership_id)->first();
        if($recod) {
            $account = ChartOfAccount::where('user_id','=',$recod->user_id)->first();
        }
        $id = $account->id;
        $data = ChartOfAccount::ledger('',$id,$account->parent_id,'','');
        $endRow = end($data['data']);
        $pending_balance = $endRow['balance'];
        $balance = str_replace(',', '', $pending_balance);
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Generate Bills";
        $audit_create->route = $routeName;
        $audit_create->save();
        $amount_in_words = $converter->convert($balance);
        return view('backEnd.members.BilGenerate',compact('pending_balance','recod','amount_in_words'));
    }
    public function GenBill()
    {

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Generate Bill";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('backEnd.members.voutcher');
    }
    public function showbill(Request $request){

        $id=$request->input('memberid');
        $record=DB::table('hotel_report')
        ->where('membership_id','=',$id)
        ->join('members_record','hotel_report.members_id','=','members_record.membership_id')->get();

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();

        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Search Member Bill";
        $audit_create->route = $routeName;
        $audit_create->save();

        return view('backEnd.Hotel.membertotalbill',compact('record'));
    }

}
