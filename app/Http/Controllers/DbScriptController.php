<?php

namespace App\Http\Controllers;

use App\ChartOfAccount;
use App\ChartOfAccountDimension;
use App\Constants;
use App\Dimension;
use App\Member;
use App\User;
use App\UserDimVmInitialEntry;
use App\VoucherDetail;
use App\VoucherMaster;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Response;
use App\Audit;
use Auth;
use DateTime;
use function Psy\sh;

class DbScriptController extends Controller
{
    public function create_member_ob()
    {
        $users = DB::table('users')
            ->join('chart_of_accounts', 'users.id', '=', 'chart_of_accounts.user_id')
            ->join('members_record', 'users.id', '=', 'members_record.user_id')
            ->select('chart_of_accounts.id', 'users.role_id', 'members_record.membership_id')
            ->get();
        $filename = "UserCOA.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('coa_id', 'membership_id'));
        foreach ($users as $row) {
            if ($row->role_id == 3) {
                fputcsv($handle, array($row->id, $row->membership_id));
            }
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );
        
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Create Member OB";
        $audit_create->route = $routeName;
        $audit_create->save();

        return Response::download($filename, 'UserCOA.csv', $headers);
    }

    public function inser_ob_records()
    {
        $fileName = storage_path('/app/ob-members-data.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $member_id = $column[0];
            $member = DB::table('members_record')->where('membership_id', '=', $member_id)->first();
            if ($member) {
                $coa = DB::table('chart_of_accounts')->where('user_id', '=', $member->user_id)->first();
                $amount = $column[1];
                $data = array('coa_id' => $coa->id, "description" => "N/A", "debit" => 0, "credit" => $amount);
                DB::table('balances')->insert($data);
                echo "Inserted";
            }
        }
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Insert OB Records";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function insert_joining_date() {
        $fileName = storage_path('/app/joiningDate.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $member_id = $column[0];
            $member = Member::where('membership_id','=',$member_id)->first();
            if ($member) {
                $excel_date = $column[1]; //here is that value 41621 or 41631
                $unix_date = ($excel_date - 25569) * 86400;
                $excel_date = 25569 + ($unix_date / 86400);
                $unix_date = ($excel_date - 25569) * 86400;
                $member->JoiningDate = date("Y-m-d", $unix_date);
                $member->save();
                echo "Inserted <br>";
            }
        }
        echo "Finished Script<br>";
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Insert Joining Date Records By Script";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function insert_dimentions_record()
    {
        Dimension::create([
            'dim_name' => 'Membership Fee',
        ]);
        Dimension::create([
            'dim_name' => 'Registration Fee',
        ]);
        Dimension::create([
            'dim_name' => 'Monthly Subscription Fee',
        ]);
        echo "Inserted";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Insert Dimension Records";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function insert_coa_dimensions_record()
    {
        $accounts = ChartOfAccount::where('parent_id', '=', 54)->get();
        foreach ($accounts as $key => $value) {
            $dimensions = Dimension::all();
            foreach ($dimensions as $dim => $dimVal) {
                ChartOfAccountDimension::create([
                    'coa_id' => $value->id,
                    'dim_id' => $dimVal->id
                ]);
            }
        }
        echo "Inserted";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Insert COA Dimensions Records";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function insert_vouchers_record()
    {
        $fileName = storage_path('/app/membership-fee-dim-inserted.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $member_id = $column[0];
            $member = DB::table('members_record')->where('membership_id', '=', $member_id)->first();
            if ($member) {
                if ($column[1] != 0) {
                    $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                    if ($member_acc) {
                        $master_id = VoucherMaster::create([
                            'voucher_type' => 'Journal Voucher',
                            'voucher_date' => '2019-10-1',
                        ]);
                        VoucherDetail::create([
                            'coa_id' => $member_acc->id,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Oct Fee Debited',
                            'debit' => $column[1],
                            'credit' => 0,
                            'dim_id' => 3,
                        ]);
                        VoucherDetail::create([
                            'coa_id' => 871,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Service Revenue Oct Fee Credit',
                            'debit' => 0,
                            'credit' => $column[1],
                            'dim_id' => 3,
                        ]);
                    }
                }
                if ($column[2] != 0) {
                    $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                    if ($member_acc) {
                        $master_id = VoucherMaster::create([
                            'voucher_type' => 'Journal Voucher',
                            'voucher_date' => '2019-11-1',
                        ]);
                        VoucherDetail::create([
                            'coa_id' => $member_acc->id,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Nov Fee Debited',
                            'debit' => $column[2],
                            'credit' => 0,
                            'dim_id' => 3,
                        ]);
                        VoucherDetail::create([
                            'coa_id' => 871,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Service Revenue Nov Fee Credited',
                            'debit' => 0,
                            'credit' => $column[2],
                            'dim_id' => 3,
                        ]);
                    }
                    if ($column[3] != 0) {
                        $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                        if ($member_acc) {
                            $master_id = VoucherMaster::create([
                                'voucher_type' => 'Journal Voucher',
                                'voucher_date' => '2019-12-1',
                            ]);

                            VoucherDetail::create([
                                'coa_id' => $member_acc->id,
                                'voucher_master_id' => $master_id->id,
                                'narration' => 'Dec Fee Debited',
                                'debit' => $column[3],
                                'credit' => 0,
                                'dim_id' => 3,
                            ]);

                            VoucherDetail::create([
                                'coa_id' => 871,
                                'voucher_master_id' => $master_id->id,
                                'narration' => 'Service Revenue Dec Fee Credited',
                                'debit' => 0,
                                'credit' => $column[3],
                                'dim_id' => 3,
                            ]);
                        }
                    }
                    echo "Inserted";
                }
            }
        }
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Insert Voucher Records";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function get_missing_ids()
    {
        $fileName = storage_path('/app/sheet-members.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $sheet_member_id = $column[0];
            $member = DB::table('members_record')->where('membership_id', '=', $sheet_member_id)->first();
            if ($member) {
                echo "Member Exist in software with ID: " . $member->membership_id . "<br>";
            } else {
                echo "Member not exist in software with ID: " . $sheet_member_id . "<br>";
            }
        }
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Get Missing IDs";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function fee_zero()
    {
        $members = Member::where('membership_fee', '=', 0)->orWhere('membership_fee', null)->get();
        foreach ($members as $key => $value) {
            echo "Membership Fee zero Entered in software of member id: " . $value->membership_id . "<br>";
        }
        $members = Member::where('registration_fee', '=', 0)->orWhere('registration_fee', null)->get();
        foreach ($members as $key => $value) {
            echo "Registration fee zero entered in software of member id: " . $value->membership_id . "<br>";
        }
        $members = Member::where('monthly_sub_fee', '=', 0)->orWhere('monthly_sub_fee', null)->get();
        foreach ($members as $key => $value) {
            echo "Monthly fee is 0 in software for member id: " . $value->membership_id . "<br>";
        }

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Get Fee Zero";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function voucher_entry_script1()
    {
        $fileName = storage_path('/app/final-balance.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $member_id = $column[0];
            $member = Member::where('membership_id', '=', $member_id)->first();
            if ($member) {
                if ($column[1] != 0) {
                    $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                    if ($member_acc) {
                        //Create MembershipVoucher:
                        $dimension = Dimension::where('dim_name', 'Membership Fee')->first();
                        $member->membership_fee = $column[1];
                        //  Debit Bank membership Fee
                        $voucherMaster = VoucherMaster::create([
                            'voucher_type' => 'Bank Receipt',
                            'voucher_date' => '2019-12-01',
                        ]);
                        VoucherDetail::create([
                            'voucher_master_id' => $voucherMaster->id,
                            'coa_id' => Constants::$bank_acc_id,
                            'narration' => 'Membership Fee Paid',
                            'debit' => $column[1],
                            'dim_id' => $dimension->id,
                            'credit' => 0
                        ]);
                        VoucherDetail::create([
                            'voucher_master_id' => $voucherMaster->id,
                            'coa_id' => Constants::$membership_fee_rev_id,
                            'narration' => 'Membership Fee Paid',
                            'credit' => $column[1],
                            'dim_id' => $dimension->id,
                            'debit' => 0
                        ]);
                        UserDimVmInitialEntry::create([
                            'user_id' => $member_acc->user_id,
                            'voucher_master_id' => $voucherMaster->id,
                            'dim_id' => $dimension->id
                        ]);

                        $dimension = Dimension::where('dim_name', 'Registration Fee')->first();
                        $member->membership_fee = $column[1];
                        $member->save();
                        //  Debit Bank membership Fee
                        $voucherMaster = VoucherMaster::create([
                            'voucher_type' => 'Bank Receipt',
                            'voucher_date' => '2019-12-01',
                        ]);
                        VoucherDetail::create([
                            'voucher_master_id' => $voucherMaster->id,
                            'coa_id' => Constants::$bank_acc_id,
                            'narration' => 'Membership Fee Paid',
                            'debit' => $column[2],
                            'dim_id' => $dimension->id,
                            'credit' => 0
                        ]);
                        VoucherDetail::create([
                            'voucher_master_id' => $voucherMaster->id,
                            'coa_id' => Constants::$membership_fee_rev_id,
                            'narration' => 'Membership Fee Paid',
                            'credit' => $column[2],
                            'dim_id' => $dimension->id,
                            'debit' => 0
                        ]);
                        UserDimVmInitialEntry::create([
                            'user_id' => $member_acc->user_id,
                            'voucher_master_id' => $voucherMaster->id,
                            'dim_id' => $dimension->id
                        ]);
                    }
                }
            }
        }
        echo "Inserted";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Voucher Entry Script 1";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function voucher_entry_script2()
    {
        $fileName = storage_path('/app/final-balance.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $member_id = $column[0];
            $member = Member::where('membership_id', '=', $member_id)->first();
            if ($member) {
                if ($column[0] != 0) {
                    $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                    if ($member_acc)
                        $master_id = VoucherMaster::create([
                            'voucher_type' => 'Journal Voucher',
                            'voucher_date' => '2019-10-1',
                        ]);
                    VoucherDetail::create([
                        'coa_id' => $member_acc->id,
                        'voucher_master_id' => $master_id->id,
                        'narration' => 'Bal Debited',
                        'debit' => $column[3],
                        'credit' => 0,
                        'dim_id' => 3,
                    ]);
                    VoucherDetail::create([
                        'coa_id' => 871,
                        'voucher_master_id' => $master_id->id,
                        'narration' => 'Service Revenue Fee Credit',
                        'debit' => 0,
                        'credit' => $column[3],
                        'dim_id' => 3,
                    ]);
                }
            }
        }
        echo "Inserted";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Voucher Entry Script 2";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function set_email_pass_members() {
        $members = Member::all();
        foreach ($members as $key => $value) {
            $ex = Member::where('membership_id','=',$value->membership_id)->first();
            if($ex) {
                if($ex->email != null) {
                    $ex->password = Hash::make('123456');
                }
                else {
                    $ex->email = $value->membership_id .'@gmail.com';
                    $ex->password = Hash::make('123456');
                }
                $ex->save();
            }
        }
        echo "Updated";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Set Email pass Members";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function voucher_entry_script3()
    {
        $fileName = storage_path('/app/final-balance.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $member_id = $column[0];
            $member = Member::where('membership_id', '=', $member_id)->first();
            if ($member) {
                if ($column[4] != 0) {
                    $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                    if ($member_acc) {
                        $master_id = VoucherMaster::create([
                            'voucher_type' => 'Journal Voucher',
                            'voucher_date' => '2019-12-10',
                        ]);
                        VoucherDetail::create([
                            'coa_id' => $member_acc->id,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Paid Voucher',
                            'credit' => $column[4],
                            'debit' => 0,
                            'dim_id' => 3,
                        ]);
                        VoucherDetail::create([
                            'coa_id' => Constants::$bank_acc_id,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Fee Recieved Voucher',
                            'debit' => $column[4],
                            'credit' => 0,
                            'dim_id' => 3,
                        ]);
                    }
                }
            }
        }
        echo "Inserted";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Voucher Entry Script 3";
        $audit_create->route = $routeName;
        $audit_create->save();
    }


    public function insert_oct_nov_fee_vouchers()
    {
        $fileName = storage_path('/app/oct-nov-fee.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $member_id = $column[0];
            $member = DB::table('members_record')->where('membership_id', '=', $member_id)->first();
            if ($member) {
                if ($column[1] != 0) {
                    $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                    if ($member_acc) {
                        $master_id = VoucherMaster::create([
                            'voucher_type' => 'Journal Voucher',
                            'voucher_date' => '2019-10-1',
                        ]);
                        VoucherDetail::create([
                            'coa_id' => $member_acc->id,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Oct Fee Debited',
                            'debit' => $column[1],
                            'credit' => 0,
                            'dim_id' => 3,
                        ]);
                        VoucherDetail::create([
                            'coa_id' => 871,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Service Revenue Oct Fee Credit',
                            'debit' => 0,
                            'credit' => $column[1],
                            'dim_id' => 3,
                        ]);
                    }
                }
                if ($column[2] != 0) {
                    $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                    if ($member_acc) {
                        $master_id = VoucherMaster::create([
                            'voucher_type' => 'Journal Voucher',
                            'voucher_date' => '2019-11-1',
                        ]);
                        VoucherDetail::create([
                            'coa_id' => $member_acc->id,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Nov Fee Debited',
                            'debit' => $column[2],
                            'credit' => 0,
                            'dim_id' => 3,
                        ]);
                        VoucherDetail::create([
                            'coa_id' => 871,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Service Revenue Nov Fee Credited',
                            'debit' => 0,
                            'credit' => $column[2],
                            'dim_id' => 3,
                        ]);
                    }
                    echo "Inserted";
                }
            }
        }
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Insert Oct Nov Fee Vouchers";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function insert_paid_vouchers_record()
    {
        $fileName = storage_path('/app/paid-record.csv');
        $file = fopen($fileName, "r");
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $member_id = $column[0];
            $member = DB::table('members_record')->where('membership_id', '=', $member_id)->first();
            if ($member) {
                if ($column[2] != 0) {
                    $member_acc = ChartOfAccount::where('user_id', '=', $member->user_id)->first();
                    if ($member_acc) {
                        $master_id = VoucherMaster::create([
                            'voucher_type' => 'Journal Voucher',
                            'voucher_date' => '2019-12-10',
                        ]);
                        VoucherDetail::create([
                            'coa_id' => $member_acc->id,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Paid Voucher No ' . $column[1],
                            'credit' => $column[2],
                            'debit' => 0,
                            'dim_id' => 3,
                        ]);
                        VoucherDetail::create([
                            'coa_id' => Constants::$bank_acc_id,
                            'voucher_master_id' => $master_id->id,
                            'narration' => 'Fee Recieved Voucher No. ' . $column[1],
                            'debit' => $column[2],
                            'credit' => 0,
                            'dim_id' => 3,
                        ]);
                    }
                }
            }
        }
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Insert Paid Voucher Records";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function map_service_charges_to_subscription_fee()
    {
        $members = Member::all();
        foreach ($members as $key => $value) {
            $existing = Member::where('membership_id', '=', $value->membership_id)->first();
            $existing->monthly_sub_fee = $existing->service_charges;
            $existing->save();
        }
        echo "Updated";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Map Service Charges to Subscription fee";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function insert_membership_fee_reg_fee()
    {
        $members = Member::all();
        foreach ($members as $key => $value) {
            $dimension = Dimension::where('dim_name', 'Membership Fee')->first();
            //  Debit Bank membership Fee
            $voucherMaster = VoucherMaster::create([
                'voucher_type' => 'Bank Receipt',
                'voucher_date' => '2019-12-01',
            ]);
            VoucherDetail::create([
                'voucher_master_id' => $voucherMaster->id,
                'coa_id' => Constants::$bank_acc_id,
                'narration' => 'Membership Fee Paid',
                'debit' => $value->membership_fee,
                'dim_id' => $dimension->id,
                'credit' => 0
            ]);
            VoucherDetail::create([
                'voucher_master_id' => $voucherMaster->id,
                'coa_id' => Constants::$membership_fee_rev_id,
                'narration' => 'Membership Fee Paid',
                'credit' => $value->membership_fee,
                'dim_id' => $dimension->id,
                'debit' => 0
            ]);
            UserDimVmInitialEntry::create([
                'user_id' => $value->user_id,
                'voucher_master_id' => $voucherMaster->id,
                'dim_id' => $dimension->id
            ]);
            //Registration Fee;
            $dimension = Dimension::where('dim_name', 'Registration Fee')->first();
            $voucherMaster = VoucherMaster::create([
                'voucher_type' => 'Bank Receipt',
                'voucher_date' => '2019-12-01',
            ]);
            VoucherDetail::create([
                'voucher_master_id' => $voucherMaster->id,
                'coa_id' => Constants::$bank_acc_id,
                'narration' => 'Membership Fee Paid',
                'dim_id' => $dimension->id,
                'debit' => $value->registration_fee,
                'credit' => 0
            ]);
            VoucherDetail::create([
                'voucher_master_id' => $voucherMaster->id,
                'coa_id' => Constants::$reg_fee_rev_id,
                'narration' => 'Membership Fee Paid',
                'dim_id' => $dimension->id,
                'credit' => $value->membership_fee,
                'debit' => 0
            ]);
            $dimension = Dimension::where('dim_name', 'Membership Fee')->first();
            UserDimVmInitialEntry::create([
                'user_id' => $value->user_id,
                'voucher_master_id' => $voucherMaster->id,
                'dim_id' => $dimension->id
            ]);
        }
        echo "Inserted";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Insert Membership Fee Reg Fee";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function debit_credit_for_service_revenue_dec()
    {
        $members = Member::all();
        foreach ($members as $key => $value) {
            //  Debit Bank membership Fee
            $dimension = Dimension::where('dim_name', 'Monthly Subscription Fee')->first();
            $member_rec_acc = ChartOfAccount::where('user_id', '=', $value->user_id)->first();
            $voucherMaster = VoucherMaster::create([
                'voucher_type' => 'Journal Voucher',
                'voucher_date' => '2019-12-01',
            ]);
            VoucherDetail::create([
                'voucher_master_id' => $voucherMaster->id,
                'coa_id' => $member_rec_acc->id,
                'narration' => 'Dec Fee Added',
                'debit' => $value->monthly_sub_fee,
                'dim_id' => $dimension->id,
                'credit' => 0
            ]);
            VoucherDetail::create([
                'voucher_master_id' => $voucherMaster->id,
                'coa_id' => Constants::$service_rev_acc_id,
                'narration' => 'Dec Fee For Member',
                'credit' => $value->monthly_sub_fee,
                'dim_id' => $dimension->id,
                'debit' => 0
            ]);
            UserDimVmInitialEntry::create([
                'user_id' => $value->user_id,
                'voucher_master_id' => $voucherMaster->id,
                'dim_id' => $dimension->id
            ]);
        }
        echo "Inserted";

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Debit Credit for Service Revenue";
        $audit_create->route = $routeName;
        $audit_create->save();
    }

    public function update_subs_fee()
    {
        $members = Member::where('membership_id', '>=', 324)->get();
        foreach ($members as $key => $value) {
            $curr_member = Member::where('membership_id', '=', $value->membership_id)->first();
            $curr_member->monthly_sub_fee = 1500;
            $curr_member->save();
            $dimension = Dimension::where('dim_name', 'Monthly Subscription Fee')->first();
            $member_rec_acc = ChartOfAccount::where('user_id', '=', $value->user_id)->first();
            $voucherMaster = VoucherMaster::create([
                'voucher_type' => 'Journal Voucher',
                'voucher_date' => '2019-12-01',
            ]);
            VoucherDetail::create([
                'voucher_master_id' => $voucherMaster->id,
                'coa_id' => $member_rec_acc->id,
                'narration' => 'Dec Fee Added',
                'debit' => $curr_member->monthly_sub_fee,
                'dim_id' => $dimension->id,
                'credit' => 0
            ]);
            VoucherDetail::create([
                'voucher_master_id' => $voucherMaster->id,
                'coa_id' => Constants::$service_rev_acc_id,
                'narration' => 'Dec Fee For Member',
                'credit' => $curr_member->monthly_sub_fee,
                'dim_id' => $dimension->id,
                'debit' => 0
            ]);
            UserDimVmInitialEntry::create([
                'user_id' => $value->user_id,
                'voucher_master_id' => $voucherMaster->id,
                'dim_id' => $dimension->id
            ]);
        }
        echo "Inserted";
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Update Subs Fee";
        $audit_create->route = $routeName;
        $audit_create->save();
    }
        

}
