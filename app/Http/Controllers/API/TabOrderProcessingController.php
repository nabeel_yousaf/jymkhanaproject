<?php

namespace App\Http\Controllers\API;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrderDetail;
use App\OrderMaster;
use App\Product;

class TabOrderProcessingController extends Controller
{
    public function tab_categories(Request $request) {
            $categories = Category::select('id', 'name', 'description')->get();
            return collect([
                'success' => true,
                'data' => $categories
            ]);
    }

    public function tab_products(Request $request) {
        $allProducts = Product::all();
        if($allProducts) {
            return collect([
                'success' => true,
                'products' => $allProducts
            ]);
        } else {
            return collect([
                'success' => false,
                'message' => 'not found'
            ]);
        }
    }
    public function tab_order_processing(Request $request) {
        $count = json_decode($request->get('order_details_list'),true);
        if($count == '') {
            return collect([
                'sucess' => false,
                'message' => 'Invalid Data Provided'
            ]);
        }
        foreach($count as $data)
        {
            $orderMaster = OrderMaster::create([
                'member_id' => $request->get('member_id'),
                'date_time' => $request->get('date')
            ]);
            $orderDetails = OrderDetail::create([
                'order_master_id' => $orderMaster->id,
                'product_id' => $data['product_id'],
                'quantity' => $data['quantity']
            ]);
        }

        return collect([
            'success' => true,
            'message' => 'order is in Process'
        ]);
    }
}
