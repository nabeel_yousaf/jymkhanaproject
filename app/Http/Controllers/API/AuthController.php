<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Product;
use App\Member;
use App\OrderDetail;
use App\OrderMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;

class AuthController extends Controller
{
    function __construct()
    {
        Config::set('jwt.user', Member::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Member::class,
        ]]);
    }
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email','password');
        $password = $request->password;
        $member = Member::where('email','=',$request->email)->first();
        if(!is_null($member)) {
            if (Hash::check($password, $member->password)) {
                try {
                    /* token expiry time 10 days */
                    if (!$token = JWTAuth::attempt($credentials,['exp' => \Carbon\Carbon::now()->addDays(10)->timestamp])) {
                        return response()->json(['success' => false, 'message' => 'invalid_credentials'], 400);
                    }
                } catch (JWTException $e) {
                    return response()->json(['success' => false, 'message' => 'could_not_create_token'], 500);
                }
                $member->setAttribute('image_url',url('images/members_image').'/'.$member->image);
                return collect([
                    'success' => true,
                    'result' => $member,
                    'token' => $token
                ]);
            }
            return collect([
                'success' => false,
                'message' => 'Auth Failed'
            ]);
        } else {
            return collect([
                'success' => false,
                'message' => 'not found'
            ]);
        }
    }

    public function category($memberId){
        $member = Member::where('membership_id','=',$memberId)->first();
        if(!is_null($member)) {
            $categories = Category::select('id', 'name', 'description')->get();
            return collect([
                'success' => true,
                'data' => $categories
            ]);
        } else {
            return collect([
                'success' => true,
                'message' => 'not found'
            ]);
        }

    }

    public function orderProcessing(Request $request)
    {
        $data = json_decode($request->get('order_details_list'),true);
        
        // foreach($count as $data)
        // {
            $orderMaster = OrderMaster::create([
                'member_id' => $request->get('member_id'),
                'date_time' => $request->get('date')
            ]);
            $orderDetails = OrderDetail::create([
                'order_master_id' => $orderMaster->id,
                'product_id' => $data['product_id'],
                'quantity' => $data['quantity']
            ]);
        // }

        return collect([
            'success' => true,
            'message' => 'order is in Process'
        ]);
    }

    public function productByCategory()
    {
//        $categoryDetails = Product::where('id','=',$category_id)->get();
        $allProducts = Product::all();
        if($allProducts) {
            return collect([
                'success' => true,
                'products' => $allProducts
            ]);
        } else {
            return collect([
                'success' => false,
                'message' => 'not found'
            ]);
        }
    }
    public function ledger(Request $request) {
        $input = $request->only('membership_id','startDate','endDate');
        $validator = Validator::make($input, [
            'membership_id' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['success'=> false, 'error' => $validator->errors()], 400);
        }
        $membership_id = $input->get('membership_id');
        $startDate = $input->get('startDate');
        $endDate = $input->get('endDate');
        $member = Member::where('membership_id','=',$membership_id)->first();
        if($member) {
            $account = ChartOfAccount::where('user_id','=',$member->user_id)->first();
            if($account) {
                $data = ChartOfAccount::ledger('',$account->id,$account->parent_id,$startDate,$endDate);
                return response()->json(['success'=>true,'data'=>$data['data']],200);
            }
        }
        return response()->json(['success'=>false,'error'=>'No Record Found']);
    }
}
