<?php

namespace App\Http\Controllers\API;

use App\ChartOfAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;
use Illuminate\Support\Facades\Validator;

class MemberLedgerController extends Controller
{
    public function index(Request $request) {
        $input = $request->only('membership_id','startDate','endDate');
        $validator = Validator::make($input, [
            'membership_id' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['success'=> false, 'error' => $validator->errors()], 400);
        }
        $membership_id = $input['membership_id'];
        $startDate = isset($input['startDate']) ? $input['startDate'] : '';
        $endDate = isset($input['endDate']) ? $input['endDate'] : '';
        $member = Member::where('membership_id','=',$membership_id)->first();
        if($member) {
            $account = ChartOfAccount::where('user_id','=',$member->user_id)->first();
            if($account) {
                $data = ChartOfAccount::ledger('',$account->id,$account->parent_id,$startDate,$endDate);
                return response()->json(['success'=>true,'data'=>$data['data']],200);
            }
        }
        return response()->json(['success'=>false,'error'=>'No Record Found']);
    }
}
