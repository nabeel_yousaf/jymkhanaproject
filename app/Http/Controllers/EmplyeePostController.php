<?php

namespace App\Http\Controllers;

use App\employeePost;
use App\employee;
use Session;
use Illuminate\Http\Request;
use App\Audit;
use Auth;

class EmplyeePostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_active=1;
        $count=1;
        $posts=employeePost::all();
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "All Employees Post";
        $audit_create->route = $routeName;
        $audit_create->save();
         return view('backEnd.employees.all-employees', compact('posts','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Create Employee Post";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.employees.create-post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'title'=>'required|unique:emplyee_posts,title',
        ]);
        $data=$request->all();
        //dd($data);
        employeePost::create($data);
        Session::flash('success','You Successfully Add The Post !');
        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Store Employee Post";
        $audit_create->route = $routeName;
        $audit_create->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\emplyeePost  $emplyeePost
     * @return \Illuminate\Http\Response
     */
    public function show(emplyeePost $emplyeePost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\emplyeePost  $emplyeePost
     * @return \Illuminate\Http\Response
     */
    public function edit(emplyeePost $emplyeePost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\emplyeePost  $emplyeePost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, emplyeePost $emplyeePost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\emplyeePost  $emplyeePost
     * @return \Illuminate\Http\Response
     */
    public function destroy(emplyeePost $emplyeePost)
    {
        //
    }
}
