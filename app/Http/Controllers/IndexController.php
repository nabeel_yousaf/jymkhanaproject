<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        
        return view('frontEnd.index');
    }

 
    public function contact(){

        return view('frontEnd.contact');
    }
    public function about(){

        return view('frontEnd.about');
    }
    public function terms(){

        return view('frontEnd.terms');
    }

}
