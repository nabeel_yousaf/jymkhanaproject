<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\balance;
use Illuminate\Http\Request;
use App\ChartOfAccount;
use Session;

class BalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coa = ChartOfAccount::all()->where('is_parent' ,'=', 3);

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Balance Create";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.balance.create', compact('coa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'coa_id'=>'required',
            'description'=>'required',
            'amount'=>'required|numeric',
        ]);
        $input_data=$request->all();
        if ($request->submit == 'debit'){
            $id = $request->input('coa_id');
            $des = $request->input('description');
            $amount = $request->input('amount');
            $data=array('coa_id'=>$id,"description"=>$des,"debit"=>$amount,"credit"=>0);
            DB::table('balances')->insert($data);

            Session::flash('success','You Successfully Add openinig bill !');
            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
                
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Balance Store";
            $audit_create->route = $routeName;
            $audit_create->save();
            return redirect()->back();
        }else{
            $id = $request->input('coa_id');
            $des = $request->input('description');
            $amount = $request->input('amount');
            $data=array('coa_id'=>$id,"description"=>$des,"debit"=>0,"credit"=>$amount);
            DB::table('balances')->insert($data);

            Session::flash('success','You Successfully Add openinig bill !');

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
                
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Balance Store";
            $audit_create->route = $routeName;
            $audit_create->save();
            return redirect()->back();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function show(balance $balance)
    {
        $bal = DB::table('balances')
            ->join('chart_of_accounts', 'balances.coa_id', '=', 'chart_of_accounts.id')
            ->select('balances.*', 'chart_of_accounts.name')->get();

            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
                
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Balance Show";
            $audit_create->route = $routeName;
            $audit_create->save();
        return view('backEnd.balance.index', compact('bal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coa = ChartOfAccount::all();
        $edit = balance::all()->where('id', '=',$id);

        $logged_in_user = Auth::user();
        $logged_in_user = $logged_in_user->id;
        $routeName = url()->current();
            
        $audit_create = new Audit();
        $audit_create->user_id = $logged_in_user;
        $audit_create->action = "Balance Edit";
        $audit_create->route = $routeName;
        $audit_create->save();
        return view('backEnd.balance.edit')->with('data', ['coa'=>$coa, 'edit'=>$edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'coa_id'=>'required',
            'description'=>'required',
            'amount'=>'required|numeric',
        ]);
        $input_data=$request->all();
        if ($request->submit == 'debit'){
            $coaid = $request->input('coa_id');
            $des = $request->input('description');
            $amount = $request->input('amount');
            $update =DB::table('balances')->where('id', $id)
                ->update(['coa_id' => "$coaid",'description'=>"$des", 'debit'=>"$amount",'credit'=>0]);
            if ($update){
                Session::flash('success','You Successfully Add openinig bill !');

                $logged_in_user = Auth::user();
                $logged_in_user = $logged_in_user->id;
                $routeName = url()->current();
                    
                $audit_create = new Audit();
                $audit_create->user_id = $logged_in_user;
                $audit_create->action = "Balance Update";
                $audit_create->route = $routeName;
                $audit_create->save();
                return redirect()->back();
            }
        }else{
            $coaid = $request->input('coa_id');
            $des = $request->input('description');
            $amount = $request->input('amount');
            $update =DB::table('balances')->where('id', $id)
                ->update(['coa_id' => "$coaid",'description'=>"$des", 'debit'=>0 ,'credit'=>"$amount"]);
            if ($update){
                Session::flash('success','You Successfully Add openinig bill !');

                $logged_in_user = Auth::user();
                $logged_in_user = $logged_in_user->id;
                $routeName = url()->current();
                    
                $audit_create = new Audit();
                $audit_create->user_id = $logged_in_user;
                $audit_create->action = "Balance Update";
                $audit_create->route = $routeName;
                $audit_create->save();
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $balance = balance::where('id','=',$id)->delete();
        if ($balance){
            Session::flash('success','Billing deleted successfully !');
            
            $logged_in_user = Auth::user();
            $logged_in_user = $logged_in_user->id;
            $routeName = url()->current();
                
            $audit_create = new Audit();
            $audit_create->user_id = $logged_in_user;
            $audit_create->action = "Balance Delete";
            $audit_create->route = $routeName;
            $audit_create->save();
            return redirect()->back();
        }
    }
}
