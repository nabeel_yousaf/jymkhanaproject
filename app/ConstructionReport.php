<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConstructionReport extends Model
{
    protected $table='construction_reports';
    protected $primaryKey='id';
    protected $fillable = ['Vendor','material', 'qty','unit_price','total','credit','payment','labour','no_labour','labour_payments','skills'];
   
    public function constructionmaterial()
    {
        return $this->hasOne('App\ConstructionMaterial','id','material');
    }

}
