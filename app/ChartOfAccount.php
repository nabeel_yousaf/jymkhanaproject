<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use function Psy\sh;

class ChartOfAccount extends Model
{
    protected $fillable = [
        'user_id','name','description','type','is_parent','parent_id'
    ];

    public static function ledger($draw,$id,$parend_id,$startDate,$endDate) {
        $data = [];
        $data['draw'] = $draw;
        $curr_data = array();
        if ($parend_id == 0) {
            if ($startDate == '' && $endDate == '') {
                $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->where('chart_of_accounts.parent_id', '=', $id)
                    ->where('voucher_masters.is_active', '=', 1)
                    ->groupBy('dim_id')
                    ->selectRaw('dim_id,voucher_master_id,voucher_masters.voucher_date,voucher_masters.voucher_type,sum(voucher_details.debit) as debit_sum,sum(voucher_details.credit) as credit_sum')->get();
            } else {
                $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->where('chart_of_accounts.parent_id', '=', $id)
                    ->where('voucher_masters.is_active', '=', 1)
                    ->where('voucher_masters.voucher_date', '>=', $startDate)
                    ->where('voucher_masters.voucher_date', '<=', $endDate)
                    ->groupBy('dim_id')
                    ->selectRaw('dim_id,voucher_master_id,voucher_masters.voucher_date,voucher_masters.voucher_type,sum(voucher_details.debit) as debit_sum,sum(voucher_details.credit) as credit_sum')->get();
            }
            $balance = 0;
            $count = count($vouchers);
            foreach ($vouchers as $key => $value) {
                $dimension = Dimension::where('id', '=', $value->dim_id)->first();
                if ($dimension) {
                    $dim_name = $dimension->dim_name;
                } else {
                    $dim_name = "N/A";
                }
                $balance += ($value->debit_sum - $value->credit_sum);
                $current_sum = $balance;
                $curr_data[] = array('m_id' => 'N/A', 'id' => $value->voucher_master_id, 'voucher_date' => $value->voucher_date, 'voucher_type' => $value->voucher_type, 'narration' => 'Collective Dimension Wise Data', 'dim_name' => $dim_name, 'debit' => number_format($value->debit_sum,2), 'credit' => number_format($value->credit_sum,2), 'balance' => number_format($current_sum,2));
            }
            $data['data'] = $curr_data;
            $data['recordsTotal'] = $count;
            $data['recordsFiltered'] = $count;
            return $data;
        }
        else {
            $chart_of_account = ChartOfAccount::find($id);
            if($startDate == '' && $endDate == '') {
                if($chart_of_account->user_id != 1) {
                    $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->join('members_record','chart_of_accounts.user_id','=','members_record.user_id')
                    ->where('chart_of_accounts.id', '=', $id)
                    ->where('voucher_masters.is_active', '=', 1)
                    ->select('members_record.membership_id','voucher_masters.id','voucher_masters.voucher_date','voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','voucher_details.dim_id')->get();
                    $member_id = true;
                }
                else {
                    $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->where('chart_of_accounts.id', '=', $id)
                    ->where('voucher_masters.is_active', '=', 1)
                    ->select('voucher_masters.voucher_date','voucher_masters.id','voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','voucher_details.dim_id')->get();
                    $member_id = false;
                }
            }
            else {
                if($chart_of_account->user_id != 1) {
                    $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->join('members_record','chart_of_accounts.user_id','=','members_record.user_id')
                    ->where('chart_of_accounts.id', '=', $id)
                    ->where('voucher_masters.is_active', '=', 1)
                    ->where('voucher_masters.voucher_date', '>=', $startDate)
                    ->where('voucher_masters.voucher_date', '<=', $endDate)
                    ->select('members_record.membership_id','voucher_masters.id','voucher_masters.voucher_date','voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','voucher_details.dim_id')->get();
                    $member_id = true;
                }
                else {
                    $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->where('chart_of_accounts.id', '=', $id)
                    ->where('voucher_masters.is_active', '=', 1)
                    ->where('voucher_masters.voucher_date', '>=', $startDate)
                    ->where('voucher_masters.voucher_date', '<=', $endDate)
                    ->select('voucher_masters.voucher_date','voucher_masters.id','voucher_masters.voucher_type','voucher_details.narration','voucher_details.debit','voucher_details.credit','voucher_details.dim_id')->get();
                    $member_id = false;
                }

            }
            $balance = 0;
            $count = count($vouchers);
            foreach ($vouchers as $key => $value) {
                $dimension = Dimension::where('id', '=', $value->dim_id)->first();
                if ($dimension) {
                    $dim_name = $dimension->dim_name;
                } else {
                    $dim_name = "N/A";
                }
                $balance += ($value->debit - $value->credit);
                $current_sum = $balance;
                if($member_id) {
                    $m_id = $value->membership_id;
                }
                else {
                    $m_id = 'N/A';
                }
                $curr_data[] = array('m_id' => $m_id, 'id' => $value->id, 'voucher_date' => $value->voucher_date, 'voucher_type' => $value->voucher_type, 'narration' => $value->narration, 'dim_name' => $dim_name, 'debit' => number_format($value->debit,2), 'credit' => number_format($value->credit,2), 'balance' => number_format($current_sum,2));
            }
            $data['data'] = $curr_data;
            $data['recordsTotal'] = $count;
            $data['recordsFiltered'] = $count;
            return $data;
        }
    }
    
    public static function detail_ledger($draw,$id,$parend_id,$startDate,$endDate) {
        $data = [];
        $data['draw'] = $draw;
        $curr_data = array();
        if ($parend_id == 0) {
            if ($startDate == '' && $endDate == '') {
                if($id != 0){
                    $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->leftJoin('members_record','chart_of_accounts.user_id','=','members_record.user_id')
                    ->where('chart_of_accounts.parent_id', '=', $id)
                    ->where('voucher_masters.is_active', '=', 1)
                    ->selectRaw('members_record.membership_id,dim_id,voucher_masters.voucher_date,voucher_masters.voucher_type,voucher_master_id,voucher_details.debit,voucher_details.credit,voucher_details.narration')->get();
                }
                else{
                    $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->leftJoin('members_record','chart_of_accounts.user_id','=','members_record.user_id')
                    ->where('voucher_masters.is_active', '=', 1)
                    ->selectRaw('members_record.membership_id,dim_id,voucher_masters.voucher_date,voucher_masters.voucher_type,voucher_master_id,voucher_details.debit,voucher_details.credit,voucher_details.narration')->get();
                }
            } else {
                if($id != 0){
                    $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->leftJoin('members_record','chart_of_accounts.user_id','=','members_record.user_id')
                    ->where('chart_of_accounts.parent_id', '=', $id)
                    ->where('voucher_masters.is_active', '=', 1)
                    ->where('voucher_masters.voucher_date', '>=', $startDate)
                    ->where('voucher_masters.voucher_date', '<=', $endDate)
                    ->selectRaw('members_record.membership_id,dim_id,voucher_masters.voucher_date,voucher_masters.voucher_type,voucher_master_id,voucher_details.debit,voucher_details.credit,voucher_details.narration')->get();
                }
                else{
                    $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
                    ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
                    ->leftJoin('members_record','chart_of_accounts.user_id','=','members_record.user_id')
                    ->where('voucher_masters.is_active', '=', 1)
                    ->where('voucher_masters.voucher_date', '>=', $startDate)
                    ->where('voucher_masters.voucher_date', '<=', $endDate)
                    ->selectRaw('members_record.membership_id,dim_id,voucher_masters.voucher_date,voucher_masters.voucher_type,voucher_master_id,voucher_details.debit,voucher_details.credit,voucher_details.narration')->get();
                }
            }
            $balance = 0;
            $count = count($vouchers);
            foreach ($vouchers as $key => $value) {
                $dimension = Dimension::where('id', '=', $value->dim_id)->first();
                if ($dimension) {
                    $dim_name = $dimension->dim_name;
                } else {
                    $dim_name = "N/A";
                }
                if ($value->narration != "") {
                    $narration = $value->narration;
                } else {
                    $narration = "N/A";
                }
                if ($value->membership_id != "") {
                    $m_id = $value->membership_id;
                } else {
                    $m_id = "N/A";
                }
                $balance += ($value->debit - $value->credit);
                $current_sum = $balance;
                $curr_data[] = array('m_id' => $m_id, 'id' => $value->voucher_master_id, 'voucher_date' => $value->voucher_date, 'voucher_type' => $value->voucher_type, 'narration' => $narration, 'dim_name' => $dim_name, 'debit' => number_format($value->debit,2), 'credit' => number_format($value->credit,2), 'balance' => number_format($current_sum,2));
            }
            $data['data'] = $curr_data;
            $data['recordsTotal'] = $count;
            $data['recordsFiltered'] = $count;
            return $data;
        }
        
    }

    public static function member_balance() {
        $vouchers = VoucherMaster::join('voucher_details', 'voucher_masters.id', '=', 'voucher_details.voucher_master_id')
        ->join('chart_of_accounts', 'chart_of_accounts.id', '=', 'voucher_details.coa_id')
        ->where('chart_of_accounts.parent_id','=',Constants::$members_rec_acc_id)
        ->where('voucher_masters.is_active', '=', 1)
        ->groupBy('chart_of_accounts.id')
        ->selectRaw('sum(voucher_details.debit) as debit_sum,sum(voucher_details.credit) as credit_sum,chart_of_accounts.id')->get();

        $balance = 0;
        $pen_sum = 0;
        $pending_bal = array();
        $rec_bal = array();
        $pen_bal = array();
        foreach ($vouchers as $key => $value) {
             $dimension = Dimension::where('id', '=', 3)->first();
            if ($dimension) {
                $dim_name = $dimension->dim_name;
            } else {
                $dim_name = "N/A";
            }
            $bal = $value->debit_sum - $value->credit_sum;
            $account = ChartOfAccount::where('id','=',$value->id)->first();
            if($account) {
                $member = Member::where('user_id','=',$account->user_id)->first();
            }
            if($bal == 0) {
                $balance += $value->credit_sum;
                $rec_bal[] = array('m_id' => $member['membership_id'], 'name' => $member->name,'debit'=>number_format($value->debit_sum,2),'credit' => number_format($member->credit_sum,2),'balance'=>number_format($value->debit_sum-$value->credit_sum));
            }
            else {
                $pen_bal[] = array('m_id' => $member['membership_id'], 'name' => $member['name'],'debit'=>number_format($value->debit_sum,2),'credit' => number_format($value->credit_sum,2),'balance'=>number_format($value->debit_sum-$value->credit_sum));
            }
        }
        $data['pending'] = $pen_bal;
        $data['recieved'] = $rec_bal;
        return $data;
    }
}
