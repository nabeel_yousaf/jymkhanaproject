<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherMasterPending extends Model
{
    protected $table='voucher_masters_pending';
    protected $fillable = ['voucher_type','voucher_date'];
}
