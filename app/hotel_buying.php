<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hotel_buying extends Model
{
    protected $table='hotel_buying';
    protected $primaryKey='hOTEL_BUYING_iD';
    protected $fillable = ['hOTEL_BUYING_iD','HB_Material','Quantity','Total','Credits','Peyment','saved_at'];
}