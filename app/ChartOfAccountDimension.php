<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChartOfAccountDimension extends Model
{
    protected $fillable = ['coa_id','dim_id'];
}
