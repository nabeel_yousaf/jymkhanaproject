<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = ['order_master_id','product_id','quantity'];

    public function order_master()
    {
        return $this->hasMany(OrderMaster::class,'id','order_master_id');
    }
    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }

}
