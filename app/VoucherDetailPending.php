<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherDetailPending extends Model
{
    protected $table='voucher_details_pending';
    protected $fillable = ['voucher_master_id', 'coa_id','narration','debit','credit','dim_id'];
}
