<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constants extends Model
{
    protected $fillable = [];
    public static $bank_acc_id = 883;
    public static $service_rev_acc_id = 871;
    public static $members_rec_acc_id = 54;
    public static $membership_fee_rev_id = 884;
    public static $reg_fee_rev_id = 885;
}
