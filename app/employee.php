<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
	 protected $table='employees';
    protected $primaryKey='membership_id';
    protected $fillable = ['membership_id','name','custom_id', 'email', 'password','address','city','image','status','cnic','phone','employee_post_id','password_confirmation'];

    public function pay()
    {
        return $this->hasOne(EmyloyeePay::class,'employee_id','membership_id');
    }

    public function orders()
    { 
        return $this->morphOne(OrderMaster::class,'memberable_id','membership_id');
    }

}
