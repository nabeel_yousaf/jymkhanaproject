<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	protected $table='customers';
    protected $primaryKey='id';
    protected $fillable = ['name', 'email', 'password','address','city','image','status','cnic','phone','registration_fee','month','member_id','password_confirmation'];

    public function memberspayrecord()
   {
       return $this->belongsTo(MembersPayRecord::class);
   }
}
