<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConstructionMaterial extends Model
{
    //
    protected $table='construction_material';
    protected $primaryKey='id';
    protected $fillable = ['mat_name'];
}
