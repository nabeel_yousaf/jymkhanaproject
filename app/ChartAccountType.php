<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChartAccountType extends Model
{
    protected  $fillable = [
        'name'
    ];
}
